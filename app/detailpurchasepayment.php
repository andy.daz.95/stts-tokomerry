<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailpurchasepayment extends Model
{
    //
    protected $table = 'payment_purchase_details';
    protected $primaryKey='payment_purchase_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}