<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class additionalproductinfo extends Model
{
    //
    protected $table = 'additional_product_info';
    protected $primaryKey='additional_product_info_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function product(){
    	return $this->belongsTo('App\product','product_id','product_id')
    	->where('status',1);
    }

    public function warehouse(){
    	return $this->belongsTo('App\warehouse','warehouse_id','warehouse_id')
    	->where('status',1);
    }

	public function owner(){
		return $this->hasMany('App\additionalproductowner','additional_product_info_id','additional_product_info_id')
			->where('status',1);
	}
}