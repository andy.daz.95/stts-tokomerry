<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class supplier extends Model
{
    //
    protected $table = 'supplier';
    protected $primaryKey='supplier_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

 	public function target()
    {
        return $this->belongsTo('App\target','supplier_id','supplier_id');
    }   
}