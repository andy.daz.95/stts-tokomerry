<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //
    protected $table = 'product';
    protected $primaryKey='product_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function hpp(){
        return $this->hasOne('App\pricepurchase', 'product_id', 'product_id')->first()->hpp;
    }

		public function product_child(){
			return $this->hasOne('App\productchild', 'product_id', 'product_id');
		}

    public function qty_from()
    {
    	return $this->hasOne('App\productwarehouse', 'product_id', 'product_id');
    }
    public function qty_to()
    {
    	return $this->hasOne('App\productwarehouse', 'product_id', 'product_id');
    }

    public function qtyinwarehouse()
    {
        return $this->hasMany('App\productwarehouse', 'product_id', 'product_id');
    }
    public function unit()
    {
        return $this->hasOne('App\unit','unit_id','unit_id');
    }
    public function satuan()
    {
        return $this->hasOne('App\satuan','unit_id','unit_id');
    }
    public function last_product_info(){
    	return $this->hasMany('App\additionalproductinfo','product_id','product_id')->latest();
		}
}