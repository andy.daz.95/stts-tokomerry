<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailsalesreturn extends Model
{
	//
	protected $table = 'sales_retur_details';
	protected $primaryKey='sales_retur_details_id';
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'last_update';

	public function product(){
		return $this->hasOne('App\product','product_id','product_id');
	}

	public function draft(){
		return $this->hasOne('App\detaildraftdelivery','sales_retur_details_id','sales_retur_details_id')
			->where('status',1);
	}
	public function unit(){
		return $this->hasOne('App\satuan','unit_id','unit_id');
	}

	public function unit_child(){
		return $this->hasOne('App\unitchild','unit_child_id','unit_id');
	}

}