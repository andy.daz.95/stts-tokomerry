<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stockmovement extends Model
{
    //
    protected $table = 'stock_movement';
    protected $primaryKey='stock_movement_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function details()
    {
        return $this->hasMany('App\detailstockmovement', 'stock_movement_id', 'stock_movement_id')
					->where('status',1);
    }
    public function warehouse_from_data()
    {
        return $this->hasOne('App\warehouse', 'warehouse_id', 'warehouse_from');
    }
    public function warehouse_to_data()
    {
        return $this->hasOne('App\warehouse', 'warehouse_id', 'warehouse_to');
    }
}