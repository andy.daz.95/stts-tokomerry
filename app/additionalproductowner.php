<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class additionalproductowner extends Model
{
    //
    protected $table = 'additional_product_owner';
    protected $primaryKey='id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function detail()
		{
			return $this->hasMany('App\additionalproductdetail','additional_product_owner_id','id');
		}
}