<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailstockmovement extends Model
{
    //
    protected $table = 'stock_movement_details';
    protected $primaryKey='stock_movement_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function stock_movement()
    {
        return $this->belongsTo('App\stockmovement','stock_movement_id','stock_movement_id');
    }

    public function product()
    {
        return $this->hasOne('App\product','product_id','product_id');
    }

    public function detailpo()
    {
        return $this->hasOne('App\detailpurchaseorder','purchase_order_details_id','purchase_order_details_id');
    }
}