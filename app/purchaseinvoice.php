<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchaseinvoice extends Model
{
    //
    protected $table = 'invoice_purchase';
    protected $primaryKey='invoice_purchase_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function payment(){
    	return $this->hasMany('App\purchasepayment','invoice_purchase_id','invoice_purchase_id')
    				->has('details')
						->where('payment_purchase.status',1);
    }

    public function details(){
        return $this->hasMany('App\detailpurchaseinvoice','invoice_purchase_id','invoice_purchase_id');
    }

    public function po(){
    	return $this->belongsTo('App\purchaseorder','purchase_order_id','purchase_order_id');
	}
}