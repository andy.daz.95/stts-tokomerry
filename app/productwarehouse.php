<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productwarehouse extends Model
{
    //
    protected $table = 'product_warehouse';
    protected $primaryKey='product_warehouse_id'; 
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}