<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class inventoryvaluation extends Model
{
    //
    protected $table = 'inventory_valuation';
    protected $primaryKey = 'id';

		public $timestamps = false;
}