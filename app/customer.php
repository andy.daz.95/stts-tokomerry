<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    //
    protected $table = 'customer';
    protected $primaryKey='customer_id'; 
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}