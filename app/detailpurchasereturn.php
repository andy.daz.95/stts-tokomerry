<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailpurchasereturn extends Model
{
    //
    protected $table = 'purchase_retur_details';
    protected $primaryKey='purchase_retur_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}