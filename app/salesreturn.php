<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salesreturn extends Model
{
    //
    protected $table = 'sales_retur';
    protected $primaryKey='sales_retur_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function so(){
    	return $this->belongsTo('App\salesorder','sales_order_id','sales_order_id');
    }

    public function details(){
      return $this->hasMany('App\detailsalesreturn','sales_retur_id','sales_retur_id')
        ->with('product')
        ->with('unit')
				->with('unit_child')
        ->where('status',1);
    }

    public function details_not_received(){
			return $this->details()->whereColumn('received_qty','<','qty');
		}

    public function totalcogs(){
      return $this->hasOne('App\detailsalesreturn','sales_retur_id','sales_retur_id')
      ->selectRaw('sales_retur_id, Sum(cogs) as totalcogs')
      ->groupBy('sales_retur_id');
    }

    public function sales_return_receive(){
    	return $this->hasMany('App\salesreturnreceive','sales_retur_id','sales_retur_id')
				->where('status',1);
		}
}