<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deliveryorder extends Model
{
    //
    protected $table = 'delivery_order';
    protected $primaryKey='delivery_order_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function details(){
    	return $this->hasMany('App\detaildeliveryorder','delivery_order_id','delivery_order_id')
    	->where('status',1);
    }

    public function draft(){
    	return $this->belongsTo('App\draftdelivery','draft_delivery_order_id','draft_delivery_order_id')
        ->where('status',1);
    }
    
    public function warehouse(){
    	return $this->belongsTo('App\warehouse','warehouse_id','warehouse_id');
    }
}