<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class account extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    protected $table = 'account';
    protected $primaryKey='account_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'full_name', 'profile_picture','email','phone','birth_date','gender','address','postal_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    protected $dates = ['deleted_on'];

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
    const DELETED_AT = 'deleted_on';

    public function roles()
    {
        return $this->belongsToMany('App\role', 'account_role', 'account_id', 'role_id');
    }

    public function staff() {
        return $this->belongsToMany('App\role','account_role', 'account_id', 'role_id')->where('account_role.role_id','<>', 1);
    }

    public function gudang() {
        return $this->belongsToMany('App\role','account_role', 'account_id', 'role_id')->whereIn('account_role.role_id',[3,4,7]);
    }

    public function master() {
        return $this->belongsToMany('App\role','account_role', 'account_id', 'role_id')->whereIn('account_role.role_id',[1]);
    }

    public function sales() {
        return $this->belongsToMany('App\role','account_role', 'account_id', 'role_id')->where('account_role.role_id', 12);
    }
}
