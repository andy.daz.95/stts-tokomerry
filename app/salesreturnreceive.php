<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salesreturnreceive extends Model
{
    //
    protected $table = 'sales_retur_receive';
    protected $primaryKey='sales_retur_receive_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function retur(){
    	return $this->belongsTo('App\salesreturn','sales_retur_id','sales_retur_id');
    }

    public function details(){
    	return $this->hasMany('App\detailsalesreturnreceive','sales_retur_receive_id','sales_retur_receive_id')
				->where('status',1);
		}

		public function warehouse(){
			return $this->hasOne('App\warehouse','warehouse_id','warehouse_id');
		}
}