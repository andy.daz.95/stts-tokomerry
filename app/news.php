<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class news extends Model
{
    //
    protected $table = 'news';
    protected $primaryKey='news_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}