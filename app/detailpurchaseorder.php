<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailpurchaseorder extends Model
{
    //
    protected $table = 'purchase_order_details';
    protected $primaryKey='purchase_order_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function product(){
    	return $this->hasOne('App\product','product_id','product_id');
    }

    public function po(){
        return $this->belongsTo('App\purchaseorder','purchase_order_id','purchase_order_id')
        ->where('status',1);
    }

    public function detailsm(){
        return $this->hasMany('App\detailstockmovement','purchase_order_details_id','purchase_order_details_id')
        ->where('status',1);
    }

    public function detailgoodreceive(){
        return $this->hasMany('App\detailgoodreceive','purchase_order_details_id','purchase_order_details_id')
        ->where('status',1);
    }
}