<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salesinvoice extends Model
{
    //
    protected $table = 'invoice_sales';
    protected $primaryKey='invoice_sales_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function payments(){
    	return $this->hasMany('App\salespayment','invoice_sales_id','invoice_sales_id')
				->where('status',1);
		}

		public function totalpayments(){
				return $this->payments()
					->selectRaw('SUM(total_paid) as total, invoice_sales_id')
					->groupBy('invoice_sales_id');
		}

	public function so(){
		return $this->belongsTo('App\salesorder','sales_order_id','sales_order_id');
	}
}