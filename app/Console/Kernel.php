<?php

namespace App\Console;

use App\detaildraftdelivery;
use App\detailsalesorder;
use App\draftdelivery;
use App\inventoryvaluation;
use App\product;
use App\salesinvoice;
use App\salesorder;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Redis;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
				//	SELECT product.product_id, sum(quantity) as total_qty, hpp, Sum(quantity * hpp) as total_value
				//  FROM product
				//  LEFT JOIN product_warehouse on product.product_id = product_warehouse.product_id
				// 	Left JOIN price_purchase on product.product_id = price_purchase.product_id
				// 	Where product.status = 2
				//  group by product.product_id
			$schedule->call(function () {
                $valuation = product::leftjoin('product_warehouse','product.product_id','=', 'product_warehouse.product_id')
                    ->leftJoin('price_purchase','product.product_id','=','price_purchase.product_id')
                    ->groupBy('product.product_id')
                    ->where('product.status',2)
                    ->selectRaw('product.product_id, sum(quantity) as total_qty, hpp, Sum(quantity * hpp) as total_value')
                    ->get();

                $iv = new inventoryvaluation();
                $iv->datetime = Carbon::now()->toDateTimeString();
                $iv->valuation = $valuation->sum('total_value');
                $iv->save();

                echo "calculate inventory valuation on ".Carbon::now()->toDateTimeString();
            })->daily();

            $schedule->call(function () {
                $start = Carbon::now()->startOfDay()->toDateString();

                $invoice = salesinvoice::with(['payments', 'so.customer'])
                ->leftjoin('payment_sales', function($join){
                    $join->on('invoice_sales.invoice_sales_id','=','payment_sales.invoice_sales_id');
                    $join->on("payment_sales.status","=",DB::Raw(1));
                })
                ->groupBy('invoice_sales.invoice_sales_id')
                ->selectRaw('invoice_sales.invoice_sales_id, invoice_sales.total_amount, invoice_sales.sales_order_id, SUM(IFNULL(payment_sales.total_paid, 0)) as total_payment')
                ->where('invoice_sales.status',1)
                ->where('invoice_sales.created_at','<', $start)
                ->havingRaw('total_payment < invoice_sales.total_amount')
                ->get();

                Redis::set(env('APP_ENV').'-invoice-for-payment', json_encode($invoice));
                echo "get invoice for payment on ".Carbon::now()->toDateTimeString();
            })->daily();

            $schedule->call(function () {
                $salesorderdetails = detailsalesorder::with('detail_delivery')
                ->where('status',1)
                ->whereColumn('sent','<','quantity')
                ->get();

                foreach ($salesorderdetails as $key => $value) {
                    $sent = $value->detail_delivery->sum('quantity_sent');

                    $value->sent = $sent;
                    $value->save();
                }

                echo "update so detail sent quanity on ".Carbon::now()->toDateTimeString();
            })->everyMinute();

            $schedule->call(function () {
                $draftdetails = detaildraftdelivery::with('detail_delivery')
                ->where('status',1)
                ->whereColumn('sent','<','quantity')
                ->get();

                foreach ($draftdetails as $key => $value) {
                    $sent = $value->detail_delivery->sum('quantity_sent');
                    $value->sent = $sent;
                    $value->save();
                }

                echo "update draft detail sent quantity on ".Carbon::now()->toDateTimeString();
            })->hourly();

            // $schedule->call(function () {
            //     $draft = draftdelivery::
            //     join('customer','customer.customer_id','=','draft_delivery_order.customer_id')
            //     ->join('warehouse','warehouse.warehouse_id','=','draft_delivery_order.warehouse_id')
            //     ->whereExists(function ($query) {
            //         $query->select(DB::raw('1'))
            //         ->from('draft_delivery_order_details')
            //         ->whereColumn('sent','<','quantity')
            //         ->where('status',1)
            //         ->whereRaw('draft_delivery_order_details.draft_delivery_order_id = draft_delivery_order.draft_delivery_order_id');
            //     })
            //     ->select('draft_delivery_order.draft_delivery_order_id','draft_delivery_order.draft_delivery_order_number','draft_delivery_order.shipping_term_id','customer.first_name','customer.last_name','warehouse.warehouse_name')
            //     ->get();

            //     Redis::set(env('APP_ENV').'-draft-for-delivery', json_encode($draft));

            //     echo "get draft for delivery on ".Carbon::now()->toDateTimeString();
            // })->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
