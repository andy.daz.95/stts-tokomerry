<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class expense extends Model
{
    //
    protected $table = 'expense';
    protected $primaryKey='expense_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}