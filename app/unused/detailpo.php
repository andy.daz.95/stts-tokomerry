<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class detailpo extends Model
{
    //
    protected $table = 'po_d';
    public $incrementing = false;
    protected $primaryKey='No';
    public $timestamps = false;

    public static function deletedata($kode)
    {
    	DB::table('po_d')->where('Kode_po',$kode)->delete();
    }
    public static function insertdata($datadet, $kode)
    {
        $datadet['Kode_po'] = $kode;
    	DB::table('po_d')->insert($datadet); 
    	return "updated";
    }
}