<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class barang extends Model
{
    //
    protected $table = 'barang';
    public $incrementing = false;
    protected $primaryKey='Kode';
 
    public function qty_in_warehouse()
    {
    	return $this->hasMany('App\warehouse', 'warehouse_id', 'warehouse_id');
    }

}