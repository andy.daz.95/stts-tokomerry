<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class detailretur extends Model
{
    //
    protected $table = 'retur_d';
    public $incrementing = false;
    protected $primaryKey='No';
    public $timestamps = false;

     public static function insertdata($datadet)
    {
    	DB::table('retur_d')->insert($datadet);
    }
    public static function deletedata($so)
    {
    	DB::table('retur_d')->where('No_Do',$so)->delete();
    }
}