<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class invoice extends Model
{
    //
    protected $table = 'invoice_h';
    public $incrementing = false;
    protected $primaryKey='Kode';
    public $timestamps = false;

    public static function insertdata($data, $po)
    {
    	$rowcount = DB::table('invoice_h')->where('Kode',$po)->count();
    	if($rowcount == 0)
    	{
    		DB::table('invoice_h')->insert($data);
    		return "ok";
    	}
    	else{
    		return "gagal";
    	}
    }
    public static function updatedata($data, $po)
    {
        DB::table('invoice_h')->where('Kode',$po)->update($data);
        return "ok";
    }
    public static function deletedata($po)
    {
        DB::table('invoice_h')->where('Kode',$po)->delete();
        return "ok";
    }
    public static function updatekontrak($kodebarang, $qty, $kontrak)
    {
    	/*$this->db->set('QtyTemp', "QtyTemp - '$qty'", FALSE);
			
			$where = "Kode_barang = '$kdbrg' AND Kode_po = '$kontrak';";
			
			$this->db->where($where);
			$this->db->update('kontrak_d');*/
			$getqty = DB::table('kontrak_d')->whereraw("Kode_barang = '$kodebarang' AND Kode_po = '$kontrak'")->first()->QtyTemp;
			$Jumlah = $getqty - $qty;

		DB::table('kontrak_d')
		->whereraw("Kode_barang = '$kodebarang' AND Kode_po = '$kontrak'")
		->update(['QtyTemp' => $Jumlah]);
    }
    public static function insertkurs($datakurs)
    {
    	DB::table('kurs')->insert($datakurs);
    }
    public static function cancelinvoice($po)
    {
    	/*$rr=$this->db->query("select Kode_barang,Jumlah,no_kontrak from invoice_d where Kode_po = '$po'");
			$selectBox="";
           foreach ($rr->result() as $row)
			{
				$kode = $row->Kode_barang;
				$qty = $row->Jumlah;
				$no_kontrak = $row->no_kontrak;

				$this->db->set('QtyTemp', "QtyTemp + '$qty'", FALSE);
				$where = "Kode_barang = '$kode' AND Kode_po = '$no_kontrak';";
				$this->db->where($where);
				$this->db->update('kontrak_d');
			   
			}*/
			$result = DB::table('invoice_d')
			->select('Kode_barang','Jumlah','no_kontrak')
			->where('Kode_po',$po)
			->get();

			/*return $result;*/

			foreach ($result as $row) {
				$kode = $row->Kode_barang;
				$qty = $row->Jumlah;
				$no_kontrak = $row->no_kontrak;
				$getqty = DB::table('kontrak_d')->whereraw("Kode_barang = '$kode' AND Kode_po = '$no_kontrak'")->first()->QtyTemp;
				$Jumlah = $getqty + $qty;
				
				DB::table('kontrak_d')
				->whereraw("Kode_barang = '$kode' AND Kode_po = '$no_kontrak'")
				->update(["QtyTemp" => $Jumlah]);
			}
    }
}