<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class detailso extends Model
{
    //
    protected $table = 'do_d';
    public $incrementing = false;
    protected $primaryKey='No';
    public $timestamps = false;

    public static function insertdata($datadet,$so)
    {
    	DB::table('do_d')->insert($datadet);
    }
    public static function deletedata($so)
    {
    	DB::table('do_d')->where('No_Do',$so)->delete();
    }
}