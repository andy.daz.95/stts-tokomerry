<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class kurs extends Model
{
    //
    protected $table = 'kurs';
    public $incrementing = false;
    protected $primaryKey='No';
    public $timestamps = false; 

    public static function deletedata($kode)
    {
    	DB::table('kurs')->where('invoice',$kode)->delete();
    }
}