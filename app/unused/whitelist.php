<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class whitelist extends Model
{
    //
    protected $table = 'whitelist';
    public $incrementing = false;
    protected $primaryKey='ip';
    
}