<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class unitchild extends Model
{
    //
    protected $table = 'unit_child';
    protected $primaryKey='unit_child_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function unit(){
    	return $this->belongsTo('App\unit','unit_id','unit_id');
		}
}