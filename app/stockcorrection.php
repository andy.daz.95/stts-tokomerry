<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class stockcorrection extends Model
{
    //
    protected $table = 'stock_correction';
    protected $primaryKey='stock_correction_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function account(){
    	return $this->hasOne('App\account','account_id','created_by');
    }

	public function warehouse(){
		return $this->hasOne('App\warehouse','warehouse_id','warehouse_id');
	}
}