<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class existingreceiveabletransaction extends Model
{
	//
	protected $table = 'existing_receiveable_transaction';
	protected $primaryKey='existing_receiveable_transaction_id';
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'last_update';

	public function customer(){
		return $this->hasOne('App\customer', 'customer_id', 'customer_id');
	}
	public function method(){
		return $this->hasOne('App\paymentmethod','payment_method_id','payment_method_id');
	}
}