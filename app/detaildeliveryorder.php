<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detaildeliveryorder extends Model
{
    //
    protected $table = 'delivery_order_details';
    protected $primaryKey='delivery_order_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function product(){
    	return $this->hasOne('App\product','product_id','product_id');
    }

    public function delivery(){
    	return $this->hasOne('App\deliveryorder','delivery_order_id','delivery_order_id');
    }

    public function so(){
    	return $this->delivery()->hasOne('App\salesorder','sales_order_id','sales_order_id');
    }
}