<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymentmethod extends Model
{
    //
    protected $table = 'payment_method';
    protected $primaryKey='payment_method_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}