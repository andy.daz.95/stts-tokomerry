<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailsalespayment extends Model
{
    //
    protected $table = 'payment_sales_details';
    protected $primaryKey='payment_sales_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}