<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class productchild extends Model
{
    //
    protected $table = 'product_child_price';
    protected $primaryKey='product_child_price_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}