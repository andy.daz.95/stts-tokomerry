<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bank extends Model
{
    //
    protected $table = 'bank';
    protected $primaryKey='bank_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}