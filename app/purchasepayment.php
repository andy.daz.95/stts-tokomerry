<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchasepayment extends Model
{
    //
    protected $table = 'payment_purchase';
    protected $primaryKey='payment_purchase_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

	public function details(){
		return $this->hasMany('App\detailpurchasepayment','payment_purchase_id','payment_purchase_id')
			->where('status',1);
	}

	public function invoice(){
		return $this->belongsTo('App\purchaseinvoice','invoice_purchase_id','invoice_purchase_id')
			->where('status',1);
	}
}