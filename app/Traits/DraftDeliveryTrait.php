<?php
namespace App\Traits;
use App\detaildraftdelivery;
use App\draftdelivery;
use App\salesreturn;

trait DraftDeliveryTrait{
	public function lastDraftDeliveryNumber(){
		$lastddtoday = draftdelivery::where('draft_delivery_order_number','like','DD/'.date('dmy').'/%')
			->orderby('draft_delivery_order_id','desc')
			->first();

		if(empty($lastddtoday))
		{
			$newddnumber = "DD/".date('dmy')."/1";
			return $newddnumber;
		}
		else{
			$tmpdd = explode('/',$lastddtoday->draft_delivery_order_number);
			$lastnumber = $tmpdd[2];
			$newddnumber = "DD/".date('dmy')."/".($lastnumber+1);
			return $newddnumber;
		}
	}

	public function lastAntaranNumber(){
		$lasttoday = draftdelivery::where('draft_delivery_order_number','like','SA/'.date('dmy').'/%')
			->orderby('draft_delivery_order_id','desc')
			->first();

		if(empty($lasttoday))
		{
			$newnumber = "SA/".date('dmy')."/1";
			return $newnumber;
		}
		else{
			$tmp = explode('/',$lasttoday->draft_delivery_order_number);
			$lastnumber = $tmp[2];
			$newnumber = "SA/".date('dmy')."/".($lastnumber+1);
			return $newnumber;
		}
	}

	public function lastReturNumber(){
		$lasttoday = draftdelivery::where('draft_delivery_order_number','like','DR/'.date('dmy').'/%')
			->orderby('draft_delivery_order_id','desc')
			->first();

		if(empty($lasttoday))
		{
			$newnumber = "DR/".date('dmy')."/1";
			return $newnumber;
		}
		else{
			$tmp = explode('/',$lasttoday->draft_delivery_order_number);
			$lastnumber = $tmp[2];
			$newnumber = "DR/".date('dmy')."/".($lastnumber+1);
			return $newnumber;
		}
	}

	public function createDraftDelivery($type, $id, $customer, $lastnumber,$user, $idgudang, $date ,$shippingterm, $notes){
		$draftdelivery = new draftdelivery;

		if($type == "so"){
			$draftdelivery->sales_order_id = $id;
			$draft_type = 1;
		}else if($type == 'retur'){
			$draftdelivery->sales_retur_id = $id;
			$draftdelivery->sales_order_id = salesreturn::find($id)->first()->sales_order_id;
			$draft_type = 2;
		}

		$draftdelivery->customer_id = $customer;
		$draftdelivery->warehouse_id = $idgudang;
		$draftdelivery->user_id = $user;
		$draftdelivery->draft_delivery_order_number = $lastnumber;
		$draftdelivery->date_draft_delivery  = date('Y-m-d',strtotime($date));
		$draftdelivery->shipping_term_id = $shippingterm;
		$draftdelivery->notes = $notes;
		$draftdelivery->draft_type = $draft_type;
		$draftdelivery->status = 1;
		$draftdelivery->save();

		return $draftdelivery;
	}

	public function createDetailDraftDelivery($draft, $iddetailso, $unit, $type, $idbarang, $qty){

		$draft_type = draftdelivery::find($draft)->draft_type;

		$detaildraftdelivery = new detaildraftdelivery;
		$detaildraftdelivery->draft_delivery_order_id = $draft;
		if($draft_type == 1){
			$detaildraftdelivery->sales_order_details_id = $iddetailso;
		}else{
			$detaildraftdelivery->sales_retur_details_id = $iddetailso;
		}
		$detaildraftdelivery->product_id = $idbarang;
		$detaildraftdelivery->unit_id = $unit;
		$detaildraftdelivery->unit_type= $type;
		$detaildraftdelivery->quantity = $qty;
		$detaildraftdelivery->status = 1;
		$detaildraftdelivery->save();

		return $detaildraftdelivery;
	}
}