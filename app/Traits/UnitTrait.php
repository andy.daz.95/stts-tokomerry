<?php
namespace App\Traits;

trait UnitTrait
{
	static function my_offset($text) {
		preg_match('/\d/', $text, $m, PREG_OFFSET_CAPTURE);
		if (sizeof($m))
			return $m[0][1]; // 24 in your example

		// return anything you need for the case when there's no numbers in the string
		return strlen($text);
	}
}