<?php
namespace App\Traits;
use App\salespayment;

trait SalesPaymentTrait{

	public function lastSalesPaymentNumber(){
		$lastpaymenttoday = salespayment::where('payment_sales_number','like','SP/'.date('dmy').'/%')->orderby('payment_sales_id','desc')->first();
		if(empty($lastpaymenttoday))
		{
			$newpaymentnumber = "SP/".date('dmy')."/1";
			return $newpaymentnumber;
		}
		else{
			$tmppayment = explode('/',$lastpaymenttoday->payment_sales_number);
			$lastnumber = $tmppayment[2];
			$newpaymentnumber = "SP/".date('dmy')."/".($lastnumber+1);
			return $newpaymentnumber;
		}
	}

	public function createSalesPayment($idcustomer, $idinvoice, $date, $total, $paymentmethod){
		$lastpayment = $this->lastSalesPaymentNumber();

		$salespayment = new salespayment;
		$salespayment->customer_id = $idcustomer;
		$salespayment->invoice_sales_id = $idinvoice;
		$salespayment->payment_sales_number = $lastpayment;
		$salespayment->date_payment_sales = date('Y-m-d', strtotime($date));
		$salespayment->total_paid = $total;
		$salespayment->payment_methods = $paymentmethod == 'tunai'? 1 : $paymentmethod;
		$salespayment->status = 1;
		$salespayment->save();

		return $salespayment;
	}


}