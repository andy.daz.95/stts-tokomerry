<?php
namespace App\Traits;
use App\log;

trait LogTrait{

	public function createLog($activity, $object, $number){
		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = $activity;
		$log->object = $object;
		$log->object_details = $number;
		$log->status = 1;
		$log->save();

		return $log;
	}
}