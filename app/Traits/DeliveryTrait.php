<?php
namespace App\Traits;
use App\deliveryorder;
use App\detaildeliveryorder;

trait DeliveryTrait{
	public function lastDeliveryNumber(){
		$lastdotoday = deliveryorder::where('delivery_order_number','like','DO/'.date('dmy').'/%')->orderby('delivery_order_id','desc')->first();
		if(empty($lastdotoday))
		{
			$newdonumber = "DO/".date('dmy')."/1";
			return $newdonumber;
		}
		else{
			$tmpdo = explode('/',$lastdotoday->delivery_order_number);
			$lastnumber = $tmpdo[2];
			$newdonumber = "DO/".date('dmy')."/".($lastnumber+1);
			return $newdonumber;
		}
	}

	public function createDelivery($iddraft, $iduser, $nodelivery, $date, $note, $driver, $nopol, $gudang = null){
		$deliveryorder = new deliveryorder;
		$deliveryorder->draft_delivery_order_id = $iddraft;
		$deliveryorder->user_id = $iduser;
		$deliveryorder->warehouse_id = $gudang;
		$deliveryorder->delivery_order_number = $nodelivery;
		$deliveryorder->date_delivery  = date('Y-m-d',strtotime($date));
		$deliveryorder->note = $note;
		$deliveryorder->driver_name = $driver;
		$deliveryorder->vehicle_number = $nopol;
		$deliveryorder->status = 1;
		$deliveryorder->save();

		return $deliveryorder;
	}

	public function createDetailDelivery($iddelivery, $idproduct, $idunit, $unittype,  $sendqty, $detailso, $detaildraft){
		$detaildeliveryorder = new detaildeliveryorder;
		$detaildeliveryorder->sales_order_details_id = $detailso;
		$detaildeliveryorder->draft_delivery_order_details_id = $detaildraft;
		$detaildeliveryorder->delivery_order_id = $iddelivery;
		$detaildeliveryorder->product_id = $idproduct;
		$detaildeliveryorder->unit_id = $idunit;
		$detaildeliveryorder->unit_type = $unittype;
		$detaildeliveryorder->quantity_sent = $sendqty;
		$detaildeliveryorder->status = 1;
		$detaildeliveryorder->save();
	}
}