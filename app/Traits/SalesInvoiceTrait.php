<?php
namespace App\Traits;
use App\salesinvoice;

trait SalesInvoiceTrait{

	public function lastSalesInvoiceNumber(){
		$lastinvoicetoday = salesinvoice::where('invoice_sales_number','like','SIN/'.date('dmy').'/%')->orderby('invoice_sales_id','desc')->first();
		if(empty($lastinvoicetoday))
		{
			$newinvoicenumber = "SIN/".date('dmy')."/1";
			return $newinvoicenumber;
		}
		else{
			$tmpinvoice = explode('/',$lastinvoicetoday->invoice_sales_number);
			$lastnumber = $tmpinvoice[2];
			$newinvoicenumber = "SIN/".date('dmy')."/".($lastnumber+1);
			return $newinvoicenumber;
		}
	}

	public function createSalesInvoice($idso, $total){
		$lastinvoice = $this->lastSalesInvoiceNumber();
		$salesinvoice = new salesinvoice;
		$salesinvoice->sales_order_id = $idso;
		$salesinvoice->invoice_sales_number = $lastinvoice;
		$salesinvoice->total_amount = $total;
		$salesinvoice->status = 1;
		$salesinvoice->save();

		return $salesinvoice;
	}


}