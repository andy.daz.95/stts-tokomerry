<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailsalesorder extends Model
{
	//
	protected $table = 'sales_order_details';
	protected $primaryKey='sales_order_details_id';
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'last_update';

	public function so(){
		return $this->belongsTo('App\salesorder','sales_order_id','sales_order_id')
			->where('status',1);
	}

	public function product(){
		return $this->hasOne('App\product','product_id','product_id');
	}

	public function detail_delivery()
	{
	  return $this->hasMany('App\detaildeliveryorder','sales_order_details_id', 'sales_order_details_id')
	  	->where('status',1);
	}

	public function send_sum()
	{
	  return $this->hasOne('App\detaildeliveryorder','sales_order_details_id', 'sales_order_details_id')
	    ->where('status',1)
	    ->selectRaw('sales_order_details_id, SUM(quantity_sent) as total')
	    ->groupBy('sales_order_details_id');
	}

//    public function faktur(){
//    	return $this->hasOne('App\product','product_id','product_id')
//    				->where('is_faktur',1);
//    }
	public function nonfaktur(){
		return $this->hasOne('App\product','product_id','product_id')
			->where('is_faktur',0);
	}

	public function draft(){
		return $this->hasOne('App\detaildraftdelivery','sales_order_details_id','sales_order_details_id')
			->where('status',1);
	}

	public function unit(){
		return $this->hasOne('App\satuan','unit_id','unit_id');
	}

	public function unit_child(){
		return $this->hasOne('App\unitchild','unit_child_id','unit_id');
	}
}
