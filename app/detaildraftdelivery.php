<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detaildraftdelivery extends Model
{
	//
	protected $table = 'draft_delivery_order_details';
	protected $primaryKey='draft_delivery_order_details_id';
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'last_update';

	public function product(){
		return $this->hasOne('App\product','product_id','product_id');
	}

	public function detailso(){
		return $this->hasOne('App\detailsalesorder','sales_order_details_id','sales_order_details_id');
	}

	public function unit(){
		return $this->hasOne('App\satuan','unit_id','unit_id');
	}

	public function unit_child(){
		return $this->hasOne('App\unitchild','unit_child_id','unit_id');
	}

	public function draft(){
		return $this->belongsTo('App\draftdelivery','draft_delivery_order_id','draft_delivery_order_id');
	}

	public function detail_delivery()
	{
	  return $this->hasMany('App\detaildeliveryorder','draft_delivery_order_details_id','draft_delivery_order_details_id')
	  	->where('status',1);
	}
}