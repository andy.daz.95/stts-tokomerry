<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchaseorder extends Model
{
    //
    protected $table = 'purchase_order';
    protected $primaryKey='purchase_order_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function invoice(){
    	return $this->hasOne('App\purchaseinvoice','purchase_order_id','purchase_order_id')
    				->leftjoin('invoice_purchase_details','invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
						->where('invoice_purchase.status',1);
    }

    public function supplier(){
        return $this->hasOne('App\supplier','supplier_id','supplier_id');
    }
    public function detail(){
        return $this->hasMany('App\detailpurchaseorder','purchase_order_id','purchase_order_id')
					->where('status',1);
    }
    public function paymenttype(){
    	return $this->hasOne('App\paymenttype','payment_type_id','payment_type_id');
    }

    public function discount(){
    	return $this->hasMany('App\purchasediscount','purchase_order_id','purchase_order_id')
				->where('status',1);
		}
}