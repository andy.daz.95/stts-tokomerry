<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class paymenttype extends Model
{
    //
    protected $table = 'payment_type';
    protected $primaryKey='payment_type_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}