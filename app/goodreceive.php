<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class goodreceive extends Model
{
    //
    protected $table = 'good_receive';
    protected $primaryKey='good_receive_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function details()
    {
        return $this->hasMany('App\detailgoodreceive', 'good_receive_id', 'good_receive_id')
					->where('status',1);
    }
}