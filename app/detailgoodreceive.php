<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailgoodreceive extends Model
{
    //
    protected $table = 'good_receive_details';
    protected $primaryKey='good_receive_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function goodreceive()
    {
        return $this->hasOne('App\goodreceive', 'good_receive__id', 'good_receive_id');
    }
    public function product()
    {
        return $this->hasOne('App\product', 'product_id', 'product_id');
    }
    public function warehouse()
    {
        return $this->hasOne('App\warehouse', 'warehouse_id', 'warehouse_id');
    }
}