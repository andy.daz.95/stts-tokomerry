<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class additionalproductdetail extends Model
{
    //
    protected $table = 'additional_product_detail';
    protected $primaryKey='id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}