<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class returtype extends Model
{
    //
    protected $table = 'retur_type';
    protected $primaryKey='retur_type_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}