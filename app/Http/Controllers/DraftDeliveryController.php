<?php

namespace App\Http\Controllers;

use App\account;
use App\product;
use App\salesinvoice;
use Illuminate\Http\Request;
use App\salesorder;
use App\detailsalesorder;
use App\salesreturn;
use App\shippingterm;
use App\warehouse;
use App\productwarehouse;
use App\draftdelivery;
use App\detaildraftdelivery;
use App\log;
use DataTable;
use DOMPDF;
use App\Traits\DeliveryTrait;
use DB;

class DraftDeliveryController extends Controller
{
	//
	use DeliveryTrait;
	public function showDraftDeliveryPage()
	{
		$data['so'] = detailsalesorder::with('so')
			->leftjoin('sales_order','sales_order.sales_order_id','=','sales_order_details.sales_order_id')
			->whereDoesntHave('draft')
			->where('sales_order.status', 1)
			->where('sales_order_details.status', 1)
			->groupBy('sales_order_details.sales_order_id')
			->get();

		// $data['so'] = salesorder::doesntHave('details.draft')->where('status',1)->limit(100)->get();
		$data['retur'] = salesreturn::doesntHave('details.draft')->where('retur_type_id',5)->where('status',1)->get();
		$data['shippingterm'] = shippingterm::where('status',1)->get();
		$data['warehouse'] = warehouse::where('status',1)->get();
		$data['account'] = account::where('status',1)->get();

		return view('draft_delivery.draft_delivery', compact('data'));
	}

	public function getDraftTable(Request $request){
		$draft = draftdelivery::join('sales_order','sales_order.sales_order_id','=','draft_delivery_order.sales_order_id')
			->join('customer','customer.customer_id','=','sales_order.customer_id')
			->join('account','account.account_id','=','draft_delivery_order.user_id')
			->where('draft_delivery_order.status',1)
			->orderby('draft_delivery_order.draft_delivery_order_id','desc')
			->select('draft_delivery_order.draft_delivery_order_id','draft_delivery_order.draft_delivery_order_number','account.full_name','sales_order.sales_order_number','customer.first_name','customer.last_name','draft_delivery_order.date_draft_delivery');

		if ($request->nodraft && $request->nodraft != "")
		{
			$draft = $draft->where('draft_delivery_order_number','like','%'.$request->nodraft.'%');
		}
		if($request->customer && $request->customer != "")
		{
			$draft = $draft->whereRaw("CONCAT_WS(' ', `first_name`, `last_name`) like '%$request->customer%'");
		}

		return DataTable::of($draft)
			->setRowAttr([
				'value' => function($draft) {
					return $draft->draft_delivery_order_id;
				},
			])
			->addColumn('sales_order_number', function ($draft){
				//kalau tipe draft retur tambahin prefix retur dibelakang no so
				if($draft->draft_type == 2){
					return $draft->sales_order_number.' (Retur)';
				}else{
					return $draft->sales_order_number;
				}
			})
			->addColumn('customer_name', function ($draft){
				return $draft->first_name.' '.$draft->last_name;
			})
			->addColumn('action', function ($draft) {
					return '<a class="btn btn-sm btn-raised orange print-draft" target="_blank" href="downloaddraftdelivery/'.$draft->draft_delivery_order_id.'"><i class="material-icons">print</i></a>';
			})
			->smart(false)
			->make(true);
	}

	public function getDraftDelivery(Request $request){
		$draft = draftdelivery::where('draft_delivery_order_id',$request->id)
			->with('so.customer')
			->with('retur.so.customer')
			->with('details.product.satuan.unit_child')
			->first();

		return $draft;
	}

	public function getStockForDraft(Request $request){
		$stock = productwarehouse::whereIn('product_id',$request->idproduct)
			->where('warehouse_id',$request->idgudang)
			->get();

		//check if product has unit_child
		foreach($stock as $key => $value)
		{
			$unit_child = product::where('product_id',$value->product_id)
				->with('satuan.unit_child')
				->whereHas('satuan.unit_child');

			if($unit_child->count() > 0){
				$unit_child = $unit_child->first()->satuan->unit_child;
				$value->qty_child = number_format((float)$value->quantity * $unit_child->multiplier, 2, '.', '');;
			}	else{
				$value->qty_child = null;
			}
		}
		return $stock;
	}

	public function getSalesOrderData(Request $request)
	{
		$so = salesorder::with('details.draft')
			->with('details.product')
			->with('details.unit')
			->with('details.unit_child')
			->with('customer')
			->where('status',1)
			->where('sales_order_id',$request->id)
			->first();

		return $so;
	}

	public function getSalesReturData(Request $request)
	{
		$retur = salesreturn::with('details.draft')
			->with('details.product')
			->with('so.customer')
			->where('status',1)
			->where('sales_retur_id',$request->id)
			->first();

		return $retur;
	}

	public function getLastDraftDeliveryNumber()
	{
		$lastddtoday = draftdelivery::where('draft_delivery_order_number','like','DD/'.date('dmy').'/%')
			->orderby('draft_delivery_order_id','desc')
			->first();

		if(empty($lastddtoday))
		{
			$newddnumber = "DD/".date('dmy')."/1";
			return $newddnumber;
		}
		else{
			$tmpdd = explode('/',$lastddtoday->draft_delivery_order_number);
			$lastnumber = $tmpdd[2];
			$newddnumber = "DD/".date('dmy')."/".($lastnumber+1);
			return $newddnumber;
		}
	}

	public function createDraftDelivery(Request $request, $type)
	{
		$lastnumber = $this->getLastDraftDeliveryNumber();

		$draftdelivery = new draftdelivery;

		if($type == "so"){
			$draftdelivery->sales_order_id = $request->so;
			$draft_type = 1;
		}else if($type == 'retur'){
			$draftdelivery->sales_retur_id = $request->so;
			$draftdelivery->sales_order_id = salesreturn::find($request->so)->first()->sales_order_id;
			$draft_type = 2;
		}

		$draftdelivery->warehouse_id = $request->gudang;
		$draftdelivery->user_id = $request->user;
		$draftdelivery->draft_delivery_order_number = $lastnumber;
		$draftdelivery->date_draft_delivery  = date('Y-m-d',strtotime($request->tgldd));
		$draftdelivery->shipping_term_id = $request->shippingterm;
		$draftdelivery->notes = $request->notes;
		$draftdelivery->draft_type = $draft_type;
		$draftdelivery->status = 1;
		$draftdelivery->save();

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			if(in_array($request->iddetail[$i], $request->check) != false) { // bila ada di array check baru save
				$detaildraftdelivery = new detaildraftdelivery;
				$detaildraftdelivery->draft_delivery_order_id = $draftdelivery->draft_delivery_order_id;

				if($type == 'so'){
					$detaildraftdelivery->sales_order_details_id = $request->iddetail[$i];
				}else if($type == 'retur'){
					$detaildraftdelivery->sales_retur_details_id = $request->iddetail[$i];
				}

				$detaildraftdelivery->product_id = $request->idbarang[$i];
				$detaildraftdelivery->quantity = $request->orderqty[$i];
				$detaildraftdelivery->status = 1;
				$detaildraftdelivery->save();
			}
		}

		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "menambahkan";
		$log->object = "Draft Delivery Order";
		$log->object_details = $draftdelivery->draft_delivery_order_number;
		$log->status = 1;
		$log->save();


		if(isset($request->auto)){
//			$lastnumber = $this->lastDeliveryNumber();

//			$deliveryoder = $this->createDelivery($draftdelivery->draft_delivery_order_id, $lastnumber, $draftdelivery->date_draft_delivery, 'Kirim Dari Toko', null, null, $draftdelivery->user_id);
//
//			for ($i=0; $i <sizeof($request->idbarang); $i++)
//			{
//				if(in_array($request->iddetail[$i], $request->check) != false) { // bila ada di array check baru save
//					$this->createDetailDelivery($deliveryoder->delivery_order_id, $request->idbarang[$i], $request->orderqty[$i]);
//
//					productwarehouse::where('product_id',$request->idbarang[$i])->where('warehouse_id',$request->gudang)->update([
//						'quantity' => DB::raw('quantity-'.$request->orderqty[$i]),
//					]);
//				}
//			}
		}
	}

	public function updateDraftDelivery(Request $request){
		$draftdelivery = draftdelivery::find($request->iddd);
		$draftdelivery->user_id = $request->user;
		$draftdelivery->notes = $request->notes;
		$draftdelivery->shipping_term_id = $request->shippingterm;
		$draftdelivery->driver_name = $request->driver;
		$draftdelivery->vehicle_number = $request->nopol;
		$draftdelivery->update();
	}

	public function deleteDraftDelivery(Request $request){
		$draftdelivery = draftdelivery::find($request->id);

		detaildraftdelivery::where('draft_delivery_order_id',$draftdelivery->draft_delivery_order_id)->update([
			'status' => 2,
		]);

		$draftdelivery->status = 2;
		$draftdelivery->update();

	}

	public function downloadDraftDelivery(Request $request, $id, $size = 'regular')
	{

		$draft = draftdelivery::with('details.product')
			->with('so.customer')
			->with('retur.so.customer')
			->with('warehouse')
			->where('draft_delivery_order_id',$id)
			->first();

		if($draft->so){
			$so = $draft->so;
			$customer = $draft->so->customer->customer_id;
		}else if ($draft->retur->so){
			$so = $draft->retur->so;
			$customer = $draft->retur->so->customer->customer_id;
		}

		$invoicebelumlunas = salesinvoice::with('payments')
			->leftjoin('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
			->whereExists(function ($query) {
				$query->select(DB::raw('Sum(payment_sales.total_paid) as total'))
					->from('payment_sales')
					->groupBy('payment_sales.invoice_sales_id')
					->whereRaw('invoice_sales.invoice_sales_id = payment_sales.invoice_sales_id AND payment_sales.status = 1')
					->havingRaw('total < invoice_sales.total_amount');
			})
			->where('invoice_sales.status',1)
			->where('sales_order.customer_id',$customer);

			if($customer == 2) // Customer Tanpa Nama
			{
				$invoicebelumlunas->where('sales_order.sales_order_id',$so->sales_order_id);
			}

			$invoicebelumlunas = $invoicebelumlunas->get();

		$retur = salesreturn::leftjoin('sales_order','sales_order.sales_order_id','=','sales_retur.sales_order_id')
			->with(['details' => function ($query) {
				$query->whereColumn('received_qty', '<', 'qty');
			}])
			->whereHas('details', function ($query) {
				$query->whereColumn('received_qty', '<', 'qty');
			})
			->where('sales_order.customer_id',$customer)
			->whereIn('retur_type_id',[2,7])
			->get();

		$order = $request->order ? $request->order : null;

		if($size == 'small'){
			$pdf = DOMPDF::loadView('draft_delivery.draft_pdf_small', compact('draft','retur', 'invoicebelumlunas','order'))
				->setPaper(array(0, 0, 270, 391));
		}else{
			$pdf = DOMPDF::loadView('draft_delivery.draft_pdf', compact('draft','retur', 'invoicebelumlunas','order'))
				->setPaper(array(0, 0, 609, 391));
		}
		return $pdf->stream('Draft.pdf');
	}
}
