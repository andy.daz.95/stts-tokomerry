<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\salesinvoice;
use App\detailsalesinvoice;
use App\salesorder;
use App\log;
use App\customer;
use DOMPDF;
use DataTable;
use DB;

class SalesInvoiceController extends Controller
{
	public function showSalesInvoicePage()
	{
		/*select sales_order.sales_order_id, sales_order.grand_total_idr, tableb.total_paid from sales_order left join (SELECT sales_order.sales_order_id, sales_order.sales_order_number, sales_order.grand_total_idr, sum(total_amount) as total_paid FROM sales_order LEFT join invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id LEFT JOIN invoice_sales_details on invoice_sales.invoice_sales_id = invoice_sales_details.invoice_sales_id GROUP by sales_order_id) tableb on tableb.sales_order_id = sales_order.sales_order_id*/

		$so = salesorder::with('customer')->leftjoin(DB::raw(
			'(SELECT sales_order.sales_order_id, sales_order.sales_order_number, sales_order.grand_total_idr, sum(invoice_sales.total_amount) as total_paid
            FROM sales_order
            LEFT join invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id
            where sales_order.status <> 2 and invoice_sales.status <> 2
            GROUP by sales_order_id) tableb'
		),'sales_order.sales_order_id','=','tableb.sales_order_id')
			->select('sales_order.sales_order_id', 'sales_order.sales_order_number', 'sales_order.grand_total_idr','tableb.total_paid','sales_order.customer_id')
			->where('sales_order.status','<>',2)
			->Where(function ($query) {
				$query->whereRaw('total_paid  < sales_order.grand_total_idr - reduction')
					->orWhereNull('total_paid');
			})
			->get();


		// ->whereRaw('total_paid  < sales_order.grand_total_idr - reduction')
		// ->orWhereNull('total_paid')
		return view('sales_invoice.sales_invoice', compact('so','invoice'));
	}

	public function getSalesInvoiceTable(request $request){
		$invoice = salesinvoice::leftjoin('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
			->leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
			->where('invoice_sales.status',1)
			->orderby('invoice_sales.invoice_sales_id','desc')
			->select('invoice_sales.invoice_sales_id','invoice_sales.total_amount', 'invoice_sales.invoice_sales_number','sales_order.sales_order_id','sales_order.sales_order_number','date_sales_order','sales_order.grand_total_idr', 'customer.first_name', 'customer.last_name');

		if ($request->noinvoice && $request->noinvoice != "")
		{
			$invoice = $invoice->where('invice_sales.invoice_sales_number','like','%'.$request->noinvoice.'%');
		}
		if($request->customer && $request->customer != "")
		{
			$invoice = $invoice->whereRaw("CONCAT_WS(' ', `first_name`, `last_name`) like '%$request->customer%'");
		}

		return DataTable::of($invoice)
			->setRowAttr([
				'value' => function($invoice) {
					return $invoice->invoice_sales_id;
				},
			])
			->addColumn('customer_name', function ($invoice){
				return $invoice->first_name.' '.$invoice->last_name;
			})
			->addColumn('action', function ($invoice) {
					return
						'<a class="btn btn-sm btn-raised orange print-invoice" target="_blank" href="downloadsalesinvoice/' . $invoice->invoice_sales_id . '"><i class="material-icons">print</i></a>
    				<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
			})
			->editColumn('grand_total_idr', function($invoice){
				return number_format($invoice->grand_total_idr);
			})
			->editColumn('total_amount', function($invoice){
				return number_format($invoice->total_amount);
			})
			->smart(false)
			->make(true);
	}

	public function getLastInvoiceNumber()
	{
		$lastinvoicetoday = salesinvoice::where('invoice_sales_number','like','SIN/'.date('dmy').'/%')->orderby('invoice_sales_id','desc')->first();
		if(empty($lastinvoicetoday))
		{
			$newinvoicenumber = "SIN/".date('dmy')."/1";
			return $newinvoicenumber;
		}
		else{
			$tmpinvoice = explode('/',$lastinvoicetoday->invoice_sales_number);
			$lastnumber = $tmpinvoice[2];
			$newinvoicenumber = "SIN/".date('dmy')."/".($lastnumber+1);
			return $newinvoicenumber;
		}
	}

	public function getSalesInvoice(Request $request)
	{
		/*select invoice_sales.*, invoice_sales_details.total_amount, sales_order.grand_total_idr, tablea.total from `invoice_sales` left join `invoice_sales_details` on `invoice_sales`.`invoice_sales_id` = `invoice_sales_details`.`invoice_sales_id` LEFT JOIN sales_order on sales_order.sales_order_id = invoice_sales.sales_order_id LEFT JOIN (SELECT sales_order.sales_order_id, SUM(invoice_sales_details.total_amount) as total FROM sales_order LEFT JOIN invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id LEFT JOIN invoice_sales_details on invoice_sales.invoice_sales_id = invoice_sales_details.invoice_sales_details_id WHERE sales_order.sales_order_id = 14)tablea on sales_order.sales_order_id = tablea.sales_order_id where invoice_sales.invoice_sales_id = 2*/
		$so = salesinvoice::where('invoice_sales_id',$request->id)->first()->sales_order_id;
		$invoice = salesinvoice::leftjoin('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
			->leftjoin(DB::Raw(
				'(SELECT sales_order.sales_order_id, SUM(invoice_sales.total_amount) as total
                FROM sales_order
                LEFT JOIN invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id
                WHERE sales_order.sales_order_id ='.$so.')tablea'
			),'sales_order.sales_order_id','=','tablea.sales_order_id')
			->leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
			->leftjoin('payment_type','payment_type.payment_type_id','=','sales_order.payment_type_id')
			->where('invoice_sales.invoice_sales_id', $request->id)
			->select('invoice_sales.*','sales_order.grand_total_idr','sales_order.date_sales_order','sales_order.reduction','tablea.total','customer.*','sales_order.sales_order_id','sales_order.sales_order_number','payment_type.payment_description')
			->first();

		return $invoice;
	}

	public function getSalesOrderData(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}
		$so = salesorder::leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
			->leftjoin('payment_type','payment_type.payment_type_id','=','sales_order.payment_type_id')
			->leftjoin('invoice_sales','invoice_sales.sales_order_id','=','sales_order.sales_order_id')
			->where('sales_order.sales_order_id',$request->id)
			->where('invoice_sales.status','<>',2)
			->selectraw('sales_order.*, sum(invoice_sales.total_amount) as total, customer.first_name, customer.last_name, payment_description, customer.next_invoice_reduction')
			->first();
		/*SELECT sales_order.sales_order_id, sales_order.sales_order_number, sales_order.grand_total_idr, sum(total_amount) as total_paid FROM sales_order LEFT join invoice_sales on sales_order.sales_order_id = invoice_sales.sales_order_id LEFT JOIN invoice_sales_details on invoice_sales.invoice_sales_id = invoice_sales_details.invoice_sales_id*/
		return $so;
	}

	public function createInvoice(Request $request)
	{
		$salesinvoice = new salesinvoice;
		$salesinvoice->sales_order_id = $request->id;
		$salesinvoice->invoice_sales_number = $request->number;
		$salesinvoice->total_amount = $request->payment;
		$salesinvoice->prev_invoice_reduction = $request->prev;
		$salesinvoice->status = 1;
		$salesinvoice->save();

		if($request->prev > 0)
		{
			$customer = salesorder::find($request->id)->customer_id;
			customer::where('customer_id',$customer)->update([
				'next_invoice_reduction' => DB::raw('next_invoice_reduction-'.$request->prev),
			]);
		}

		customer::where('customer_id',$request->idcustomer)->update([
			'total_balance' => DB::raw('total_balance-'.$request->payment),
		]);

		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "menambahkan";
		$log->object = "Sales Invoice";
		$log->object_details = $salesinvoice->invoice_sales_number;
		$log->status = 1;
		$log->save();
	}

	public function updateInvoice(Request $request)
	{
		$salesinvoice = salesinvoice::find($request->idinvoice);

		customer::where('customer_id',$request->idcustomer)->update([
			'total_balance' => DB::raw('total_balance+'.$salesinvoice->total_amount),
		]);

		$salesinvoice->total_amount = $request->payment;
		$salesinvoice->save();

		customer::where('customer_id',$request->idcustomer)->update([
			'total_balance' => DB::raw('total_balance-'.$salesinvoice->total_amount),
		]);


		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "mengubah";
		$log->object = "Sales Invoice";
		$log->object_details = $salesinvoice->invoice_sales_number;
		$log->status = 1;
		$log->save();
	}

	public function deleteInvoice(Request $request)
	{

		$salesinvoice = salesinvoice::find($request->id);
		$customer = salesinvoice::leftjoin('sales_order','invoice_sales.sales_order_id','=','sales_order.sales_order_id')->where('invoice_sales_id',$request->id)->first()->customer_id;

		customer::where('customer_id',$customer)->update([
			'total_balance' => DB::raw('total_balance+'.$salesinvoice->total_amount),
		]);

		if($salesinvoice->prev_invoice_reduction > 0)
		{
			customer::where('customer_id',$customer)->update([
				'next_invoice_reduction' => DB::raw('next_invoice_reduction+'.$salesinvoice->prev_invoice_reduction),
			]);
		}

		salesinvoice::where('invoice_sales_id',$request->id)->update(["status"=>2]);

		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "menghapus";
		$log->object = "Sales Invoice";
		$log->object_details = $salesinvoice->invoice_sales_number;
		$log->status = 1;
		$log->save();
	}

	public function downloadSalesInvoice(Request $request, $id)
	{
		$invoiceheader = salesinvoice::join('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
			->leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
			->where('invoice_sales.invoice_sales_id',$id)
			->first();

		$sodetail = salesorder::leftjoin('sales_order_details', function ($join) {
			$join->on('sales_order.sales_order_id','=','sales_order_details.sales_order_id')
				->on('sales_order_details.status', '<>' ,DB::raw("'2'"));
		})
			->join('product','sales_order_details.product_id','=','product.product_id')
			->leftjoin('unit','unit.unit_id','=','product.unit_id')
			->select('sales_order_details.*','product.product_code','product.product_name','unit.unit_description')
			->where('sales_order.sales_order_id', $invoiceheader->sales_order_id)
			->get();

		$pdf = DOMPDF::loadView('sales_invoice.invoice_pdf', compact('invoiceheader','sodetail'));
		return $pdf->stream('SalesInvoice.pdf');
	}

	public function recacheInvoice(){
		// Need To Re Cache for Sales Payment
		$data['invoice'] = salesinvoice::with(['payments', 'so.customer'])
			->leftjoin('payment_sales', function($join){
				$join->on('invoice_sales.invoice_sales_id','=','payment_sales.invoice_sales_id');
				$join->on("payment_sales.status","=",DB::Raw(1));
			})
			->groupBy('invoice_sales.invoice_sales_id')
			->selectRaw('invoice_sales.invoice_sales_id, invoice_sales.total_amount, invoice_sales.sales_order_id, SUM(IFNULL(payment_sales.total_paid, 0)) as total_payment')
			->where('invoice_sales.status',1)
			->havingRaw('total_payment < invoice_sales.total_amount')
			->get();

		Redis::set('invoice-'.env('APP_ENV'), json_encode($invoice));
	}

}