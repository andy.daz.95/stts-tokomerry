<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\supplier;
use App\purchaseorder;
use App\purchasediscount;
use App\detailpurchaseorder;
use App\paymenttype;
use App\log;
use DB;
use App;
use DataTable;
use DOMPDF;

class PurchaseOrderController extends Controller
{
	public function showPurchaseOrderPage()
	{
		$data['supplier'] = supplier::where('status','<>',2)->get();
		$data['payment_term'] = paymenttype::all();
		return view('po.po', compact('data'));
	}

	public function getPurchaseOrderTable(Request $request){
		$po = purchaseorder::join('supplier','purchase_order.supplier_id','=','supplier.supplier_id')
			->join('payment_type','purchase_order.payment_type_id','=','payment_type.payment_type_id')
			->leftjoin(DB::raw("
                            (SELECT * 
                                from good_receive 
                                where status <> 2 
                                GROUP BY purchase_order_id )gr"
			),"purchase_order.purchase_order_id",'=','gr.purchase_order_id')
			->leftjoin('invoice_purchase',
				function($join){
					$join->on('purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
						->on('invoice_purchase.status','<>', DB::raw('2'));
				})
			->selectRaw("purchase_order.* , payment_type.payment_description, supplier.company_name, gr.good_receive_id, invoice_purchase.invoice_purchase_id, DATE_ADD(purchase_order.date_purchase_order, INTERVAL purchase_order.payment_term DAY) as jatuh_tempo, (CASE WHEN (gr.good_receive_id IS NULL AND invoice_purchase.invoice_purchase_id IS NULL) Then 'editable' ELSE 'not' END) as edit_status")
			->where('purchase_order.status','<>',2)
			->orderby('purchase_order.purchase_order_id','desc');

		if ($request->nopo && $request->nopo != "")
		{
			$po= $po->where('purchase_order_number','like','%'.$request->nopo.'%');
		}
		if($request->supplier && $request->supplier != "")
		{
			$po = $po->whereRaw("company_name like '%$request->supplier%'");
		}
		$po = $po->get();

		return DataTable::of($po)
			->setRowAttr([
				'value' => function($po) {
					return $po->purchase_order_id;
				},
			])
			->addColumn('action', function ($po){
				if($po->edit_status == 'editable'){
					return
						'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$po->purchase_order_id.'"><i class="material-icons">edit</i></a>
						<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
						<a class="btn btn-sm btn-raised orange print-po" target="_blank" href="downloadpurchaseorder/'.$po->purchase_order_id.'"><i class="material-icons">print</i></a>';
				}else{
					return '<a class="btn btn-sm btn-raised orange print-po" target="_blank" href="downloadpurchaseorder/'.$po->purchase_order_id.'"><i class="material-icons">print</i></a>';
				}
			})
			->editColumn('is_pkp', function($po){
				if ($po->is_pkp == 0){
					return '-';
				}else{
					return 'pkp';
				}
			})
			->editColumn('grand_total_idr', function($po){
				return number_format($po->grand_total_idr);
			})
			->smart(false)
			->make(true);
	}

	public function getLastPurchaseNumber(Request $request)
	{
		$lastpurchasetoday = purchaseorder::where('purchase_order_number','like','PO/'.date('dmy').'/%')->orderby('purchase_order_id','desc')->first();
		if(empty($lastpurchasetoday))
		{
			$newponumber = "PO/".date('dmy')."/1";
			return $newponumber;
		}
		else{
			$tmppurchase = explode('/',$lastpurchasetoday->purchase_order_number);
			$lastnumber = $tmppurchase[2];
			$newponumber = "PO/".date('dmy')."/".($lastnumber+1);
			return $newponumber;
		}
	}

	public function checkPurchaseNumber(Request $request)
	{
		$count = purchaseorder::where('purchase_order_number','like',$request->nopo)->count();

		if($count > 0){
			return 'false';
		}else{
			return 'true';
		}
	}

	public function getPurchaseOrder(Request $request)
	{
		$po = purchaseorder::leftjoin('purchase_order_details','purchase_order.purchase_order_id','=','purchase_order_details.purchase_order_id')
			->leftjoin('product','product.product_id','=','purchase_order_details.product_id')
			->where('purchase_order.purchase_order_id',$request->id)
			->where('purchase_order.status','<>',2)
			->select('purchase_order.*','purchase_order_details.*','product.product_name as p_name','product.price_sale')
			->get();
		$discount = purchasediscount::leftjoin('purchase_order','purchase_order.purchase_order_id','=','purchase_discount.purchase_order_id')
			->where('purchase_order.purchase_order_id',$request->id)
			->where('purchase_order.status',"<>",2)
			->where('purchase_discount.status',"<>",2)
			->get();
		return compact('po','discount');
	}

	public function createPurchaseOrder(Request $request)
	{
		$total= array_sum($request->subtotal);
		// $tax= 0.01 * $total;
		$tax= 0;
		$grandtotal = $total + $tax;

		$purchaseorder = new purchaseorder;
		$purchaseorder->supplier_id = $request->supplier;
		$purchaseorder->payment_type_id = $request->pembayaran;
		$purchaseorder->shipping_term_id = 1;
		$purchaseorder->purchase_order_number = $request->nopo;
		$purchaseorder->sub_total = $total;
		$purchaseorder->tax = $tax;
		$purchaseorder->payment_term = $request->terms;
		$purchaseorder->note = $request->notes;
		$purchaseorder->date_purchase_order = date('Y-m-d', strtotime($request->tglpo));
		$purchaseorder->grand_total_idr = $grandtotal;
		$purchaseorder->status = 1;
		$purchaseorder->discount_type = $request->discounttype;
		$purchaseorder->is_pkp = $request->pkp;
		$purchaseorder->save();

		if($request->discount)
		{
			$discount = trim($request->discount," ");
			$discount = str_replace("Rp", '', $discount);
			$discount = str_replace("%",'',$discount);
			$disc_array = explode(";",$discount);
			for ($i=0; $i < sizeof($disc_array) ; $i++) {
				$detailarray = explode('/',$disc_array[$i]);
				$purchasediscount = new purchasediscount;
				$purchasediscount->purchase_order_id = $purchaseorder->purchase_order_id;
				$purchasediscount->discount = $detailarray[0] ;
				$purchasediscount->discount_period = $detailarray[1];
				$purchasediscount->status = 1;
				$purchasediscount->save();
			}
		}

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			$detailpo = new detailpurchaseorder;
			$detailpo->purchase_order_id = $purchaseorder->purchase_order_id;
			$detailpo->product_id = $request->idbarang[$i];
			$detailpo->price = $request->harga[$i];
			$detailpo->quantity = $request->qty[$i];
			$detailpo->free_qty = $request->free[$i] ? $request->free[$i] : 0;
			$detailpo->discount_1 = $request->disc1[$i] ? $request->disc1[$i] : 0;
			$detailpo->discount_2 = $request->disc2[$i] ? $request->disc2[$i] : 0;
			$detailpo->discount_3 = $request->disc3[$i] ? $request->disc3[$i] : 0;
			$detailpo->discount_nominal = $request->discnominal[$i] ? $request->discnominal[$i] : 0;
			$detailpo->sub_total = $request->subtotal[$i];
			$detailpo->total_price = $request->subtotal[$i];
			$detailpo->detail_note = $request->detailnote[$i] ;
			$detailpo->status = 1;
			$detailpo->save();
		}

		$log = new log;
		$log->actor = Session('user')->full_name;
		$log->activity = "menambahkan";
		$log->object = "Purchase Order";
		$log->object_details = $purchaseorder->purchase_order_number;
		$log->status = 1;
		$log->save();
	}

	public function updatePurchaseOrder(Request $request)
	{

		$total= array_sum($request->subtotal);
		// $tax= 0.01 * $total;
		$tax= 0;
		$grandtotal = $total + $tax;

		$purchaseorder = purchaseorder::find($request->id);
		$purchaseorder->purchase_order_number = $request->nopo;
		$purchaseorder->payment_type_id = $request->pembayaran;
		$purchaseorder->shipping_term_id = 1;
		$purchaseorder->sub_total = $total;
		$purchaseorder->note = $request->notes;
		$purchaseorder->payment_term = $request->terms;
		$purchaseorder->date_purchase_order = date('Y-m-d', strtotime($request->tglpo));
		$purchaseorder->grand_total_idr = $grandtotal;
		$purchaseorder->status = 1;
		$purchaseorder->save();

		if($request->discount)
		{
			purchasediscount::where('purchase_order_id', $request->id)->delete();
			$discount = trim($request->discount," ");
			$discount = str_replace("Rp", '', $discount);
			$discount = str_replace("%",'',$discount);
			$disc_array = explode(";",$discount);


			for ($i=0; $i < sizeof($disc_array) ; $i++) {
				if($disc_array[$i] != "")
				{
					$detailarray = explode('/',$disc_array[$i]);
					$purchasediscount = new purchasediscount;
					$purchasediscount->purchase_order_id = $purchaseorder->purchase_order_id;
					$purchasediscount->discount = $detailarray[0] ;
					$purchasediscount->discount_period = $detailarray[1];
					$purchasediscount->status = 1;
					$purchasediscount->save();
				}
			}
		}
		// return $disc_array;

		detailpurchaseorder::where('purchase_order_id',$request->id)->delete();
		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			$detailpo = new detailpurchaseorder;
			$detailpo->purchase_order_id = $request->id;
			$detailpo->product_id = $request->idbarang[$i];
			$detailpo->price = $request->harga[$i];
			$detailpo->quantity = $request->qty[$i];
			$detailpo->free_qty = $request->free[$i] ? $request->free[$i] : 0;
			$detailpo->discount_1 = $request->disc1[$i] ? $request->disc1[$i] : 0;
			$detailpo->discount_2 = $request->disc2[$i] ? $request->disc2[$i] : 0;
			$detailpo->discount_3 = $request->disc3[$i] ? $request->disc3[$i] : 0;
			$detailpo->discount_nominal = $request->discnominal[$i] ? $request->discnominal[$i] : 0;
			$detailpo->sub_total = $request->subtotal[$i];
			$detailpo->total_price = $request->subtotal[$i];
			$detailpo->status = 1;
			$detailpo->save();
		}

		$log = new log;
		$log->actor = Session('user')->full_name;
		$log->activity = "mengubah";
		$log->object = "Purchase Order";
		$log->object_details = $purchaseorder->purchase_order_number;
		$log->status = 1;
		$log->save();

	}

	public function deletePurchaseOrder(Request $request)
	{
		purchaseorder::where('purchase_order_id', $request->id)->update(['status'=>2]);
		purchasediscount::where('purchase_order_id', $request->id)->update(['status'=>2]);
		detailpurchaseorder::where('purchase_order_id',$request->id)->update(['status'=>2]);
	}

	public function downloadPurchaseOrder(Request $request, $id)
	{
		$po = purchaseorder::leftjoin('purchase_order_details','purchase_order.purchase_order_id','=','purchase_order_details.purchase_order_id')
			->leftjoin('product','product.product_id','=','purchase_order_details.product_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->leftjoin('payment_type','purchase_order.payment_type_id','=','payment_type.payment_type_id')
			->where('purchase_order.purchase_order_id',$id)
			->select('purchase_order.*','purchase_order_details.*','product.product_name as p_name','product.price_sale','supplier.*','payment_type.payment_description')
			->get();

		$poheader = purchaseorder::leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->leftjoin('payment_type','purchase_order.payment_type_id','=','payment_type.payment_type_id')
			->where('purchase_order.purchase_order_id',$id)
			->first();

		$podetail = detailpurchaseorder::leftjoin('product','product.product_id','=','purchase_order_details.product_id')
			->where('purchase_order_id',$id)
			->where('purchase_order_details.status','<>',2)
			->select('purchase_order_details.*','product.product_name as p_name')->get();

		$pdf = DOMPDF::loadView('po.po_pdf', compact('poheader','podetail'));
		return $pdf->stream('PO.pdf');
	}

	// public function reportFakturPO(Request $request){
	//     $startdate = date('Y-m-d');
	//     $enddate = date('Y-m-d');

	//     if($request->start){
	//         $startdate = date('Y-m-d', strtotime($request->start));
	//     }
	//     if($request->end){
	//         $enddate = date('Y-m-d', strtotime($request->end));
	//     }


	//     $powithoutinvoice = purchaseorder::doesntHave('invoice')
	//             ->whereBetween('date_purchase_order',[$startdate, $enddate]);
	//     $po = purchaseorder::with('supplier')
	//             ->with('invoice.payment')
	//             ->whereBetween('date_purchase_order',[$startdate, $enddate]);
	//     if($request->fakturtype){
	//         if($request->fakturtype == 1){
	//             $po = $po->where('is_pkp', 1);
	//             $powithoutinvoice = $powithoutinvoice->where('is_pkp', 1);
	//         }else if ($request->fakturtype == 2){
	//             $po = $po->where('is_pkp', 0);
	//             $powithoutinvoice = $powithoutinvoice->where('is_pkp', 0);
	//         }
	//     }
	//     $po = $po->get();
	//     $powithoutinvoice = $powithoutinvoice->get();


	//     if($request->start || $request->end || $request->fakturtype){
	//         return view('po.history_purchase_order_detail', compact('po','powithoutinvoice'));
	//     }
	//     return view('po.history_purchase_order', compact('po'));
	// }
}
