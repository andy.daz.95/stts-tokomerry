<?php

namespace App\Http\Controllers;

use App\detailsalesorder;
use App\purchasereturn;
use App\purchasepayment;
use App\salesorder;
use App\salesreturn;
use App\purchaseorder;
use App\detailpurchaseorder;
use App\supplier;
use App\customer;
use App\deliveryorder;
use App\detaildeliveryorder;
use App\product;
use App\salespayment;
use App\expense;
use App\target;
use App\warehouse;
use App\account;
use App\detailstockmovement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Excel;
use DOMPDF;
use DB;

class ReportController extends Controller
{
	public function showReportPO(Request $request){
		$po = $this->getPoQuery();

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$po = $po->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
		}
		if($request->supplier)
		{
			$po = $po->where('supplier.supplier_id', $request->supplier);
		}
		$po = $po->get();


		$supplier = supplier::all();
		if(($request->start && $request->end)||$request->supplier)
		{
			return view('report.report_po_detail', compact('po'));
		}
		return view('report.report_po', compact('po','supplier'));
	}

	public function getDetailPo(Request $request)
	{

		$detail = detailpurchaseorder::leftjoin('product','product.product_id','=','purchase_order_details.product_id')
		->where('purchase_order_id',$request->id)
		->select('purchase_order_details.*','product.product_name','product.product_code')
		->get();

		return view('report.modal-detail-po', compact('detail'));
	}

	public function showReportPoByProduct(Request $request)
	{
		$po = detailpurchaseorder::with('po');

		if($request->product){
			$product = $request->product;
			$po = $po->where('product_id',$product);
		}
		if($request->supplier)
		{
			$supplier = $request->supplier;
			$po = $po->whereHas('po', function($query) use ($supplier){
				$query->where('purchase_order.supplier_id', $supplier);
			});
		}

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
		}

		$po = $po->whereHas('po', function($query) use ($start, $end){
				$query->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
			})->get();

		$product = product::where('status',1)->get();
		$supplier = supplier::where('status',1)->get();

		if(($request->start && $request->end)||$request->product)
		{
			return view('report.report_po_by_product_detail', compact('po'));
		}
		return view('report.report_po_by_product', compact('po','product','supplier'));
	}

	public function downloadPoByProductExcel(Request $request)
	{
		$po = detailpurchaseorder::with('po');

		if($request->product){
			$product = $request->product;
			$po = $po->where('product_id',$product);
		}
		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$po = $po->whereHas('po', function($query) use ($start, $end){
				$query->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
			});
		}
		if($request->supplier)
		{
			$supplier = $request->supplier;
			$po = $po->whereHas('po', function($query) use ($supplier){
				$query->where('purchase_order.supplier_id', $supplier);
			});
		}
		$po = $po->get();

		Excel::create('Report PO Per Barang', function($excel) use($po){
			$excel->sheet('PO', function($sheet) use ($po){
				$sheet->loadView('report.po_by_product_excel',['po'=>$po]);
			});
		})->export('xlsx');

	}

	public function downloadPoExcel(Request $request, $start, $end, $supplier)
	{
		$start = date('Y-m-d', strtotime($start));
		$end = date('Y-m-d' ,strtotime($end));

		$general = purchaseorder::leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
		->leftjoin('invoice_purchase','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')
		->leftjoin('invoice_purchase_details',function ($join) {
			$join->on('invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
			->on('invoice_purchase.status','<>', DB::raw("'2'"));
		})
		->leftjoin('payment_purchase',function ($join) {
			$join->on('payment_purchase.invoice_purchase_id','=','invoice_purchase.invoice_purchase_id')
			->on('payment_purchase.status','<>', DB::raw("'2'"));
		})
		->leftjoin('payment_purchase_details','payment_purchase.payment_purchase_id','=','payment_purchase_details.payment_purchase_id')
		->selectraw("*, (CASE WHEN payment_purchase.payment_purchase_id IS NULL Then 'Not Paid' ELSE 'Paid' END) as po_status, sum(payment_purchase_details.paid) as totalpaid")
		->where('purchase_order.status','<>',2)
		->whereBetween('purchase_order.date_purchase_order',[$start, $end])
		->groupby('purchase_order.purchase_order_id');
		if($supplier != "0")
		{
			$general = $general->where('supplier.supplier_id', $request->supplier);
		}
		$general = $general->get();

		$totalpo = 0;
		$totaldiskon = 0;
		$totalpayment = 0;
		$totalreduction = 0;
		$totalprevreduction = 0;
		foreach ($general as $key => $value) {
			$totalpo += $value->grand_total_idr;
			$totaldiskon += $value->discount_nominal? $value->discount_nominal : 0;
			$totalreduction += $value->reduction? $value->reduction : 0;
			$totalprevreduction += $value->prev_invoice_reduction? $value->prev_invoice_reduction : 0;
			$totalpayment += $value->totalpaid ? $value->totalpaid : 0;
		}

		$po = purchaseorder::leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
		->leftjoin('purchase_order_details','purchase_order.purchase_order_id','=','purchase_order_details.purchase_order_id')
		->leftjoin('product','purchase_order_details.product_id','=','product.product_id')
		->where('purchase_order.status','<>',2)
		->where('purchase_order_details.status','<>',2)
		->whereBetween('purchase_order.date_purchase_order',[$start, $end]);

		if($supplier != "0")
		{
			$po = $po->where('supplier.supplier_id', $request->supplier);
		}
		$po = $po->get();

		Excel::create('Report PO', function($excel) use($general, $po, $totalpo, $totaldiskon, $totalpayment, $totalreduction, $totalprevreduction){
			$excel->sheet('General', function($sheet) use ($general, $totalpo, $totaldiskon, $totalpayment,$totalreduction, $totalprevreduction){
				$sheet->loadView('report.po_excel_general',['general'=>$general, 'totalpo'=>$totalpo, 'totaldiskon'=>$totaldiskon,'totalreduction'=>$totalreduction, 'totalpayment'=>$totalpayment, 'totalprevreduction'=>$totalprevreduction]);
			});
			$excel->sheet('PO Details', function($sheet) use($po){
				$sheet->loadView('report.po_excel_details', ['po'=>$po]);
			});
		})->export('xlsx');
	}

	public function reportPaymentPO(Request $request){
		$startdate = date('Y-m-d');
		$enddate = date('Y-m-d');

		if($request->start){
			$startdate = date('Y-m-d', strtotime($request->start));
		}
		if($request->end){
			$enddate = date('Y-m-d', strtotime($request->end));
		}

		$payment =
		purchasepayment::with('invoice.po.supplier')
		->whereBetween('date_payment_purchase',[$startdate, $enddate])
		->where('status',1)
		->get();

		if($request->start || $request->end || $request->fakturtype){
			return view('report.report_payment_po_detail', compact('payment'));
		}
		return view('report.report_payment_po', compact('payment'));
	}

	public function showReportUnsettledPaymentPO(Request $request)
	{
		$po = $this->getPoQuery();

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$po = $po->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
		}
		if($request->supplier)
		{
			$po = $po->where('supplier.supplier_id', $request->supplier);
		}
		$po = $po->get();


		$supplier = supplier::all();
		if(($request->start && $request->end)||$request->supplier)
		{
			return view('report.report_unsettled_po_detail', compact('po'));
		}
		return view('report.report_unsettled_po', compact('po','supplier'));
	}

	public function downloadUnsettledPaymentPoExcel(Request $request)
	{
		$po = $this->getPoQuery();
		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$po = $po->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
		}
		if($request->supplier)
		{
			$po = $po->where('supplier.supplier_id', $request->supplier);
		}
		$po = $po->get();

		Excel::create('Report PO', function($excel) use($po){
			$excel->sheet('General', function($sheet) use ($po){
				$sheet->loadView('report.unsettled_po_excel', compact('po'));
			});
		})->export('xlsx');
	}

	public function downloadPaymentPoExcel(Request $request)
	{
		if($request->start){
			$startdate = date('Y-m-d', strtotime($request->start));
		}
		if($request->end){
			$enddate = date('Y-m-d', strtotime($request->end));
		}

		$payment =
		purchasepayment::with(['invoice.po.supplier', 'invoice.details', 'details'])->whereHas('invoice.po', function($query) use ($startdate, $enddate){
				$query->whereBetween('date_purchase_order',[$startdate, $enddate]);
			})->where('status', '=', 1)->get();

		Excel::create('Report Payment PO', function($excel) use($payment){
			$excel->sheet('General', function($sheet) use ($payment){
				$sheet->loadView('report.payment_po_excel', compact('payment'));
			});
		})->export('xlsx');
	}

	public function showReportSo(Request $request)
	{
		$so = salesorder::with('customer')
		->with('account')
		->with('invoice.payments')
		->selectraw("sales_order.*")
		->where('sales_order.status','<>',2)
		->groupby('sales_order.sales_order_id');

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}
		if($request->customer)
		{
			$so = $so->where('customer_id', $request->customer);
		}
		if($request->account)
		{
			$so = $so->where('account_id', $request->account);
		}
		$so = $so->get();

		$customer = customer::where('status','<>',2)->get();
		$account = account::where('status','<>',2)->whereHas('roles',function($query){
			$query->where('role.role_id', '<>', 12); // Not Sales
		})->get();

		if(($request->start && $request->end)||$request->customer)
		{
			return view('report.report_so_detail', compact('so'));
		}
		return view('report.report_so', compact('so','customer','account'));
	}

	public function downloadSoExcel(Request $request, $start, $end, $customer)
	{
		$start = date('Y-m-d', strtotime($start));
		$end = date('Y-m-d' ,strtotime($end));

		$general = salesorder::leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
		->leftjoin('invoice_sales',function ($join) {
			$join->on('invoice_sales.sales_order_id','=','sales_order.sales_order_id')
			->on('invoice_sales.status','<>', DB::raw("'2'"));
		})
		->selectraw("sales_order.*, customer.*, invoice_sales.total_amount,sum(invoice_sales.total_amount) as totalinvoice, (CASE WHEN invoice_sales.invoice_sales_id IS NULL Then 'Invoiced Not Issused Yet' ELSE 'Invoice Issued' END) as so_status")
		->where('sales_order.status','<>',2)
		->groupby('sales_order.sales_order_id')
		->whereBetween('sales_order.date_sales_order',[$start, $end]);
		if($customer != 0)
		{
			$general = $general->where('sales_order.customer_id', $request->customer);
		}
		$general = $general->get();

		$totalso = 0;
		$totalinvoice = 0;

		foreach ($general as $key => $value) {
			$totalso += $value->grand_total_idr;
			$totalinvoice += $value->total_amount ? $value->total_amount : 0;
		}

		$so = salesorder::with('customer')
		->with('invoice.payments')
		->selectraw("sales_order.*")
		->where('sales_order.status','<>',2)
		->groupby('sales_order.sales_order_id');

		if($start && $end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}
		if($customer != 0)
		{
			$so = $so->where('customer_id', $request->customer);
		}
		$so = $so->get();


		Excel::create('Report SO', function($excel) use($general, $so, $totalinvoice, $totalso ){
			$excel->sheet('General', function($sheet) use ($general, $totalso, $totalinvoice){
				$sheet->loadView('report.so_excel_general',['general'=>$general, 'totalso'=>$totalso, 'totalinvoice'=>$totalinvoice]);
			});
			$excel->sheet('SO Details', function($sheet) use($so){
				$sheet->loadView('report.so_excel_details', ['so'=>$so]);
			});
		})->export('xlsx');
	}


	public function reportPaymentSO(Request $request){
		$startdate = date('Y-m-d');
		$enddate = date('Y-m-d');

		if($request->start){
			$startdate = date('Y-m-d', strtotime($request->start));
		}
		if($request->end){
			$enddate = date('Y-m-d', strtotime($request->end));
		}

		$payment =
		salespayment::with('invoice.so.customer')
		->with('all_payments_for_this_invoice')
		->where('status',1)
		->whereHas('invoice.so', function($query) use ($startdate, $enddate){
				$query->whereBetween('date_sales_order',[$startdate, $enddate]);
			})->get();


		if($request->start || $request->end || $request->fakturtype){
			return view('report.report_payment_so_detail', compact('payment'));
		}
		return view('report.report_payment_so', compact('payment'));
	}

	public function downloadPaymentSoExcel(Request $request)
	{
		if($request->start){
			$startdate = date('Y-m-d', strtotime($request->start));
		}
		if($request->end){
			$enddate = date('Y-m-d', strtotime($request->end));
		}

		$payment =
		salespayment::with('invoice.so.customer')->whereHas('invoice.so', function($query) use ($startdate, $enddate){
				$query->whereBetween('date_sales_order',[$startdate, $enddate]);
			})->get();

		Excel::create('Report Payment SO', function($excel) use($payment){
			$excel->sheet('General', function($sheet) use ($payment){
				$sheet->loadView('report.payment_so_excel', compact('payment'));
			});
		})->export('xlsx');
	}

	public function showReportUnsettledPaymentSO(Request $request)
	{
		$so = salesorder::with('customer')
		->with('invoice.payments')
		->selectraw("sales_order.*")
		->where('sales_order.status','<>',2)
		->groupby('sales_order.sales_order_id');

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}
		if($request->customer)
		{
			$so = $so->where('customer_id', $request->customer);
		}
		$so = $so->get();

		$customer = customer::where('status','<>',2)->get();

		if(($request->start && $request->end)||$request->customer)
		{
			return view('report.report_unsettled_so_detail', compact('so'));
		}
		return view('report.report_unsettled_so', compact('so','customer'));
	}

	public function downloadUnsettledPaymentSoExcel(Request $request)
	{
		$so = salesorder::with('customer')
		->with('invoice.payments')
		->selectraw("sales_order.*")
		->where('sales_order.status','<>',2)
		->groupby('sales_order.sales_order_id');

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}
		if($request->customer)
		{
			$so = $so->where('customer_id', $request->customer);
		}
		$so = $so->get();

		Excel::create('Report SO', function($excel) use($so){
			$excel->sheet('General', function($sheet) use ($so){
				$sheet->loadView('report.unsettled_so_excel', compact('so'));
			});
		})->export('xlsx');
	}

	public function showReportDo(Request $request){
		$do = deliveryorder::where('delivery_order.status','<>',2);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$do = $do->whereBetween('delivery_order.date_delivery',[$start, $end]);
		}
		if($request->product)
		{
			$product = $request->product;
			$do = $do->whereHas('details', function ($query) use ($product) {
				$query->where('product_id', $product);
			});
			$do = $do->with(['details'=> function ($query) use ($product) {
				$query->where('product_id', $product);
			}]);
		}
		$do = $do->with('draft.so')->with('draft.retur.so')->get();

		$product = product::where('status',1)->get();
		$warehouse = warehouse::where('status',1)->get();

		if(($request->start && $request->end)||$request->product)
		{
			return view('report.report_do_detail', compact('do','warehouse'));
		}
		return view('report.report_do', compact('do','product'));
	}

	public function downloadDoExcel(Request $request, $start, $end)
	{
		$do = deliveryorder::where('delivery_order.status','<>',2);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$do = $do->whereBetween('delivery_order.date_delivery',[$start, $end]);
		}
		if($request->product)
		{
			$product = $request->product;
			$do = $do->whereHas('details', function ($query) use ($product) {
				$query->where('product_id', $product);
			});
			$do = $do->with(['details'=> function ($query) use ($product) {
				$query->where('product_id', $product);
			}]);
		}
		$do = $do->with('draft.so')->with('draft.retur.so')->get();

		$totalqty = 0;
		foreach ($do as $key => $value){
			$totalqty += $value->details->first()->quantity_sent;
		}

		$product = product::where('product_id',$request->product)->first();
		$warehouse = warehouse::where('status',1)->get();
		$periode = $start.' - '.$end;

		Excel::create('Report DO', function($excel) use($do, $totalqty, $warehouse, $product, $periode){
			$excel->sheet('General', function($sheet) use ($do, $totalqty, $warehouse, $product, $periode){
				$sheet->loadView('report.do_excel',['do'=>$do, 'totalqty'=>$totalqty, 'warehouse'=>$warehouse, 'product'=>$product, 'periode'=>$periode]);
			});
		})->export('xlsx');
	}

	public function showReportDoRemaining(Request $request){

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
		}

		$so = detailsalesorder::
					with(['send_sum','so.customer','product'])
					->whereHas('so',function($query) use ($start, $end){
						$query->whereBetween('date_sales_order', [$start, $end]);
					})->get();

		if($request->start && $request->end)
		{
			return view('report.report_remaining_do_detail', compact('so'));
		}
		return view('report.report_do_remaining', compact('so'));
	}

	public function showReportDoDaily(Request $request){

		$do = detaildeliveryorder::whereHas('delivery', function($query){
			$query->where('status','<>',2);
		});

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d', strtotime($request->end));
			$do = $do->whereHas('delivery', function($query) use($start, $end){
				$query->whereBetween('delivery_order.date_delivery',[$start, $end]);
			});
		}
		if($request->warehouse != 0)
		{
			$warehouse = $request->warehouse;
			$do = $do->whereHas('delivery', function ($query) use ($warehouse) {
				$query->where('warehouse_id', $warehouse);
			});
			$do = $do->with(['delivery'=> function ($query) use ($warehouse) {
				$query->where('warehouse_id', $warehouse);
			}]);
		}

		$do = $do->with('delivery.draft.so')
		->with('delivery.draft.retur.so')
		->with('product')
		->get();

		$warehouse = warehouse::where('status',1)->get();

		if($request->start||$request->warehouse)
		{
			return view('report.report_do_daily_detail', compact('do','warehouse'));
		}
		return view('report.report_do_daily', compact('do','warehouse'));
	}

	public function showGrafikPenjualan(){
		return view('so.grafik_penjualan');
	}


	public function getGrafikData($startdate, $enddate){
		$data = salesorder::leftjoin('sales_order_details','sales_order_details.sales_order_id','=','sales_order.sales_order_id')
		->leftjoin('product','product.product_id','=','sales_order_details.product_id')
		->selectraw("product.product_name, sum(sales_order_details.sub_total) as product_sales")
		->whereBetween('sales_order.date_sales_order',[$startdate,$enddate])
		->groupby('product.product_id')
		->where('sales_order.status',1)
		->where('sales_order_details.status',1)
		->get();
		return $data;
	}

	public function showGrafikTarget(){
		return view('home.grafik_target');
	}

	public function getGrafikTarget(){
		$data = target::with('supplier')
		->with('product')
		->where('status',1)
		->whereRaw("Now() between date_start AND date_end")
		->get();


		foreach($data as $key =>$value)
		{
			$value['pembelian'] = detailpurchaseorder::where('product_id',$value->product_id)
			->whereHas('po',function($query) use ($value){
				$query->where('supplier_id',$value->supplier_id);
			})->sum('total_price');
		}
		return $data;
	}

	public function showReportIncome(Request $request){
		$payment = salespayment::leftjoin('customer','payment_sales.customer_id','=','customer.customer_id')
		->leftjoin('bank','payment_sales.bank_id','=','bank.bank_id')
		->where('payment_sales.status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$payment = $payment->whereBetween('payment_sales.date_payment_sales',[$start, $end]);
		}
		if($request->paymentmethod)
		{
			if($request->paymentmethod == 1)
			{
				$payment = $payment->where('payment_methods',1);
			}else
			{
				$payment = $payment->where('payment_methods',2);
			}
		}

		$payment = $payment->get();

		if(($request->start && $request->end)||$request->paymentmethod)
		{
			return view('report.report_income_detail', compact('payment'));
		}
		return view('report.report_income', compact('payment'));
	}

	public function showReportExpense(Request $request){
		$expense = expense::where('status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$expense = $expense->whereBetween('expense.date_expense',[$start, $end]);
		}

		$expense = $expense->get();

		if(($request->start && $request->end)||$request->paymentmethod)
		{
			return view('report.report_expense_detail', compact('expense'));
		}
		return view('report.report_expense', compact('expense'));
	}

	public function showReportFaktur(Request $request){
		$so = salesorder::with('customer')
		->where('status',1);

		$fakturtype = $request->fakturtype;

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}

		if($request->fakturtype == 1)
		{
			$so = $so->has('details_faktur')->with('details_faktur');
		}
		else if($request->fakturtype == 2)
		{
			$so = $so->has('details_non_faktur')->with('details_non_faktur');
		}
		$so = $so->get();

		if(($request->start && $request->end)||$request->fakturtype)
		{
			return view('report.report_faktur_detail', compact('so','fakturtype'));
		}
		return view('report.report_faktur', compact('so'));
	}

	public function printReportFaktur(Request $request){

		$fakturtype = $request->fakturtype;
		$datatype = $request->datatype;

		$so = salesorder::with('customer')
		->where('status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$so = $so->whereBetween('sales_order.date_sales_order',[$start, $end]);
		}
		if($request->fakturtype == 1)
		{
			$so = $so->has('details_faktur')->with('details_faktur');
		}else if($request->fakturtype == 2)
		{
			$so = $so->has('details_non_faktur')->with('details_non_faktur');
		}
		$so = $so->get();

        // return view('report.report_faktur_pdf', compact('so','fakturtype'));
        // dd($so);

//          dd($so);

		if($datatype == 'so')
		{
			$pdf = DOMPDF::loadView('report.report_faktur_pdf', compact('so','fakturtype'));
			return $pdf->stream('SO.pdf');
		}else{
			$pdf = DOMPDF::loadView('report.report_faktur_product_only_pdf', compact('so','fakturtype'));
			return $pdf->stream('SO.pdf');
		}
	}

	public function showReportNetSales(Request $request){
		$sales =
		salesorder::where('status',1)
		->with('totalcogs')
		->with('customer');

		$retur =
		salesreturn::with('so.customer')
		->where('status',1)
		->whereIn('retur_type_id',[1,2,7]);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$sales = $sales->whereBetween('sales_order.date_sales_order',[$start, $end]);
			$retur = $retur->whereBetween('sales_retur.date_retur',[$start, $end]);
		}
		$sales = $sales->get();
		$retur = $retur->get();

		if(($request->start && $request->end))
		{
			return view('report.report_net_sales_detail', compact('sales','retur'));
		}
		return view('report.report_net_sales', compact('sales','retur'));
	}

	public function showReportProfitLoss(Request $request){
		$sales = salesorder::leftjoin(DB::raw("(
			select sales_order_id, sum(cogs)as totalcogs from sales_order_details where sales_order_details.status = 1 group by sales_order_id )tablea") ,'tablea.sales_order_id', 'sales_order.sales_order_id');

		$retur = salesreturn::leftjoin(DB::raw("(
			select sales_retur_id, sum(cogs)as totalcogs from sales_retur_details where sales_retur_details.status = 1 group by sales_retur_id) tablea") ,'tablea.sales_retur_id', 'sales_retur.sales_retur_id');

		$expense = expense::where('status',1);
		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$sales = $sales->whereBetween('sales_order.date_sales_order',[$start, $end]);
			$expense = $expense->whereBetween('expense.date_expense',[$start, $end]);
		}
		$sales = $sales
		->select('sales_order.*','tablea.totalcogs')
		->where('status',1)
		->get();

		$data['retur'] = $retur
		->whereIn('sales_retur.retur_type_id',[1,2,7])
		->select('sales_retur.*','tablea.totalcogs')
		->where('status',1)
		->get();

		$data['totalsales'] = $sales->sum('grand_total_idr');
		$data['totalcogs'] = $sales->sum('totalcogs');
		$data['expense'] = $expense->get();

		if(($request->start && $request->end))
		{
			return view('report.report_profit_loss_detail', compact('data'));
		}
		return view('report.report_profit_loss', compact('data'));
	}

	public function printReportNetSales(Request $request){

		$sales =
		salesorder::where('status',1)
		->with('totalcogs')
		->with('customer');

		$retur =
		salesreturn::with('so.customer')
		->where('status',1)
		->whereIn('retur_type_id',[1,2,7]);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$sales = $sales->whereBetween('sales_order.date_sales_order',[$start, $end]);
			$retur = $retur->whereBetween('sales_retur.date_retur',[$start, $end]);
		}
		$sales = $sales->get();
		$retur = $retur->get();

		$totalsales = $sales->sum('grand_total_idr');
		$totalsalescogs = $sales->sum('totalcogs.totalcogs');
		$totalpenjualanbersih = $totalsales - $totalsalescogs;

		$totalretur = $retur->sum('total_price');
		$totalreturcogs = $retur->sum('totalcogs.totalcogs');
		$totalreturbersih = $totalretur - $totalreturcogs;
        // return view('report.report_faktur_pdf', compact('so','fakturtype'));
        // dd($so);
		$periode = $request->start . ' s/d ' . $request->end;
		$pdf = DOMPDF::loadView('report.report_net_sales_pdf', compact('sales','retur','totalpenjualanbersih','totalreturbersih','periode'));
		return $pdf->stream('Report Pendapatan Bersih.pdf');
	}

	public function printReportProfitLoss(Request $request){

		$sales =
		salesorder::leftjoin(DB::raw("(
			select sales_order_id, sum(cogs)as totalcogs from sales_order_details group by sales_order_id
		)tablea") ,'tablea.sales_order_id', 'sales_order.sales_order_id');

		$retur =
		salesreturn::leftjoin(DB::raw("(
			select sales_retur_id, sum(cogs)as totalcogs from sales_retur_details group by sales_retur_id
		)tablea") ,'tablea.sales_retur_id', 'sales_retur.sales_retur_id');

		$expense = expense::where('status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$sales = $sales->whereBetween('sales_order.date_sales_order',[$start, $end]);
			$expense = $expense->whereBetween('expense.date_expense',[$start, $end]);
		}
		$sales = $sales
		->select('sales_order.*','tablea.totalcogs')
		->where('status',1)
		->get();

		$data['retur'] = $retur
		->whereIn('sales_retur.retur_type_id',[1,2,7])
		->select('sales_retur.*','tablea.totalcogs')
		->where('status',1)
		->get();

		$data['totalsales'] = $sales->sum('grand_total_idr');
		$data['totalcogs'] = $sales->sum('totalcogs');
		$data['expense'] = $expense->get();

		$data['periode'] = $request->start . ' s/d ' . $request->end;
		$pdf = DOMPDF::loadView('report.report_profit_loss_pdf', compact('data'));
		return $pdf->stream('Report Laba Rugi.pdf');
	}

	public function downloadExpenseExcel(Request $request, $start, $end)
	{
		$start = date('Y-m-d', strtotime($start));
		$end = date('Y-m-d' ,strtotime($end));

		$expense =
		expense::where('status',1)
		->whereBetween('expense.date_expense',[$start, $end])
		->get();

		$periode = $start . ' sd ' . $end;
		Excel::create('Report Expense', function($excel) use($expense, $periode){
			$excel->sheet('Report Expense ', function($sheet) use ($expense, $periode){
				$sheet->loadView('report.expense_excel',['expense'=>$expense, 'periode' => $periode]);
			});
		})->export('xlsx');
	}

	public function reportFakturPO(Request $request){
		$startdate = date('Y-m-d');
		$enddate = date('Y-m-d');

		if($request->start){
			$startdate = date('Y-m-d', strtotime($request->start));
		}
		if($request->end){
			$enddate = date('Y-m-d', strtotime($request->end));
		}

		$powithoutinvoice =
		purchaseorder::doesntHave('invoice')
		->whereBetween('date_purchase_order',[$startdate, $enddate])
		->where('status',1);

		$po =
		purchaseorder::with('supplier')
		->with('invoice.payment')
		->whereBetween('date_purchase_order',[$startdate, $enddate])
		->where('status',1);

		if($request->fakturtype){
			if($request->fakturtype == 1){
				$po = $po->where('is_pkp', 1);
				$powithoutinvoice = $powithoutinvoice->where('is_pkp', 1);
			}else if ($request->fakturtype == 2){
				$po = $po->where('is_pkp', 0);
				$powithoutinvoice = $powithoutinvoice->where('is_pkp', 0);
			}
		}
		$po = $po->get();
		$powithoutinvoice = $powithoutinvoice->get();


		if($request->start || $request->end || $request->fakturtype){
			return view('report.report_faktur_po_detail', compact('po','powithoutinvoice'));
		}
		return view('report.report_faktur_po', compact('po'));
	}

	public function reportPaymentFakturPO(Request $request){
		$startdate = date('Y-m-d');
		$enddate = date('Y-m-d');

		if($request->start){
			$startdate = date('Y-m-d', strtotime($request->start));
		}
		if($request->end){
			$enddate = date('Y-m-d', strtotime($request->end));
		}

		$payment =
		purchasepayment::with('invoice.po.supplier')->whereHas('invoice.po', function($query) use ($request, $startdate, $enddate){
			if($request->fakturtype == 1){
				$query->where('is_pkp', 1);
			}else if ($request->fakturtype == 2){
				$query->where('is_pkp', 0);
			}
		})
		->where('status',1)
		->whereBetween('date_payment_purchase',[$startdate, $enddate])
		->get();

		if($request->start || $request->end || $request->fakturtype){
			return view('report.report_payment_faktur_po_detail', compact('payment'));
		}
		return view('report.report_payment_faktur_po', compact('payment'));
	}

	public function downloadPaymentFakturPoExcel(Request $request)
	{
		if($request->start){
			$startdate = date('Y-m-d', strtotime($request->start));
		}
		if($request->end){
			$enddate = date('Y-m-d', strtotime($request->end));
		}

		$payment =
		purchasepayment::with('invoice.po.supplier')->whereHas('invoice.po', function($query) use ($request, $startdate, $enddate){
			if($request->fakturtype == 1){
				$query->whereBetween('date_purchase_order',[$startdate, $enddate])
				->where('is_pkp', 1);
			}else if ($request->fakturtype == 2){
				$query->whereBetween('date_purchase_order',[$startdate, $enddate])
				->where('is_pkp', 0);
			}
		})->get();

		Excel::create('Report Payment Faktur PO', function($excel) use($payment){
			$excel->sheet('General', function($sheet) use ($payment){
				$sheet->loadView('report.payment_faktur_po_excel', compact('payment'));
			});
		})->export('xlsx');
	}

	public function printReportFakturPO(Request $request){
		$fakturtype = $request->fakturtype;
		$po = purchaseorder::with('supplier')
		->with('detail')
		->with('paymenttype')
		->where('status',1);

		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
			$po = $po->whereBetween('purchase_order.date_purchase_order',[$start, $end]);
		}

		if($request->fakturtype == 1)
		{
			$po = $po->where('is_pkp',1);
		}else if($request->fakturtype == 2)
		{
			$po = $po->where('is_pkp',0);
		}
		$po = $po->get();

        // dd($po);
		$pdf = DOMPDF::loadView('report.report_faktur_po_pdf', compact('po','fakturtype'));
		return $pdf->stream('PO.pdf');
	}

	public function showReportCustomerPurchase(){
		$customer = customer::where('status','<>',2)->get();
		return view('report.report_customer_purchase', compact('customer'));
	}

	public function getReportCustomerPurchase(Request $request){
		$idbarang = explode(',',$request->idbarang);

		$detailso = detailsalesorder::whereHas('so',function($query) use($request){
			$query->where('customer_id',$request->customer)
			->whereDate('date_sales_order','>=',Carbon::createFromFormat('d-m-Y', $request->start)->toDateString())
			->whereDate('date_sales_order','<=',Carbon::createFromFormat('d-m-Y', $request->end)->toDateString());
		})
		->where('status',1)
		->whereIn('product_id',$idbarang)
		->get();

		return view('report.table_customer_purchase',compact('detailso','idbarang'));
	}

	public function showReportRemainingParangloe(){
		$remaining =
			detailpurchaseorder::with('detailgoodreceive')
				->with(['po','product','detailsm'])
				->whereHas('po', function($query){
					$query->whereDate('date_purchase_order','>','2019-01-01');
				})
				->whereHas('detailgoodreceive')
				->get();
		return view('report.report_remaining_parangloe', compact('remaining'));
	}

	public function printReportRemainingPArangloe(){
		$remaining = detailpurchaseorder::with('detailgoodreceive')
		->with('po')
		->with('product')
		->with('detailsm')
		->whereHas('po')
		->whereHas('detailgoodreceive')
		->get();

		$pdf = DOMPDF::loadView('report.report_remaining_parangloe_pdf', compact('remaining'));
		return $pdf->stream('Sisa.pdf');
	}

	public function showReportStockMovement(Request $request){
		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
		}

		$stockmovement = detailstockmovement::with('stock_movement.warehouse_from_data')
							->with('stock_movement.warehouse_to_data')
							->with('product')
							->where('status',1)
							->whereHas('stock_movement', function($query) use($start , $end){
								$query->whereBetween('stock_movement.date_stock_movement',[$start, $end]);
							})
							->get();

		if($request->start && $request->end)
		{
			return view('report.report_stock_movement_detail', compact('stockmovement'));
		}

		return view('report.report_stock_movement', compact('stockmovement'));
	}

	public function downloadStockMovementExcel(Request $request){
		if($request->start && $request->end)
		{
			$start = date('Y-m-d', strtotime($request->start));
			$end = date('Y-m-d' ,strtotime($request->end));
		}else{
			$start = Carbon::now()->startOfMonth()->toDateString();
			$end = Carbon::now()->endOfMonth()->toDateString();
		}

		$stockmovement = detailstockmovement::with('stock_movement.warehouse_from_data')
							->with('stock_movement.warehouse_to_data')
							->with('product')
							->where('status',1)
							->whereHas('stock_movement', function($query) use($start , $end){
								$query->whereBetween('stock_movement.date_stock_movement',[$start, $end]);
							})
							->get();

		Excel::create('Report Stock Trasnfer', function($excel) use($stockmovement){
			$excel->sheet('PO', function($sheet) use ($stockmovement){
				$sheet->loadView('report.stock_movement_excel',['stockmovement'=>$stockmovement]);
			});
		})->export('xlsx');
	}

	// Reused QUERY //

	public function getPoQuery()
	{
		$po = purchaseorder::leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
		->leftjoin('invoice_purchase','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')
		->leftjoin('invoice_purchase_details',function ($join) {
			$join->on('invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
			->on('invoice_purchase.status','<>', DB::raw("'2'"));
		})
		->leftjoin('payment_purchase',function ($join) {
			$join->on('payment_purchase.invoice_purchase_id','=','invoice_purchase.invoice_purchase_id')
			->on('payment_purchase.status','<>', DB::raw("'2'"));
		})
		->leftjoin('payment_purchase_details','payment_purchase.payment_purchase_id','=','payment_purchase_details.payment_purchase_id')
		->selectraw("*, (CASE WHEN payment_purchase.payment_purchase_id IS NULL Then 'Not Paid' ELSE 'Paid' END) as po_status, sum(payment_purchase_details.paid) as totalpaid")
		->where('purchase_order.status','<>',2)
		->groupby('purchase_order.purchase_order_id');

		return $po;
	}
}