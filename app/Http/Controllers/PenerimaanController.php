<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\purchaseorder;
use App\purchasereturn;
use App\productwarehouse;
use App\goodreceive;
use App\pricepurchase;
use App\detailgoodreceive;
use App\detailpurchaseorder;
use App\warehouse;
use App\log;
use App;
use DB;
use DataTable;
use DOMPDF;

class PenerimaanController extends Controller
{
	public function showPenerimaanBarangPage(){
		$data['po'] = purchaseorder::leftjoin(DB::raw('(
            SELECT good_receive.purchase_order_id, sum(good_receive_details.quantity_received) as total_receive 
            FROM good_receive 
            Right JOIN good_receive_details on good_receive.good_receive_id = good_receive_details.good_receive_id 
            WHERE good_receive.status <> 2 AND good_receive_details.status <> 2
            GROUP by good_receive.purchase_order_id) tablea')
			, 'tablea.purchase_order_id','=','purchase_order.purchase_order_id')
			->leftjoin(DB::raw('(
                SELECT purchase_order.purchase_order_id, sum(quantity)as qty, sum(free_qty) as free_qty
                FROM purchase_order 
                join purchase_order_details on purchase_order.purchase_order_id = purchase_order_details.purchase_order_id and purchase_order_details.status <> 2
                Where purchase_order.status <> 2
                GROUP by purchase_order.purchase_order_id)tableb')
				,'tableb.purchase_order_id','=','purchase_order.purchase_order_id')
			->leftjoin(DB::raw('(
                SELECT purchase_retur.purchase_order_id, purchase_retur.purchase_retur_id, sum(qty) as retur_qty
                FROM purchase_retur
                join purchase_retur_details on purchase_retur.purchase_retur_id = purchase_retur_details.purchase_retur_id And purchase_retur_details.status <> 2
                Where purchase_retur.retur_type_id = 4 AND purchase_retur.status <> 2
                GROUP by purchase_retur.purchase_order_id)tablec')
				,'tablec.purchase_order_id','=','purchase_order.purchase_order_id')
			->selectRaw('purchase_order.purchase_order_id, supplier_id, tablea.total_receive , purchase_order.purchase_order_number, tableb.qty, tableb.free_qty, tablec.retur_qty' )
			->where('purchase_order.status',"<>",2)
			->havingRaw('total_receive IS NULL Or total_receive < qty + free_qty + IFNULL(retur_qty, 0)')
			->get();

		$data['warehouse'] = warehouse::where('status',"<>",2)->get();

		return view('penerimaan_barang.penerimaan_barang', compact('data'));
	}

	public function getPenerimaanTable(Request $request){
		$bpb = goodreceive::leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('good_receive.status','<>',2)
			->orderby('good_receive.good_receive_id','desc');

		if ($request->nobpb && $request->nobpb != "")
		{
			$bpb= $bpb->where('good_receive_code','like','%'.$request->nobpb.'%');
		}
		if ($request->nopo && $request->nopo != "")
		{
			$bpb= $bpb->where('purchase_order_number','like','%'.$request->nopo.'%');
		}
		if($request->supplier && $request->supplier != "")
		{
			$bpb = $bpb->whereRaw("company_name like '%$request->supplier%'");
		}
		$bpb = $bpb->get();

		return DataTable::of($bpb)
			->setRowAttr([
				'value' => function($bpb) {
					return $bpb->good_receive_id;
				},
			])
			->addColumn('action', function ($bpb){
				return
					'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$bpb->good_receive_id.'"><i class="material-icons">edit</i></a>
						<a class="btn btn-sm btn-raised orange print-bpb" target="_blank" href="downloadpenerimaanbarang/'.$bpb->good_receive_id.'"><i class="material-icons">print</i></a>
						<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
			})
			->smart(false)
			->make(true);
	}

	public function getLastBpbNumber(Request $request)
	{
		$lastbpbtoday = goodreceive::where('good_receive_code','like','GR/'.date('dmy').'/%')->orderby('good_receive_id','desc')->first();
		if(empty($lastbpbtoday))
		{
			$newbpbnumber = "GR/".date('dmy')."/1";
			return $newbpbnumber;
		}
		else{
			$tmpbpb = explode('/',$lastbpbtoday->good_receive_code);
			$lastnumber = $tmpbpb[2];
			$newbpbnumber = "GR/".date('dmy')."/".($lastnumber+1);
			return $newbpbnumber;
		}
	}

	public function getPenerimaan(Request $request)
	{
		$po = goodreceive::where('good_receive_id',$request->id)->first()->purchase_order_id;
		$bpb = goodreceive::leftjoin('good_receive_details','good_receive.good_receive_id','=','good_receive_details.good_receive_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->leftjoin('product','product.product_id','=','good_receive_details.product_id')
			->leftjoin('purchase_order_details', function ($join) {
				$join->on('purchase_order_details.purchase_order_id', '=', 'purchase_order.purchase_order_id')->on('purchase_order_details.purchase_order_details_id','=','good_receive_details.purchase_order_details_id');
			})
			->leftjoin(DB::raw('(SELECT SUM(quantity_received) as total_receive, product_id, good_receive.good_receive_id, good_receive.purchase_order_id, good_receive_details.purchase_order_details_id 
            FROM good_receive 
            LEFT JOIN good_receive_details on good_receive.good_receive_id = good_receive_details.good_receive_id 
            WHERE good_receive.purchase_order_id = '.$po.' AND good_receive.status <> 2 
            AND good_receive_details.status <> 2 
            GROUP BY purchase_order_details_id) tablea')
				,function($join){
					$join->on('tablea.purchase_order_id', '=', 'good_receive.purchase_order_id')
						->on('tablea.purchase_order_details_id','=','purchase_order_details.purchase_order_details_id');
				})
			->leftjoin(DB::raw('(SELECT SUM(qty) as total_retur, product_id, purchase_retur.purchase_retur_id, purchase_retur.purchase_order_id ,purchase_retur_details.purchase_order_details_id
            FROM purchase_retur
            LEFT JOIN purchase_retur_details on purchase_retur.purchase_retur_id = purchase_retur_details.purchase_retur_id 
            WHERE purchase_retur.retur_type_id = 4 AND purchase_retur.purchase_order_id = '.$po.' AND purchase_retur.status <> 2 
            AND purchase_retur_details.status <> 2 
            GROUP BY purchase_order_details_id) tabler')
				,function($join){
					$join->on('tabler.purchase_order_id', '=', 'purchase_order.purchase_order_id')
						->on('tabler.purchase_order_details_id','=','purchase_order_details.purchase_order_details_id');
				})
			->select('good_receive.*', 'purchase_order.*','good_receive_details.good_receive_details_id','good_receive_details.quantity_received','good_receive_details.warehouse_id','purchase_order_details.free_qty', 'supplier.company_name', 'product.product_name', 'product.product_id', 'purchase_order_details.quantity', 'tablea.total_receive', 'tabler.total_retur','purchase_order_details.purchase_order_details_id')
			->where('good_receive.status','<>',2)
			->where('good_receive_details.status','<>',2)
			->where('good_receive.good_receive_id', $request->id)
			->get();

		$historygr = goodreceive::where('good_receive.purchase_order_id',$po)
			->where('good_receive.status','<>',2)
			->with('details.product','details.warehouse')
			->get();

		$history = View('penerimaan_barang.penerimaan_history',compact('historygr'))->render();

		return compact('bpb','history');
	}

	public function getPurchaseOrderData(Request $request)
	{
		if($request->id != '0')
		{
			$id = $request->id;
			$podata = purchaseorder::join('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
				->leftjoin('good_receive','good_receive.purchase_order_id','=','purchase_order.purchase_order_id')
				->where('purchase_order.purchase_order_id',$id)
				->select('purchase_order.*','supplier.company_name as s_name','good_receive.good_receive_id as g_id')
				->first();

			$podetail = detailpurchaseorder::join('product','product.product_id','purchase_order_details.product_id')
				->where('purchase_order_id',$podata->purchase_order_id)
				->orderBy('purchase_order_details.purchase_order_details_id')
				->select('purchase_order_details.*','product.product_name as p_name')
				->get();

			$grdetail = collect();
			foreach ($podetail as $key => $value)
			{
				$sum = goodreceive::leftjoin('good_receive_details as detailgr','detailgr.good_receive_id','=','good_receive.good_receive_id')
					->where('detailgr.purchase_order_details_id',$value->purchase_order_details_id)
					->selectraw('sum(quantity_received) as sum')
					->where('detailgr.status','<>',2)
					->groupBy('product_id', 'purchase_order_details_id')
					->first();

				empty($sum) ? $grdetail->push(collect(['sum'=>0])) : $grdetail->push($sum);
			}


			$returdetail = purchasereturn::leftjoin('purchase_retur_details','purchase_retur_details.purchase_retur_id','=','purchase_retur.purchase_retur_id')
				->where('purchase_retur.status',"<>",2)
				->where('purchase_retur_details.status','<>',2)
				->where('purchase_retur.retur_type_id',4) // show if the type is return products
				->where('purchase_retur.purchase_order_id',$podata->purchase_order_id)
				->selectraw('sum(qty) as sum')
				->orderBy('purchase_order_details_id')
				->groupby('product_id','purchase_order_details_id')
				->get();

			$historygr = goodreceive::where('good_receive.purchase_order_id',$podata->purchase_order_id)
				->where('good_receive.status','<>',2)
				->with('details.product','details.warehouse')
				->get();

			$history = View('penerimaan_barang.penerimaan_history',compact('historygr'))->render();

			return compact('podata', 'podetail', 'grdetail','returdetail','historygr','history');
		}
	}

	public function createPenerimaanBarang(Request $request)
	{
		$goodreceive = new goodreceive;
		$goodreceive->purchase_order_id = $request->po;
		$goodreceive->good_receive_code = $request->donumber;
		$goodreceive->good_receive_date  = date('Y-m-d',strtotime($request->tglbpb));
		$goodreceive->reference_number = "";
		$goodreceive->note = $request->notes;
		$goodreceive->status = 1;
		$goodreceive->save();

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			if($request->earnedqty[$i] > 0)
			{
				$detailgoodreceive = new detailgoodreceive;
				$detailgoodreceive->good_receive_id = $goodreceive->good_receive_id;
				$detailgoodreceive->product_id = $request->idbarang[$i];
				$detailgoodreceive->quantity_received = $request->earnedqty[$i];
				$detailgoodreceive->warehouse_id = $request->gudang[$i];
				$detailgoodreceive->purchase_order_details_id = $request->detailpo[$i];
				$detailgoodreceive->status = 1;
				$detailgoodreceive->save();

				productwarehouse::where('product_id',$request->idbarang[$i])->where('warehouse_id',$request->gudang[$i])->update([
					'quantity' => DB::raw('quantity+'.$request->earnedqty[$i]),
				]);

				$saldo_awal = pricepurchase::where('product_id',$request->idbarang[$i])->first()->balance;
				$quantity_awal = pricepurchase::where('product_id',$request->idbarang[$i])->first()->last_quantity;
				$harga_item = purchaseorder::join('purchase_order_details','purchase_order_details.purchase_order_id','=','purchase_order.purchase_order_id')->where('purchase_order.purchase_order_id', $request->po)->where('product_id',$request->idbarang[$i])->first()-> price;
				$qty_penerimaan = $request->earnedqty[$i];
				$harga_penerimaan = $harga_item * $qty_penerimaan;
				$quantity_akhir = $quantity_awal + $qty_penerimaan;
				$balance_akhir = $saldo_awal + $harga_penerimaan;
				$hpp = $balance_akhir/($quantity_akhir);

				pricepurchase::where('product_id', $request->idbarang[$i])->update([
					'balance' => $balance_akhir,
					'last_quantity' => $quantity_akhir,
					'hpp' => $hpp
				]);
			}
		}
		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "menambahkan";
		$log->object = "bukti penerimaan barang";
		$log->object_details = $goodreceive->good_receive_code;
		$log->status = 1;
		$log->save();
	}

	public function updatepenerimaanbarang(Request $request){
		$goodreceive = goodreceive::find($request->id);
		$goodreceive->good_receive_date  = date('Y-m-d',strtotime($request->tglbpb));
		$goodreceive->save();

		for ($i=0; $i <sizeof($request->detailbpb); $i++)
		{
			$detail = detailgoodreceive::where('good_receive_details_id',$request->detailbpb[$i])->first();
			$warehouse = $detail->warehouse_id;
			$product = $detail->product_id;
			$qty = $detail->quantity_received;

			productwarehouse::where('product_id',$product)->where('warehouse_id',$warehouse)->update([
				'quantity' => DB::raw('quantity-'.$qty),
			]);

			$saldo_awal = pricepurchase::where('product_id',$product)->first()->balance;
			$quantity_awal = pricepurchase::where('product_id',$product)->first()->last_quantity;
			$harga_item = purchaseorder::join('purchase_order_details','purchase_order_details.purchase_order_id','=','purchase_order.purchase_order_id')->where('purchase_order.purchase_order_id', $request->po)->where('product_id',$product)->first()->price;
			$qty_penerimaan = $qty;
			$harga_penerimaan = $harga_item * $qty;
			$quantity_akhir = $quantity_awal - $qty;
			$balance_akhir = $saldo_awal - $harga_penerimaan;

			if($balance_akhir == 0 && $quantity_akhir == 0)
			{
				$hpp = 0;
			}else{
				$hpp = $balance_akhir/($quantity_akhir);
			}

			pricepurchase::where('product_id', $product)->update([
				'balance' => $balance_akhir,
				'last_quantity' => $quantity_akhir,
				'hpp' => $hpp
			]);
		}

		detailgoodreceive::where('good_receive_id',$request->id)->update(['status'=>2]);

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			if($request->earnedqty[$i] > 0)
			{
				$detailgoodreceive = new detailgoodreceive;
				$detailgoodreceive->good_receive_id = $goodreceive->good_receive_id;
				$detailgoodreceive->product_id = $request->idbarang[$i];
				$detailgoodreceive->quantity_received = $request->earnedqty[$i];
				$detailgoodreceive->warehouse_id = $request->gudang[$i];
				$detailgoodreceive->purchase_order_details_id = $request->detailpo[$i];
				$detailgoodreceive->status = 1;
				$detailgoodreceive->save();

				productwarehouse::where('product_id',$request->idbarang[$i])->where('warehouse_id',$request->gudang[$i])->update([
					'quantity' => DB::raw('quantity+'.$request->earnedqty[$i]),
				]);

				$saldo_awal = pricepurchase::where('product_id',$request->idbarang[$i])->first()->balance;
				$quantity_awal = pricepurchase::where('product_id',$request->idbarang[$i])->first()->last_quantity;
				$harga_item = purchaseorder::join('purchase_order_details','purchase_order_details.purchase_order_id','=','purchase_order.purchase_order_id')->where('purchase_order.purchase_order_id', $request->po)->where('product_id',$request->idbarang[$i])->first()->price;

				$qty_penerimaan = $request->earnedqty[$i];
				$harga_penerimaan = $harga_item * $qty_penerimaan;
				$quantity_akhir = $quantity_awal + $qty_penerimaan;
				$balance_akhir = $saldo_awal + $harga_penerimaan;
				$hpp = $balance_akhir/($quantity_akhir);

				pricepurchase::where('product_id', $request->idbarang[$i])->update([
					'balance' => $balance_akhir,
					'last_quantity' => $quantity_akhir,
					'hpp' => $hpp
				]);

			}
		}
	}

	public function deletePenerimaanBarang(Request $request)
	{
		$detail = goodreceive::leftjoin('good_receive_details','good_receive.good_receive_id','=','good_receive_details.good_receive_id')->where('good_receive.good_receive_id',$request->id)->where('good_receive_details.status','<>',2)->get();
		foreach ($detail as $key => $value) {
			$warehouse = $value->warehouse_id;
			$product = $value->product_id;
			$qty = $value->quantity_received;
			$po = $value->purchase_order_id;


			productwarehouse::where('product_id',$product)->where('warehouse_id',$warehouse)->update([
				'quantity' => DB::raw('quantity-'.$qty),
			]);

			$saldo_awal = pricepurchase::where('product_id',$product)->first()->balance;
			$quantity_awal = pricepurchase::where('product_id',$product)->first()->last_quantity;
			$harga_item = purchaseorder::join('purchase_order_details','purchase_order_details.purchase_order_id','=','purchase_order.purchase_order_id')->where('purchase_order.purchase_order_id', $po)->where('product_id',$product)->first()->price;
			$qty_penerimaan = $qty;
			$harga_penerimaan = $harga_item * $qty;
			$quantity_akhir = $quantity_awal - $qty;
			$balance_akhir = $saldo_awal - $harga_penerimaan;

			if($balance_akhir == 0 && $quantity_akhir == 0)
			{
				$hpp = 0;
			}else{
				$hpp = $balance_akhir/($quantity_akhir);
			}

			pricepurchase::where('product_id', $product)->update([
				'balance' => $balance_akhir,
				'last_quantity' => $quantity_akhir,
				'hpp' => $hpp
			]);
		}
		goodreceive::where('good_receive_id', $request->id)->update(["status"=>2]);
		detailgoodreceive::where('good_receive_id',$request->id)->update(["status"=>2]);
	}

	public function downloadPenerimaanBarang(Request $request, $id)
	{
		$grheader = goodreceive::join('purchase_order','purchase_order.purchase_order_id','=','good_receive.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('good_receive.good_receive_id',$id)
			->first();

		$grdetail = goodreceive::join('good_receive_details','good_receive_details.good_receive_id','=','good_receive.good_receive_id')
			->join('product','good_receive_details.product_id','=','product.product_id')
			->leftjoin('unit','unit.unit_id','=','product.unit_id')
			->leftjoin('warehouse','good_receive_details.warehouse_id','=','warehouse.warehouse_id')
			->select('good_receive_details.*','product.product_code','product.product_name','unit.unit_description','warehouse.warehouse_name')
			->where('good_receive_details.good_receive_id', $id)
			->where('good_receive_details.status', 1)
			->get();

		$pdf = DOMPDF::loadView('penerimaan_barang.penerimaan_pdf', compact('grheader','grdetail'));
		return $pdf->stream('DO.pdf');
	}
}
