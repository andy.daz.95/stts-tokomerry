<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Indonesia;
use App\warehouse;
use App\account;

class GudangController extends Controller
{
    public function showGudangPage()
    {
    	$provinsi = Indonesia::allProvinces();
    	$gudang = warehouse::leftjoin('indonesia_cities as ic', 'ic.id','=','warehouse.city_id')->leftjoin('indonesia_provinces as ip','ip.id','=','ic.province_id')->select('warehouse.*','ip.name as p_name')->paginate(10);
    	$account = account::where(function($query){
				$query->has('gudang')
					->orHas('master');
			})->get();
    	return view('gudang.gudang', compact('provinsi', 'gudang','account'));
    }
    public function createGudang(Request $request)
    {
    	$warehouse = new warehouse;
    	$warehouse->city_id = $request->kota;
        $warehouse->warehouse_name = $request->nama;
    	$warehouse->head_warehouse = $request->head;
    	$warehouse->address = $request->alamat;
    	$warehouse->phone = $request->telp1;
    	$warehouse->phone2 = $request->telp2;
    	$warehouse->fax = $request->fax;
    	$warehouse->status = 1;
    	$warehouse->save();
    }
    public function getGudangData(Request $request){
    	$id = $request->id;
    	$warehouse = warehouse::leftjoin('indonesia_cities as ic', 'ic.id','=','warehouse.city_id')->leftjoin('indonesia_provinces as ip','ip.id','=','ic.province_id')->select('warehouse.*','ip.id as p_id')->where('warehouse_id',$id)->first();
    	return $warehouse;
    }

    public function modalGudangData(Request $request){
    	$id= $request->id;
    	$data= warehouse::where('warehouse_id',$id)->first();
    	return view('gudang.modal_gudang', compact('data'));
    }

    public function deleteGudang(Request $request)
    {
    	$id = $request->id;
    	warehouse::where('warehouse_id',$id)->update(['status'=>2]);
    }

    public function updateGudang(Request $request)
    {
    	$id = $request->id;
    	warehouse::where('warehouse_id', $id)->update(
    		[
    		'city_id' => $request->kota,
            'warehouse_name' => $request->nama,
    		'head_warehouse' => $request->head,
    		'address' => $request->alamat,
    		'phone' => $request->telp1,
    		'phone2' => $request->telp2,
    		'fax' => $request->fax,
			]
    	);
    }
}