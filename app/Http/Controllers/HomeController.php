<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\salesorder;
use App\purchaseorder;
use App\account;
use App\news;
use DB;
use Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     return view('home');
    // }

    public function showMainPage(Request $request)
    {   
        $user = Auth::user();
        Session::put('user', $user);
        $role = account::find($user->account_id)->roles->first();
        Session::put('roles',$role);
        // echo $_SERVER['SERVER_NAME'].'//server name </br>';
        // echo $_SERVER['SERVER_ADDR'].'//server ip address </br>';
        // echo $_SERVER['REMOTE_ADDR'].'//client ip </br>';
        // $localIP = getHostByName(getHostName());
        // echo $localIP."test";
        // return Request::ip().'//client ip </br>';
        return view('main');
    }

    public function showHomePage()
    {
        return view('home.home');
    }

    public function loadhomenews(){
        $hasil = news::all();
        return view('home.news',['hasil'=>$hasil]);
    }

    public function loadHomePiutangPo()
    {
        $piutang_po = purchaseorder::leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
            ->leftjoin('invoice_purchase','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')
            ->leftjoin(DB::Raw("(
                Select invoice_purchase.purchase_order_id, sum(payment_purchase_details.paid) as total_paid 
                    from payment_purchase 
                    left join payment_purchase_details on payment_purchase.payment_purchase_id = payment_purchase_details.payment_purchase_id and payment_purchase_details.status <> 2 
                    left join invoice_purchase on payment_purchase.invoice_purchase_id = invoice_purchase.invoice_purchase_id
                    where payment_purchase.status <> 2 
                    And invoice_purchase.status <> 2 
                    group by invoice_purchase.purchase_order_id) tablea"
                ), 'purchase_order.purchase_order_id','=','tablea.purchase_order_id')
            ->selectRaw('purchase_order.*, supplier.company_name, tablea.total_paid, date_add(purchase_order.date_purchase_order, Interval payment_term Day) as jatuh_tempo, (CASE WHEN tablea.total_paid is NULL THEN (purchase_order.grand_total_idr - purchase_order.reduction - invoice_purchase.prev_invoice_reduction - invoice_purchase.discount_nominal) ELSE (purchase_order.grand_total_idr - purchase_order.reduction) - (tablea.total_paid + invoice_purchase.discount_nominal + invoice_purchase.prev_invoice_reduction) END) as remaining_payable')
            ->where('purchase_order.status','<>',2)
            ->where('invoice_purchase.status','<>',2)
            ->having('remaining_payable','>',0)
            ->get();

        // return $piutang_po;

        return view('home.piutang_po',['piutang_po'=>$piutang_po]);
    }

    public function loadHomePiutangSo()
    {
        $piutang_so = salesorder::leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
                        ->selectRaw('sales_order.*,customer.*, date_add(sales_order.date_sales_order, Interval term_payment Day)')
												->whereNotExists(function($query){
													$query->select('*')
													->from('invoice_sales')
													->where('status',1)
													->whereRaw('sales_order.sales_order_id = invoice_sales.sales_order_id');
												})
                        ->get();

        return view('home.piutang_so',['piutang_so'=>$piutang_so]);
    }

    public function loadHomeUtangPelanggan(){
    		$customer = customer::where('status',1)
										->where('total_balance', '<', 0)
										->get();

    		return view('home.utang_pelanggan', ['customer'=>$customer]);
		}
}
