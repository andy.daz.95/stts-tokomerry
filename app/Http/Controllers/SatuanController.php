<?php

namespace App\Http\Controllers;

use App\unitchild;
use Illuminate\Http\Request;
use App\satuan;
use Excel;

class SatuanController extends Controller
{
	public function showSatuanPage()
	{
		$satuan = satuan::where('status',1)->get();
		return view('satuan.satuan', compact('satuan'));
	}

	public function createSatuan(Request $request)
	{
		$satuan = new satuan;
		$satuan->unit_code = $request->kode;
		$satuan->unit_description = $request->satuan;
		$satuan->status = 1;
		$satuan->save();
	}

	public function createAnakSatuan(Request $request)
	{
		$unitchild = new unitchild();
		$unitchild->unit_id = $request->id_satuan_atas;
		$unitchild->unit_child_code = $request->kode_satuan_anak;
		$unitchild->unit_child_name = $request->satuan_anak;
		$unitchild->multiplier = $request->ratio;
		$unitchild->status = 1;
		$unitchild->save();

	}

	public function getSatuanData(Request $request){
		$id = $request->id;
		$satuan = satuan::where('unit_id',$id)->select('*')->first();
		return $satuan;
	}

	public function getAnakSatuan(Request $request){
		$id = $request->id;
		$unitchild = unitchild::with('unit')
			->where('unit_child_id',$id)
			->first();
		return $unitchild;
	}

	public function modalSatuanData(Request $request){
		$id= $request->id;
		$data= satuan::where('unit_id',$id)->first();
		return view('satuan.modal_satuan', compact('data'));
	}

	public function deleteSatuan(Request $request)
	{
		$id = $request->id;
		satuan::where('unit_id',$id)->delete();
	}

	public function updateSatuan(Request $request)
	{
		$id = $request->id;
		satuan::where('unit_id', $id)->update(
			[
				'unit_code' => $request->kode,
				'unit_description' => $request->satuan,
			]
		);
	}

	public function updateAnakSatuan(Request $request)
	{
		$id = $request->id_satuan_anak;
		unitchild::where('unit_child_id', $id)->update(
			[
				'unit_child_code' => $request->kode_satuan_anak,
				'unit_child_name' => $request->satuan_anak,
				'multiplier' => $request->ratio,
			]
		);
	}



	public function checkUnitChild(Request $request){
		$satuan = satuan::where('unit_id',$request->id)->has('unit_child')->get();

		if($satuan->count() > 0)
		{
			return $satuan->first()->unit_child;
		}else{
			return 0;
		}
	}

	public function ShowAnakSatuanPage(){
		$anaksatuan = unitchild::where('status',1)->get();
		return view('satuan.anak_satuan', compact('anaksatuan'));
	}

	public function importSatuan(Request $request){
		if($request->hasFile('file')){
			$path = $request->file('file')->getRealPath();
			$sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

			if(!empty($sheet1) && $sheet1->count()){
				foreach ($sheet1 as $key => $value) {
					$satuan = new satuan();
					$satuan->unit_code = $value->kode;
					$satuan->unit_description = $value->unit;
					$satuan->status = 1;
					$satuan->save();

					if($value->unit_turunan != '-' && $value->unit_turunan != '')
					{
						$uc = new unitchild();
						$uc->unit_id = $satuan->unit_id;
						$uc->unit_child_code = $value->kode_unit_turunan;
						$uc->unit_child_name = $value->unit_turunan;
						$uc->multiplier = substr($value->rasio, 2);
						$uc->status = 1;
						$uc->save();
					}
				}
			}
		}
	}
}