<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\account;
use App\role;
use Validator;
use Image;

class UserController extends Controller
{
    public function showProfilePage()
    {
        return view('user.profile');
    }

    public function showChangePassword()
    {
        return view('user.change_password');
    }

    public function showManageUserPage()
    {
        $staff = account::has('staff')->where('account.status','<>',2)->get(); 
        $role = role::where('status',1)->get(); 
    	return view('user.manage_user', compact('staff','role'));
    }

    public function getAccountdata()
    {
        return account::where('account_id', Session('user')->account_id)->first();
    }

    public function updateAccount(Request $request)
    {
        $dob = $request->years.'-'.$request->months.'-'.$request->days;
        $dob = date('Y-m-d', strtotime($dob));

        $account = account::find(Session('user')->account_id);
        $account->username = $request->username;
        $account->full_name = $request->fullname;
        $account->email = $request->email;
        $account->phone = $request->phone;
        $account->birth_date = $dob;
        $account->gender = $request->gender;
        $account->save();

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = 'account-'.$account->account_id.'.jpg';
            Image::make($avatar)->resize(120,120)->save(public_path().'/uploads/avatars/'.$filename);
            $account->profile_picture = $filename;
            $account->save();
        }

        return back();
    }

    public function updateAccountManage(Request $request)
    {        
        $dob = $request->years.'-'.$request->months.'-'.$request->days;
        $dob = date('Y-m-d', strtotime($dob));

        $account = account::find($request->accountid);
        $account->username = $request->username;
        $account->full_name = $request->fullname;
        $account->email = $request->email;
        $account->phone = $request->phone;
        $account->birth_date = $dob;
        $account->gender = $request->gender;
        $account->save();

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = 'account-'.$account->account_id.'.jpg';
            Image::make($avatar)->resize(120,120)->save(public_path('/uploads/avatars/'.$filename));
            $account->profile_picture = $filename;
            $account->save();
        }

        if($request->role){
            $account->roles()->detach();
            $account->roles()->attach($request->role);
        }

        return back();
    }

    public function createAccount(Request $request)
    {   

        $dob = $request->years.'-'.$request->months.'-'.$request->days;
        $dob = date('Y-m-d', strtotime($dob));

        $account = new account;
        $account->username = $request->username;
        $account->password = Hash::make('123123');
        $account->full_name = $request->fullname;
        $account->email = $request->email;
        $account->phone = $request->phone;
        $account->birth_date = $dob;
        $account->gender = $request->gender;
        $account->save();

        if($request->role){
            $account->roles()->detach();
            $account->roles()->attach($request->role);
        }
 
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = 'account-'.$account->account_id.'.jpg';
            Image::make($avatar)->resize(120,120)->save(public_path('/uploads/avatars/'.$filename));
            $account->profile_picture = $filename;
            $account->save();
        }

        return back();
    }

    public function getAccountManage(Request $request)
    {
        $account = account::where('account_id',$request->id)->with('roles')->first();
        return $account;
    }

    public function deleteAccount(Request $request)
    {
        $account = account::where('account_id',$request->id)->update([
            'status'=>2,
        ]);
    }

    public function changePassword(Request $request)
    {
        Validator::extend('oldpassword', function($field, $value, $parameters)
        {
            return Hash::check($value, Auth::user()->password);
        });

        $rules = 
        [
            'currentpassword' => 'required|oldpassword',
            'newpassword' => 'required|same:retypepassword',
            'retypepassword' => 'required',
        ];

        $messages =
        [
            'currentpassword.required' => 'Password Saat Ini Belum Terisi',
            'currentpassword.oldpassword' => 'Password Yang Anda Masukkan Salah',
            'newpassword.required' => 'Password Baru belum Terisi',
            'retypepassword.required' => 'Password Ulang Belum Terisi',
            'newpassword.same' => 'Password Baru dan Password Ulang Harus Sama',
        ];
        $validation = Validator::make($request->all(),$rules, $messages); 

        if ($validation->fails()) {
            return back()->withInput()->withErrors($validation->messages() );
        }else{
            $account = account::find(Session('user')->account_id);
            $account->password = Hash::make($request->newpassword);
            $account->save();
            return back()->with('success', 'Password Changed!');;
        }
    }

    public function logout(Request $request)
    {

        $request->session()->flush();
        Auth::logout();
        return redirect('/login');

    }
    public function login(Request $request)
    { 
        return view('login');
    }

    public function dologin(Request $request)
    {
        return $request->all();
    }
}