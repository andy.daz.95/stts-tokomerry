<?php

namespace App\Http\Controllers;

use App\detailsalesreturn;
use App\salesreturn;
use App\salesreturnreceive;
use App\detailsalesreturnreceive;
use App\unitchild;
use App\warehouse;
use App\productwarehouse;
use Illuminate\Http\Request;
use DOMPDF;
use DataTable;
use DB;

class SalesReturnReceiveController extends Controller
{
	//
	public function showSalesReturnReceivePage(){
		$data['retur'] = salesreturn::has('details_not_received')
			->where('status',1)
			->get();

		$data['warehouse'] = warehouse::where('status',1)->get();

		return view('sales_return_receive.sales_return_receive', compact('data'));
	}

	public function getSalesReturnReceiveTable(Request $request){
		$returreceive = salesreturnreceive::join('sales_retur','sales_retur.sales_retur_id','=','sales_retur_receive.sales_retur_id')
			->join('sales_order','sales_order.sales_order_id','=','sales_retur.sales_order_id')
			->join('customer','customer.customer_id','=','sales_order.customer_id')
			->where('sales_retur_receive.status',1)
			->orderby('sales_retur_receive.sales_retur_receive_id','desc');

		if ($request->noreturreceive && $request->noreturreceive != "")
		{
			$returreceive = $returreceive->where('sales_retur_receive_number','like','%'.$request->noreturreceive.'%');
		}
		if($request->customer && $request->customer != "")
		{
			$returreceive = $returreceive->whereRaw("CONCAT_WS(' ', `first_name`, `last_name`) like '%$request->customer%'");
		}
		$returreceive = $returreceive->get();

		return DataTable::of($returreceive)
			->setRowAttr([
				'value' => function($returreceive) {
					return $returreceive->sales_retur_receive_id;
				},
			])
			->addColumn('customer_name', function ($returreceive){
				return $returreceive->first_name.' '.$returreceive->last_name;
			})
			->addColumn('action', function ($returreceive) {
				return
					'<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
						<a class="btn btn-sm btn-raised orange print-retur-receive" target="_blank" href="downloadsalesreturnreceive/'.$returreceive->sales_retur_receive_id.'"><i class="material-icons">print</i></a>';
			})
			->smart(false)
			->make(true);
	}

	public function getSalesReturnReceive(Request $request){
		$returreceive = salesreturnreceive::with('details.product')
			->with('details.unit')
			->with('details.unit_child')
			->with('retur.so.customer')
			->where('sales_retur_receive_id',$request->id)
			->first();

		foreach($returreceive->details as $key => $value){
			$retur= detailsalesreturn::where('sales_retur_id',$returreceive->sales_retur_id)
				->where('product_id',$value->product_id)
				->first();

			$retur_qty = $retur->qty;
			$received = $retur->received_qty;
			$value['retur_qty'] = $retur_qty;
			$value['received_qty'] = $received;
		}
		return $returreceive;
	}

	public function getLastReturnReceiveNumber()
	{
		$lastreturnreceivetoday = salesreturnreceive::where('sales_retur_receive_number','like','SRR/'.date('dmy').'/%')->orderby('sales_retur_receive_id','desc')->first();
		if(empty($lastreturnreceivetoday))
		{
			$newreturnreceivenumber = "SRR/".date('dmy')."/1";
			return $newreturnreceivenumber;
		}
		else{
			$tmpreturnreceive = explode('/',$lastreturnreceivetoday->sales_retur_receive_number);
			$lastnumber = $tmpreturnreceive[2];
			$newreturnreceivenumber = "SRR/".date('dmy')."/".($lastnumber+1);
			return $newreturnreceivenumber;
		}
	}

	public function createReturnReceive(Request $request)
	{
		$lastnumber = $this->getLastReturnReceiveNumber();

		$salesreturnreceive = new salesreturnreceive();
		$salesreturnreceive->sales_retur_id = $request->noretur;
		$salesreturnreceive->warehouse_id = $request->gudang;
		$salesreturnreceive->sales_retur_receive_number = $lastnumber;
		$salesreturnreceive->date_sales_retur_receive = date('Y-m-d', strtotime($request->tglpenerimaanretur));
		$salesreturnreceive->status = 1;
		$salesreturnreceive->save();

		//cek retur type, berguna untuk for loop dibawah
		$returtype =
			salesreturn::where('sales_retur_id', $request->noretur)
				->first()
				->retur_type_id;

		for ($i=0; $i <sizeof($request->detailretur); $i++)
		{
			if($request->receiveqty[$i] > 0)
			{
				$detailsalesreturnreceive = new detailsalesreturnreceive();
				$detailsalesreturnreceive->sales_retur_receive_id = $salesreturnreceive->sales_retur_receive_id;
				$detailsalesreturnreceive->product_id = $request->id[$i];
				$detailsalesreturnreceive->unit_id = $request->unitid[$i];
				$detailsalesreturnreceive->unit_type = $request->unittype[$i];
				$detailsalesreturnreceive->quantity = $request->receiveqty[$i];
				$detailsalesreturnreceive->status = 1;
				$detailsalesreturnreceive->save();

				detailsalesreturn::where('sales_retur_details_id',$request->detailretur[$i])
					->update(
						[
							'received_qty' => DB::raw('received_qty +'.$request->receiveqty[$i])
						]
					);

					//di convert dulu qty barang nya ke parent unit apabila pake unit child.
					if($request->unittype[$i] == 'child')
					{
						$multiplier = unitchild::where('unit_child_id',$request->unitid[$i])->first()->multiplier;
						$qty = $request->receiveqty[$i] / $multiplier;
					}else{
						$qty = $request->receiveqty[$i];
					}


					productwarehouse::where('warehouse_id',$request->gudang)
						->where('product_id',$request->id[$i])
						->update([
							'quantity' => DB::raw('quantity+'.$qty),
						]);
			}
		}
	}

	public function deleteReturnReceive(Request $request)
	{
		$salesreturnreceive = salesreturnreceive::find($request->id);
		$details =
			detailsalesreturnreceive::where('sales_retur_receive_id', $request->id)
				->where('status',1)
				->get();

		$returtype =
			salesreturn::where('sales_retur_id', $salesreturnreceive->sales_retur_id)
				->first()
				->retur_type_id;

		foreach ($details as $key => $value)
		{
			detailsalesreturn::where('sales_retur_id',$salesreturnreceive->sales_retur_id)
				->where('product_id', $value->product_id)
					->update(
						[
							'received_qty' => DB::raw('received_qty -'.$value->quantity)
						]
					);

			if($returtype != 5) {
				//di convert dulu qty barang nya ke parent unit apabila pake unit child.
				if ($value->unit_type == 'child') {
					$multiplier = unitchild::where('unit_child_id', $value->unit_id)->first()->multiplier;
					$qty = $value->quantity / $multiplier;
				} else {
					$qty = $value->quantity;
				}

				productwarehouse::where('warehouse_id', $salesreturnreceive->warehouse_id)
					->where('product_id', $value->product_id)
					->update([
						'quantity' => DB::raw('quantity-' . $qty),
					]);
			}
		}

		detailsalesreturnreceive::where('sales_retur_receive_id', $request->id)->update(
			[
				'status' => 2,
			]
		);

		$salesreturnreceive->status = 2;
		$salesreturnreceive->save();
	}

	public function getSalesReturnData(request $request){
		$retur = salesreturn::where('sales_retur_id', $request->id)
			->with('details.product')
			->with('details.unit')
			->with('details.unit_child')
			->with('so.customer')
			->first();

		return $retur;
	}

	public function downloadSalesReturnReceive(Request $request, $id)
	{
		$returreceive = salesreturnreceive::with('details.product')
			->with('retur.so.customer')
			->with('warehouse')
			->where('sales_retur_receive_id',$request->id)
			->first();

		foreach($returreceive->details as $key => $value){
			$retur= detailsalesreturn::where('sales_retur_id',$returreceive->sales_retur_id)
				->where('product_id',$value->product_id)
				->first();

			$retur_qty = $retur->qty;
			$value['retur_qty'] = $retur_qty;
		}

		$pdf = DOMPDF::loadView('sales_return_receive.sales_return_receive_pdf', compact('returreceive'));
		return $pdf->stream('Penerimaan Sales Return.pdf');
	}
}
