<?php

namespace App\Http\Controllers;

use App\barang;
use App\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\bank;
use App\supplier;
use App\target;
use App\userbank;
use Indonesia;
use Excel;
use DB;

class SupplierController extends Controller
{
    public function showSupplierPage()
    {
    	$provinsi = Indonesia::allProvinces();
    	$bank = bank::where('status',1)->get();
    	$supplier = supplier::where('status',1)->get();

    	return view('supplier.supplier', compact('provinsi','bank', 'supplier'));
    }

    public function createSupplier(Request $request)
    {
    	$supplier = new supplier;
    	$supplier->account_id = Session::get('user')->account_id;
    	$supplier->city_id = $request->kota;
        $supplier->first_name = $request->firstname;
        $supplier->last_name = $request->lastname;
    	$supplier->company_name = $request->perusahaan;
    	$supplier->address = $request->alamat;
    	$supplier->phone = $request->telp1;
    	$supplier->phone2 = $request->telp2;
    	$supplier->fax = $request->fax;
    	$supplier->status = 1;
    	$supplier->save();

        if($request->bank)
        {
           $userbank = new userbank;
           $userbank->customer_id = $supplier->supplier_id;
           $userbank->category_id = "2";
           $userbank->bank_id = $request->bank;
           $userbank->bank_account = $request->norek;
           $userbank->on_behalf_of = $request->atasnama;
           $userbank->status = 1;
           $userbank->save();
        }
    }

    public function importSupplier(Request $request){

        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

            if(!empty($sheet1) && $sheet1->count()){
                foreach ($sheet1 as $key => $value) {
                        $supplier = new supplier;
                        $supplier->account_id = Session::get('user')->account_id;
                        $supplier->city_id = 7371;
                        $supplier->first_name = $value->nama_depan;
                        $supplier->last_name = $value->nama_belakang;
                        $supplier->company_name = $value->nama_perusahaan;
                        $supplier->address = '';
                        $supplier->phone = $value->telp1 ? $value->telp1 : '';
                        $supplier->phone2 = $value->telp2 ? $value->telp2 : '';
                        $supplier->fax = '';
                        $supplier->status = 1;
                        $supplier->save();

                        if($value->bank !="")
                        {
                         $userbank = new userbank;
                         $userbank->customer_id = $supplier->supplier_id;
                         $userbank->category_id = "2";
                         $userbank->bank_id = $value->bank_id;
                         $userbank->bank_account = $value->no_rek;
                         $userbank->on_behalf_of = $value->atas_nama;
                         $userbank->status = 1;
                         $userbank->save();
                     }

                }
            }
        }
    }

    public function filterSupplier(Request $request){
    	$filter = $request->filter;
    	$supplier = supplier::where('company_name','like','%'.$filter.'%')->paginate(10);

    	return view('supplier.supplier_table', compact('supplier'));
    }

    public function getSupplierData(Request $request){
    	$id = $request->id;
    	$supplier = supplier::leftjoin('user_bank as ub','ub.customer_id','=','supplier.supplier_id')
    		->leftjoin('indonesia_cities as ic','ic.id','=','supplier.city_id')
    		->leftjoin('indonesia_provinces as ip','ip.id','=','ic.province_id')
    		->where('supplier.supplier_id',$id)
    		->select('supplier.*','ub.user_bank_id','ip.id as provinsi')
    		->first();

        if($supplier->user_bank_id)
            {
                $bank = userbank::leftjoin('bank','user_bank.bank_id','=','bank.bank_id')
                ->where('user_bank.category_id', 2)
                ->where('user_bank.customer_id', $request->id)
                ->where('user_bank.status', 1)
                ->first();

            }else{
                $bank = [];
            }

    	return compact('supplier','bank');
    }

    public function modalSupplierData(Request $request){
        $id= $request->id;
        $data= supplier::where('supplier_id',$id)->first();
        return view('supplier.modal_supplier', compact('data'));
    }

    public function deleteSupplier(Request $request)
    {
        $id = $request->id;
        supplier::where('supplier_id',$id)->update(['status' => 2]);
    }

    public function updateSupplier(Request $request)
    {
        $id = $request->id;
        supplier::where('supplier_id', $id)->update(
            [
            'account_id' => "1",
            'city_id' => $request->kota,
            'company_name' => $request->perusahaan,
            'address' => $request->alamat,
            'first_name' => $request->firstname,
            'last_name' => $request->lastname,
            'phone' => $request->telp1,
            'phone2' => $request->telp2,
            'fax' => $request->fax,
            ]
        );
        if($request->bank)
        {
            $userbank = userbank::where('customer_id',$id)->where('category_id',2);
            if($userbank->count() > 0)
            {
                $userbank->update(
                    [
                        'bank_id' => $request->bank,
                        'bank_account' => $request->norek,
                        'on_behalf_of' => $request->atasnama,
                    ]
                );
            }else{
                $userbank = new userbank;
                $userbank->customer_id = $id;
                $userbank->category_id = "2";
                $userbank->bank_id = $request->bank;
                $userbank->bank_account = $request->norek;
                $userbank->on_behalf_of = $request->atasnama;
                $userbank->status = 1;
                $userbank->save();
            }
        }else{
            userbank::where('customer_id',$id)->where('category_id',2)->where('status',1)->update(
                [
                    'status' => 2,
                ]
            );
        }
        // userbank::where('customer_id',$id)->where('category_id',2)->update(
        //     [
        //         'bank_id' => $request->bank,
        //         'bank_account' => $request->norek,
        //         'on_behalf_of' => $request->atasnama,
        //     ]
        //);
    }

    public function showTargetPage()
    {
        $target = target::with('supplier')
					->with('product')
					->where('status',1)
					->get();

        $supplier = supplier::where('status',1)->get();
        $product = product::where('status',1)->get();
        return view('supplier.set_target', compact('target','supplier','product'));
    }

    public function createTarget(Request $request)
    {
        $target = new target;
        $target->supplier_id = $request->supplier;
        $target->product_id = $request->product;
        $target->target_nominal = $request->nominal;
        $target->date_start = date('Y-m-d', strtotime($request->tglawal));
        $target->date_end = date('Y-m-d', strtotime($request->tglakhir));
        $target->status = 1;
        $target->save();
    }
}