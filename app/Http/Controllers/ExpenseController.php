<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\expense;

class ExpenseController extends Controller
{
    public function showExpensePage(){
    	$data['expense'] = expense::where('status','<>',2)->get();
    	
    	return view('expense.expense', compact('data'));
    }

    public function getLastExpenseNumber()
    {
    	$lastexpensetoday = expense::where('expense_number','like','EXP/'.date('dmy').'/%')->orderby('expense_id','desc')->first();
    	if(empty($lastexpensetoday))
    	{
    		$newexpensenumber = "EXP/".date('dmy')."/1";
    		return $newexpensenumber;
    	}
    	else{
    		$tmpexpense = explode('/',$lastexpensetoday->expense_number); 
    		$lastnumber = $tmpexpense[2];
    		$newexpensenumber = "EXP/".date('dmy')."/".($lastnumber+1);
    		return $newexpensenumber;
    	}
    }

    public function getExpense(Request $request)
    {
    	$expense = expense::where('expense_id',$request->id)->first();
    	return $expense;
    }

    public function createExpense(Request $request)
    {
    	$expense = new expense();
    	$expense->expense_number = $request->noexpense;
    	$expense->date_expense = date('Y-m-d', strtotime($request->tglexpense));
    	$expense->description = $request->description;
    	$expense->nominal = $request->nominal;
    	$expense->status = 1;
    	$expense->save();
    }

    public function updateExpense(Request $request)
    {
    	$expense = expense::find($request->idexpense);
    	$expense->date_expense = date('Y-m-d', strtotime($request->tglexpense));
    	$expense->description = $request->description;
    	$expense->nominal = $request->nominal;
    	$expense->save();
    }

    public function deleteExpense(Request $request)
    {
    	$expense = expense::find($request->id);
    	$expense->status = 2;
    	$expense->save();
    }
}
