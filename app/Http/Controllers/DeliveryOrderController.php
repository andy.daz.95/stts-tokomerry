<?php

namespace App\Http\Controllers;

use App\account;
use App\unitchild;
use Illuminate\Http\Request;
use App\deliveryorder;
use App\detaildeliveryorder;
use App\draftdelivery;
use App\detaildraftdelivery;
use App\salesreturn;
use App\shippingterm;
use App\salesorder;
use App\detailsalesorder;
use App\product;
use App\productwarehouse;
use App\log;
use App\warehouse;
use App\customer;
use DB;
use DOMPDF;
use Redis;
use DataTable;
use App\Traits\DeliveryTrait;
/*use App\deliveryorder;*/

class DeliveryOrderController extends Controller
{
	use DeliveryTrait;

	public function showDeliveryOrderPage()
	{
		// $draft = draftdelivery::with('warehouse')
		// 	->with('so')
		// 	->leftjoin('draft_delivery_order_details','draft_delivery_order.draft_delivery_order_id','=','draft_delivery_order_details.draft_delivery_order_id')
		// 	->leftjoin(
		// 		DB::raw("(Select delivery_order.*, SUM(quantity_sent) as sent
		// 						from delivery_order
		// 						left join delivery_order_details
		// 						on delivery_order.delivery_order_id = delivery_order_details.delivery_order_id
		// 						where delivery_order.status = 1
		// 						and delivery_order_details.status = 1
		// 						group by draft_delivery_order_id) tablea")
		// 		,'tablea.draft_delivery_order_id','=','draft_delivery_order.draft_delivery_order_id')
		// 	->where('draft_delivery_order.status',1)
		// 	->selectRaw('draft_delivery_order.draft_delivery_order_id, draft_delivery_order.draft_delivery_order_number, SUM(draft_delivery_order_details.quantity) as qty, tablea.sent')
		// 	->havingRaw('qty > sent or sent Is Null')
		// 	->groupBy('draft_delivery_order.draft_delivery_order_id')
		// 	->get();

		// $draftcache = Redis::get(env('APP_ENV').'-draft-for-delivery');
		// if($draftcache)
		// {
		// 	$draft = json_decode($draftcache);
		// }
		// else
		// {
		// 	$draft = draftdelivery::
		// 	join('customer','customer.customer_id','=','draft_delivery_order.customer_id')
		// 	->join('warehouse','warehouse.warehouse_id','=','draft_delivery_order.warehouse_id')
		// 	->whereExists(function ($query) {
		// 		$query->select(DB::raw('1'))
		// 		->from('draft_delivery_order_details')
		// 		->whereColumn('sent','<','quantity')
		// 		->where('status',1)
		// 		->whereRaw('draft_delivery_order_details.draft_delivery_order_id = draft_delivery_order.draft_delivery_order_id');
		// 	})
		// 	->select('draft_delivery_order.draft_delivery_order_id','draft_delivery_order.draft_delivery_order_number','draft_delivery_order.shipping_term_id','customer.first_name','customer.last_name','warehouse.warehouse_name')
		// 	->get();
		// }

		$draft = draftdelivery::join('customer','customer.customer_id','=','draft_delivery_order.customer_id')
			->join('warehouse','warehouse.warehouse_id','=','draft_delivery_order.warehouse_id')
			->whereExists(function ($query) {
				$query->select(DB::raw('1'))
				->from('draft_delivery_order_details')
				->whereColumn('sent','<','quantity')
				->where('status',1)
				->whereRaw('draft_delivery_order_details.draft_delivery_order_id = draft_delivery_order.draft_delivery_order_id');
			})
			->select('draft_delivery_order.draft_delivery_order_id','draft_delivery_order.draft_delivery_order_number','draft_delivery_order.shipping_term_id','customer.first_name','customer.last_name','warehouse.warehouse_name')
			->get();

		$warehouse = warehouse::all();
		$shippingterm = shippingterm::all();
		$account = account::where('status',1)->get();

		return view('delivery_order.delivery_order', compact('draft','do','shippingterm','warehouse','account'));
	}

	public function getDeliveryTable(request $request){
		$do = deliveryorder::join('draft_delivery_order','draft_delivery_order.draft_delivery_order_id','=','delivery_order.draft_delivery_order_id')
			->join('sales_order','sales_order.sales_order_id','=','draft_delivery_order.sales_order_id')
			->join('customer','customer.customer_id','=','sales_order.customer_id')
			->join('account','account.account_id','=','delivery_order.user_id')
			->where('delivery_order.status',1)
			->orderby('delivery_order.delivery_order_id','desc')
			->select('delivery_order.delivery_order_id','delivery_order.delivery_order_number','draft_delivery_order.draft_delivery_order_number', 'account.full_name','delivery_order.date_delivery','customer.first_name','customer.last_name');

		if ($request->nodo && $request->nodo!= "")
		{
			$do = $do->where('delivery_order_number','like','%'.$request->nodo.'%');
		}
		if($request->customer && $request->customer != "")
		{
			$do = $do->whereRaw("CONCAT_WS(' ', `first_name`, `last_name`) like '%$request->customer%'");
		}

		return DataTable::of($do)
			->setRowAttr([
				'value' => function($do) {
					return $do->delivery_order_id;
				},
			])
			->addColumn('customer_name', function ($do){
				return $do->first_name.' '.$do->last_name;
			})
			->addColumn('action', function ($do) {
				return
					'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$do->delivery_order_id.'"><i class="material-icons">edit</i></a>
					<a class="btn btn-sm btn-raised orange print-delivery" target="_blank" href="downloaddeliveryorder/'.$do->delivery_order_id.'"><i class="material-icons">print</i></a>
					<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
			})
			->smart(false)
			->make(true);
	}

	public function getDeliveryOrder(Request $request)
	{

		$do = deliveryorder::with('draft.so.customer')
			->with('draft.retur.so.customer')
			->with('draft.details')
			->with('details.product.satuan.unit_child')
			->where('delivery_order_id', $request->id)
			->where('status',1)
			->first();

		//untuk get Total Sent dengan iterasi setiap do yang memiliki product tersebut, , kemudian append ke details
		foreach ($do->details as $key => $value){
			$product = $value->product_id;
			$sent = deliveryorder::
			with(['details' => function ($query) use ($product) {
				$query->where('product_id', $product);
			}])
				->whereHas('details', function ($query) use ($product) {
					$query->where('product_id', $product);
				})
				->where('draft_delivery_order_id', $do->draft_delivery_order_id)
				->get();

			if($sent->count() > 0) {
				$total = 0;
				foreach($sent as $key2 => $value2){
					$total += $value2->details[0]->quantity_sent;
				}
				$value['sent'] = $total;
			}else{
				$value['sent'] = 0;
			}
		}

		return $do;
	}

	public function getLastDeliveryNumber()
	{
		$lastdotoday = deliveryorder::where('delivery_order_number','like','DO/'.date('dmy').'/%')->orderby('delivery_order_id','desc')->first();
		if(empty($lastdotoday))
		{
			$newdonumber = "DO/".date('dmy')."/1";
			return $newdonumber;
		}
		else{
			$tmpdo = explode('/',$lastdotoday->delivery_order_number);
			$lastnumber = $tmpdo[2];
			$newdonumber = "DO/".date('dmy')."/".($lastnumber+1);
			return $newdonumber;
		}
	}

	public function getStockInWarehouse(Request $request)
	{
		$stock = productwarehouse::whereIn('product_id',$request->idproduct)
			->where('warehouse_id',$request->idgudang)
			->get();
		return $stock;
	}

	public function getDraftDeliveryData(Request $request)
	{
		if($request->id == 0){
			return;
		}
		$id = $request->id;
		$draft = draftdelivery::with('details.product.satuan.unit_child')
			->with('details')
			->with('so.customer')
			->with('retur.so.customer')
			->where('draft_delivery_order_id',$id)
			->first();

		//untuk get Total Sent dengan iterasi setiap do yang memiliki product tersebut, kemudian append ke details
		foreach ($draft->details as $key => $value){
			$product = $value->product_id;
			$sent = deliveryorder::
			with(['details' => function ($query) use ($product) {
				$query->where('product_id', $product);
			}])
				->whereHas('details', function ($query) use ($product) {
					$query->where('product_id', $product);
				})
				->where('draft_delivery_order_id', $id)
				->get();

			if($sent->count() > 0) {
				$total = 0;
				foreach($sent as $key2 => $value2){
					$total += $value2->details[0]->quantity_sent;
				}
				$value['sent'] = $total;
			}else{
				$value['sent'] = 0;
			}
		}

		return compact('draft');
	}

	public function createDeliveryOrder(Request $request)
	{
		$lastnumber = $this->getLastDeliveryNumber();

		$deliveryorder = $this->createDelivery($request->draft, $request->user, $lastnumber , $request->tgldo, $request->note, $request->driver, $request->nopol, $request->gudang);

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			if($request->sendqty[$i] > 0 && in_array($request->idbarang[$i], $request->check) != false)
			{
				$detaildeliveryorder = $this->createDetailDelivery($deliveryorder->delivery_order_id, $request->idbarang[$i], $request->unit[$i], $request->unittype[$i], $request->sendqty[$i], $request->detailso[$i], $request->detaildraft[$i]);

				//di convert dulu qty barang nya ke parent unit apabila pake unit child.
				if($request->unittype[$i] == 'child')
				{
					$multiplier = unitchild::where('unit_child_id',$request->unit[$i])->first()->multiplier;
					$qty = $request->sendqty[$i] / $multiplier;
				}else{
					$qty = $request->sendqty[$i];
				}

				productwarehouse::where('product_id',$request->idbarang[$i])->where('warehouse_id',$request->gudang)->update([
					'quantity' => DB::raw('quantity-'.$qty),
				]);

				//update total sent in detail draft
				detaildraftdelivery::where('draft_delivery_order_details_id',$request->detaildraft[$i])->update([
					'sent' => DB::raw('sent+'.$qty),
				]);
			}
		}

		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "menambahkan";
		$log->object = "Delivery Order";
		$log->object_details = $deliveryorder->delivery_order_number;
		$log->status = 1;
		$log->save();

		return $deliveryorder->delivery_order_number;
	}

	public function updateDeliveryOrder(Request $request)
	{
		$deliveryorder = deliveryorder::find($request->iddo);
		$lastwarehouse = $deliveryorder->warehouse_id;

		$deliveryorder->date_delivery = date('Y-m-d',strtotime($request->tgldo));
		$deliveryorder->user_id = $request->user;
		$deliveryorder->save();

		$detaildeliveryorder =
			detaildeliveryorder::where('delivery_order_id',$request->iddo)
			->where('status',1)
			->get();


		foreach ($detaildeliveryorder as $key => $value) {
			$qty = $value->quantity_sent;
			$product_id = $value->product_id;

			if($value->unit_type == 'child')
			{
				$multiplier = unitchild::where('unit_child_id',$value->unit_id)->first()->multiplier;
				$qty = $qty / $multiplier;
			}else{
				$qty = $qty;
			}

			$a = productwarehouse::where('product_id',$product_id)
				->where('warehouse_id',$lastwarehouse)
				->first()
				->quantity;

			productwarehouse::where('product_id',$product_id)
				->where('warehouse_id',$lastwarehouse)
				->update([
					'quantity' => DB::raw('quantity+'.$qty),
				]);

			detaildraftdelivery::where('draft_delivery_order_details_id',$value->draft_delivery_order_details_id)->update([
					'sent' => DB::raw('sent-'.$qty),
				]);
		}

		$detaildeliveryorder = detaildeliveryorder::where('delivery_order_id',$request->iddo)
			->update([
				'status'=>2
			]);

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			if($request->sendqty[$i] > 0)
			{
				$detaildeliveryorder = $this->createDetailDelivery($deliveryorder->delivery_order_id, $request->idbarang[$i], $request->unit[$i], $request->unittype[$i], $request->sendqty[$i], $request->detailso[$i], $request->detaildraft[$i]);

				//di convert dulu qty barang nya ke parent unit apabila pake unit child.
				if($request->unittype[$i] == 'child')
				{
					$multiplier = unitchild::where('unit_child_id',$request->unit[$i])->first()->multiplier;
					$qty = $request->sendqty[$i] / $multiplier;
				}else{
					$qty = $request->sendqty[$i];
				}

				productwarehouse::where('product_id',$request->idbarang[$i])
					->where('warehouse_id',$request->gudang)->update([
						'quantity' => DB::raw('quantity-'.$qty),
					]);

				detaildraftdelivery::where('draft_delivery_order_details_id',$request->detaodraft[$i])->update([
					'sent' => DB::raw('sent+'.$qty),
				]);
			}
		}

		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "mengubah";
		$log->object = "Delivery Order";
		$log->object_details = $deliveryorder->delivery_order_number;
		$log->status = 1;
		$log->save();
	}

	public function deleteDeliveryOrder(Request $request)
	{
		$detaildeliveryorder =
			detaildeliveryorder::with('delivery')
				->where('delivery_order_id',$request->id)
				->where('status',1)
				->get();

		foreach ($detaildeliveryorder as $key => $value) {
			$qty = $value->quantity_sent;
			$product_id = $value->product_id;
			$warehouse_id = $value->delivery->warehouse_id;

			if($value->unit_type == 'child')
			{
				$multiplier = unitchild::where('unit_child_id',$value->unit_id)->first()->multiplier;
				$qty = $qty / $multiplier;
			}else{
				$qty = $qty;
			}


			productwarehouse::where('product_id',$product_id)
				->where('warehouse_id',$warehouse_id)
				->update([
					'quantity' => DB::raw('quantity+'.$qty),
				]);

			detaildraftdelivery::where('draft_delivery_order_details_id',$value->draft_delivery_order_details_id)->update([
					'sent' => DB::raw('sent-'.$qty),
				]);
		}

		$detaildeliveryorder = detaildeliveryorder::where('delivery_order_id',$request->id)
			->update([
				'status'=>2
			]);

		deliveryorder::where('delivery_order_id',$request->id)->update(["status"=>2]);

		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "mengubah";
		$log->object = "Delivery Order";
		$log->object_details = $request->id;
		$log->status = 1;
		$log->save();
	}

	public function downloadDeliveryOrder(Request $request, $id, $size = 'regular')
	{
		$do = deliveryorder::with('details.product.satuan.unit_child')
			->with('draft.so.customer')
			->with('draft.retur.so.customer')
			->where('delivery_order.delivery_order_id',$id)
			->first();

		foreach ($do->details as $key => $value) {
			$quantity_draft = detaildraftdelivery::where('draft_delivery_order_id',$do->draft_delivery_order_id)
				->where('product_id', $value->product_id)
				->where('status',1)
				->first()
				->quantity;
			$value['quantity_draft'] = $quantity_draft;

			$value['total_sent'] = detaildeliveryorder::leftjoin('delivery_order','delivery_order.delivery_order_id','=','delivery_order_details.delivery_order_id')
				->where('delivery_order.draft_delivery_order_id',$do->draft_delivery_order_id)
				->where('delivery_order.status',1)
				->where('delivery_order_details.status',1)
				->where('delivery_order_details.product_id',$value->product_id)
				->sum('quantity_sent');
		}

		if($do->draft->so){
			$customer = $do->draft->so->customer->customer_id;
		}else if ($do->draft->retur->so){
			$customer = $do->draft->retur->so->customer->customer_id;
		}

		$retur = salesreturn::leftjoin('sales_order','sales_order.sales_order_id','=','sales_retur.sales_order_id')
			->with(['details' => function ($query) {
				$query->whereColumn('received_qty', '<', 'qty');
			}])
			->whereHas('details', function ($query) {
				$query->whereColumn('received_qty', '<', 'qty');
			})
			->where('sales_order.customer_id',$customer)
			->whereIn('retur_type_id',[2,7])
			->get();

			$pdf = DOMPDF::loadView('delivery_order.do_pdf', compact('do','retur'))
				->setPaper(array(0, 0, 270, 403));
			return $pdf->stream('DO.pdf');
	}
}