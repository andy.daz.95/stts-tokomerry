<?php

namespace App\Http\Controllers;

use App\account;
use App\additionalproductdetail;
use App\additionalproductinfo;
use App\additionalproductowner;
use App\purchasereturn;
use Illuminate\Http\Request;
use App\product;
use App\stockmovement;
use App\warehouse;
use App\productwarehouse;
use App\goodreceive;
use App\deliveryorder;
use App\purchaseorder;
use App\salesorder;
use App\stockcorrection;
use App\detailstockmovement;
use Carbon\Carbon;
use DB;
use DOMPDF;
use DataTable;

class InventoryController extends Controller
{
	public function showStockOverviewPage(){
		$data['product'] = product::leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')
			->with('satuan.unit_child')
			->selectRaw('product.*, Sum(quantity)as t_qty')
			->groupby('product.product_id')
			->where('product.status',1)
			->get();
		$data['warehouse'] = warehouse::all();
		return view('stock.stockoverview', compact('data'));
	}

	public function detailStock(Request $request){
		$data['detail'] = product::with('satuan.unit_child')
			->leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')
			->selectRaw('product.*, product_warehouse.quantity, product_warehouse.warehouse_id')
			->where('product.product_id',$request->id)
			->get();
		return $data;
	}

	public function showAdditionalInfoPage(Request $request){
		$data['product'] = product::where('status',1)->where('is_spandek',1)->get();
		$data['warehouse'] = warehouse::where('status',1)->get();
		$data['user'] = account::where('status',1)->get();
		return view('stock.additional_info', compact('data'));
	}

	public function getAdditionalInfoTable(Request $request){
		$data = additionalproductinfo::with('product')
			->with('warehouse')
			->where('status',1)
			->orderBy('additional_product_info_id','desc');
		if($request->spandek)
		{
			$data = $data->whereHas('product' , function($query) use ($request){
				$query->whereRaw("product.product_name like '%$request->spandek%'");
			});
		}
		$data = $data->get();

		return DataTable::of($data)
			->setRowAttr([
				'value' => function($data) {
					return $data->additional_product_info_id;
				},
			])
			->addColumn('action', function ($data) {
				return '<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
					<a class="btn btn-sm btn-raised blue open-info"><i class="material-icons">search</i></a>';
			})
			->smart(false)
			->make(true);
	}

	public function getAdditionalInfoData(Request $request){
		$addinfo = additionalproductinfo::with('owner.detail')
			->with('product')
			->with('warehouse')
			->where('product_id',$request->id)
			->where('status',1)
			->orderby('additional_product_info_id','desc')
			->first();

		return $addinfo;
	}

	public function getAdditionalInfoDatabyId(Request $request){
		$addinfo = additionalproductinfo::with('owner.detail')
			->with('product')
			->with('warehouse')
			->where('additional_product_info_id',$request->id)
			->where('status',1)
			->orderby('additional_product_info_id','desc')
			->first();

		return $addinfo;
	}

	public function createAdditionalInfo(Request $request)
	{
		$addinfo = new additionalproductinfo();
		$addinfo->product_id = $request->main['product'];
		$addinfo->warehouse_id = $request->main['warehouse'];
		$addinfo->user_id = $request->main['user'];
		$addinfo->date = date('Y-m-d', strtotime($request->main['date']));;
		$addinfo->status = 1;
		$addinfo->save();

		foreach($request['detail'] as $key => $value){
			$addinfoowner = new additionalproductowner();
			$addinfoowner->additional_product_info_id = $addinfo->additional_product_info_id;
			$addinfoowner->name = $value['owner'];
			$addinfoowner->status = 1;
			$addinfoowner->save();

			echo 'detail '.$key;

			for($i = 0 ; $i < sizeof($value['info']); $i++)
			{
				echo 'ukuran :'.$value['info'][$i]['ukuran'];
				echo '<br>';

				$addinfodetail = new additionalproductdetail();
				$addinfodetail->additional_product_owner_id = $addinfoowner->id;
				$addinfodetail->ukuran = $value['info'][$i]['ukuran'];
				$addinfodetail->lembar = $value['info'][$i]['lembar'];
				$addinfodetail->status = 1;
				$addinfodetail->save();

				echo 'info '.$i;
			}
		}
	}

	public function showStockTransferPage($source = null){
		$data['gudang'] = warehouse::all();
		$data['barang'] = product::all();

		$data['stockmovement'] = stockmovement::with('warehouse_from_data')
			->with('warehouse_to_data.head_warehouse_data')
			->orderBy('is_received','asc')
			->orderBy('stock_movement_id','desc')
			->where('stock_movement.status','<>',2)
			->whereDate('created_at', '>', Carbon::now()->subMonths(6));

		if($source != null)
		{
			$data['source'] = $source;
			$data['stockmovement'] = $data['stockmovement']
			->whereHas('details',function($query){
				$query->whereNotNull('purchase_order_details_id');
			});
		}else{
			$data['stockmovement'] = $data['stockmovement']
			->whereHas('details',function($query){
				$query->whereNull('purchase_order_details_id');
			});
		}
		$data['stockmovement'] = $data['stockmovement']->get();

		return view('stock.stock_transfer', compact('data'));
	}

	public function downloadStockTransfer(Request $request, $id)
	{
		$sm = stockmovement::where('stock_movement_id', $request->id);
		$from = $sm->first()->warehouse_from;
		$to = $sm->first()->warehouse_to;

		$stockmovement = stockmovement::with('details.product')
			->with('warehouse_from_data')
			->with('warehouse_to_data')
			->where('stock_movement_id', $request->id)
			->first();

		$pdf = DOMPDF::loadView('stock.stock_transfer_pdf', compact('stockmovement'));
		return $pdf->stream('Stock Transfer.pdf');
	}

	public function getBarangTransfer(Request $request)
	{
		$product = product::where('product_id',$request->id)->first();
		$qtyfrom = productwarehouse::where('product_id',$request->id)->where('warehouse_id', $request->gudangfrom)->first()->quantity;
		$qtyto = productwarehouse::where('product_id',$request->id)->where('warehouse_id', $request->gudangto)->first()->quantity;
		return ['qtyfrom' => $qtyfrom, 'qtyto'=> $qtyto, 'product'=>$product];
	}

	public function getLastTransferNumber(Request $request)
	{
		$lasttransfertoday = stockmovement::where('stock_movement_number','like','SM/'.date('dmy').'/%')->orderby('stock_movement_id','desc')->first();
		if(empty($lasttransfertoday))
		{
			$newtransfernumber = "SM/".date('dmy')."/1";
			return $newtransfernumber;
		}
		else{
			$tmptransfer = explode('/',$lasttransfertoday->stock_movement_number);
			$lastnumber = $tmptransfer[2];
			$newtransfernumber = "SM/".date('dmy')."/".($lastnumber+1);
			return $newtransfernumber;
		}
	}

	public function getStockTransfer(Request $request)
	{
		$sm = stockmovement::where('stock_movement_id', $request->id);
		$from = $sm->first()->warehouse_from;
		$to = $sm->first()->warehouse_to;
		$stockmovement = stockmovement::
		with(['details.product.qty_from' => function ($query) use ($from) {
			$query->where('warehouse_id', $from);
		}])
		->with(['details.product.qty_to' => function ($query) use ($to) {
			$query->where('warehouse_id', $to);
		}])
		->with('details.detailpo')
		->where('stock_movement_id', $request->id)
		->first();

		return $stockmovement;
	}

	public function createStockTransfer(Request $request)
	{
		$stockmovement = new stockmovement;
		$stockmovement->warehouse_from = $request->gudangfrom;
		$stockmovement->warehouse_to = $request->gudangto;
		$stockmovement->date_stock_movement = date('Y-m-d', strtotime($request->tglsm));
		$stockmovement->stock_movement_number = $request->nostocktransfer;
		$stockmovement->driver_name = $request->sopir;
		$stockmovement->vehicle_number = $request->nopol;
		$stockmovement->is_received = 0;
		$stockmovement->status = 1;
		$stockmovement->save();

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			$detailsm = new detailstockmovement;
			$detailsm->stock_movement_id = $stockmovement->stock_movement_id;
			$detailsm->purchase_order_details_id = $request->detailpo[$i];
			$detailsm->product_id = $request->idbarang[$i];
			$detailsm->faktur_number = $request->fakturnumber[$i];
			$detailsm->note= $request->note[$i];
			$detailsm->quantity = $request->qty[$i];
			$detailsm->status = 1;
			$detailsm->save();

			productwarehouse::where('product_id',$request->idbarang[$i])->where('warehouse_id', $request->gudangfrom)->update([
				'quantity' => DB::raw('quantity-'.$request->qty[$i]),
			]);
		}

	}

	public function acceptStockTransfer(Request $request)
	{
		// return $request->all();
		$stockmovement = stockmovement::find($request->id);
		$stockmovement->is_received = 1;
		$stockmovement->save();

		$warehouseto = $stockmovement->warehouse_to;

		$sm = stockmovement::with('details')->where('stock_movement_id',$request->id)->first();

		foreach ($sm->details as $key => $value) {
			productwarehouse::where('product_id',$value->product_id)->where('warehouse_id', $warehouseto)->update([
				'quantity' => DB::raw('quantity+'.$value->quantity),
			]);
		}
	}

	public function updateStockTransfer(Request $request)
	{
		// return $request->all();
		$stockmovement = stockmovement::find($request->idstocktransfer);

		$stockmovement->date_stock_movement = date('Y-m-d', strtotime($request->tglsm));
		$lastwarehousefrom = $stockmovement->warehouse_from;
		$lastwarehouseto = $stockmovement->warehouse_to;

		foreach ($stockmovement->details as $key => $value) {
			$lastqty = $value->quantity;
			$product = $value->product_id;

			if($stockmovement->is_received == 0){
				productwarehouse::where('product_id',$product)->where('warehouse_id', $lastwarehousefrom)->update([
					'quantity' => DB::raw('quantity+'.$lastqty),
				]);
			}else{
				productwarehouse::where('product_id',$product)->where('warehouse_id', $lastwarehousefrom)->update([
					'quantity' => DB::raw('quantity+'.$lastqty),
				]);

				productwarehouse::where('product_id',$product)->where('warehouse_id', $lastwarehouseto)->update([
					'quantity' => DB::raw('quantity-'.$lastqty),
				]);
			}
		}

		detailstockmovement::where('stock_movement_id',$request->idstocktransfer)->update(['status'=>2]);

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			$detailsm = new detailstockmovement;
			$detailsm->stock_movement_id = $stockmovement->stock_movement_id;
			$detailsm->purchase_order_details_id = $request->detailpo[$i];
			$detailsm->product_id = $request->idbarang[$i];
			$detailsm->faktur_number = $request->fakturnumber[$i];
			$detailsm->note= $request->note[$i];
			$detailsm->quantity = $request->qty[$i];
			$detailsm->status = 1;
			$detailsm->save();

			productwarehouse::where('product_id',$request->idbarang[$i])->where('warehouse_id', $request->gudangfrom)->update([
				'quantity' => DB::raw('quantity-'.$request->qty[$i]),
			]);
		}

		$stockmovement->save();
	}

	public function deleteStockTransfer(Request $request)
	{
		$sm = stockmovement::with('details')->where('stock_movement_id',$request->id)->first();
		$lastwarehousefrom = $sm->warehouse_from;
		$lastwarehouseto = $sm->warehouse_to;

		foreach ($sm->details as $key => $value) {
			$lastqty = $value->quantity;
			$product = $value->product_id;

			if($sm->is_received == 0){
				productwarehouse::where('product_id',$product)->where('warehouse_id', $lastwarehousefrom)->update([
					'quantity' => DB::raw('quantity+'.$lastqty),
				]);
			}else{
				productwarehouse::where('product_id',$product)->where('warehouse_id', $lastwarehousefrom)->update([
					'quantity' => DB::raw('quantity+'.$lastqty),
				]);

				productwarehouse::where('product_id',$product)->where('warehouse_id', $lastwarehouseto)->update([
					'quantity' => DB::raw('quantity-'.$lastqty),
				]);
			}
		}

		detailstockmovement::where('stock_movement_id',$request->id)->update(['status'=>2]);

		$sm->status = 2;
		$sm->save();
	}

	public function showKartuStockPage(Request $request)
	{
		$data['barang'] = product::all();
		$data['gudang'] = warehouse::all();
		return view('stock.kartu_stock', compact('data'));
	}

	public function getKartuStock(Request $request)
	{
		$tglawal = date('Y-m-d', strtotime($request->tglawal));
		$tglakhir = date('Y-m-d' ,strtotime($request->tglakhir));

		$gr = goodreceive::leftjoin('good_receive_details','good_receive.good_receive_id','=','good_receive_details.good_receive_id')
			->where('good_receive_details.product_id',$request->barang)
			->where('good_receive.status','<>',2)
			->whereBetween('good_receive.good_receive_date', [$tglawal, $tglakhir])
			->where('good_receive_details.status','<>',2)
			->selectraw("good_receive.good_receive_id as id, good_receive.good_receive_code as tcode, good_receive.good_receive_date as date, good_receive_details.quantity_received as qty, 'in' as stat");
		$do = deliveryorder::leftjoin('delivery_order_details','delivery_order_details.delivery_order_id','=','delivery_order.delivery_order_id')
			->where('delivery_order_details.product_id',$request->barang)
			->where('delivery_order.status','<>',2)
			->whereBetween('delivery_order.date_delivery', [$tglawal, $tglakhir])
			->where('delivery_order_details.status','<>',2)
			->selectraw("delivery_order.delivery_order_id, delivery_order.delivery_order_number, delivery_order.date_delivery, delivery_order_details.quantity_sent, 'out' as stat");
		// get hanya retur yang kemabliin barang
		$returout = purchasereturn::leftjoin('purchase_retur_details','purchase_retur.purchase_retur_id','=', 'purchase_retur_details.purchase_retur_id')
			->where('purchase_retur_details.product_id',$request->barang)
			->where('purchase_retur.retur_type_id',4)
			->where('purchase_retur.status','<>',2)
			->whereBetween('date_retur', [$tglawal, $tglakhir])
			->where('purchase_retur_details.status','<>',2)
			->selectraw("purchase_retur.purchase_retur_id, purchase_retur.retur_number, purchase_retur.date_retur, purchase_retur_details.qty, 'out' as stat");

		$smout = stockmovement::leftjoin('stock_movement_details','stock_movement.stock_movement_id','=','stock_movement_details.stock_movement_id')
			->selectraw("stock_movement.stock_movement_id, stock_movement.stock_movement_number, stock_movement.date_stock_movement, stock_movement_details.quantity, 'out' as stat")
			->where('stock_movement_details.product_id',$request->barang)
			->whereBetween('stock_movement.date_stock_movement',[$tglawal, $tglakhir])
			->where('stock_movement.status','<>',2);

		$smin = stockmovement::leftjoin('stock_movement_details','stock_movement.stock_movement_id','=','stock_movement_details.stock_movement_id')
			->selectraw("stock_movement.stock_movement_id, stock_movement.stock_movement_number, stock_movement.date_stock_movement, stock_movement_details.quantity, 'in' as stat")
			->where('stock_movement_details.product_id',$request->barang)
			->whereBetween('stock_movement.date_stock_movement',[$tglawal, $tglakhir])
			->where('stock_movement.status','<>',2);

		$scout = stockcorrection::selectraw("stock_correction.stock_correction_id, stock_correction.stock_correction_number, stock_correction.date_stock_correction, (stock_correction.projected_qty - stock_correction.actual_qty) as quantity, 'out' as stat")
			->where('stock_correction.product_id',$request->barang)
			->whereBetween('stock_correction.date_stock_correction',[$tglawal, $tglakhir])
			->whereColumn('stock_correction.projected_qty','>','stock_correction.actual_qty');

		$scin = stockcorrection::selectraw("stock_correction.stock_correction_id, stock_correction.stock_correction_number, stock_correction.date_stock_correction, (stock_correction.actual_qty - stock_correction.projected_qty) as quantity, 'in' as stat")
			->where('stock_correction.product_id',$request->barang)
			->whereBetween('stock_correction.date_stock_correction',[$tglawal, $tglakhir])
			->whereColumn('stock_correction.projected_qty','<','stock_correction.actual_qty');

		$saldogr = goodreceive::leftjoin('good_receive_details','good_receive.good_receive_id','=','good_receive_details.good_receive_id')
			->where('good_receive_details.product_id',$request->barang)
			->where('good_receive.status','<>',2)
			->where('good_receive_details.status','<>',2)
			->where('good_receive.good_receive_date','<',$tglawal)
			->selectraw("sum(good_receive_details.quantity_received) as saldo");

		$saldoreturout = purchasereturn::leftjoin('purchase_retur_details','purchase_retur.purchase_retur_id','=', 'purchase_retur_details.purchase_retur_id')
			->where('purchase_retur_details.product_id',$request->barang)
			->where('purchase_retur.retur_type_id',4)
			->where('purchase_retur.status','<>',2)
			->where('purchase_retur_details.status','<>',2)
			->where('purchase_retur.date_retur','<',$tglawal)
			->selectraw("sum(purchase_retur_details.qty) as saldo");

		$saldodo = deliveryorder::leftjoin('delivery_order_details','delivery_order_details.delivery_order_id','=','delivery_order.delivery_order_id')
			->where('delivery_order_details.product_id',$request->barang)
			->where('delivery_order.status','<>',2)
			->where('delivery_order.date_delivery','<',$tglawal)
			->where('delivery_order_details.status','<>',2)
			->selectraw("sum(delivery_order_details.quantity_sent) as saldo");

		$saldosmin = stockmovement::leftjoin('stock_movement_details','stock_movement.stock_movement_id','=','stock_movement_details.stock_movement_id')
			->selectraw("sum(stock_movement_details.quantity)as saldo")
			->where('stock_movement_details.product_id',$request->barang)
			->where('stock_movement.date_stock_movement','<',$tglawal)
			->where('stock_movement.status','<>',2);

		$saldosmout = stockmovement::leftjoin('stock_movement_details','stock_movement.stock_movement_id','=','stock_movement_details.stock_movement_id')
			->selectraw("sum(stock_movement_details.quantity) as saldo")
			->where('stock_movement_details.product_id',$request->barang)
			->where('stock_movement.date_stock_movement','<',$tglawal)
			->where('stock_movement.status','<>',2);

		$saldoscout = stockcorrection::selectraw("sum((stock_correction.projected_qty - stock_correction.actual_qty)) as saldo")
			->where('stock_correction.product_id',$request->barang)
			->where('stock_correction.date_stock_correction','<',$tglawal)
			->where('stock_correction.status','<>',2)
			->whereColumn('stock_correction.projected_qty','>','stock_correction.actual_qty');

		$saldoscin = stockcorrection::selectraw("sum((stock_correction.actual_qty - stock_correction.projected_qty)) as saldo")
			->where('stock_correction.product_id',$request->barang)
			->whereColumn('stock_correction.projected_qty','<','stock_correction.actual_qty')
			->where('stock_correction.date_stock_correction','<',$tglawal)
			->where('stock_correction.status','<>',2);

		if ($request->gudang !== "0") {
			$gr = $gr->where('good_receive_details.warehouse_id', $request->gudang);
			$do = $do->where('delivery_order.warehouse_id', $request->gudang);
			$smout = $smout->where('stock_movement.warehouse_from',$request->gudang);
			$smin = $smin->where('stock_movement.warehouse_to',$request->gudang);
			$scout = $scout->where('stock_correction.warehouse_id',$request->gudang);
			$scin = $scin->where('stock_correction.warehouse_id',$request->gudang);
			$returout = $returout->where('purchase_retur.warehouse_id',$request->gudang);

			$saldogr = $saldogr->where('good_receive_details.warehouse_id', $request->gudang)->first();
			$saldodo = $saldodo->where('delivery_order.warehouse_id', $request->gudang)->first();
			$saldosmout = $saldosmout->where('stock_movement.warehouse_from',$request->gudang)->first();
			$saldosmin = $saldosmin->where('stock_movement.warehouse_to',$request->gudang)->first();
			$saldoscin = $saldoscin->where('stock_correction.warehouse_id',$request->gudang)->first();
			$saldoscout = $saldoscout->where('stock_correction.warehouse_id',$request->gudang)->first();
			$saldoreturout = $saldoreturout->where('purchase_retur.warehouse_id',$request->gudang)->first();

			$saldogr = $saldogr->saldo ? $saldogr->saldo : 0;
			$saldodo = $saldodo->saldo ? $saldodo->saldo : 0;
			$saldosmin = $saldosmin->saldo ? $saldosmin->saldo : 0;
			$saldosmout = $saldosmout->saldo ? $saldosmout->saldo : 0;
			$saldoscin = $saldoscin->saldo ? $saldoscin->saldo : 0;
			$saldoscout = $saldoscout->saldo ? $saldoscout->saldo : 0;
			$saldoreturout = $saldoreturout->saldo ? $saldoscout->saldo : 0;

			// echo $saldogr . "<br>";
			// echo $saldodo . "<br>";
			// echo $saldosmin . "<br>";
			// echo $saldosmout . "<br>";
			// echo $saldoscin . "<br>";
			// echo $saldoscout . "<br>";

			$finalsaldo = $saldogr + $saldosmin+ $saldoscin - $saldodo - $saldosmout - $saldoscout - $saldoreturout;

			$overall = $gr->union($do)->union($smin)->union($smout)->union($scin)->union($scout)->union($returout)->orderby('date','asc')->orderby('stat','asc')->get();
		}else{
			$saldogr = $saldogr->first()->saldo;
			$saldodo = $saldodo->first()->saldo;
			$saldoscin = $saldoscin->first()->saldo;
			$saldoscout = $saldoscout->first()->saldo;
			$saldoreturout = $saldoreturout->first()->saldo;

			$finalsaldo = $saldogr + $saldoscin - $saldodo - $saldoscout - $saldoreturout;

			$small = stockmovement::leftjoin('stock_movement_details','stock_movement.stock_movement_id','=','stock_movement_details.stock_movement_id')
			->selectraw("stock_movement.stock_movement_id, stock_movement.stock_movement_number, stock_movement.date_stock_movement, stock_movement_details.quantity, 'inout' as stat")
				->where('stock_movement_details.product_id',$request->barang)
				->where('stock_movement.status','<>',2)
				->whereBetween('stock_movement.date_stock_movement',[$tglawal, $tglakhir]);
			$overall = $gr->union($do)->union($small)->union($scin)->union($scout)->union($returout)->orderby('date')->get();
		}

		$viewoverall = view('stock.table-ks-overall', compact('overall','finalsaldo','tglawal'))->render();
		return compact('viewoverall');
	}

	public function showStockCorrection(Request $request)
	{
		$data['barang'] = product::all();
		$data['gudang'] = warehouse::all();
		$data['stockcorrection'] = stockcorrection::with('account')
			->with('warehouse')
			->leftjoin('product','stock_correction.product_id','=','product.product_id')
			->orderBy('is_approved','asc')
			->orderBy('stock_correction_id','desc')
			->limit(1000)
			->get();
		return view('stock.stock_correction', compact('data'));
	}

	public function getStockInWarehouse(Request $request)
	{
		$stock = product::leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')->where('product.product_id',$request->barang)->leftjoin('warehouse','warehouse.warehouse_id','=','product_warehouse.warehouse_id')
			->where('product_warehouse.warehouse_id',$request->gudang)
			->first();
		return $stock;
	}

	public function getLastCorrectionNumber(Request $request)
	{
		$lastcorrectiontoday = stockcorrection::where('stock_correction_number','like','SC/'.date('dmy').'/%')->orderby('stock_correction_id','desc')->first();
		if(empty($lastcorrectiontoday))
		{
			$newcorrectionnumber = "SC/".date('dmy')."/1";
			return $newcorrectionnumber;
		}
		else{
			$tmpcorrection = explode('/',$lastcorrectiontoday->stock_correction_number);
			$lastnumber = $tmpcorrection[2];
			$newcorrectionnumber = "SC/".date('dmy')."/".($lastnumber+1);
			return $newcorrectionnumber;
		}
	}

	public function createStockCorrection(Request $request)
	{
		$stockcorrection = new stockcorrection;
		$stockcorrection->product_id = $request->barang;
		$stockcorrection->warehouse_id = $request->gudang;
		$stockcorrection->stock_correction_number = $request->nostockcorrection;
		$stockcorrection->date_stock_correction = date('Y-m-d', strtotime($request->tglsc));
		$stockcorrection->projected_qty = $request->projectedqty;
		$stockcorrection->actual_qty = $request->actualqty;
		$stockcorrection->note = $request->notes;
		$stockcorrection->created_by = Session('user')->account_id;
		$stockcorrection->is_approved = 0;
		$stockcorrection->status = 1;
		$stockcorrection->save();
	}

	public function approveStockCorrection(Request $request){
		$stock = stockcorrection::where('stock_correction_id',$request->id)->first();
		$stock->is_approved = 1;
		$stock->update();

		if($stock->projected_qty > $stock->actual_qty)
		{
			$difference = $stock->projected_qty - $stock->actual_qty;
			productwarehouse::where('product_id', $stock->product_id)
				->where('warehouse_id',$stock->warehouse_id)
				->update([
					'quantity' => DB::raw('quantity-'.$difference),
				]);
		}else{
			$difference = $stock->actual_qty - $stock->projected_qty;
			productwarehouse::where('product_id',$stock->product_id)
				->where('warehouse_id',$stock->warehouse_id)
				->update([
					'quantity' => DB::raw('quantity+'.$difference),
				]);
		}
	}
}