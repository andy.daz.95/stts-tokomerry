<?php
namespace App\Http\Controllers;

use App\detaildraftdelivery;
use App\draftdelivery;
use App\shippingterm;
use App\Traits\SalesInvoiceTrait;
use App\Traits\SalesPaymentTrait;
use App\unitchild;
use Illuminate\Http\Request;
use App\Http\Controllers\SalesInvoiceController;
use App\Http\Controllers\SalesPaymentController;
use App\customer;
use App\paymenttype;
use App\salesorder;
use App\account;
use App\salesinvoice;
use App\salespayment;
use App\detailsalesorder;
use App\log;
use App\product;
use App\warehouse;
use App;
use Indonesia;
use DB;
use DataTable;
use DOMPDF;
use App\Traits\DraftDeliveryTrait;
use App\Traits\LogTrait;

class SalesOrderController extends Controller
{
	use DraftDeliveryTrait;
	use LogTrait;
	use SalesInvoiceTrait;
	use SalesPaymentTrait;

	public function showSalesOrderPage()
	{
		$customer = customer::where('status','<>',2)->get();
		$payment_term = paymenttype::all();
		$sales = account::has('sales')->get();
		$shippingterm = shippingterm::where('shipping_term_name','<>','retur')->get();
		$warehouse = warehouse::whereRaw("warehouse_name NOT LIKE '%Parangloe%'")->get();

		return view('so.sales_order', compact( 'customer','payment_term','sales','shippingterm','warehouse'));
	}

	public function getSalesTable(Request $request){
		$so = salesorder::with(['invoice', 'delivery'])
			->leftjoin('customer','sales_order.customer_id','=','customer.customer_id')
			->leftjoin('payment_type','sales_order.payment_type_id','=','payment_type.payment_type_id')
			->leftjoin('account','account.account_id','=','sales_order.sales_id')
			->selectRaw('sales_order.sales_order_id, sales_order.sales_order_number, sales_order.date_sales_order, sales_order.note, sales_order.grand_total_idr, CONCAT_WS(" ", `first_name`, `last_name`) as fullname, payment_type.payment_description, account.full_name as sales_name, DATE_ADD(sales_order.date_sales_order, INTERVAL sales_order.term_payment DAY) as jatuh_tempo')
			->orderby('sales_order_id',"desc")
			->where('sales_order.status',1);

		if ($request->noso)
		{
			$so = $so->where('sales_order_number','like','%'.$request->noso.'%');
		}
		if($request->customer)
		{
			$so = $so->whereRaw("CONCAT_WS(' ', `first_name`, `last_name`) like '%$request->customer%'");
		}

		return DataTable::of($so)
			->setRowAttr([
				'value' => function($so) {
					return $so->sales_order_id;
				},
			])
			->addColumn('action', function ($so) {
				if($so->delivery->count()>0 ||
					($so->payment_type_id == 2 && $so->invoice)){
					return
						'<a class="btn btn-sm btn-raised orange print-so" target="_blank" href="downloadsalesorder/'.$so->sales_order_id.'"><i class="material-icons">print</i></a>';

				}else{
						return '<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$so->sales_order_id.'"><i class="material-icons">edit</i></a>
					<a class="btn btn-sm btn-raised orange print-so" target="_blank" href="downloadsalesorder/'.$so->sales_order_id.'"><i class="material-icons">print</i></a>
					<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				}
			})
			->editColumn('grand_total_idr', function($so){
				return number_format($so->grand_total_idr);
			})
			->smart(false)
			->make(true);
	}

	public function getLastSalesNumber()
	{
		$lastsalestoday = salesorder::where('sales_order_number','like','SO/'.date('dmy').'/%')->orderby('sales_order_id','desc')->first();
		if(empty($lastsalestoday))
		{
			$newsonumber = "SO/".date('dmy')."/1";
			return $newsonumber;
		}
		else{
			$tmpsales = explode('/',$lastsalestoday->sales_order_number);
			$lastnumber = $tmpsales[2];
			$newsonumber = "SO/".date('dmy')."/".($lastnumber+1);
			return $newsonumber;
		}
	}

	public function getSalesOrder(Request $request)
	{
		$so = salesorder::leftjoin('sales_order_details','sales_order.sales_order_id','=','sales_order_details.sales_order_id')
			->leftjoin('product','product.product_id','=','sales_order_details.product_id')
			->where('sales_order.sales_order_id',$request->id)
			->where('sales_order.status','<>',2)
			->where('sales_order_details.status','<>',2)
			->select('sales_order.*','sales_order_details.*','product.product_name as p_name','product.is_faktur')
			->get();

		return $so;
	}

	public function createSalesOrder(Request $request)
	{
		$lastnumber = $this->getLastSalesNumber();
		$total= array_sum($request->subtotal);
		$tax = 0;

		$salesorder = new salesorder;
		$salesorder->customer_id = $request->pelanggan;
		$salesorder->payment_type_id = $request->pembayaran;
		$salesorder->shipping_term_id = $request->deliverytype;
		$salesorder->sales_id = $request->sales;
		$salesorder->sales_order_number = $lastnumber;
		$salesorder->date_sales_order = date('Y-m-d', strtotime($request->tglso));
		$salesorder->date_delivery_order = date('Y-m-d', strtotime('+7 day',strtotime($request->tglso)));
		$salesorder->term_payment = $request->terms;
		$salesorder->sub_total = $total;
		$salesorder->note = $request->notes;
		$salesorder->tax = $tax;
		$salesorder->grand_total_idr = $request->grandtotal;
		$salesorder->price_type = $request->pricetype;
		$salesorder->account_id = Session('user')->account_id;
		$salesorder->status = 1;
		$salesorder->save();

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			$hpp = product::where('product_id',$request->idbarang[$i])->first()->hpp();
			$cogs = $hpp * $request->qty[$i];

			//kalau unit child yang dipake maka hpp nya dibagi multipliernya juga
			//karena hpp nya sesuai dengan unit saat pembelian
			//yang mana yang dipakai itu unit parent
			if($request->type[$i] ==  'child'){
				$multiplier = unitchild::where('unit_child_id',$request->unit[$i])->first()->multiplier;
				$hpp = $hpp / $multiplier;
			}
			$cogs = $hpp * $request->qty[$i];

			$detailso = new detailsalesorder;
			$detailso->sales_order_id = $salesorder->sales_order_id;
			$detailso->product_id = $request->idbarang[$i];
			$detailso->unit_id = $request->unit[$i];
			$detailso->unit_type = $request->type[$i];
			$detailso->starting_price = $request->harga[$i];
			$detailso->price_reduction = $request->potongan[$i]? $request->potongan[$i] : 0;
			$detailso->price = $request->hargasatuan[$i];
			$detailso->quantity = $request->qty[$i];
			$detailso->weight = $request->weight[$i];
			$detailso->sub_total = $request->subtotal[$i];
			$detailso->total_price = $request->subtotal[$i];
			$detailso->hpp = $hpp;
			$detailso->cogs = $cogs;
			$detailso->detail_note = $request->detailnote[$i];
			$detailso->is_faktur = product::find($request->idbarang[$i])->is_faktur;
			$detailso->status = 1;
			$detailso->save();
		}
		$log = $this->createLog("menambahkan","Sales Order", $salesorder->sales_order_number);

		//Kalau tipe pengirimananya DO
		//Buat DO untuk setiap gudang yang diisi di modal

		if($request->deliverytype == 1) // DO
		{
			$count['toko'] = array_sum($request->qtytoko);
			$count['pasar'] = array_sum($request->qtypasar);
			$count['ponci'] = array_sum($request->qtyponci);

			$this->createDO($count, $salesorder, $request);

			// Kalau Tipe Pembayarannya Cash
			// Sekaligus dibuat Invoice dan Pembayarananya
			if($request->pembayaran == 1) // Cash
			{
				$salesinvoice= $this->createSalesInvoice($salesorder->sales_order_id, $request->grandtotal);

				customer::where('customer_id',$request->pelanggan)->update([
					'total_balance' => DB::raw('total_balance-'.$request->grandtotal),
				]);

				$log = $this->createLog("menambahkan","Sales Invoice", $salesinvoice->invoice_sales_number);

				$salespayment = $this->createSalesPayment($request->pelanggan, $salesinvoice->invoice_sales_id, $request->tglso, $request->grandtotal, 'tunai');

				customer::where('customer_id',$request->pelanggan)->update([
					'total_balance' => DB::raw('total_balance+'.$request->grandtotal),
				]);

				$log = $this->createLog("menambahkan","Sales Payment", $salespayment->payment_sales_number);
			}
		}
		else if($request->deliverytype == 2)//Kalau Tipe Antaran
		{
			$lastnumber = $this->lastAntaranNumber();
			$draft = $this->createDraftDelivery('so', $salesorder->sales_order_id, $salesorder->customer_id, $lastnumber, $salesorder->sales_id, null, $salesorder->date_sales_order, $request->deliverytype, $request->alamatantaran);
			$details = detailsalesorder::where('sales_order_id',$salesorder->sales_order_id)
				->where('status',1)
				->get();

			foreach ($details as $key => $value)
			{
				$detaildraft = $this->createDetailDraftDelivery($draft->draft_delivery_order_id, $value->sales_order_details_id, $value->unit_id, $value->unit_type, $value->product_id, $value->quantity);
			}

			if($request->totalbayarantaran > 0)
			{
				$salesinvoice = $this->createSalesInvoice($salesorder->sales_order_id, $request->grandtotal);

				customer::where('customer_id',$request->pelanggan)->update([
					'total_balance' => DB::raw('total_balance-'.$request->grandtotal),
				]);
				$log = $this->createLog("menambahkan", "Sales Invoice", $salesinvoice->invoice_sales_number);


				$salespayment = $this->createSalesPayment($request->pelanggan, $salesinvoice->invoice_sales_id, $request->tglso, $request->totalbayarantaran, 'tunai');

				customer::where('customer_id',$request->pelanggan)->update([
					'total_balance' => DB::raw('total_balance+'.$request->totalbayarantaran),
				]);

				$log = $this->createLog("menambahkan", "Sales Payment", $salespayment->payment_sales_number);
			}
		}
		return $salesorder->sales_order_number;
	}

	public function updateSalesOrder(Request $request)
	{
		$salesorder = salesorder::find($request->idso);
		$lasttotal = $salesorder->grand_total_idr;

		$salesorder->date_sales_order = date('Y-m-d', strtotime($request->tglso));
		$salesorder->grand_total_idr = $request->grandtotal;
		$salesorder->save();

		detailsalesorder::where('sales_order_id',$request->idso)->update(['status'=>2]);

		for ($i=0; $i <sizeof($request->idbarang); $i++)
		{
			$hpp = product::where('product_id',$request->idbarang[$i])->first()->hpp();
			$cogs = $hpp * $request->qty[$i];

			//kalau unit child yang dipake maka hpp nya dibagi multipliernya juga
			//karena hpp nya sesuai dengan unit saat pembelian
			//yang mana yang dipakai itu unit parent
			if($request->type[$i] ==  'child'){
				$multiplier = unitchild::where('unit_child_id',$request->unit[$i])->first()->multiplier;
				$hpp = $hpp / $multiplier;
			}
			$cogs = $hpp * $request->qty[$i];

			$detailso = new detailsalesorder;
			$detailso->sales_order_id = $salesorder->sales_order_id;
			$detailso->product_id = $request->idbarang[$i];
			$detailso->unit_id = $request->unit[$i];
			$detailso->unit_type = $request->type[$i];
			$detailso->starting_price = $request->harga[$i];
			$detailso->price_reduction = $request->potongan[$i]? $request->potongan[$i] : 0;
			$detailso->price = $request->hargasatuan[$i];
			$detailso->quantity = $request->qty[$i];
			$detailso->weight = $request->weight[$i];
			$detailso->sub_total = $request->subtotal[$i];
			$detailso->total_price = $request->subtotal[$i];
			$detailso->hpp = $hpp;
			$detailso->cogs = $cogs;
			$detailso->detail_note = $request->detailnote[$i];
			$detailso->is_faktur = product::find($request->idbarang[$i])->is_faktur;
			$detailso->status = 1;
			$detailso->save();
		}

		$draft = draftdelivery::where('sales_order_id',$request->idso)->get();

		foreach($draft as $key => $value)
		{
			detaildraftdelivery::where('draft_delivery_order_id', $value->draft_delivery_order_id)
				->update([
					'status' => 2
				]);

			draftdelivery::where('draft_delivery_order_id',$value->draft_delivery_order_id)->update(
				[
					'status' => 2
				]);
		}

		if($request->deliverytype == 1)
		{
			$count['toko'] = array_sum($request->qtytoko);
			$count['pasar'] = array_sum($request->qtypasar);
			$count['ponci'] = array_sum($request->qtyponci);

			$this->createDO($count, $salesorder, $request);
		}else{
			foreach ($salesorder->details as $key => $value)
			{
				$detaildraft = $this->createDetailDraftDelivery($draft->draft_delivery_order_id, $value->sales_order_details_id, $value->unit_id, $value->unit_type, $value->product_id, $value->quantity);
			}
		}


		if($salesorder->payment_type_id == 1) // Cash
		{
			$invoice = salesinvoice::where('sales_order_id',$salesorder->sales_order_id)->where('status','<>',2);
			$total = $invoice->first()->total_amount;
			$invoice->update(['total_amount'=>$request->grandtotal]);

			customer::where('customer_id',$request->pelanggan)->update([
				'total_balance' => DB::raw('total_balance+'.$total),
			]);

			customer::where('customer_id',$request->pelanggan)->update([
				'total_balance' => DB::raw('total_balance-'.$request->grandtotal),
			]);

			$log = $this->createLog("mengubah","Sales Invoice", $invoice->first()->invoice_sales_number);

			$payment = salespayment::where('total_paid',$lasttotal)
				->where('invoice_sales_id',$invoice->first()->invoice_sales_id)
				->first();

			if($payment->count() > 0){
				$totalpayment = $payment->first()->total_paid;
				$payment->total_paid = $request->grandtotal;
				$payment->update();

				customer::where('customer_id',$request->pelanggan)->update([
					'total_balance' => DB::raw('total_balance-'.$totalpayment),
				]);

				customer::where('customer_id',$request->pelanggan)->update([
					'total_balance' => DB::raw('total_balance+'.$request->grandtotal),
				]);

				$log = $this->createLog("mengubah","Sales Payment", $payment->payment_sales_number);
			}
		}
	}

	public function deleteSalesOrder(Request $request)
	{
		$salesorder = salesorder::where('sales_order_id', $request->id)->first();
		$paymenttype = $salesorder->payment_type_id;

		if($paymenttype == 1){
			$invoice = salesinvoice::where('sales_order_id',$request->id)->where('status','<>',2)->first();
			$total= $invoice->total_amount;

			customer::where('customer_id',$request->pelanggan)->update([
				'total_balance' => DB::raw('total_balance+'.$total),
			]);

			$log = $this->createLog("menghapus","Sales Invoice", $invoice->first()->invoice_sales_number);

			$payment = salespayment::where('total_paid',$total)->where('invoice_sales_id',$invoice->invoice_sales_id)->first();
			$totalpayment = $payment->total_paid;

			customer::where('customer_id',$request->pelanggan)->update([
				'total_balance' => DB::raw('total_balance-'.$totalpayment),
			]);

			$log = $this->createLog("mengubah","Sales Payment", $payment->first()->payment_sales_number);

			salesinvoice::where('invoice_sales_id',$invoice->invoice_sales_id)->update(['status'=>2]);
			salespayment::where('payment_sales_id',$payment->payment_sales_id)->update(['status'=>2]);
		}

		//change so status to 2
		$salesorder->status = 2;
		$salesorder->save();

		$detailsalesorder = detailsalesorder::where('sales_order_id', $salesorder->sales_order_id)->first();
		$detailsalesorder->status = 2;
		$detailsalesorder->save();

		$draft = draftdelivery::where('sales_order_id',$request->id)->get();

		foreach($draft as $key => $value)
		{
			detaildraftdelivery::where('draft_delivery_order_id', $value->draft_delivery_order_id)
				->update([
					'status' => 2
				]);

			draftdelivery::where('draft_delivery_order_id',$value->draft_delivery_order_id)->update(
				[
					'status' => 2
				]);
		}
	}

	public function downloadSalesOrder(Request $request, $id, $size = 'regular')
	{
		$soheader = salesorder::leftjoin('customer','customer.customer_id','=','sales_order.customer_id')
			->leftjoin('payment_type','sales_order.payment_type_id','=','payment_type.payment_type_id')
			->leftjoin('account','sales_order.sales_id','=','account.account_id')
			->where('sales_order.sales_order_id',$id)
			->first();

		$sodetail = detailsalesorder::leftjoin('product','product.product_id','=','sales_order_details.product_id')
			->where('sales_order_id',$id)
			->where('sales_order_details.status','<>',2)
			->select('sales_order_details.*','product.product_name as p_name')
			->get();

		if($size == 'small'){
			$pdf = DOMPDF::loadView('so.so_pdf_small', compact('soheader','sodetail'))
				->setPaper(array(0, 0, 270, 391));
			return $pdf->stream('SO.pdf');
		}else{
			$pdf = DOMPDF::loadView('so.so_pdf', compact('soheader','sodetail'))
				->setPaper(array(0, 0, 609, 391));
			return $pdf->stream('SO.pdf');
		}

	}

	protected function createDetailSo($detailData){
		$detailso = new detailsalesorder;
		$detailso->sales_order_id = $detailData['sales_order_id'];
		$detailso->product_id = $detailData['product_id'];
		$detailso->unit_id = $detailData['unit_id'];
		$detailso->unit_type = $detailData['unit_type'];
		$detailso->starting_price = $detailData['starting_price'];
		$detailso->price_reduction = $detailData['price_reduction']? $detailData['price_reduction'] : 0;
		$detailso->price = $detailData['price'];
		$detailso->quantity = $detailData['quantity'];
		$detailso->weight = $detailData['weight'];
		$detailso->sub_total = $detailData['sub_total'];
		$detailso->total_price = $detailData['total_price'];
		$detailso->hpp = $detailData['hpp'];
		$detailso->cogs = $detailData['cogs'];
		$detailso->detail_note = $detailData['detail_note'];
		$detailso->is_faktur = product::find($detailData['product_id'])->is_faktur;
		$detailso->status = 1;
		$detailso->save();
	}

	protected function createDO($count, $salesorder, $request){
		if($count['toko'] > 0){
			$gudang = App\warehouse::where('warehouse_name','like','%toko%')->first()->warehouse_id;
			$lastnumber = $this->lastDraftDeliveryNumber();
			$draft = $this->createDraftDelivery('so',$salesorder->sales_order_id, $salesorder->customer_id, $lastnumber, $salesorder->sales_id, $gudang, $salesorder->date_sales_order, $request->deliverytype, '');
			foreach ($request->qtytoko as $key => $value)
			{
				if($value>0)
				{
					$detailso = detailsalesorder::where('sales_order_id',$salesorder->sales_order_id)
						->where('product_id', $request->idbarang[$key])
						->where('status',1)
						->first();
					$detaildraft = $this->createDetailDraftDelivery($draft->draft_delivery_order_id, $detailso->sales_order_details_id ,$request->unit[$key], $request->type[$key], $request->idbarang[$key], $value);
				}
			}
		}

		if($count['pasar'] > 0){
			$lastnumber = $this->lastDraftDeliveryNumber();
			$gudang = App\warehouse::where('warehouse_name','like','%pasar%')->first()->warehouse_id;
			$draft = $this->createDraftDelivery('so',$salesorder->sales_order_id, $salesorder->customer_id, $lastnumber, $salesorder->sales_id, $gudang, $salesorder->date_sales_order, $request->deliverytype, '');
			foreach ($request->qtypasar as $key => $value)
			{
				if($value>0)
				{
					$detailso = detailsalesorder::where('sales_order_id',$salesorder->sales_order_id)
						->where('product_id', $request->idbarang[$key])
						->where('status',1)
						->first();
					$detaildraft = $this->createDetailDraftDelivery($draft->draft_delivery_order_id, $detailso->sales_order_details_id, $request->unit[$key], $request->type[$key], $request->idbarang[$key], $value);
				}
			}
		}

		if($count['ponci'] > 0){
			$lastnumber = $this->lastDraftDeliveryNumber();
			$gudang = App\warehouse::where('warehouse_name','like','%ponci%')->first()->warehouse_id;
			$draft = $this->createDraftDelivery('so',$salesorder->sales_order_id, $lastnumber, $salesorder->sales_id, $gudang, $salesorder->date_sales_order, $request->deliverytype, '');
			foreach ($request->qtyponci as $key => $value)
			{
				if($value>0)
				{
					$detailso = detailsalesorder::where('sales_order_id',$salesorder->sales_order_id)
						->where('product_id', $request->idbarang[$key])
						->where('status',1)
						->first();
					$detaildraft = $this->createDetailDraftDelivery($draft->draft_delivery_order_id, $detailso->sales_order_details_id, $request->unit[$key], $request->type[$key], $request->idbarang[$key], $value);
				}
			}
		}
	}
}
