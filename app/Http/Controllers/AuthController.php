<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\account;
use App\whitelist;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $ip = $request->ip;
        // return $ip;
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            $user = Auth::user();
            $role = account::find($user->account_id)->roles->first();

            if($role->role_id != 1) 
            {
            	if($request->access != "true")
							{
								$errors = ['email' => "Anda tidak dapat masuk ke dalam sistem"];
								return redirect('login')->withErrors($errors);
							}
//                $count = whitelist::where('ip',$ip)->count();
//                if($count == 0)
//                {
//                    $errors = ['email' => "Anda tidak dapat masuk ke dalam sistem"];
//                    return redirect('login')->withErrors($errors);
//                }
            }
            return redirect()->intended('/');
        }else{
            $errors = ['email' => trans('auth.failed')];
            return redirect('login')->withErrors($errors);
        }
    }

    public function showGrantAccessPage(){
    	return view('grant_access');
		}

		public function validMaster(Request $request){
			$email = $request->email;
			$password = $request->password;

			if (Auth::attempt(['email' => $email, 'password' => $password])) {
				$user = Auth::user();
				$role = account::find($user->account_id)->roles->first();

				if($role->role_id != 1){
					return 'false';
				}else{
					return 'true';
				}
			}else{
				return 'false';
			}
		}
}