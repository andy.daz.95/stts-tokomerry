<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kategori_barang;

class KategoriBarangController extends Controller
{
    public function showKategoriBarangPage()
    {
    	$kategori = kategori_barang::all();
    	return view('kategori_barang.kategori_barang', compact('kategori'));
    }
    public function createKategoriBarang(Request $request)
	{
		$kategori = new kategori_barang;
		$kategori->description = $request->kategori;
		$kategori->status = 1;
		$kategori->save();
	}

	public function getKategoriData(Request $request){
		$id = $request->id;
		$kategori = kategori_barang::where('product_category_id',$id)->select('*')->first();
		return $kategori;
	}

	public function modalKategoriData(Request $request){
		$id= $request->id;
		$data= kategori_barang::where('product_category_id',$id)->first();
		return view('kategori_barang.modal_kategori', compact('data'));
	}

	public function deleteKategoriBarang(Request $request)
	{
		$id = $request->id;
		kategori_barang::where('product_category_id',$id)->delete();
	}

	public function updateKategoriBarang(Request $request)
	{
		$id = $request->id;
		kategori_barang::where('product_category_id', $id)->update(
			[
				'description' => $request->kategori,
			]
			);
	}
}