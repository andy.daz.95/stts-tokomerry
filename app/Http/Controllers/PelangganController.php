<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\customer;
use App\userbank;
use App\bank;
use App\salespayment;
use App\salesorder;
use Indonesia;
use Excel;
use DB;
use DataTable;

class PelangganController extends Controller
{
    public function showPelangganPage()
    {
//    $customer = customer::where('status',1)->get();
    	$provinsi = Indonesia::allProvinces();
    	$bank = bank::where('status',1)->get();

    	return view('pelanggan.pelanggan', compact('customer','provinsi','bank'));
    }

    public function getPelangganTable(Request $request){
			$customer = customer::where('status',1)
				->selectRaw('*, CONCAT_WS(" ", `first_name`, `last_name`) as fullname');

			if($request->customer)
			{
				$customer = $customer->whereRaw("CONCAT_WS(' ', `first_name`, `last_name`) like '%$request->customer%'");
			}
			$customer = $customer->get();


			return DataTable::of($customer)
				->setRowAttr([
					'value' => function($customer) {
						return $customer->customer_id;
					},
				])
				->addColumn('action', function ($customer) {
					return
						'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$customer->customer_id.'"><i class="material-icons">edit</i></a>
					<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				})
				->smart(false)
				->make(true);
		}

    public function modalPelangganData(Request $request){
        $id= $request->id;
        $data= customer::where('customer_id',$id)->first();

        return view('pelanggan.modal_pelanggan', compact('data'));
    }


    public function getPelangganData(Request $request){
        $id = $request->id;
        $customer = customer::
        leftjoin('user_bank','user_bank.customer_id','=','customer.customer_id')
        ->leftjoin('indonesia_cities as ic','ic.id','=','customer.city_id')
        ->leftjoin('indonesia_provinces as ip','ip.id','=','ic.province_id')
        ->where('customer.customer_id',$id)
        ->select('customer.*','user_bank.user_bank_id', 'ip.id as provinsi')
        ->first();

        if($customer->user_bank_id)
        {
            $bank = userbank::leftjoin('bank','user_bank.bank_id','=','bank.bank_id')
            ->where('user_bank.category_id', 1)
            ->where('user_bank.customer_id', $request->id)
            ->where('user_bank.status', 1)
            ->first();
        }else{
            $bank = [];
        }
        return compact('customer','bank');
    }

    public function getLimitKredit(Request $request){
        $id = $request->id;
        $customer = customer::find($id);
        $limit = $customer->credit_limit;
        $payable = $customer->total_balance;
        return compact('limit', 'payable');
    }

    public function deletePelanggan(Request $request)
    {
        $id = $request->id;
        customer::where('customer_id',$id)->delete();
    }

    public function createPelanggan(Request $request)
    {
        $customer = new customer;
        $customer->account_id = Session::get('user')->account_id;
        $customer->city_id = $request->kota ;
        $customer->first_name = $request->namadepan;
        $customer->last_name = $request->namabelakang;
        $customer->company_name = $request->perusahaan;
        $customer->address = $request->alamat;
        $customer->postal_code = $request->kodepos;
        $customer->phone = $request->telp1;
        $customer->phone2 = $request->telp2;
        $customer->fax = $request->fax;
        $customer->credit_limit = $request->limit;
        $customer->NPWP = $request->npwp;
        $customer->status = 1;
        $customer->save();

        if($request->bank)
        {
            $userbank = new userbank;
            $userbank->customer_id = $customer->customer_id;
            $userbank->category_id = "1";
            $userbank->bank_id = $request->bank;
            $userbank->bank_account = $request->norek;
            $userbank->on_behalf_of = $request->atasnama;
            $userbank->status = 1;
            $userbank->save();
        }
    }

    public function importPelanggan(Request $request){

        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            $sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

            if(!empty($sheet1) && $sheet1->count()){
                foreach ($sheet1 as $key => $value) {
                    $customer = new customer;
                    $customer->account_id = Session::get('user')->account_id;
                    $customer->city_id = $value->kota_id;
                    $customer->first_name = $value->nama_depan;
                    $customer->last_name = $value->nama_belakang ? $value->nama_belakang : '';
                    $customer->company_name = '';
                    $customer->address = $value->alamat;
                    $customer->postal_code = '';
                    $customer->phone = '';
                    $customer->phone2 = '';
                    $customer->fax = '';
                    $customer->credit_limit = '';
                    $customer->NPWP = '';
                    $customer->total_balance = 0;
                    $customer->existing_receiveable = $value->total_hutang;
                    $customer->status = 1;
                    $customer->save();
                }
            }
        }
    }


    public function updatePelanggan(Request $request)
    {
        $id = $request->id;
        customer::where('customer_id', $id)->update(
            [
            'city_id' => $request->kota,
            'first_name' => $request->namadepan,
            'last_name' => $request->namabelakang,
            'company_name' => $request->perusahaan,
            'address' => $request->alamat,
            'postal_code' => $request->kodepos,
            'phone' => $request->telp1,
            'phone2' => $request->telp2,
            'fax' => $request->fax,
            'credit_limit' => $request->limit,
            'NPWP' => $request->npwp,
            ]
        );

        if($request->bank)
        {
            $userbank = userbank::where('customer_id',$id)->where('category_id',1)->where('status',1);
            if($userbank->count() > 0)
            {
                $userbank->update(
                    [
                        'bank_id' => $request->bank,
                        'bank_account' => $request->norek,
                        'on_behalf_of' => $request->atasnama,
                    ]
                );
            }else{
                $userbank = new userbank;
                $userbank->customer_id = $id;
                $userbank->category_id = "1";
                $userbank->bank_id = $request->bank;
                $userbank->bank_account = $request->norek;
                $userbank->on_behalf_of = $request->atasnama;
                $userbank->status = 1;
                $userbank->save();
            }
        }else{
            userbank::where('customer_id',$id)->where('category_id',1)->where('status',1)->update(
                [
                    'status' => 2,
                ]
            );
        }
    }
    public function showBalancePage(Request $request)
    {
        $data['customer'] = customer::all();
        return view('pelanggan.balance', compact('data'));
    }

    public function showBalanceDetail(Request $request)
    {
        $invoice = customer::join('sales_order','sales_order.customer_id','=','customer.customer_id')
            ->leftjoin('invoice_sales','invoice_sales.sales_order_id','=','sales_order.sales_order_id')
            ->where('customer.customer_id',$request->id)
            ->where('invoice_sales.status','<>',2)
            ->where('sales_order.status','<>',2)
            ->select('invoice_sales.invoice_sales_id as transaction_id','invoice_sales_number as description','invoice_sales.total_amount','invoice_sales.created_at as date', DB::raw("'invoice' as 'source'"));

        $balance = salespayment::where('customer_id',$request->id)
            ->select('payment_sales.payment_sales_id as transaction_id','payment_sales.payment_sales_number as description','payment_sales.total_paid as total_amount','payment_sales.created_at as date' , DB::Raw("'payment' as 'source'"))
            ->where('payment_sales.status','<>',2)
            ->union($invoice)
            ->orderby('date')
            ->get();

        // return $balance;
        $view = view('pelanggan.detail-balance', compact('balance'))->render();
        $totalbalance = customer::where('customer.customer_id',$request->id)->first()->total_balance;
        return compact('view','totalbalance');
    }

    public function getTransaction(Request $request){
        $data = salesorder::leftjoin('invoice_sales','invoice_sales.sales_order_id','=','sales_order.sales_order_id')
            ->leftjoin('sales_order_details','sales_order.sales_order_id','=','sales_order_details.sales_order_id')
            ->leftjoin('product','product.product_id','=','sales_order_details.product_id')
            ->leftjoin('payment_type','sales_order.payment_type_id','=','payment_type.payment_type_id')
            ->where('invoice_sales.invoice_sales_id',$request->id)
            ->where('sales_order.status','<>',2)
            ->where('sales_order_details.status','<>',2)
            ->select('sales_order.*','sales_order_details.*','payment_type.payment_description','product.product_name as p_name','product.price_sale','invoice_sales.*')
            ->get();

        return $data;
    }
}
