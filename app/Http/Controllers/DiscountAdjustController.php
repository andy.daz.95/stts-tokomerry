<?php

namespace App\Http\Controllers;

use App\purchasediscount;
use App\purchaseorder;
use Illuminate\Http\Request;

class DiscountAdjustController extends Controller
{
    //
	public function showDiscountAdjustPage(){
		$data['po'] = purchaseorder::whereHas('invoice',function($query){
			$query->whereDoesntHave('payment');
		})->get();

		return view('discount_adjust.discount_adjust', compact('data'));
	}

	public function getDiscountData(Request $request){

		return purchaseorder::with('discount')->where('purchase_order_id', $request->id)->first();
	}

	public function createDiscountAdjust(Request $request){

		purchasediscount::where('purchase_order_id',$request->nopo)->update([
			'status' => 2,
		]);

		$count = 0;
		for($i = 0; $i < sizeof($request->discountamount); $i++){
			if(!empty($request->discountamount[$i]))
			{
				$purchasediscount = new purchasediscount();
				$purchasediscount->purchase_order_id = $request->nopo;
				$purchasediscount->discount = $request->discountamount[$i];
				$purchasediscount->discount_period = $request->discountperiod[$i];
				$purchasediscount->status = 1;
				$purchasediscount->save();
				$count++;
			}
		}

		if($count > 0) $discounttype = $request->discounttype; else $discounttype = 0;

		purchaseorder::where('purchase_order_id',$request->nopo)
			->update([
				'discount_type' => $discounttype,
			]);
	}
}
