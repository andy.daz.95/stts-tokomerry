<?php

namespace App\Http\Controllers;

use App\productchild;
use App\unitchild;
use Illuminate\Http\Request;
use App\productcategory;
use App\satuan;
use App\warehouse;
use App\product;
use App\pricepurchase;
use App\productwarehouse;
use Excel;
use DataTable;

class BarangController extends Controller
{
	public function showBarangPage()
	{
		$gudang = warehouse::where('status',1)->get();
		$productcategory = productcategory::where('status',1)->get();
		$satuan = satuan::where('status',1)->get();
//    	$product = product::leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')
//                ->leftjoin('price_purchase','price_purchase.product_id','=','product.product_id')
//                ->selectRaw('product.*, Sum(quantity)as t_qty, price_purchase.hpp')
//                ->groupby('product.product_id')
//                ->where('product.is_spandek',0)
//                ->where('product.status',1)
//                ->get();

		return view('barang.barang', compact('productcategory','satuan','gudang'));
	}

	public function showSpandekPage()
	{
		$gudang = warehouse::where('status',1)->get();
		$productcategory = productcategory::where('status',1)->get();
		$satuan = satuan::where('status',1)->get();

		return view('barang.spandek', compact('productcategory','satuan','gudang'));
	}

	public function getBarangTable(Request $request){
		$product = product::leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')
			->leftjoin('price_purchase','price_purchase.product_id','=','product.product_id')
			->selectRaw('product.*, Sum(quantity)as t_qty, price_purchase.hpp')
			->groupby('product.product_id')
			->where('product.is_spandek',0)
			->where('product.status',1);

		if($request->name)
		{
			$product = $product->where('product_name','like','%'.$request->name.'%');
		}
		if($request->code)
		{
			$product = $product->where('product_code','like','%'.$request->code.'%');
		}
		$product= $product->get();

		return DataTable::of($product)
			->setRowAttr([
				'value' => function($product) {
					return $product->product_id;
				},
			])
			->addColumn('action', function ($product) {
				return
					'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$product->product_id.'"><i class="material-icons">edit</i></a>
					<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
			})
			->editColumn('is_faktur', function ($product) {
				return $product->is_faktur == 0 ? 'non faktur' : 'faktur';
			})
			->editColumn('hpp', function ($product) {
				return $product->hpp? number_format($product->hpp): 0;
			})
			->editColumn( 'price_sale' , function ($product){
				return number_format($product->price_sale);
			})
			->editColumn( 'price_wholesale' , function ($product){
				return number_format($product->price_wholesale);
			})
			->smart(false)
			->make(true);
	}

	public function getSpandekTable(Request $request){
		$product = product::leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')
			->leftjoin('price_purchase','price_purchase.product_id','=','product.product_id')
			->selectRaw('product.*, Sum(quantity)as t_qty, price_purchase.hpp')
			->groupby('product.product_id')
			->where('product.is_spandek',1)
			->where('product.status',1);

		if($request->name)
		{
			$product = $product->where('product_name','like','%'.$request->name.'%');
		}
		if($request->code)
		{
			$product = $product->where('product_code','like','%'.$request->code.'%');
		}
		$product= $product->get();

		return DataTable::of($product)
			->setRowAttr([
				'value' => function($product) {
					return $product->product_id;
				},
			])
			->addColumn('action', function ($product) {
				return
					'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$product->product_id.'"><i class="material-icons">edit</i></a>
					<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
			})
			->editColumn('is_faktur', function ($product) {
				return $product->is_faktur == 0 ? 'non faktur' : 'faktur';
			})
			->editColumn('hpp', function ($product) {
				return $product->hpp? number_format($product->hpp): 0;
			})
			->editColumn( 'price_sale' , function ($product){
				return number_format($product->price_sale);
			})
			->editColumn( 'price_wholesale' , function ($product){
				return number_format($product->price_wholesale);
			})
			->smart(false)
			->make(true);
	}

	public function createBarang(Request $request)
	{
		$product = new product;
		$product->unit_id = $request->satuan;
		$product->product_category_id = $request->productcategory;
		$product->product_code = $request->kode;
		$product->product_name = $request->nama;
		$product->total_quantity = 0;
		$product->is_faktur = $request->faktur;
		$product->is_spandek = 0;
		$product->weight = $request->weight? $request->weight : 0;
		$product->diameter = $request->diameter ? $request->diameter : 0;
		$product->price_sale = $request->hargajual? $request->hargajual : 0;
		$product->price_wholesale = $request->hargajualpartai? $request->hargajualpartai : 0;
		$product->status = 1;
		$product->save();

		if(isset($request->unitchild))
		{
			$productchild = new productchild;
			$productchild->product_id = $product->product_id;
			$productchild->unit_child_id = $request->unitchild;
			$productchild->price_sale = $request->hargajualchild ? $request->hargajualchild : 0;
			$productchild->price_wholesale = $request->hargajualpartaichild ? $request->hargajualpartaichild : 0;
			$productchild->status = 1;
			$productchild->save();
		}

		$qtygudang  = array_map(function($val){ if($val == null){return 0;}else{return $val;}},$request->qtygudang);

		for ($i=0; $i < sizeof($request->idgudang); $i++) {
			$productwarehouse = new productwarehouse;
			$productwarehouse->product_id = $product->product_id;
			$productwarehouse->warehouse_id = $request->idgudang[$i];
			$productwarehouse->quantity = $qtygudang[$i];
			$productwarehouse->status = 1;
			$productwarehouse->save();
		}

		$this->createhpp($product);
	}

	protected function createhpp($product){
		$pricepurchase = new pricepurchase;
		$pricepurchase->product_id = $product->product_id;
		$pricepurchase->last_quantity = 0;
		$pricepurchase->balance = 0;
		$pricepurchase->hpp = 0;
		$pricepurchase->status = 1;
		$pricepurchase->save();
	}

	public function createSpandek(Request $request){

		$product = new product;
		$product->unit_id = $request->satuan;
		$product->product_category_id = $request->productcategory;
		$product->product_code = $request->kode;
		$product->product_name = $request->nama;
		$product->total_quantity = 0;
		$product->is_faktur = $request->faktur;
		$product->is_spandek = 1;
		$product->weight = $request->weight? $request->weight : 0;
		$product->diameter = $request->diameter ? $request->diameter : 0;
		$product->price_sale = $request->hargajual? $request->hargajual : 0;
		$product->price_wholesale = $request->hargajualpartai? $request->hargajualpartai : 0;
		$product->status = 1;
		$product->save();

		for ($i=0; $i < sizeof($request->idgudang); $i++) {
			$productwarehouse = new productwarehouse;
			$productwarehouse->product_id = $product->product_id;
			$productwarehouse->warehouse_id = $request->idgudang[$i];
			$productwarehouse->quantity = 0;
			$productwarehouse->status = 1;
			$productwarehouse->save();
		}

		$this->createhpp($product);
	}

	public function importBarang(Request $request){
		ini_set('memory_limit', '-1');
		if($request->hasFile('file')){
			$path = $request->file('file')->getRealPath();
			$sheet1 = Excel::selectSheetsByIndex(0)->load($path, function($reader){})->get(); // untuk ambil sheet pertama

			if(!empty($sheet1) && $sheet1->count()){
				foreach ($sheet1 as $key => $value) {
					if ($value->nama_produk != '')
					{
						$product = new product;
						$product->unit_id = $value->unit_id;
						$product->is_faktur = trim($value->jenis_faktur) == '' ? 0 : 1;
						$product->is_spandek = trim($value->spandek) == '' ? 0 : 1;
						$product->product_category_id = 1;
						$product->product_code = $value->kode_produk;
						$product->product_name = $value->nama_produk;
						$product->total_quantity = 0;
						$product->weight = 0;
						$product->diameter = 0;
						$product->price_sale = $value->harga_eceran_unit_utama ? $value->harga_eceran_unit_utama : 0;
						$product->price_wholesale = $value->harga_partai_unit_utama ? $value->harga_partai_unit_utama : 0;
						$product->status = 1;
						$product->save();

						if ($value->rasio != '') {
							$productchild = new productchild;
							$productchild->product_id = $product->product_id;
							$productchild->unit_child_id = unitchild::where('unit_id',$value->unit_id)->first()->unit_child_id;
							$productchild->price_sale = $value->harga_eceran_unit_turunan ? $value->harga_eceran_unit_turunan : 0;
							$productchild->price_wholesale = $value->harga_partai_unit_turunan ? $value->harga_partai_unit_turunan : 0;
							$productchild->status = 1;
							$productchild->save();
						}

						$gudang = warehouse::where('status', 1)->get();
						foreach ($gudang as $key => $valuegudang) {
							$productwarehouse = new productwarehouse;
							$productwarehouse->product_id = $product->product_id;
							$productwarehouse->warehouse_id = $valuegudang->warehouse_id;
							if ($valuegudang->warehouse_name == "Gudang Parangloe") {
								$productwarehouse->quantity = 500;
							} else if ($valuegudang->warehouse_name == "Gudang Pasar") {
								$productwarehouse->quantity = 500;
							} else if ($valuegudang->warehouse_name == "Gudang Ponci") {
								$productwarehouse->quantity = 500;
							}
							$productwarehouse->status = 1;
							$productwarehouse->save();
						}

						$modal = $value->modal? $value->modal :9999;
						$pricepurchase = new pricepurchase;
						$pricepurchase->product_id = $product->product_id;
						$pricepurchase->last_quantity = 1500;
						$pricepurchase->balance = 1500 * $modal;
						$pricepurchase->hpp = $modal;
						$pricepurchase->status = 1;
						$pricepurchase->save();
					}
				}
			}
		}
	}

	public function filterBarang(Request $request){
		$filter = $request->filter;
		$product = product::where('company_name','like','%'.$filter.'%')->paginate(10);

		return view('product.product_table', compact('product'));
	}

	public function getBarangData(Request $request){
		$id = $request->id;
		$product =
			product::with('unit.unit_child')
				->with('product_child')
				->where('product_id',$id)
				->select('*')
				->first();
		$product_warehouse = productwarehouse::where('product_id',$id)->get();

		return compact('product', 'product_warehouse');
	}

	public function modalBarangData(Request $request){
		$id= $request->id;
		$data= product::where('product_id',$id)->first();
		return view('barang.modal_barang', compact('data'));
	}

	public function deleteBarang(Request $request)
	{
		$id = $request->id;
		product::where('product_id',$id)->update([
			'status' => 2,
		]);
	}

	public function updateBarang(Request $request)
	{
		$id = $request->id;

		$product = product::find($id);
		$product->unit_id = $request->satuan;
		$product->product_code = $request->kode;
		$product->product_name = $request->nama;
		$product->is_faktur = $request->faktur;
		$product->weight = $request->weight;
		$product->diameter = $request->diameter;
		$product->price_sale = $request->hargajual? $request->hargajual : 0;
		$product->price_wholesale = $request->hargajualpartai? $request->hargajualpartai : 0;
		$product->save();

		if(isset($request->unitchild))
		{
			$productchild = productchild::where('product_id',$product->product_id)
				->first();

			if(!$productchild)
			{
				$productchild = new productchild();
				$productchild->product_id = $product->product_id;
			}

			$productchild->unit_child_id = $request->unitchild;
			$productchild->price_sale = $request->hargajualchild ? $request->hargajualchild : 0;
			$productchild->price_wholesale = $request->hargajualpartaichild ? $request->hargajualpartaichild : 0;
			$productchild->status = 1;
			$productchild->save();
		}
	}
}