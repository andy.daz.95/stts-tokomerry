<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\purchasepayment;
use App\detailpurchasepayment;
use App\purchaseorder;
use App\purchasediscount;
use App\detailpurchaseorder;
use App\purchaseinvoice;
use App\detailpurchaseinvoice;
use App\log;
use App;
use DB;
use DataTable;
use DOMPDF;

class PurchasePaymentController extends Controller
{
	public function showPurchasePaymentPage(){

		$data['invoice'] = purchaseinvoice::leftjoin('invoice_purchase_details','invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
			->leftjoin(DB::raw("(
                Select sum(paid) as total, payment_purchase.payment_purchase_id, payment_purchase.invoice_purchase_id from payment_purchase left join payment_purchase_details on payment_purchase.payment_purchase_id = payment_purchase_details.payment_purchase_id where payment_purchase.status <> 2 group by payment_purchase.invoice_purchase_id) tablea"
			),'invoice_purchase.invoice_purchase_id','=','tablea.invoice_purchase_id')
			->select('invoice_purchase.invoice_purchase_id', 'invoice_purchase.invoice_purchase_number', 'tablea.total' ,'invoice_purchase_details.total_amount', 'invoice_purchase.purchase_order_id')
			->where('invoice_purchase.status','<>',2)
			->Where(function ($query) {
				$query->whereNull('tablea.payment_purchase_id')
					->orWhereRaw('tablea.total + invoice_purchase.discount_nominal < invoice_purchase_details.total_amount');
			})
			// ->whereNull('tablea.payment_purchase_id')
			// ->orWhereRaw('tablea.total + invoice_purchase.discount_nominal < invoice_purchase_details.total_amount')
			->get();

		// return $data['invoice'];
		return view('purchase_payment.purchase_payment', compact('data'));
	}

	public function getPurchasePaymentTable(Request $request){
		$payment = purchasepayment::leftjoin('payment_purchase_details','payment_purchase.payment_purchase_id','=','payment_purchase_details.payment_purchase_id')
			->leftjoin('invoice_purchase','invoice_purchase.invoice_purchase_id','=','payment_purchase.invoice_purchase_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('payment_purchase.status','<>',2)
			->select('payment_purchase.*', 'payment_purchase_details.*', 'supplier.company_name')
			->orderby('payment_purchase.payment_purchase_id',"desc");

		if ($request->nopayment && $request->nopayment != "")
		{
			$payment= $payment->where('payment_purchase_number','like','%'.$request->nopayment.'%');
		}
		if($request->supplier && $request->supplier != "")
		{
			$payment = $payment->whereRaw("company_name like '%$request->supplier%'");
		}
		$payment = $payment->get();

		return DataTable::of($payment)
			->setRowAttr([
				'value' => function($payment) {
					return $payment->payment_purchase_id;
				},
			])
			->addColumn('action', function ($payment){
				return
					'<a class="btn btn-sm btn-raised orange print-payment" target="_blank" href="downloadpurchasepayment/'.$payment->payment_purchase_id.'"><i class="material-icons">print</i></a>
						<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
			})
			->editColumn('paid', function($payment){
				return number_format($payment->paid);
			})
			->smart(false)
			->make(true);
	}

	public function getLastPaymentNumber(Request $request)
	{
		$lastpaymenttoday = purchasepayment::where('payment_purchase_number','like','PP/'.date('dmy').'/%')->orderby('payment_purchase_id','desc')->first();
		if(empty($lastpaymenttoday))
		{
			$newpaymentnumber = "PP/".date('dmy')."/1";
			return $newpaymentnumber;
		}
		else{
			$tmppayment = explode('/',$lastpaymenttoday->payment_purchase_number);
			$lastnumber = $tmppayment[2];
			$newpaymentnumber = "PP/".date('dmy')."/".($lastnumber+1);
			return $newpaymentnumber;
		}
	}

	public function getPurchasePayment(Request $request)
	{
		$payment = purchasepayment::leftjoin('invoice_purchase','invoice_purchase.invoice_purchase_id','=','payment_purchase.invoice_purchase_id')
			->leftjoin('payment_purchase_details','payment_purchase_details.payment_purchase_id','=','payment_purchase.payment_purchase_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','purchase_order.supplier_id')
			->leftjoin('payment_type','purchase_order.payment_type_id','=','payment_type.payment_type_id')
			->leftjoin('invoice_purchase_details','invoice_purchase_details.invoice_purchase_id','=','invoice_purchase.invoice_purchase_id')
			->select('payment_purchase.*','payment_purchase_details.*','invoice_purchase.invoice_purchase_id','invoice_purchase.discount_nominal','invoice_purchase.*','supplier.company_name', 'payment_type.*','purchase_order.*')
			->where('payment_purchase.payment_purchase_id', $request->id)
			->first();

		return $payment;
	}

	public function getInvoiceData(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}
		$invoice = purchaseinvoice::leftjoin('invoice_purchase_details','invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->leftjoin(DB::raw("(
                Select payment_purchase.invoice_purchase_id, sum(paid) as total from `payment_purchase` left join payment_purchase_details on payment_purchase_details.payment_purchase_id = payment_purchase.payment_purchase_id where payment_purchase.status <> 2 group by invoice_purchase_id) tablea"
			),'tablea.invoice_purchase_id','=','invoice_purchase.invoice_purchase_id')
			->leftjoin('payment_type','purchase_order.payment_type_id','=','payment_type.payment_type_id')
			->select('purchase_order.*','invoice_purchase_details.total_amount as t_amount','supplier.company_name','payment_type.*','tablea.*','invoice_purchase.*')
			->where('invoice_purchase.invoice_purchase_id', $request->id)
			->first();

		$poid = $invoice->purchase_order_id;

		//dicek apakah sudah ada discount dari pembayaran pertama, kalau ada pake term diskon yang sesuai dengan pembayaran pertama. kalau belum ada baru dicoccokan tanggal saat ini dengan diskon term yang ada.
		if($invoice->purchase_discount_id){
			$diskon = purchaseorder::join('purchase_discount','purchase_discount.purchase_order_id','=','purchase_order.purchase_order_id')
				->where('purchase_discount_id',$invoice->purchase_discount_id)
				->first();

			$selected['discount'] = $diskon->discount;
			$selected['period'] = $diskon->discount_period;
			$selected['type'] = $diskon->discount_type;
			$selected['id'] = $diskon->purchase_discount_id;
		}else{
			$discount = purchaseorder::join('purchase_discount','purchase_discount.purchase_order_id','=','purchase_order.purchase_order_id')
				->where('purchase_order.purchase_order_id',$poid)
				->select('purchase_discount_id','discount','discount_period','purchase_order.discount_type')
				->where('purchase_discount.status','<>',2)
				->orderby('discount_period','asc')
				->get();

			if(sizeof($discount) > 0)
			{
				$today = strtotime('today');
				$podate = strtotime($invoice->date_purchase_order);
				$selected = [];
				foreach ($discount as $key => $value) {
					$daylimit = strtotime("+".$value->discount_period."days", $podate);
					if($today <= $daylimit ){
						$selected['discount'] = $value->discount;
						$selected['period'] = $value->discount_period;
						$selected['type'] = $value->discount_type;
						$selected['id'] = $value->purchase_discount_id;
						break;
					}
				}
			}else{
				$selected['discount'] = 0;
				$selected['period'] = 0;
			}
		}
		return compact('invoice','selected');
	}

	private function getDiscountByDate($poid, $date, $podate){

		$discount = purchaseorder::join('purchase_discount','purchase_discount.purchase_order_id','=','purchase_order.purchase_order_id')
			->where('purchase_order.purchase_order_id',$poid)
			->select('purchase_discount_id','discount','discount_period','purchase_order.discount_type')
			->where('purchase_discount.status','<>',2)
			->orderby('discount_period','asc')
			->get();

		if(sizeof($discount) > 0)
		{
			$today = strtotime($date);
			$podate = strtotime($podate);
			$selected = [];

			foreach ($discount as $key => $value) {
				$daylimit = strtotime("+".$value->discount_period."days", $podate);
				if($today <= $daylimit ){
					$selected['discount'] = $value->discount;
					$selected['period'] = $value->discount_period;
					$selected['type'] = $value->discount_type;
					$selected['id'] = $value->purchase_discount_id;
					break;
				}
			}

			if(empty($selected)){
				$selected['discount'] = 0;
				$selected['period'] = 0;
			}
		}else{
			$selected['discount'] = 0;
			$selected['period'] = 0;
		}

		return $selected;
	}

	public function getDiscountData(Request $request){
		$date = date('Y-m-d', strtotime($request->date));

		$invoice = purchaseinvoice::where('invoice_purchase_id',$request->invoice)
			->with('po')
			->first();

		$poid = $invoice->po->purchase_order_id;
		$podate = $invoice->po->date_purchase_order;

		$selected = $this->getDiscountByDate($poid, $date, $podate);

		return $selected;
	}



	public function createPayment(Request $request)
	{
		$purchasepayment = new purchasepayment;
		$purchasepayment->invoice_purchase_id = $request->noinvoice;
		$purchasepayment->payment_purchase_number = $request->nopayment;
		$purchasepayment->date_payment_purchase = date('Y-m-d', strtotime($request->tglpayment));
		$purchasepayment->status = 1;
		$purchasepayment->save();

		$detailpurchasepayment = new detailpurchasepayment;
		$detailpurchasepayment->payment_purchase_id = $purchasepayment->payment_purchase_id;
		$detailpurchasepayment->payment_type_id = $request->paymenttype;
		$detailpurchasepayment->term_number = 1;
		$detailpurchasepayment->paid = $request->pembayaran;
		$detailpurchasepayment->status = 1;
		$detailpurchasepayment->save();

		if($request->diskon)
		{
			purchaseinvoice::where('invoice_purchase_id',$request->noinvoice)
				->update([
					'discount_nominal'=>$request->diskon,
					'purchase_discount_id'=>$request->iddiskon,
				]);
		}

		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "menambahkan";
		$log->object = "Sales Payment";
		$log->object_details = $purchasepayment->payment_purchase_number;
		$log->status = 1;
		$log->save();
	}

	public function deletePayment(Request $request)
	{
		purchasepayment::where('payment_purchase.payment_purchase_id', $request->id)->update(["status"=>2]);
		detailpurchasepayment::where('payment_purchase_details.payment_purchase_id', $request->id)->update(["status"=>2]);
	}

	public function downloadPurchasePayment(Request $request, $id)
	{
		$paymentheader = purchasepayment::leftjoin('payment_purchase_details', 'payment_purchase_details.payment_purchase_id','=','payment_purchase.payment_purchase_id')
			->leftjoin('invoice_purchase','payment_purchase.invoice_purchase_id','=','invoice_purchase.invoice_purchase_id')
			->leftjoin('invoice_purchase_details','invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
			->join('purchase_order','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('payment_purchase.payment_purchase_id',$id)
			->first();

		// Select payment_purchase.invoice_purchase_id, sum(paid) as total from `payment_purchase` left join payment_purchase_details on payment_purchase_details.payment_purchase_id = payment_purchase.payment_purchase_id where payment_purchase.status <> 2 group by invoice_purchase_id) tablea

		$invoice = $paymentheader->invoice_purchase_id;
		$totaldiskon = $paymentheader->discount_nominal;
		$totalinvoice = $paymentheader->total_amount;
		$totalpayment = purchasepayment::leftjoin('payment_purchase_details','payment_purchase_details.payment_purchase_id','=','payment_purchase.payment_purchase_id')
			->selectRaw('payment_purchase.invoice_purchase_id, sum(paid) as total')
			->groupby('invoice_purchase_id')
			->where('payment_purchase.payment_purchase_id',$id)
			->first()
			->total;
		$remaining = $totalinvoice - $totaldiskon - $totalpayment;

		// return "totalpayment = ".$totalpayment."<> totalinvoice = ".$totalinvoice." <> totaldiskon = ".$totaldiskon;

		$pdf = DOMPDF::loadView('purchase_payment.payment_pdf', compact('paymentheader','totalinvoice','totalpayment','totaldiskon','remaining'));
		return $pdf->stream('Payment Purchase.pdf');
	}
}
