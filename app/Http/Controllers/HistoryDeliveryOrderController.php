<?php

namespace App\Http\Controllers;

use App\deliveryorder;
use App\draftdelivery;
use App\salesinvoice;
use Carbon\Carbon;
use DB;
use DOMPDF;
use DataTable;
use Illuminate\Http\Request;

class HistoryDeliveryOrderController extends Controller
{
	public function showHistoryDOPage(){
		$start = Carbon::now()->startOfMonth()->toDateString();
		$end = Carbon::now()->endOfMonth()->toDateString();

		$suratantar =
			draftdelivery::with('customer')
			->join('sales_order','draft_delivery_order.sales_order_id','=','sales_order.sales_order_id')
			->join('customer','draft_delivery_order.customer_id','=','customer.customer_id')
			->where('draft_delivery_order.status',1)
			->whereBetween('date_draft_delivery',[$start,$end])
			->select('draft_delivery_order.draft_delivery_order_id','draft_delivery_order.draft_delivery_order_number','sales_order.sales_order_number','customer.first_name','customer.last_name')
			->get();

		return view('historydo.history_do', compact('suratantar'));
	}

	public function getHistoryByDraft(Request $request){
		$suratantar = draftdelivery::with('details.product')
				->where('draft_delivery_order_id', $request->id)
				->where('status',1)
				->first();

		$do = deliveryorder::with('details')
			->with('warehouse')
			->where('draft_delivery_order_id', $request->id)
			->where('status',1)
			->get();

		return view('historydo.detail_history_do', compact('suratantar','do'))->render();
	}

	public function downloadRemainingDraft(Request $request){
		$draft = draftdelivery::with('details.product.satuan.unit_child')
			->with('so.customer')
			->with('retur.so.customer')
			->where('draft_delivery_order_id', $request->id)
			->where('status',1)
			->first();

		if($draft->so){
			$customer = $draft->so->customer->customer_id;
		}else if ($draft->retur->so){
			$customer = $draft->retur->so->customer->customer_id;
		}

		$invoicebelumlunas = salesinvoice::with('payments')
			->leftjoin('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
			->whereExists(function ($query) {
				$query->select(DB::raw('Sum(payment_sales.total_paid) as total'))
					->from('payment_sales')
					->groupBy('payment_sales.invoice_sales_id')
					->whereRaw('invoice_sales.invoice_sales_id = payment_sales.invoice_sales_id AND payment_sales.status = 1')
					->havingRaw('total < invoice_sales.total_amount');
			})
			->where('invoice_sales.status',1)
			->where('sales_order.customer_id',$customer)
			->get();

		$do = deliveryorder::with('details')
			->with('warehouse')
			->where('draft_delivery_order_id', $request->id)
			->where('status',1)
			->get();

		$pdf = DOMPDF::loadView('historydo.remaining_draft_pdf', compact('draft','do','invoicebelumlunas'));
		return $pdf->stream('Draft.pdf');
	}
}