<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use App\warehouse;
use App\log;

class LogController extends Controller
{
	public function showLogPage(){
		$log = log::all();
		return view('log.log', compact('log'));	
	}
}