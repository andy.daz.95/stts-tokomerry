<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\salesorder;
use App\detailsalesorder;
use App\salesreturn;
use App\detailsalesreturn;
use App\deliveryorder;
use App\draftdelivery;
use App\detaildraftdelivery;
use App\product;
use App\productwarehouse;
use App\detaildeliveryorder;
use App\returtype;
use App\customer;
use App\warehouse;
use App\Http\Controllers\DeliveryOrderController;
use DB;
use DataTable;
use DOMPDF;
use App\Traits\DraftDeliveryTrait;

class SalesReturnController extends Controller
{
	use DraftDeliveryTrait;

	public function showSalesReturnPage()
	{
		$lastmonthdate = date('Y-m-d', strtotime('-1 month', strtotime(date('Y-m-d'))));
		$so = salesorder::wherebetween('date_sales_order',[$lastmonthdate, date('Y-m-d')])
			->where('sales_order.status','<>',2)
			->whereExists(function ($query) {
				$query->select('*')
					->from('draft_delivery_order')
					->leftjoin('delivery_order','draft_delivery_order.draft_delivery_order_id','=','delivery_order.draft_delivery_order_id')
					->where('draft_delivery_order.status',1)
					->where('delivery_order.status',1)
					->whereRaw('draft_delivery_order.sales_order_id = sales_order.sales_order_id');
			})
			->get();

		$returtype = returtype::where('retur_for',1)->get();
		$warehouse = warehouse::all();

		return view('sales_return.sales_return', compact('so','returtype','retur','warehouse'));
	}

	public function getSalesReturnTable(request $request){
		$retur = salesreturn::join('sales_order','sales_order.sales_order_id','=','sales_retur.sales_order_id')
			->join('customer','customer.customer_id','=','sales_order.customer_id')
			->join('retur_type','sales_retur.retur_type_id','=','retur_type.retur_type_id')
			->where('sales_order.status','<>',2)
			->where('sales_retur.status','<>',2)
			->select('sales_retur.*','customer.*','sales_order.sales_order_number','retur_type.retur_type_name')
			->orderby('sales_retur.sales_retur_id','desc');

		if ($request->noretur && $request->noretur != "")
		{
			$retur = $retur->where('retur_number','like','%'.$request->noretur.'%');
		}
		if($request->customer && $request->customer != "")
		{
			$retur = $retur->whereRaw("CONCAT_WS(' ', `first_name`, `last_name`) like '%$request->customer%'");
		}
		if($request->noso && $request->noso != "")
		{
			$retur = $retur->where('sales_order_number','like','%'.$request->noretur.'%');
		}
		$retur = $retur->get();

		return DataTable::of($retur)
			->setRowAttr([
				'value' => function($retur) {
					return $retur->sales_retur_id;
				},
			])
			->addColumn('customer_name', function ($retur){
				return $retur->first_name.' '.$retur->last_name;
			})
			->addColumn('action', function ($retur) {
				if(salesreturn::where('sales_retur_id',$retur->sales_retur_id)->has('sales_return_receive')->count() > 0
					||
					draftdelivery::where('sales_retur_id', $retur->sales_retur_id)->has('delivery')->count()>0) {
					return
						'<a class="btn btn-sm btn-raised orange print-retur" target="_blank" href="downloadsalesreturn/' . $retur->sales_retur_id . '"><i class="material-icons">print</i></a>'
							;

				}else{
					return
						'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="' . $retur->sales_retur_id . '"><i class="material-icons">edit</i></a>
							<a class="btn btn-sm btn-raised orange print-retur" target="_blank" href="downloadsalesreturn/' . $retur->sales_retur_id . '"><i class="material-icons">print</i></a>
							<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				}
			})
			->smart(false)
			->make(true);
	}

	public function getLastReturNumber(Request $request)
	{
		$lastreturntoday = salesreturn::where('retur_number','like','SR/'.date('dmy').'/%')->orderby('sales_retur_id','desc')->first();
		$lastreturntoday = salesreturn::where('retur_number','like','SR/'.date('dmy').'/%')->orderby('sales_retur_id','desc')->first();
		if(empty($lastreturntoday))
		{
			$newreturnnumber = "SR/".date('dmy')."/1";
			return $newreturnnumber;
		}
		else{
			$tmpreturn = explode('/',$lastreturntoday->retur_number);
			$lastnumber = $tmpreturn[2];
			$newreturnnumber = "SR/".date('dmy')."/".($lastnumber+1);
			return $newreturnnumber;
		}
	}

	public function getSalesOrderData(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}

		$data['so'] = salesorder::with('details.product')
			->with('details.unit')
			->with('details.unit_child')
			->with('customer')
			->with('delivery')
			->where('sales_order.sales_order_id',$request->id)
			->first();

		//cek jumlah return yang telah dilakukan dari setiap barang dalam so tersebut
		//hitung quota dengan mengurangi order qty dengan total retur
		//append quota ke dalam collection
		foreach ($data['so']->details as $key => $value) {
			$totalreturn = salesreturn::
			leftjoin('sales_retur_details','sales_retur.sales_retur_id','=','sales_retur_details.sales_retur_id')
				->where('sales_retur.sales_order_id', $value->sales_order_id)
				->where('sales_retur_details.sales_order_details_id', $value->sales_order_details_id)
				->where('sales_retur.status',1)
				->where('sales_retur_details.status',1)
				->sum('qty');

			$quota = $value->quantity - $totalreturn;
			$value['totalreturn'] = $totalreturn;
			$value['quota'] = $quota;
		}


		//cek status so apakah ada invoice yang dikeluarkan dan telah terbayar sepenuhnya.
		//bila invoice telah dikeluaran seluruhnya maka hanya dapat menggunaka tipe retur Refund, Retur Barang, Potong Invoice Selanjutnya
		//bila belum terbayar semuanya maka maka hanya dapat menggunaka tipe retur Refund, Potong Nota, Retur Barang
		$status = salesorder::leftjoin('invoice_sales','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
			->where('sales_order.sales_order_id', $request->id)
			->where('invoice_sales.status', "<>",2)
			->selectraw("sales_order.grand_total_idr, sum(invoice_sales.total_amount)as invoice, if(grand_total_idr = sum(invoice_sales.total_amount), 'closed', 'open') as status")
			->first()
			->status;

		if($status == 'open')
		{
			$data['returtype'] = returtype::whereIn('retur_type_id',[1,2,5])->get(); // Refund, Porong Nota, Retur Barang
		}else{
			$data['returtype'] = returtype::whereIn('retur_type_id',[1,5,7])->get(); // refund, retur Barang, Potong Invoice Selanjutnya
		}

		// $data['delivery'] = deliveryorder::where('sales_order_id',$request->id)->where('status','<>',2)->first();
		return $data;
	}

	public function getSalesReturn(Request $request)
	{
		$so = salesreturn::where('sales_retur_id',$request->id)->first()->sales_order_id;
		$retur = salesreturn::with('so.customer')->where('sales_retur_id',$request->id)->first();
		$retur_details = $retur->details;
		foreach ($retur_details as $key => $value) {
			$totalqty = detailsalesorder::where('sales_order_details_id', $value->sales_order_details_id)->first()->quantity;

			$quota = $totalqty - $value->qty;
			$value['quota'] = $quota;
		}
		return compact('so', 'retur', 'retur_details');
	}


	public function createReturn(Request $request)
	{
		$return = new salesreturn;
		$return->sales_order_id = $request->noso;
		$return->account_id = Session::get('user')->account_id;
		$return->status_id = 1;
		$return->retur_number = $request->noretur;
		$return->total_price = 0;
		$return->retur_type_id = $request->returtype;
		$return->date_retur = date('Y-m-d', strtotime($request->tglretur));
		$return->status = 1;
		$return->save();

		$total = 0;
		for ($i=0; $i < sizeof($request->id); $i++) {
			if($request->qty[$i] != 0)
			{
				$subtotal = $request->qty[$i] * $request->price[$i];
				$hpp = detailsalesorder::where('sales_order_id', $return->sales_order_id)->where('product_id',$request->id[$i])->where('status',1)->first()->hpp;
				$detailreturn = new detailsalesreturn;
				$detailreturn->sales_retur_id = $return->sales_retur_id;
				$detailreturn->sales_order_details_id = $request->detailsales[$i];
				$detailreturn->product_id = $request->id[$i];
				$detailreturn->unit_id = $request->unitid[$i];
				$detailreturn->unit_type = $request->unittype[$i];
				$detailreturn->qty = $request->qty[$i];
				$detailreturn->weight = 0;
				$detailreturn->price = $request->price[$i];
				$detailreturn->cogs = $request->qty[$i] * $hpp;
				$detailreturn->status = 1;
				$detailreturn->save();
				$total += $subtotal;
			}
		}

		salesreturn::where('sales_retur_id', $return->sales_retur_id)->update(['total_price'=>$total]);

		//Untuk Retur Refund / Kembali Uang Perlu dicatat Gudang Nya Untuk Memberitahu customer dia harus balikin barangnya ke gudang yang mana
		//Untuk Retur Potong Nota di tambahkan reduction di table so agar saat buat nota bisa di kurangin nilainya
		//Untuk Retur Potong Invoice selanjutnya di tambahkan next invoice reduction di table customer untuk potongan di pembelian berikutnya

		if($request->returtype == 1){ // Kembali Uang
			$return->warehouse_id = $request->gudang;
			$return->save();
		}
		else if ($request->returtype == 2){ // potong Nota
			salesorder::where('sales_order_id',$request->noso)->update([
				'reduction' => DB::raw('reduction+'.$total),
			]);
		}else if($request->returtype == 5){ //Retur Barang
			$lastnumber = $this->lastReturNumber();
			$draft = $this->createDraftDelivery('retur', $return->sales_retur_id, $lastnumber, $return->so->sales_id, null, $return->date_retur, 3, 'Barang Retur');
			$details = $return->details;

			foreach ($details as $key => $value)
			{
				$detaildraft = $this->createDetailDraftDelivery($draft->draft_delivery_order_id, $value->sales_retur_details_id, $value->unit_id, $value->unit_type, $value->product_id, $value->qty);
			}
		}else if($request->returtype == 7){ // Potong Invoice Selanjutnya
			$customer = salesorder::find($request->noso)->customer_id;
			customer::where('customer_id',$customer)->update([
				'next_invoice_reduction' => DB::raw('next_invoice_reduction+'.$total),
			]);
		}
	}

	public function updateReturn(Request $request){

		$return = salesreturn::find($request->idretur);
		$lasttotal = $return->total_price;

		$return->date_retur = date('Y-m-d', strtotime($request->tglretur));
		$return->save();

		$total = 0;
		detailsalesreturn::where('sales_retur_id',$request->idretur)->update(["status"=>2]);

		if ($return->retur_type_id == 2){ // Potong Nota

			salesorder::where('sales_order_id',$request->noso)->update([
				'reduction' => DB::raw('reduction-'.$lasttotal),
			]);

		}else if ($return->retur_type_id == 5){
			$draft = draftdelivery::where('sales_retur_id',$return->sales_retur_id)
				->update([
				'status' => 2,
			]);

			detaildraftdelivery::where('draft_delivery_order_id', $draft->draft_delivery_order_id)->update([
				'status' => 2,
			]);

		}else if ($return->retur_type_id == 7){
			$customer = salesorder::find($return->sales_order_id)->customer_id;
			customer::where('customer_id',$customer)->update([
				'next_invoice_reduction' => DB::raw('next_invoice_reduction-'.$total),
			]);
		}

		for ($i=0; $i < sizeof($request->id); $i++) {
				$subtotal = $request->qty[$i] * $request->price[$i];
				$hpp = detailsalesorder::where('sales_order_id', $return->sales_order_id)->where('product_id',$request->id[$i])->where('status',1)->first()->hpp;
				$detailreturn = new detailsalesreturn;
				$detailreturn->sales_retur_id = $return->sales_retur_id;
				$detailreturn->sales_order_details_id = $request->detailsales[$i];
				$detailreturn->product_id = $request->id[$i];
				$detailreturn->unit_id = $request->unitid[$i];
				$detailreturn->unit_type = $request->unittype[$i];
				$detailreturn->qty = $request->qty[$i];
				$detailreturn->weight = 0;
				$detailreturn->price = $request->price[$i];
				$detailreturn->cogs = $request->qty[$i] * $hpp;
				$detailreturn->status = 1;
				$detailreturn->save();
				$total += $subtotal;
		}

		if($request->returtype == 2)
		{
			salesorder::where('sales_order_id',$request->noso)->update([
				'reduction' => DB::raw('reduction+'.$total),
			]);
		}else if($request->returtype == 5)
		{
			$lastnumber = $this->lastReturNumber();
			$draft = $this->createDraftDelivery('retur', $return->sales_retur_id, $lastnumber, $return->so->sales_id, null, $return->date_retur, 3, 'Barang Retur');
			$details = $return->details;

			foreach ($details as $key => $value)
			{
				$detaildraft = $this->createDetailDraftDelivery($draft->draft_delivery_order_id, $value->sales_retur_details_id, $value->unit_id, $value->unit_type, $value->product_id, $value->qty);
			}
		}else if($request->returtype == 7){
			$customer = salesorder::find($request->noso)->customer_id;
			customer::where('customer_id',$customer)->update([
				'next_invoice_reduction' => DB::raw('next_invoice_reduction+'.$total),
			]);
		}

		salesreturn::where('sales_retur_id', $return->sales_retur_id)->update(['total_price'=>$total]);
	}

	public function deleteReturn(Request $request){
		$return = salesreturn::find($request->id);
		$lasttotal = $return->total_price;
		$so = $return->sales_order_id;

		detailsalesreturn::where('sales_retur_id',$request->idretur)->update(["status"=>2]);

		if ($return->retur_type_id == 2){ // Potong Nota

			salesorder::where('sales_order_id',$so)->update([
				'reduction' => DB::raw('reduction-'.$lasttotal),
			]);

		}else if ($return->retur_type_id == 5){
			$draft = draftdelivery::where('sales_retur_id',$return->sales_retur_id)->first();

			detaildraftdelivery::where('draft_delivery_order_id', $draft->draft_delivery_order_id)->update([
				'status' => 2,
			]);

			$draft->status = 2;
			$draft->save();

		}else if ($return->retur_type_id == 7){
			$customer = salesorder::find($return->sales_order_id)->customer_id;
			customer::where('customer_id',$customer)->update([
				'next_invoice_reduction' => DB::raw('next_invoice_reduction-'.$total),
			]);
		}

		$return->status = 2;
		$return->save();

	}

	public function showRefundHistory(Request $request){
		return view('sales_return.sales_refund',compact('refund'));
	}

	public function getRefundHistoryTable(Request $request){
		$refund = salesreturn::leftjoin('sales_order','sales_order.sales_order_id','=','sales_retur.sales_order_id')->leftjoin('customer','sales_order.customer_id','=','customer.customer_id')
			->where('retur_type_id',1)
			->where('sales_retur.status','<>',2);

		if ($request->noso && $request->noso != "")
		{
			$refund = $refund->where('sales_order_number','like','%'.$request->noso.'%');
		}
		if($request->customer && $request->customer != "")
		{
			$refund = $refund->whereRaw("CONCAT_WS(' ', `first_name`, `last_name`) like '%$request->customer%'");
		}
		$refund = $refund->get();

		return DataTable::of($refund)
			->setRowAttr([
				'value' => function($refund) {
					return $refund->sales_retur_id;
				},
			])
			->addColumn('customer_name', function ($refund){
				return $refund->first_name.' '.$refund->last_name;
			})
			->smart(false)
			->make(true);
	}

	public function downloadSalesReturn(Request $request, $id)
	{
		$returheader = salesreturn::leftjoin('sales_order','sales_order.sales_order_id','=','sales_retur.sales_order_id')
			->leftjoin('retur_type','sales_retur.retur_type_id','=','retur_type.retur_type_id')
			->leftjoin('warehouse','warehouse.warehouse_id','=','sales_retur.warehouse_id')
			->where('sales_retur_id', $request->id)
			->first();

		$returdetail =
			detailsalesreturn::with('unit')
				->with('unit_child')
				->leftjoin('product','product.product_id','=','sales_retur_details.product_id')
				->leftjoin('sales_retur','sales_retur_details.sales_retur_id','=','sales_retur.sales_retur_id')
				->leftjoin('sales_order','sales_retur.sales_order_id','=','sales_order.sales_order_id')
				->leftjoin('sales_order_details','sales_order.sales_order_id','=','sales_order_details.sales_order_id')
				->where('sales_retur.sales_retur_id',$id)
				->where('sales_retur_details.status','<>',2)
				->select('sales_retur_details.*','product.product_name as p_name','sales_order_details.quantity as orderqty')
				->groupby('sales_retur_details.sales_retur_details_id')
				->get();

		$pdf = DOMPDF::loadView('sales_return.return_pdf', compact('returheader','returdetail'));
		return $pdf->stream('Sales Return.pdf');
	}

}