<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\customer;
use App\salesinvoice;
use App\paymentmethod;
use App\salespayment;
use App\detailsalespayment;
use App\log;
use App\bank;
use Carbon\Carbon;
use DB;
use DataTable;
use DOMPDF;
use Redis;

class SalesPaymentController extends Controller
{
	public function showSalesPaymentPage()
	{
		$data['customer'] = customer::all();
		$data['bank'] = bank::where('status','<>',2)->get();
		$data['payment_method'] = paymentmethod::where('status','<>',2)->get();
		$query = salesinvoice::with(['payments', 'so.customer'])
		->leftjoin('payment_sales', function($join){
			$join->on('invoice_sales.invoice_sales_id','=','payment_sales.invoice_sales_id');
			$join->on("payment_sales.status","=",DB::Raw(1));
		})
		->groupBy('invoice_sales.invoice_sales_id')
		->selectRaw('invoice_sales.invoice_sales_id, invoice_sales.total_amount, invoice_sales.sales_order_id, SUM(IFNULL(payment_sales.total_paid, 0)) as total_payment')
		->where('invoice_sales.status',1)
		->havingRaw('total_payment < invoice_sales.total_amount');

		$invoicecache = Redis::get(env('APP_ENV').'-invoice-for-payment');
		if($invoicecache)
		{
			$start = Carbon::now()->startOfDay()->toDateTimeString();
			$end = Carbon::now()->endOfDay()->toDateTimeString();
			$data['invoicetoday'] = $query->whereBetween('invoice_sales.created_at', [$start, $end])->get();
			$data['invoice'] = json_decode($invoicecache);
		}
		else
		{
			$data['invoice'] = $query->get();
			Redis::set(env('APP_ENV').'-invoice-for-payment', json_encode($data['invoice']));
		}

		// $data['invoice'] = salesinvoice::with(['payments', 'so.customer'])
		// 	->leftjoin('sales_order','sales_order.sales_order_id','=','invoice_sales.sales_order_id')
		// 	->where('invoice_sales.status',1)
		// 	->where(function($query){
		// 		$query->whereExists(function ($query) {
		// 			$query->select(DB::raw('Sum(payment_sales.total_paid) as total'))
		// 				->from('payment_sales')
		// 				->groupBy('payment_sales.invoice_sales_id')
		// 				->whereRaw('invoice_sales.invoice_sales_id = payment_sales.invoice_sales_id AND payment_sales.status = 1')
		// 				->havingRaw('total < invoice_sales.total_amount');
		// 		})->orWhereNotExists(function ($query){
		// 			$query->select(DB::raw(1))
		// 				->from('payment_sales')
		// 				->whereRaw('invoice_sales.invoice_sales_id = payment_sales.invoice_sales_id AND payment_sales.status = 1');
		// 		});
		// 	})
		// 	->get();

		return view('sales_payment.sales_payment', compact('data'));
	}

	public function getSalesPaymentTable(Request $request){
		$payment = salespayment::leftjoin('customer','customer.customer_id','payment_sales.customer_id')
			->leftjoin('payment_method','payment_method.payment_method_id','=','payment_sales.payment_methods')
			->where('payment_sales.status','<>',2)
			->orderby('payment_sales.payment_sales_id',"desc")
			->select('payment_sales.payment_sales_id','payment_sales.payment_sales_number','customer.first_name','customer.last_name','payment_sales.date_payment_sales','payment_method.payment_method_name','payment_sales.total_paid');

		if ($request->nopayment && $request->nopayment != "")
		{
			$payment = $payment->where('payment_sales_number','like','%'.$request->nopayment.'%');
		}
		if($request->customer && $request->customer != "")
		{
			$payment = $payment->whereRaw("CONCAT_WS(' ', `first_name`, `last_name`) like '%$request->customer%'");
		}

		return DataTable::of($payment)
			->setRowAttr([
				'value' => function($payment) {
					return $payment->payment_sales_id;
				},
			])
			->addColumn('customer_name', function ($payment){
				return $payment->first_name.' '.$payment->last_name;
			})
			->addColumn('action', function ($payment) {
				return
					'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$payment->payment_sales_id.'"><i class="material-icons">edit</i></a>
						<a class="btn btn-sm btn-raised orange print-payment" target="_blank" href="downloadsalespayment/'.$payment->payment_sales_id.'"><i class="material-icons">print</i></a>
						<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
			})
			->editColumn('total_paid', function($payment){
				return number_format($payment->total_paid);
			})
			->smart(false)
			->make(true);
	}

	public function getLastPaymentNumber()
	{
		$lastpaymenttoday = salespayment::where('payment_sales_number','like','SP/'.date('dmy').'/%')->orderby('payment_sales_id','desc')->first();
		if(empty($lastpaymenttoday))
		{
			$newpaymentnumber = "SP/".date('dmy')."/1";
			return $newpaymentnumber;
		}
		else{
			$tmppayment = explode('/',$lastpaymenttoday->payment_sales_number);
			$lastnumber = $tmppayment[2];
			$newpaymentnumber = "SP/".date('dmy')."/".($lastnumber+1);
			return $newpaymentnumber;
		}
	}

	public function getSalesPayment(Request $request)
	{
		$payment = salespayment::leftjoin('customer','customer.customer_id','payment_sales.customer_id')->select('payment_sales.*','customer.*')->where('payment_sales.payment_sales_id', $request->id)->first();
		return $payment;
	}


	public function getCustomerData(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}
		$customer = customer::where('customer_id', $request->id)->first();
		return $customer;
	}

	public function getInvoiceData(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}

		$invoice = salesinvoice::where('invoice_Sales_id',$request->id)
			->with('payments')
			->with('totalpayments')
			->with('so')
			->where('status',1)
			->first();
		return $invoice;
	}

	public function createPayment(Request $request)
	{
		$salespayment = new salespayment;
		$salespayment->customer_id = $request->customer;
		$salespayment->invoice_sales_id = $request->invoice;
		$salespayment->payment_sales_number = $request->nopayment;
		$salespayment->date_payment_sales = date('Y-m-d', strtotime($request->tglpayment));
		$salespayment->total_paid = $request->pembayaran;
		$salespayment->payment_methods = $request->paymentmethod;
		$salespayment->bank_id = $request->paymentmethod == 2 ? $request->bank : NULL;
		$salespayment->status = 1;
		$salespayment->save();

		customer::where('customer_id',$request->customer)->update([
			'total_balance' => DB::raw('total_balance+'.$request->pembayaran),
		]);

		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "menambahkan";
		$log->object = "Sales Payment";
		$log->object_details = $salespayment->payment_sales_number;
		$log->status = 1;
		$log->save();
	}

	public function updatePayment(Request $request){
		$salespayment = salespayment::find($request->idpayment);
		$lastpayment = $salespayment->total_paid;

		customer::where('customer_id',$request->customer)->update([
			'total_balance' => DB::raw('total_balance-'.$lastpayment),
		]);
		$salespayment->total_paid = $request->pembayaran;
		$salespayment->date_payment_sales = date('Y-m-d', strtotime($request->tglpayment));
		$salespayment->save();

		customer::where('customer_id',$request->customer)->update([
			'total_balance' => DB::raw('total_balance+'.$request->pembayaran),
		]);

		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "mengubah";
		$log->object = "Sales Payment";
		$log->object_details = $salespayment->payment_sales_number;
		$log->status = 1;
		$log->save();
	}

	public function deletePayment(Request $request){
		$salespayment = salespayment::find($request->id);
		$customer = $salespayment->customer_id;
		$lastpayment = $salespayment->total_paid;

		customer::where('customer_id',$customer)->update([
			'total_balance' => DB::raw('total_balance-'.$lastpayment),
		]);

		$salespayment->status = 2;
		$salespayment->save();
	}

	public function downloadSalesPayment(Request $request, $id)
	{
		$paymentheader = salespayment::leftjoin('customer','customer.customer_id','=','payment_sales.customer_id')
			->where('payment_sales.payment_sales_id',$id)
			->first();

		$pdf = DOMPDF::loadView('sales_payment.payment_pdf', compact('paymentheader'));
		return $pdf->stream('Payment Sales.pdf');
	}
}