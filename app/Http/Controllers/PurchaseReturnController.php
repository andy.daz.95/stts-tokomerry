<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\purchaseorder;
use App\purchasereturn;
use App\purchaseinvoice;
use App\detailpurchasereturn;
use App\product;
use App\warehouse;
use App\productwarehouse;
use App\goodreceive;
use App\returtype;
use App\supplier;
use DB;
use DataTable;
use DOMPDF;

class PurchaseReturnController extends Controller
{
    public function showPurchaseReturnPage()
    {
        // $lastmonthdate = date('Y-m-d', strtotime('-1 month', strtotime(date('Y-m-d'))));
        // $so = salesorder::wherebetween('date_sales_order',[$lastmonthdate, date('Y-m-d')])->get();
        $po = purchaseorder::where('purchase_order.status','<>',2)
                ->whereRaw("date_purchase_order between DATE_ADD(NOW(), INTERVAL -2 MONTH) and Now()")
                ->whereExists(function ($query) {
                $query->select('*')
                      ->from('good_receive')
                      ->whereRaw('good_receive.status <> 2')
                      ->whereRaw('good_receive.purchase_order_id = purchase_order.purchase_order_id');
            })->get();

        //         $status = salesorder::leftjoin('invoice_sales','sales_order.sales_order_id','=','invoice_sales.sales_order_id')->where('sales_order.sales_order_id', $request->id)->selectraw("sales_order.grand_total_idr, sum(invoice_sales.total_amount)as invoice, if(grand_total_idr = sum(invoice_sales.total_amount), 'closed', 'open') as status")->first()->status;

        $returtype = returtype::where('retur_for',0)->get();
        $warehouse = warehouse::all();

    	return view('purchase_return.purchase_return', compact('po','returtype','retur','warehouse'));
    }

    public function getPurchaseReturnTable(Request $request){
			$retur = purchasereturn::leftjoin('purchase_order','purchase_order.purchase_order_id','=','purchase_retur.purchase_order_id')
				->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
				->leftjoin('retur_type','purchase_retur.retur_type_id','=','retur_type.retur_type_id')
				->where('purchase_retur.status','<>',2)
				->select('purchase_retur.*','supplier.company_name','purchase_order.purchase_order_number','retur_type.retur_type_name')
				->orderby('purchase_retur.purchase_retur_id','desc');

			if ($request->noretur && $request->noretur != "")
			{
				$retur= $retur->where('retur_number','like','%'.$request->noretur.'%');
			}
			if ($request->nopo && $request->nopo != "")
			{
				$retur = $retur->where('purchase_order_number','like','%'.$request->nopo.'%');
			}
			if($request->supplier && $request->supplier != "")
			{
				$retur = $retur->whereRaw("company_name like '%$request->supplier%'");
			}
			$retur = $retur->get();

			return DataTable::of($retur)
				->setRowAttr([
					'value' => function($retur) {
						return $retur->purchase_retur_id;
					},
				])
				->addColumn('action', function ($retur) {
					return
						'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$retur->purchase_retur_id.'"><i class="material-icons">edit</i></a>
						<a class="btn btn-sm btn-raised orange print-retur" target="_blank" href="downloadpurchasereturn/'.$retur->purchase_retur_id.'"><i class="material-icons">print</i></a>
						<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				})
				->smart(false)
				->make(true);
		}

    public function getLastReturNumber(Request $request)
    {
        $lastreturntoday = purchasereturn::where('retur_number','like','PR/'.date('dmy').'/%')->orderby('purchase_retur_id','desc')->first();
        if(empty($lastreturntoday))
        {
            $newreturnnumber = "PR/".date('dmy')."/1";
            return $newreturnnumber;
        }
        else{
            $tmpreturn = explode('/',$lastreturntoday->retur_number); 
            $lastnumber = $tmpreturn[2];
            $newreturnnumber = "PR/".date('dmy')."/".($lastnumber+1);
            return $newreturnnumber;
        }
    }

    public function getPurchaseOrderData(Request $request)
    {
        if($request->id == 0)
        {
            return;
        }

        $data['po'] = purchaseorder::leftjoin('purchase_order_details','purchase_order_details.purchase_order_id','=','purchase_order.purchase_order_id')
            ->leftjoin('product','product.product_id','=','purchase_order_details.product_id')
            ->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
            ->leftjoin(DB::raw(
                '(select purchase_order_id, qty as totalqty, product_id, purchase_order_details_id from(
                    SELECT purchase_order.purchase_order_id, sum(purchase_retur_details.qty) as qty, purchase_retur_details.product_id, purchase_retur_details.purchase_order_details_id
                    FROM purchase_order LEFT JOIN purchase_retur ON purchase_order.purchase_order_id = purchase_retur.purchase_order_id
                    LEFT JOIN purchase_retur_details on purchase_retur.purchase_retur_id = purchase_retur_details.purchase_retur_id
                    WHERE purchase_order.purchase_order_id = '.$request->id.' AND  purchase_order.status <> 2 AND purchase_retur_details.status <> 2
                    GROUP BY purchase_order_details_id)a GROUP by purchase_order_details_id)tablea')
            , function($join){
                $join->on('purchase_order.purchase_order_id','=','tablea.purchase_order_id')
                ->on('purchase_order_details.purchase_order_details_id','=','tablea.purchase_order_details_id');
            })
						->where('purchase_order.purchase_order_id', $request->id)
            ->selectraw('purchase_order.* , purchase_order_details.* , purchase_order_details.price as item_price, supplier.company_name, product.*, tablea.totalqty , (quantity+free_qty- IFNULL(totalqty,0)) as retur_quota')
            ->get();



        $invoice = purchaseinvoice::where('purchase_order_id',$request->id)->where('status','<>',2)->first();
        
        if($invoice)
        {
            $data['returtype'] = returtype::whereIn('retur_type_id', [4,6])->get(); // Return Barang
        }else{
            $data['returtype'] = returtype::whereIn('retur_type_id', [3,4])->get(); // Potong Invoice & Retur Barang
        }

        $data['good_receive'] = goodreceive::where('purchase_order_id',$request->id)->where('status','<>',2)->orderby('good_receive_date','desc')->first();
        return $data;
    }

    public function getPurchaseReturn(Request $request)
    {
        $po = purchasereturn::where('purchase_retur_id',$request->id)->first()->purchase_order_id;
        $retur = purchasereturn::leftjoin('purchase_retur_details','purchase_retur.purchase_retur_id','=','purchase_retur_details.purchase_retur_id')
            ->leftjoin('product','product.product_id','=','purchase_retur_details.product_id')
            ->leftjoin('purchase_order','purchase_order.purchase_order_id','=','purchase_retur.purchase_order_id')
            ->leftjoin('purchase_order_details', 
                function($join){
                    $join->on('purchase_order.purchase_order_id','=','purchase_order_details.purchase_order_id')
                    ->on('purchase_order_details.product_id','=','purchase_retur_details.product_id');
                })
            ->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
            ->leftjoin(DB::raw(
                '(select purchase_order_id, qty as totalqty, product_id 
                    from(
                        SELECT purchase_order.purchase_order_id,  purchase_retur_details.product_id, sum(purchase_retur_details.qty) as qty 
                        FROM purchase_order 
                        LEFT JOIN purchase_retur ON purchase_order.purchase_order_id = purchase_retur.purchase_order_id 
                        LEFT JOIN purchase_retur_details on purchase_retur.purchase_retur_id = purchase_retur_details.purchase_retur_id 
                        WHERE purchase_order.purchase_order_id = '.$po.' AND purchase_retur.status <> 2 AND purchase_retur_details.status <> 2
                        GROUP BY product_id)a 
                    GROUP by product_id)tablea')
                , function($join){
                    $join->on('purchase_order.purchase_order_id','=','tablea.purchase_order_id')
                    ->on('purchase_order_details.product_id','=','tablea.product_id');
                })
            ->where('purchase_retur.purchase_retur_id',$request->id)
            ->where('purchase_retur.status','<>',2)
            ->where('purchase_retur_details.status','<>',2)
            ->selectraw('purchase_retur.* ,purchase_retur_details.purchase_retur_details_id, product.product_id, product.product_name, product.product_code, purchase_retur_details.qty,  supplier.company_name , tablea.*, (quantity-totalqty) as retur_quota , purchase_order_details.price as item_price , purchase_order.purchase_order_number')->get();

        return $retur;
    }

    public function createReturn(Request $request)
    {
//    	return $request->all();
        $return = new purchasereturn;
        $return->purchase_order_id = $request->nopo;
        $return->account_id = Session::get('user')->account_id;
        $return->status_id = 1;
        $return->retur_number = $request->noretur;
        $return->total_price = 0;
        $return->retur_type_id = $request->returtype;
        $return->date_retur = date('Y-m-d', strtotime($request->tglretur));
        $return->status = 1;
        $return->save();
        
        $total = 0;
        for ($i=0; $i < sizeof($request->id); $i++) {
        		if($request->qty[$i] > 0)
						{
							$subtotal = $request->qty[$i] * $request->price[$i];
							$detailreturn = new detailpurchasereturn;
							$detailreturn->purchase_retur_id = $return->purchase_retur_id;
							$detailreturn->purchase_order_details_id = $request->detailpo[$i];
							$detailreturn->product_id = $request->id[$i];
							$detailreturn->qty = $request->qty[$i];
							$detailreturn->weight = 0;
							$detailreturn->price = $request->price[$i];
							$detailreturn->status = 1;
							$detailreturn->save();
							$total += $subtotal;
						}
        }
        
        purchasereturn::where('purchase_retur_id', $return->purchase_retur_id)->update(['total_price'=>$total]);
        
        if ($request->returtype == 3){
            purchaseorder::where('purchase_order_id',$request->nopo)->update([
                'reduction' => DB::raw('reduction+'.$total),
            ]);
        }
        else if($request->returtype == 4){
            $return->warehouse_id = $request->gudang; // perlu di save biar bisa tau pas edit itu sebelumnya dari warehouse mana
            $return->save();

            for ($i=0; $i < sizeof($request->id); $i++) {
                productwarehouse::where('warehouse_id',$request->gudang)
                    ->where('product_id',$request->id[$i])
                    ->update([
                        'quantity' => DB::raw('quantity-'.$request->qty[$i]), 
                    ]);
            }
        }else if($request->returtype == 6){
            $supplier = purchaseorder::find($request->nopo)->supplier_id;
            supplier::where('supplier_id',$supplier)->update([
                'next_invoice_reduction' => DB::raw('next_invoice_reduction+'.$total),
            ]);
        }
    }

    public function updateReturn(Request $request)
    {
        $return = purchasereturn::find($request->idretur);
        $return->date_retur = date('Y-m-d', strtotime($request->tglretur));
        $return->save();

        $lastwarehouse = $return->warehouse_id;

        $total = $return->total_price;
        $returtype = $return->retur_type_id;
        $po = $return->purchase_order_id;

        if ($return->retur_type_id == 3){
            purchaseorder::where('purchase_order_id',$po)->update([
                'reduction' => DB::raw('reduction-'.$total),
            ]);
        }
        else if ($return->retur_type_id == 4){
            $lastwarehouse = $return->warehouse_id;

            for ($i=0; $i < sizeof($request->detailretur); $i++) {
                $detail = detailpurchasereturn::where('purchase_retur_details_id', $request->detailretur[$i])->first();
                $lastqty = $detail->qty;
                $product = $detail->product_id;

                productwarehouse::where('warehouse_id',$lastwarehouse)
                    ->where('product_id',$product)
                    ->update([
                        'quantity' => DB::raw('quantity+'.$lastqty), 
                ]);
            }
        }

        detailpurchasereturn::where('purchase_retur_id', $request->idretur)->update(["status"=>2]);

        $newtotal = 0;
        for ($i=0; $i < sizeof($request->id); $i++) {
          if($request->qty[$i] > 0)
					{
						$subtotal = $request->qty[$i] * $request->price[$i];
						$detailreturn = new detailpurchasereturn;
						$detailreturn->purchase_retur_id = $return->purchase_retur_id;
						$detailreturn->product_id = $request->id[$i];
						$detailreturn->qty = $request->qty[$i];
						$detailreturn->weight = 0;
						$detailreturn->price = $request->price[$i];
						$detailreturn->status = 1;
						$detailreturn->save();
						$newtotal += $subtotal;
					}
        }

        purchasereturn::where('purchase_retur_id', $return->purchase_retur_id)->update(['total_price'=>$newtotal]);

        if ($request->returtype == 3){
            purchaseorder::where('purchase_order_id',$po)->update([
                'reduction' => DB::raw('reduction+'.$newtotal),
            ]);
        }
        else if ($request->returtype == 4){
            for ($i=0; $i < sizeof($request->id); $i++) {
                productwarehouse::where('warehouse_id',$request->gudang)
                    ->where('product_id',$request->id[$i])
                    ->update([
                        'quantity' => DB::raw('quantity-'.$request->qty[$i]), 
                    ]);
            }
        }
    }

    public function deleteReturn(Request $request)
    {
        $return = purchasereturn::where('purchase_retur_id',$request->id)->first();

        $total = $return->total_price;
        $returtype = $return->retur_type_id;
        $po = $return->purchase_order_id;

        if ($returtype == 3){
            purchaseorder::where('purchase_order_id',$po)->update([
                'reduction' => DB::raw('reduction-'.$total),
            ]);
        }else if ($returtype == 4){
            $lastdetail = purchasereturn::where('purchase_retur.purchase_retur_id',$request->id)
                ->leftjoin('purchase_retur_details','purchase_retur.purchase_retur_id','=','purchase_retur_details.purchase_retur_id')
                ->get();

            foreach ($lastdetail as $key => $value) {
                $lastqty = $value->qty;
                $product = $value->product_id;
                $lastwarehouse = $value->warehouse_id;

                productwarehouse::where('warehouse_id',$lastwarehouse)
                    ->where('product_id',$product)
                    ->update([
                        'quantity' => DB::raw('quantity+'.$lastqty), 
                ]);
            }
        }else if($returtype == 6){
					$supplier = purchaseorder::find($po)->supplier_id;
					supplier::where('supplier_id',$supplier)->update([
						'next_invoice_reduction' => DB::raw('next_invoice_reduction-'.$total),
					]);
				}

        purchasereturn::where('purchase_retur_id',$request->id)->update(['status'=>2]);
        detailpurchasereturn::where('purchase_retur_id', $request->id)->update(["status"=>2]);
    }

    public function downloadPurchaseReturn(Request $request, $id)
    {
        $returheader = purchasereturn::leftjoin('purchase_order','purchase_order.purchase_order_id','=','purchase_retur.purchase_order_id')
            ->leftjoin('retur_type','purchase_retur.retur_type_id','=','retur_type.retur_type_id')
            ->where('purchase_retur_id', $request->id)
            ->first();

        $returdetail = detailpurchasereturn::leftjoin('product','product.product_id','=','purchase_retur_details.product_id')
            ->leftjoin('purchase_retur','purchase_retur_details.purchase_retur_id','=','purchase_retur.purchase_retur_id')
            ->leftjoin('purchase_order','purchase_retur.purchase_order_id','=','purchase_order.purchase_order_id')
            ->leftjoin('purchase_order_details','purchase_order.purchase_order_id','=','purchase_order_details.purchase_order_id')
            ->where('purchase_retur.purchase_retur_id',$id)
            ->where('purchase_retur_details.status','<>',2)
            ->select('purchase_retur_details.*','product.product_name as p_name','purchase_order_details.quantity as orderqty','purchase_order_details.free_qty')
            ->groupby('purchase_retur_details.purchase_retur_details_id')
            ->get();

        $pdf = DOMPDF::loadView('purchase_return.return_pdf', compact('returheader','returdetail'));
        return $pdf->stream('Payment Purchase.pdf');
    }
}