<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\purchaseorder;
use App\detailpurchaseorder;
use App\purchaseinvoice;
use App\detailpurchaseinvoice;
use App\log;
use App\supplier;
use App;
use DB;
use DataTable;
use DOMPDF;

class PurchaseInvoiceController extends Controller
{
	public function showPurchaseInvoicePage()
	{
		$data['po'] = purchaseorder::leftjoin(DB::raw('(
            SELECT purchase_order.purchase_order_id, purchase_order.purchase_order_number, purchase_order.grand_total_idr, sum(total_amount) as total_paid, invoice_purchase.prev_invoice_reduction 
            FROM purchase_order 
            LEFT join invoice_purchase on purchase_order.purchase_order_id = invoice_purchase.purchase_order_id 
            LEFT JOIN invoice_purchase_details on invoice_purchase.invoice_purchase_id = invoice_purchase_details.invoice_purchase_id 
            WHERE purchase_order.status <> 2 AND invoice_purchase.status <> 2
            GROUP by purchase_order_id) tableb')
			,'purchase_order.purchase_order_id','=','tableb.purchase_order_id')
			->select('purchase_order.purchase_order_id','supplier_id','purchase_order.purchase_order_number', 'purchase_order.grand_total_idr', 'tableb.total_paid as total_invoice')
			->where('purchase_order.status','<>',2)
			->Where(function ($query) {
				$query->whereRaw('tableb.total_paid+tableb.prev_invoice_reduction < purchase_order.grand_total_idr - purchase_order.reduction')
					->orWhereNull('tableb.total_paid');
			})
			->get();

		return view('purchase_invoice.purchase_invoice', compact('data'));
	}

	public function getPurchaseInvoiceTable(Request $request){
		$invoice = purchaseinvoice::leftjoin('invoice_purchase_details','invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('invoice_purchase.status','<>',2)
			->orderby('invoice_purchase.invoice_purchase_id','desc');

		if ($request->noinvoice && $request->noinvoice != "")
		{
			$invoice= $invoice->where('invoice_purchase_number','like','%'.$request->noinvoice.'%');
		}
		if ($request->nopo && $request->nopo != "")
		{
			$invoice= $invoice->where('purchase_order_number','like','%'.$request->nopo.'%');
		}
		if($request->supplier && $request->supplier != "")
		{
			$invoice = $invoice->whereRaw("company_name like '%$request->supplier%'");
		}
		$invoice = $invoice->get();

		return DataTable::of($invoice)
			->setRowAttr([
				'value' => function($invoice) {
					return $invoice->invoice_purchase_id;
				},
			])
			->addColumn('action', function ($invoice){
				if(purchaseinvoice::where('invoice_purchase_id',$invoice->invoice_purchase_id)->has('payment')->count() > 0) {
					return
						'<a class="btn btn-sm btn-raised orange print-invoice" target="_blank" href="downloadpurchaseinvoice/' . $invoice->invoice_purchase_id . '"><i class="material-icons">print</i></a>';
				}else{
					return
						'<a class="btn btn-sm btn-raised orange print-invoice" target="_blank" href="downloadpurchaseinvoice/' . $invoice->invoice_purchase_id . '"><i class="material-icons">print</i></a>
					<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
				}
			})
			->editColumn('grand_total_idr', function($invoice){
				return number_format($invoice->grand_total_idr);
			})
			->editColumn('total_amount', function($invoice){
				return number_format($invoice->total_amount);
			})
			->smart(false)
			->make(true);
	}

	public function getLastInvoiceNumber(Request $request)
	{
		$lastinvoicetoday = purchaseinvoice::where('invoice_purchase_number','like','PIN/'.date('dmy').'/%')->orderby('invoice_purchase_id','desc')->first();
		if(empty($lastinvoicetoday))
		{
			$newinvoicenumber = "PIN/".date('dmy')."/1";
			return $newinvoicenumber;
		}
		else{
			$tmpinvoice = explode('/',$lastinvoicetoday->invoice_purchase_number);
			$lastnumber = $tmpinvoice[2];
			$newinvoicenumber = "PIN/".date('dmy')."/".($lastnumber+1);
			return $newinvoicenumber;
		}
	}

	public function getPurchaseInvoice(Request $request)
	{
		$po = purchaseinvoice::where('invoice_purchase_id',$request->id)->first()->purchase_order_id;
		$invoice = purchaseinvoice::leftjoin('invoice_purchase_details','invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
			->leftjoin('purchase_order','purchase_order.purchase_order_id','=','invoice_purchase.purchase_order_id')
			->leftjoin(DB::Raw('(
                SELECT purchase_order.purchase_order_id, SUM(invoice_purchase_details.total_amount) as total 
                FROM purchase_order 
                LEFT JOIN invoice_purchase on purchase_order.purchase_order_id = invoice_purchase.purchase_order_id 
                LEFT JOIN invoice_purchase_details on invoice_purchase.invoice_purchase_id = invoice_purchase_details.invoice_purchase_id 
                WHERE invoice_purchase.status <> 2 AND purchase_order.purchase_order_id = '.$po.')tablea'
			),'purchase_order.purchase_order_id','=','tablea.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->leftjoin('payment_type','payment_type.payment_type_id','=','purchase_order.payment_type_id')
			->where('invoice_purchase.invoice_purchase_id', $request->id)
			->select('invoice_purchase.*','invoice_purchase_details.total_amount','invoice_purchase_details.note','purchase_order.grand_total_idr','purchase_order.reduction','purchase_order.discount_type','tablea.total','supplier.company_name','purchase_order.purchase_order_id','purchase_order.purchase_order_number','purchase_order.date_purchase_order','payment_type.payment_description')
			->first();

		$discount = purchaseorder::
		join('purchase_discount','purchase_discount.purchase_order_id','=','purchase_order.purchase_order_id')
			->where('purchase_order.purchase_order_id',$po)
			->where('purchase_discount.status',"<>",2)
			->select('discount','discount_period')
			->orderby('discount_period','asc')
			->get();
		return compact('invoice','discount');
	}

	// public function getPurchaseOrderData(Request $request)
	// {
	//     if($request->id == 0)
	//     {
	//         return;
	//     }
	//     $po = purchaseorder::leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')->leftjoin('payment_type','payment_type.payment_type_id','=','purchase_order.payment_type_id')->leftjoin('invoice_purchase','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')->leftjoin('invoice_purchase_details','invoice_purchase_details.invoice_purchase_id','invoice_purchase.invoice_purchase_id')->selectraw('purchase_order.purchase_order_number, purchase_order.grand_total_idr , purchase_order.date_purchase_order, sum(total_amount) as total, company_name,payment_description')->where('purchase_order.purchase_order_id',$request->id)->first();

	//     $discount = purchaseorder::leftjoin('purchase_discount','purchase_discount.purchase_order_id','=','purchase_order.purchase_order_id')->where('purchase_order.purchase_order_id',$request->id)->select('discount_percentage','discount_period')->orderby('discount_period','asc')->get();

	//     $today = strtotime('today');
	//     $podate = strtotime($po->date_purchase_order);
	//     $selected = [];
	//     foreach ($discount as $key => $value) {
	//         $daylimit = strtotime("+".$value->discount_period."days", $podate);
	//         if($today <= $daylimit ){
	//             $selected['percent'] = $value->discount_percentage;
	//             $selected['period'] = $value->discount_period;
	//             break;
	//         }
	//     }
	//     return compact('selected','po');
	// }
	public function getPurchaseOrderData(Request $request)
	{
		if($request->id == 0)
		{
			return;
		}
		$po = purchaseorder::leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->leftjoin('payment_type','payment_type.payment_type_id','=','purchase_order.payment_type_id')
			->leftjoin('invoice_purchase','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')
			->leftjoin('invoice_purchase_details','invoice_purchase_details.invoice_purchase_id','invoice_purchase.invoice_purchase_id')
			->where('purchase_order.purchase_order_id',$request->id)
			->where('purchase_order.status','<>',2)
			->where('invoice_purchase.status','<>',2)
			->selectraw('purchase_order.purchase_order_number, purchase_order.grand_total_idr , purchase_order.date_purchase_order, purchase_order.reduction, purchase_order.discount_type, sum(total_amount) as total, company_name,payment_description, supplier.next_invoice_reduction')
			->first();

		$discount = purchaseorder::join('purchase_discount','purchase_discount.purchase_order_id','=','purchase_order.purchase_order_id')
			->where('purchase_order.purchase_order_id',$request->id)
			->where('purchase_discount.status','<>',2)
			->select('discount','discount_period')
			->orderby('discount_period','asc')
			->get();

		return compact('discount','po');
	}

	public function createInvoice(Request $request)
	{
		$purchaseinvoice = new purchaseinvoice;
		$purchaseinvoice->purchase_order_id = $request->nopo;
		$purchaseinvoice->invoice_purchase_number = $request->noinvoice;
		$purchaseinvoice->date_invoice_purchase = Carbon::parse($request->tglinvoice)->format('Y-m-d');;
		$purchaseinvoice->prev_invoice_reduction = $request->prev;
		$purchaseinvoice->status = 1;
		$purchaseinvoice->save();

		if($request->prev > 0)
		{
			$supplier = purchaseorder::find($request->nopo)->supplier_id;
			supplier::where('supplier_id',$supplier)->update([
				'next_invoice_reduction' => DB::raw('next_invoice_reduction-'.$request->prev),
			]);
		}

		$detailpurchaseinvoice = new detailpurchaseinvoice;
		$detailpurchaseinvoice->invoice_purchase_id = $purchaseinvoice->invoice_purchase_id;
		$detailpurchaseinvoice->total_amount = $request->payment;
		$detailpurchaseinvoice->note = $request->note;
		$detailpurchaseinvoice->status = 1;
		$detailpurchaseinvoice->save();


		$log = new log;
		$log->actor = Session('user')->full_name;;
		$log->activity = "menambahkan";
		$log->object = "Purchase Invoice";
		$log->object_details = $purchaseinvoice->invoice_purchase_number;
		$log->status = 1;
		$log->save();
	}

	public function updateInvoice(Request $request)
	{
		$detailpurchaseinvoice = detailpurchaseinvoice::where('invoice_purchase_id',$request->id)->first();
		$detailpurchaseinvoice->total_amount = $request->payment;
		$detailpurchaseinvoice->note = $request->note;
		$detailpurchaseinvoice->save();
	}

	public function deleteInvoice(Request $request)
	{
		$invoice = purchaseinvoice::where('invoice_purchase_id', $request->id);

		$supplier = purchaseorder::find($invoice->first()->purchase_order_id)->supplier_id;
		supplier::where('supplier_id',$supplier)->update([
			'next_invoice_reduction' => DB::raw('next_invoice_reduction+'.$invoice->first()->prev_invoice_reduction),
		]);

		$invoice->update(["status" => 2]);

		detailpurchaseinvoice::where('invoice_purchase_id', $request->id)->update(["status" => 2]);
	}

	public function downloadPurchaseInvoice(Request $request, $id)
	{
		$invoiceheader = purchaseinvoice::leftjoin('invoice_purchase_details','invoice_purchase.invoice_purchase_id','=','invoice_purchase_details.invoice_purchase_id')
			->join('purchase_order','invoice_purchase.purchase_order_id','=','purchase_order.purchase_order_id')
			->leftjoin('supplier','supplier.supplier_id','=','purchase_order.supplier_id')
			->where('invoice_purchase.invoice_purchase_id',$id)
			->first();

		$podetail = purchaseorder::join('purchase_order_details',function ($join) {
			$join->on('purchase_order.purchase_order_id','=','purchase_order_details.purchase_order_id')
				->on('purchase_order_details.status','<>',DB::raw("'2'"));
		})
			->join('product','purchase_order_details.product_id','=','product.product_id')
			->leftjoin('unit','unit.unit_id','=','product.unit_id')
			->select('purchase_order_details.*','product.product_code','product.product_name','unit.unit_description')
			->where('purchase_order.purchase_order_id', $invoiceheader->purchase_order_id)
			->get();

		$discount = purchaseorder::join('purchase_discount','purchase_discount.purchase_order_id','=','purchase_order.purchase_order_id')
			->where('purchase_order.purchase_order_id',$invoiceheader->purchase_order_id)
			->where('purchase_discount.status','<>',2)
			->select('discount','discount_period')
			->orderby('discount_period','asc')
			->get();

		$pdf = DOMPDF::loadView('purchase_invoice.invoice_pdf', compact('invoiceheader','podetail','discount'));
		return $pdf->stream('Invoice PO.pdf');
	}
}
