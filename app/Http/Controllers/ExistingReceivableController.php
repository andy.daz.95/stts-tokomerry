<?php

namespace App\Http\Controllers;

use App\existingreceiveabletransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\customer;
use App\salesinvoice;
use App\paymentmethod;
use App\salespayment;
use App\detailsalespayment;
use App\log;
use App\bank;
use DB;
use DataTable;
use DOMPDF;

class ExistingReceivableController extends Controller
{
	public function showExistingReceiveablePage()
	{
		$data['customer'] = customer::all();
		$data['bank'] = bank::where('status','<>',2)->get();
		$data['paymentmethod'] =
			paymentmethod::where('status',1)
				->get();

		return view('existing_receiveable.existing_receiveable', compact('data'));
	}

	public function getExistingReceiveableTable(Request $request){
		$existingreceiveable = existingreceiveabletransaction::with('method')
			->where('status',1)
			->get();

		return DataTable::of($existingreceiveable)
			->setRowAttr([
				'value' => function($existing) {
					return $existing->existing_receiveable_transaction_id;
				},
			])
			->addColumn('customer_name', function ($existing){
				return $existing->customer->first_name.' '.$existing->customer->last_name;
			})
			->addColumn('action', function ($existing) {
				return
					'<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="'.$existing->existing_receiveable_transaction_id.'"><i class="material-icons">edit</i></a>
						<a class="btn btn-sm btn-raised orange print-payment" target="_blank" href="downloadexisting/'.$existing->existing_receiveable_transaction_id.'"><i class="material-icons">print</i></a>
						<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>';
			})
			->editColumn('payment', function ($existing){
				return number_format($existing->payment);
			})
			->smart(false)
			->make(true);
	}

	public function getCustomerExistingReceiveableTable(Request $request){
		$customer = customer::where('existing_receiveable','>',0)
			->where('status',1)
			->get();

		return DataTable::of($customer)
			->setRowAttr([
				'value' => function($customer) {
					return $customer->custoemr_id;
				},
			])
			->addColumn('customer_name', function ($customer){
				return $customer->first_name.' '.$customer->last_name;
			})
			->smart(false)
			->make(true);
	}

	public function getExistingReceiveable(Request $request)
	{
		return existingreceiveabletransaction::where('existing_receiveable_transaction_id',$request->id)->first();
	}

	public function getCustomerExistingReceiveable(Request $request)
	{
		return customer::where('customer_id',$request->id)->first();
	}

	public function createExistingReceiveable(Request $request)
	{
		$existingreceiveable = new existingreceiveabletransaction();
		$existingreceiveable->customer_id = $request->customer;
		$existingreceiveable->payment_method_id = $request->paymentmethod;
		$existingreceiveable->bank_id = $request->paymentmethod == 2 ? $request->bank : Null ;
		$existingreceiveable->transaction_number = $request->nopayment;
		$existingreceiveable->transaction_date = Carbon::createFromFormat('d-m-Y',$request->tgltransaction)->toDateString();
		$existingreceiveable->notes = $request->notes;
		$existingreceiveable->payment = $request->pembayaran;
		$existingreceiveable->receiveable_before_paid = $request->existingreceiveable;
		$existingreceiveable->status = 1;
		$existingreceiveable->save();

		customer::where('customer_id',$request->customer)->update([
			'existing_receiveable' => DB::raw('existing_receiveable-'.$request->pembayaran),
		]);
	}

	public function updateExistingReceiveable(Request $request)
	{
		$existingreceiveable = existingreceiveabletransaction::find($request->id);

		customer::where('customer_id',$existingreceiveable->customer_id)->update([
			'existing_receiveable' => DB::raw('existing_receiveable+'.$existingreceiveable->payment),
		]);


		$existingreceiveable->payment_method_id = $request->paymentmethod;
		$existingreceiveable->bank_id = $request->paymentmethod == 2 ? $request->bank : Null ;
		$existingreceiveable->transaction_date = Carbon::createFromFormat('Y-m-d',$request->tgltransaction)->toDateString();
		$existingreceiveable->notes = $request->notes;
		$existingreceiveable->payment = $request->pembayaran;
		$existingreceiveable->save();
		customer::where('customer_id',$request->customer)->update([
			'existing_receiveable' => DB::raw('existing_receiveable-'.$request->pembayaran),
		]);
	}

	public function deleteExistingReceiveable(Request $request)
	{
		$existingreceiveable = existingreceiveabletransaction::find($request->id);

		customer::where('customer_id',$existingreceiveable->customer_id)->update([
			'existing_receiveable' => DB::raw('existing_receiveable+'.$existingreceiveable->payment),
		]);
		$existingreceiveable->status = 2;
		$existingreceiveable->update();
	}

	public function downloadExistingReceiveable(Request $request, $id)
	{
		$transaction = existingreceiveabletransaction::where('existing_receiveable_transaction_id',$id)
			->first();

		$pdf = DOMPDF::loadView('existing_receiveable.transaction_pdf', compact('transaction'))
			->setPaper('A5','landscape');
		return $pdf->stream('Payment Credit.pdf');
	}
}
