<?php

namespace App\Http\Controllers;

use App\deliveryorder;
use App\detaildeliveryorder;
use App\detaildraftdelivery;
use App\detailgoodreceive;
use App\detailpurchaseinvoice;
use App\detailpurchaseorder;
use App\detailpurchasepayment;
use App\detailpurchasereturn;
use App\detailsalesorder;
use App\detailsalespayment;
use App\detailsalesreturn;
use App\detailsalesreturnreceive;
use App\draftdelivery;
use App\goodreceive;
use App\productchild;
use App\purchaseinvoice;
use App\purchaseorder;
use App\purchasepayment;
use App\purchasereturn;
use App\salesinvoice;
use App\salesorder;
use App\salespayment;
use App\salesreturn;
use App\salesreturnreceive;
use App\satuan;
use App\unitchild;
use App\warehouse;
use Illuminate\Http\Request;
use DB;
use Indonesia;
use App\product;
use App\productwarehouse;

class ExtendController extends Controller
{
	public function listCities(Request $request){
		$provinsi = $request->provinsi;
		$selectedcity = $request->city;
		$cities = DB::table('indonesia_cities as city')->join('indonesia_provinces as provinsi','city.province_id','=','provinsi.id')->select('city.id','city.name')->where('provinsi.id',$provinsi)->get();
		return view('city', compact('cities','selectedcity'));
	}
	public function listBarang(Request $request){
		$reqbarang = $request->barang;
		$src = $request->src;
		$jenispkp = $request->jenispkp;
		$barang = product::with('last_product_info')->leftjoin('product_warehouse','product.product_id','=','product_warehouse.product_id')
			->selectRaw('product.*, sum(quantity) as total_qty')
			->where('product.status',1)
			->groupby('product_id');
		if($src=="po" && $jenispkp == 0) // Non PKP
		{
			$barang = $barang->where('product.is_faktur',0); //Non Faktur
		}
		if($src=="po" && $jenispkp == 1){ // PKP
			$barang = $barang->where('product.is_faktur',1); //Faktur
		}
		$barang = $barang->get();
		return view('modal.modal-barang', compact('barang','reqbarang','src'));
	}

	public function getBarangModal(Request $request){
		$barang = $request->barang;
		$id = [];
		$nama = [];
		$harga = [];
		$hargapartai = [];
		$weight = [];
		$fakturtype = [];
		$unit = [];
		$unitchild = [];
		$stock = [];
		if(!empty($barang))
		{
			foreach ($barang as $key => $value) {
				$tempbarang = product::where('product_id',$value)->select('*')->with('qtyinwarehouse')->first();
				array_push($id,$tempbarang->product_id);
				array_push($nama,$tempbarang->product_name);
				array_push($harga,$tempbarang->price_sale);
				array_push($hargapartai,$tempbarang->price_wholesale);
				array_push($weight,$tempbarang->weight);
				array_push($fakturtype,$tempbarang->is_faktur);
				array_push($stock,$tempbarang->qtyinwarehouse->sum('quantity'));
				array_push($unit,satuan::where('unit_id',$tempbarang->unit_id)->first());

				$unitchildarr = satuan::with('unit_child')->where('unit_id', $tempbarang->unit_id)->has('unit_child')->get();
				if($unitchildarr->count() > 0){
					array_push($unitchild, $unitchildarr->first()->unit_child);
				}else{
					array_push($unitchild, null);
				}
			}
//			return compact('barang','id','nama','harga','hargapartai','weight','fakturtype','stock','unit','unitchild');
				return ['barang'=>$barang, 'id'=>$id, 'nama'=>$nama, 'harga'=>$harga, 'hargapartai'=>$hargapartai, 'weight'=>$weight, 'fakturtype'=>$fakturtype, 'stock'=>$stock, 'unit'=>$unit, 'unitchild'=>$unitchild, 'objectArr'=> $request->objectArr];
		}
		return ['barang'=>[]];
	}
	public function getBarangModalStockTransfer(Request $request){
		$objectArr =  $request->objectArr;
		$barang = $request->barang;
		if(!empty($barang))
		{
			$from = $request->from;
			$to = $request->to;
			$product = product::whereIn('product_id',$barang)
			->with(['qty_from' => function ($query) use ($from) {
				$query->where('warehouse_id', $from);
			}])
			->with(['qty_to' => function ($query) use ($to) {
				$query->where('warehouse_id', $to);
			}])
			->get();
			return compact('product', 'objectArr');
		}
		return ['barang'=>[]];
	}

	public function getPriceByUnit(Request $request){
		if($request->type == 'main'){
			$product = product::where('product_id',$request->product)
				->where('unit_id',$request->unit)
				->where('status',1)
				->first();
		}else if($request->type == 'child'){
			$product = productchild::where('product_id',$request->product)
				->where('unit_child_id',$request->unit)
				->where('status',1)
				->first();
		}
		return $product;

	}

	public function getUnitByProduct(Request $request)
	{
		$product = product::where('product_id',$request->product)->with('satuan.unit_child')->first();
		return $product;
	}

	public function  getQtyByProduct(Request $request)
	{
		$product_warehouse = productwarehouse::where('product_id',$request->product)->get();

		foreach($product_warehouse as $value)
		{
			$warehouse = warehouse::find($value->warehouse_id)->warehouse_name;
			// $warehouse = str_replace(' ','',$warehouse);
			if($request->unittype == "child")
			{
				$multiplier = unitchild::find($request->unit)->multiplier;
				$qty[$warehouse] = ((float) $value->quantity * $multiplier);
			}else{
				$qty[$warehouse] = $value->quantity;
			}
		}

		return $qty;
	}

	public function getDetailPoByProduct(Request $request)
	{
		$detailpo =
		detailpurchaseorder::with('product')
			->with('po')
			->where('product_id',$request->product)
			->whereDoesntHave('detailsm', function($q){
				$q->havingRaw('SUM(quantity) >= purchase_order_details.quantity + purchase_order_details.free_qty');
			})
			->get();

		return view('modal.modal-detail-po', compact('detailpo'));
	}

	public function truncateSales(Request $request){
		DB::statement("SET foreign_key_checks=0");
		salesorder::truncate();
		detailsalesorder::truncate();
		draftdelivery::truncate();
		detaildraftdelivery::truncate();
		deliveryorder::truncate();
		detaildeliveryorder::truncate();
		salesinvoice::truncate();
		salespayment::truncate();
		detailsalespayment::truncate();
		salesreturn::truncate();
		detailsalesreturn::truncate();
		salesreturnreceive::truncate();
		detailsalesreturnreceive::truncate();
		DB::statement("SET foreign_key_checks=1");
	}

	public function truncatePurchase(Request $request){
		DB::statement("SET foreign_key_checks=0");
		purchaseorder::truncate();
		detailpurchaseorder::truncate();
		goodreceive::truncate();
		detailgoodreceive::truncate();
		purchaseinvoice::truncate();
		detailpurchaseinvoice::truncate();
		purchasepayment::truncate();
		detailpurchasepayment::truncate();
		purchasereturn::truncate();
		detailpurchasereturn::truncate();
		DB::statement("SET foreign_key_checks=1");
	}

}