<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\bank;

class BankController extends Controller
{
    public function showBankPage()
    {
    	$bank = bank::all();
    	return view('bank.bank', compact('bank'));
    }
    public function createBank(Request $request)
	{
		$bank = new bank;
		$bank->bank_name = $request->bank;
		$bank->status = 1;
		$bank->save();
	}
	public function getBankData(Request $request){
		$id = $request->id;
		$bank = bank::where('bank_id',$id)->select('*')->first();
		return $bank;
	}

	public function modalBankData(Request $request){
		$id= $request->id;
		$data= bank::where('bank_id',$id)->first();
		return view('bank.modal_bank', compact('data'));
	}

	public function deleteBank(Request $request)
	{
		$id = $request->id;
		bank::where('bank_id',$id)->delete();
	}

	public function updateBank(Request $request)
	{
		$id = $request->id;
		bank::where('bank_id', $id)->update(
			[
			'bank_name' => $request->bank,
			]
			);
	}
}