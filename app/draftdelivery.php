<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\customer;

class draftdelivery extends Model
{
    //
    protected $table = 'draft_delivery_order';
    protected $primaryKey='draft_delivery_order_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function so(){
    	return $this->belongsTo('App\salesorder','sales_order_id','sales_order_id')
        ->where('status',1);
    }

    public function details(){
    	return $this->hasMany('App\detaildraftdelivery','draft_delivery_order_id','draft_delivery_order_id')
        ->where('status',1);
    }

    public function delivery(){
    	return $this->hasMany('App\deliveryorder','draft_delivery_order_id','draft_delivery_order_id')
        ->where('status',1);
    }

    public function retur(){
        return $this->hasOne('App\salesreturn','sales_retur_id','sales_retur_id')
        ->where('status',1);
    }

    public function warehouse(){
        return $this->hasOne('App\warehouse','warehouse_id','warehouse_id')
        ->where('status',1);
    }

    public function customer(){
        return $this->hasOne('App\customer','customer_id','customer_id')
        ->where('status',1);
    }
}