<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class satuan extends Model
{
    //
    protected $table = 'unit';
    protected $primaryKey='unit_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

	public function unit_child(){
		return $this->hasOne('App\unitchild','unit_id','unit_id');
	}
}