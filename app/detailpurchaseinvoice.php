<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailpurchaseinvoice extends Model
{
    //
    protected $table = 'invoice_purchase_details';
    protected $primaryKey='invoice_purchase_details_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';
}