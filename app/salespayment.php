<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salespayment extends Model
{
    //
    protected $table = 'payment_sales';
    protected $primaryKey='payment_sales_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function details(){
    	return $this->hasMany('App\detailsalespayment','payment_sales_id','payment_sales_id')
				->where('status',1);
		}

		public function invoice(){
        return $this->belongsTo('App\salesinvoice','invoice_sales_id','invoice_sales_id')
                ->where('status',1);
        }

        public function all_payments_for_this_invoice(){
    	return $this->hasMany('App\salespayment','invoice_sales_id','invoice_sales_id')
                ->where('status',1);
		}
}