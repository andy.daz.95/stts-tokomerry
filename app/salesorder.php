<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salesorder extends Model
{
    //
    protected $table = 'sales_order';
    protected $primaryKey='sales_order_id';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'last_update';

    public function details(){
      return $this->hasMany('App\detailsalesorder','sales_order_id','sales_order_id')
        ->where('status',1);
    }

    public function account(){
      return $this->hasOne('App\account','account_id','account_id')
				->where('status',1);
    }

    public function details_faktur(){
      return $this->details()->whereHas('product', function ($query){
      	$query->where('is_faktur',1);
			});
    }

    public function totalcogs(){
      return $this->hasOne('App\detailsalesorder','sales_order_id','sales_order_id')
      ->selectRaw('sales_order_id, Sum(cogs) as totalcogs')
				->where('status',1)
      ->groupBy('sales_order_id');
    }

    public function details_non_faktur(){
      return $this->details()->whereHas('product', function ($query){
				$query->where('is_faktur',0);
			});
    }

    public function customer(){
    	return $this->belongsTo('App\customer','customer_id','customer_id');
    }

    public function total_faktur(){
      return $this->details()
      ->join('product','sales_order_details.product_id','=','product.product_id')
      ->where('product.is_faktur',1)
      ->selectRaw('sales_order_id, Sum(sub_total) as f_total')
      ->groupBy('sales_order_id');
    }

    public function total_non_faktur(){
      return $this->details()
      ->join('product','sales_order_details.product_id','=','product.product_id')
      ->where('product.is_faktur',0)
      ->selectRaw('sales_order_id, Sum(sub_total) as f_total')
      ->groupBy('sales_order_id');
    }

    public function draftDelivery(){
    	return $this->hasMany('App\draftdelivery','sales_order_id','sales_order_id')
				->where('status',1);
		}

    public function delivery(){
      return $this->hasManyThrough(
            'App\deliveryorder',
            'App\draftdelivery',
            'sales_order_id', // Foreign key on users table...
            'draft_delivery_order_id', // Foreign key on posts table...
            'sales_order_id', // Local key on countries table...
            'draft_delivery_order_id' // Local key on users table...
        )->where('delivery_order.status',1)
        ->where('draft_delivery_order.status',1);
    }

    public function invoice(){
    	return $this->hasOne('App\salesinvoice','sales_order_id','sales_order_id')
				->where('status',1);
		}
}