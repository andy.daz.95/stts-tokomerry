<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();
Route::get('/login', 'UserController@login')->name('login');
Route::get('/logout', 'UserController@logout');
Route::post('/dologin', 'AuthController@authenticate')->name('auth');
Route::get('/grantaccess', 'AuthController@showGrantAccessPage');
Route::get('/validmaster', 'AuthController@validMaster');

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'HomeController@showMainPage');
	Route::get('/homes', 'HomeController@showHomePage');
	Route::get('/home/news','HomeController@loadhomenews');
	Route::get('/home/piutang-so','HomeController@loadhomepiutangso');
	Route::get('/home/piutang-po','HomeController@loadhomepiutangpo');
	Route::get('/home/utang-pelanggan','HomeController@loadhomeutangpelanggan');

	Route::get('/user_profile', 'UserController@showProfilePage');
	Route::get('/manage_user', 'UserController@showManageUserPage');
	Route::get('/change_password', 'UserController@showChangePassword');
	Route::get('/get_account', 'UserController@getAccountData');
	Route::get('/get_account_manage', 'UserController@getAccountManage');
	Route::post('/update_account', 'UserController@updateAccount');
	Route::post('/update_account_manage', 'UserController@updateAccountManage');
	Route::post('/create_account', 'UserController@createAccount');
	Route::post('/update_password', 'UserController@changePassword');
	Route::post('/delete_account', 'UserController@deleteAccount');

	Route::get('/pelanggan', 'PelangganController@showPelangganPage');
	Route::get('/getPelangganTable', 'PelangganController@getPelangganTable');
	Route::get('/getPelangganData', 'PelangganController@getPelangganData');
	Route::get('/modalPelangganData', 'PelangganController@modalPelangganData');
	Route::get('/deletePelangdgan', 'PelangganController@deletePelanggan');
	Route::get('/getlimitkredit', 'PelangganController@getLimitKredit');
	Route::post('/createPelanggan', 'PelangganController@createPelanggan');
	Route::post('/updatePelanggan', 'PelangganController@updatePelanggan');
	Route::post('/importPelanggan', 'PelangganController@importPelanggan');
	Route::get('/balance', 'PelangganController@showBalancePage');
	Route::get('/detail-balance', 'PelangganController@showBalanceDetail');
	Route::get('/gettransactionforbalance', 'PelangganController@getTransaction');

	Route::get('/supplier', 'SupplierController@showSupplierPage');
	Route::get('/getSupplierData', 'SupplierController@getSupplierData');
	Route::get('/filterSupplier', 'SupplierController@filterSupplier');
	Route::get('/modalSupplierData', 'SupplierController@modalSupplierData');
	Route::get('/deleteSupplier', 'SupplierController@deleteSupplier');
	Route::post('/createSupplier', 'SupplierController@createSupplier');
	Route::post('/importSupplier', 'SupplierController@importSupplier');
	Route::post('/updateSupplier', 'SupplierController@updateSupplier');

	Route::get('/target', 'SupplierController@showTargetPage');
	Route::post('/createtarget', 'SupplierController@createTarget');

	Route::get('/kategori_barang', 'KategoriBarangController@showKategoriBarangPage');
	Route::get('/getKategoriData', 'KategoriBarangController@getKategoriData');
	Route::get('/modalKategoriData', 'KategoriBarangController@modalkategoriData');
	Route::get('/deleteKategoriBarang', 'KAtegoriBarangController@deleteKategoriBarang');
	Route::post('/createKategoriBarang', 'KategoriBarangController@createKategoriBarang');
	Route::post('/updateKategoriBarang', 'KategoriBarangController@updateKategoriBarang');

	Route::get('/barang', 'BarangController@showBarangPage');
	Route::get('/getBarangTable', 'BarangController@getBarangTable');
	Route::get('/getBarangData', 'BarangController@getBarangData');
	Route::get('/filterBarang', 'BarangController@filterBarang');
	Route::get('/modalBarangData', 'BarangController@modalBarangData');
	Route::get('/deleteBarang', 'BarangController@deleteBarang');
	Route::post('/createBarang', 'BarangController@createBarang');
	Route::post('/importBarang', 'BarangController@importBarang');
	Route::post('/updateBarang', 'BarangController@updateBarang');

	Route::get('/spandek', 'BarangController@showSpandekPage');
	Route::get('/getSpandekTable', 'BarangController@getSpandekTable');
	Route::get('/getSpandekData', 'BarangController@getSpandekData');
	Route::post('/createSpandek', 'BarangController@createSpandek');
	Route::post('/updateSpandek', 'BarangController@updateBarang');
	Route::get('/deleteSpandek', 'BarangController@deleteBarang');

	Route::get('/satuan', 'SatuanController@showSatuanPage');
	Route::get('/getSatuanData', 'SatuanController@getSatuanData');
	Route::get('/filterSatuan', 'SatuanController@filterSatuan');
	Route::get('/modalSatuanData', 'SatuanController@modalSatuanData');
	Route::get('/deleteSatuan', 'SatuanController@deleteSatuan');
	Route::get('/checkUnitChild', 'SatuanController@checkUnitChild');
	Route::post('/createSatuan', 'SatuanController@createSatuan');
	Route::post('/createAnakSatuan', 'SatuanController@createAnakSatuan');
	Route::get('/getAnakSatuan', 'SatuanController@getAnakSatuan');
	Route::post('/updateSatuan', 'SatuanController@updateSatuan');
	Route::post('/updateAnakSatuan', 'SatuanController@updateAnakSatuan');
	Route::post('/importSatuan', 'SatuanController@importSatuan');

	Route::get('/anaksatuan', 'SatuanController@showAnakSatuanPage');

	Route::get('/news', 'NewsController@showNewsPage');
	Route::get('/getNewsData', 'NewsController@getNewsData');
	Route::get('/filterNews', 'NewsController@filterNews');
	Route::get('/modalNewsData', 'NewsController@modalNewsData');
	Route::get('/deleteNews', 'NewsController@deleteNews');
	Route::post('/createNews', 'NewsController@createNews');
	Route::post('/updateNews', 'NewsController@updateNews');

	Route::get('/bank', 'BankController@showBankPage');
	Route::get('/getBankData', 'BankController@getBankData');
	Route::get('/modalBankData', 'BankController@modalBankData');
	Route::get('/deleteBank', 'BankController@deleteBank');
	Route::post('/createBank', 'BankController@createBank');
	Route::post('/updateBank', 'BankController@updateBank');

	Route::get('/gudang', 'GudangController@showGudangPage');
	Route::get('/getGudangData', 'GudangController@getGudangData');
	Route::get('/filterGudang', 'GudangController@filterGudang');
	Route::get('/modalGudangData', 'GudangController@modalGudangData');
	Route::get('/deleteGudang', 'GudangController@deleteGudang');
	Route::post('/createGudang', 'GudangController@createGudang');
	Route::post('/updateGudang', 'GudangController@updateGudang');

	Route::get('/salesorder', 'SalesOrderController@showSalesOrderPage');
	Route::get('/getsalesorder', 'SalesOrderController@getsalesOrder');
	Route::get('/getsalestable', 'SalesOrderController@getSalesTable');
	Route::get('/lastsalesordernumber', 'SalesOrderController@getLastSalesNumber');
	Route::post('/createsalesorder', 'SalesOrderController@createSalesOrder');
	Route::post('/updatesalesorder', 'SalesOrderController@updateSalesOrder');
	Route::post('/deletesalesorder', 'SalesOrderController@deleteSalesOrder');
	Route::get('/downloadsalesorder/{id}/{size?}', 'SalesOrderController@downloadSalesOrder');

	Route::get('/draftdelivery', 'DraftDeliveryController@showDraftDeliveryPage');
	Route::get('/getdrafttable', 'DraftDeliveryController@getDraftTable');
	Route::get('/getdraftdelivery', 'DraftDeliveryController@getDraftDelivery');
	Route::get('/getsofordraft', 'DraftDeliveryController@getSalesOrderData');
	Route::get('/getreturfordraft', 'DraftDeliveryController@getSalesReturData');
	Route::get('/getstockfordraft', 'DraftDeliveryController@getStockForDraft');
	Route::post('/createdraftdelivery/{type}', 'DraftDeliveryController@createDraftDelivery');
	Route::post('/updatedraftdelivery', 'DraftDeliveryController@updateDraftDelivery');
	Route::post('/deletedraftdelivery', 'DraftDeliveryController@deleteDraftDelivery');
	Route::get('/downloaddraftdelivery/{id}/{size?}', 'DraftDeliveryController@downloadDraftDelivery');

	Route::get('/deliveryorder', 'DeliveryOrderController@showDeliveryOrderPage');
	Route::get('/getdeliveryorder', 'DeliveryOrderController@getDeliveryOrder');
	Route::get('/getdeliverytable', 'DeliveryOrderController@getDeliveryTable');
	Route::get('/lastdeliveryordernumber', 'DeliveryOrderController@getLastDeliveryNumber');
	Route::get('/getdraftfordelivery', 'DeliveryOrderController@getDraftDeliveryData');
	Route::get('/getStockInWarehouse', 'DeliveryOrderController@getStockInWarehouse');
	Route::post('/createdeliveryorder', 'DeliveryOrderController@createDeliveryOrder');
	Route::post('/updatedeliveryorder', 'DeliveryOrderController@updateDeliveryOrder');
	Route::post('/deletedeliveryorder', 'DeliveryOrderController@deleteDeliveryOrder');
	Route::get('/downloaddeliveryorder/{id}/{size?}', 'DeliveryOrderController@downloadDeliveryOrder');

	Route::get('/historydo', 'HistoryDeliveryOrderController@showHistoryDOPage');
	Route::get('/gethistorydobydraft', 'HistoryDeliveryOrderController@getHistoryByDraft');
	Route::get('/downloadremainingdraft/{id}/{size?}', 'HistoryDeliveryOrderController@downloadRemainingDraft');

	Route::get('/salesinvoice','SalesInvoiceController@showSalesInvoicePage');
	Route::get('/getsalesinvoice','SalesInvoiceController@getSalesInvoice');
	Route::get('/getsalesinvoicetable','SalesInvoiceController@getSalesInvoiceTable');
	Route::get('/getsoforinvoice','SalesInvoiceController@getSalesOrderData');
	Route::get('/lastsalesinvoicenumber','SalesInvoiceController@getLastInvoiceNumber');
	Route::post('/createsalesinvoice','SalesInvoiceController@createInvoice');
	Route::post('/updatesalesinvoice','SalesInvoiceController@updateInvoice');
	Route::post('/deletesalesinvoice','SalesInvoiceController@deleteInvoice');
	Route::get('/downloadsalesinvoice/{id}','SalesInvoiceController@downloadSalesInvoice');

	Route::get('/salespayment','SalesPaymentController@showSalesPaymentPage');
	Route::get('/getsalespayment','SalesPaymentController@getSalesPayment');
	Route::get('/getsalespaymenttable','SalesPaymentController@getSalesPaymentTable');
	Route::get('/lastsalespaymentnumber','SalesPaymentController@getLastPaymentNumber');
	Route::get('/getcustomerforpayment','SalesPaymentController@getCustomerData');
	Route::get('/getinvoiceforpayment','SalesPaymentController@getInvoiceData');
	Route::post('/createpayment','SalesPaymentController@CreatePayment');
	Route::post('/updatepayment','SalesPaymentController@updatePayment');
	Route::post('/deletepayment','SalesPaymentController@deletePayment');
	Route::get('/downloadsalespayment/{id}','SalesPaymentController@downloadSalesPayment');

	Route::get('/existingreceiveable', 'ExistingReceivableController@showExistingReceiveablePage');
	Route::get('/getcustomerexistingreceiveabletable','ExistingReceivableController@getCustomerExistingReceiveableTable');
	Route::get('/getexistingreceiveabletable','ExistingReceivableController@getExistingReceiveableTable');
	Route::get('/getexistingreceiveable', 'ExistingReceivableController@getExistingReceiveable');
	Route::get('/getcustomerexistingreceiveable','ExistingReceivableController@getCustomerExistingReceiveable');
	Route::post('/createexistingreceiveable', 'ExistingReceivableController@createExistingReceiveable');
	Route::post('/updateexistingreceiveable', 'ExistingReceivableController@updateExistingReceiveable');
	Route::post('/deleteexistingreceiveable', 'ExistingReceivableController@deleteExistingReceiveable');
	Route::get('/downloadexistingreceiveable/{id}', 'ExistingReceivableController@downloadExistingReceiveable');

	Route::get('/salesreturn','SalesReturnController@showSalesReturnPage');
	Route::get('/getsalesreturn','SalesReturnController@getSalesReturn');
	Route::get('/getsalesreturntable','SalesReturnController@getSalesReturnTable');
	Route::get('/lastsalesreturnnumber','SalesReturnController@getLastReturNumber');
	Route::get('/getsoforreturn','SalesReturnController@getSalesOrderData');
	Route::post('/createreturn','SalesReturnController@createReturn');
	Route::post('/updatereturn','SalesReturnController@updateReturn');
	Route::post('/deletereturn','SalesReturnController@deleteReturn');
	Route::get('/downloadsalesreturn/{id}','SalesReturnController@downloadSalesReturn');

	Route::get('/salesreturnreceive','SalesReturnReceiveController@showSalesReturnReceivePage');
	Route::get('/getsalesreturnreceive','SalesReturnReceiveController@getSalesreturnReceive');
	Route::get('/getsalesreturnreceivetable','SalesReturnReceiveController@getSalesReturnReceiveTable');
	Route::get('/getreturforreceive','SalesReturnReceiveController@getSalesReturnData');
	Route::post('/createpenerimaanreturn','SalesReturnReceiveController@createReturnReceive');
	Route::post('/deletepenerimaanreturn','SalesReturnReceiveController@deleteReturnReceive');
	Route::get('/downloadsalesreturnreceive/{id}','SalesReturnReceiveController@downloadSalesReturnReceive');

	Route::get('/show_grafik_target','ReportController@showGrafikTarget');
	Route::get('/grafik_target','ReportController@getGrafikTarget');
	Route::get('/grafik_penjualan','ReportController@showGrafikPenjualan');
	Route::get('/grafik_penjualan/{startdate}/{enddate}','ReportController@getGrafikData');

	Route::get('/cities', 'ExtendController@listCities');
	Route::get('/modalListBarang', 'ExtendController@listBarang');
	Route::get('/getBarangModal', 'ExtendController@getBarangModal');
	Route::get('/getBarangModalStockTransfer', 'ExtendController@getBarangModalStockTransfer');
	Route::get('/getPriceByUnit', 'ExtendController@getPriceByUnit');
	Route::get('/getUnitByProduct', 'ExtendController@getUnitByProduct');
	Route::get('/getQtyByProduct', 'ExtendController@getQtyByProduct');
	Route::get('/getDetailPoByProduct', 'ExtendController@getDetailPoByProduct');

	Route::get('/refundhistory','SalesReturnController@showRefundHistory');
	Route::get('/getrefundhistorytable','SalesReturnController@getRefundHistoryTable');

	Route::get('/purchaseorder', 'PurchaseOrderController@showPurchaseOrderPage');
	Route::get('/getpurchaseorder', 'PurchaseOrderController@getPurchaseOrder');
	Route::get('/getpurchaseordertable', 'PurchaseOrderController@getPurchaseOrderTable');
	Route::get('/checkponumber', 'PurchaseOrderController@checkPurchaseNumber');
	Route::get('/lastpurchaseordernumber','PurchaseOrderController@getLastPurchaseNumber');
	Route::post('/createpurchaseorder', 'PurchaseOrderController@createPurchaseOrder');
	Route::post('/updatepurchaseorder', 'PurchaseOrderController@updatePurchaseOrder');
	Route::post('/deletepurchaseorder', 'PurchaseOrderController@deletePurchaseOrder');
	Route::get('/downloadpurchaseorder/{id}', 'PurchaseOrderController@downloadPurchaseOrder');

	Route::get('/penerimaanbarang', 'PenerimaanController@showPenerimaanBarangPage');
	Route::get('/getpenerimaan', 'PenerimaanController@getPenerimaan');
	Route::get('/getpenerimaantable', 'PenerimaanController@getPenerimaanTable');
	Route::get('/lastbpbnumber','PenerimaanController@getLastBpbNumber');
	Route::get('/getpoforpenerimaan', 'PenerimaanController@getPurchaseOrderData');
	Route::post('/createpenerimaanbarang','PenerimaanController@createPenerimaanBarang');
	Route::post('/updatepenerimaanbarang','PenerimaanController@updatePenerimaanBarang');
	Route::post('/deletepenerimaanbarang','PenerimaanController@deletePenerimaanBarang');
	Route::get('/downloadpenerimaanbarang/{id}','PenerimaanController@downloadPenerimaanBarang');

	Route::get('/purchaseinvoice', 'PurchaseInvoiceController@showPurchaseInvoicePage');
	Route::get('/getpurchaseinvoice','PurchaseInvoiceController@getPurchaseInvoice');
	Route::get('/getpurchaseinvoicetable','PurchaseInvoiceController@getPurchaseInvoiceTable');
	Route::get('/lastpurchaseinvoicenumber','PurchaseInvoiceController@getLastInvoiceNumber');
	Route::get('/getpoforinvoice','PurchaseInvoiceController@getPurchaseOrderData');
	Route::post('/createpurchaseinvoice','PurchaseInvoiceController@createInvoice');
	Route::post('/updatepurchaseinvoice','PurchaseInvoiceController@updateInvoice');
	Route::post('/deletepurchaseinvoice','PurchaseInvoiceController@deleteInvoice');
	Route::get('/downloadpurchaseinvoice/{id}','PurchaseInvoiceController@downloadPurchaseInvoice');

	Route::get('/discountadjust','DiscountAdjustController@showDiscountAdjustPage');
	Route::get('/getdiskonforadjust','DiscountAdjustController@getDiscountData');
	Route::post('/creatediscountadjust','DiscountAdjustController@createDiscountAdjust');

	Route::get('/purchasepayment', 'PurchasePaymentController@showPurchasePaymentPage');
	Route::get('/getpurchasepayment','PurchasePaymentController@getPurchasePayment');
	Route::get('/getpurchasepaymenttable','PurchasePaymentController@getPurchasePaymentTable');
	Route::get('/lastpurchasepaymentnumber','PurchasePaymentController@getLastPaymentNumber');
	Route::get('/getpurchaseinvoiceforpayment','PurchasePaymentController@getInvoiceData');
	Route::get('/getdiscountforpayment','PurchasePaymentController@getDiscountData');
	Route::post('/createpurchasepayment','PurchasePaymentController@CreatePayment');
	Route::post('/updatepurchasepayment','PurchasePaymentController@updatePayment');
	Route::post('/deletepurchasepayment','PurchasePaymentController@deletePayment');
	Route::get('/downloadpurchasepayment/{id}','PurchasePaymentController@downloadPurchasePayment');

	Route::get('/purchasereturn','PurchaseReturnController@showPurchaseReturnPage');
	Route::get('/getpurchasereturn','PurchaseReturnController@getPurchaseReturn');
	Route::get('/getpurchasereturntable','PurchaseReturnController@getPurchaseReturnTable');
	Route::get('/lastpurchasereturnnumber','PurchaseReturnController@getLastReturNumber');
	Route::get('/getpoforreturn','PurchaseReturnController@getPurchaseOrderData');
	Route::post('/createpurchasereturn','PurchaseReturnController@createreturn');
	Route::post('/updatepurchasereturn','PurchaseReturnController@updatereturn');
	Route::post('/deletepurchasereturn','PurchaseReturnController@deletereturn');
	Route::get('/downloadpurchasereturn/{id}','PurchaseReturnController@downloadPurchaseReturn');

	Route::get('/stockoverview', 'InventoryController@showStockOverviewPage');
	Route::get('/detailStock', 'InventoryController@detailStock');

	Route::get('/additionalinfo','InventoryController@showAdditionalInfoPage');
	Route::get('/getadditionalinfotable','InventoryController@getAdditionalInfoTable');
	Route::get('/getadditionalinfodata','InventoryController@getAdditionalInfoData');
	Route::get('/getadditionalinfodatabyid','InventoryController@getAdditionalInfoDataById');
	Route::get('/createadditionalinfo','InventoryController@createAdditionalInfo');
	Route::post('/updateadditionalinfo','InventoryController@updateAdditionalInfo');
	Route::post('/deleteadditionalinfo','InventoryController@deleteAdditionalInfo');

	Route::get('/stocktransfer/{source?}', 'InventoryController@showStockTransferPage');
	Route::get('/getstocktransfer', 'InventoryController@getStockTransfer');
	Route::get('/laststocktransfernumber', 'InventoryController@getLastTransferNumber');
	Route::get('/getBarangTransfer', 'InventoryController@getBarangTransfer');
	Route::post('/createstocktransfer', 'InventoryController@createStockTransfer');
	Route::post('/acceptstocktransfer', 'InventoryController@acceptStockTransfer');
	Route::post('/updatestocktransfer', 'InventoryController@updateStockTransfer');
	Route::post('/deletestocktransfer', 'InventoryController@deleteStockTransfer');
	Route::get('/downloadstocktransfer/{id}','InventoryController@downloadStockTransfer');

	Route::get('/kartustock', 'InventoryController@showKartuStockPage');
	Route::get('/getkartustock', 'InventoryController@getKartuStock');

	Route::get('/stockcorrection', 'InventoryController@showStockCorrection');
	Route::get('/laststockcorrectionnumber', 'InventoryController@getLastCorrectionNumber');
	Route::get('/getstockinwarehouse', 'InventoryController@getStockInWarehouse');
	Route::post('/createstockcorrection', 'InventoryController@createStockCorrection');
	Route::post('/approvestockcorrection', 'InventoryController@approveStockCorrection');

	Route::get('/log', 'LogController@showLogPage');

	Route::get('/expense', 'ExpenseController@showExpensePage');
	Route::get('/getexpense', 'ExpenseController@getExpense');
	Route::get('/lastexpensenumber', 'ExpenseController@getLastExpenseNumber');
	Route::post('/createexpense', 'ExpenseController@createExpense');
	Route::post('/updateexpense', 'ExpenseController@updateExpense');
	Route::post('/deleteexpense', 'ExpenseController@deleteExpense');

	Route::get('reportpo', 'ReportController@showReportPo');
	Route::get('detailreportpo', 'ReportController@getDetailPo');
	Route::get('reportpaymentpo', 'ReportController@reportPaymentPO');
	Route::get('reportunsettledpo', 'ReportController@showReportUnsettledPaymentPO');
	Route::get('excelpo/{start}/{end}/{supplier?}','ReportController@downloadPoExcel');
	Route::get('excelpaymentpo/{start}/{end}', 'ReportController@downloadPaymentPoExcel');
	Route::get('excelunsettledpo/{start}/{end}/{supplier}','ReportController@downloadUnsettledPaymentPoExcel');
	Route::get('reportpobyproduct', 'ReportController@showReportPoByProduct');
	Route::get('excelpobyproduct/{start}/{end}','ReportController@downloadPoByProductExcel');
	Route::get('reportso', 'ReportController@showReportSo');
	Route::get('reportunsettledso', 'ReportController@showReportUnsettledPaymentSO');
	Route::get('excelso/{start}/{end}/{customer}','ReportController@downloadSoExcel');
	Route::get('excelpaymentso/{start}/{end}', 'ReportController@downloadPaymentSoExcel');
	Route::get('excelunsettledso/{start}/{end}/{customer}','ReportController@downloadUnsettledPaymentSoExcel');
	Route::get('reportpaymentso', 'ReportController@reportPaymentSo');
	Route::get('detailreportso', 'ReportController@getDetailSo');
	Route::get('reportdo', 'ReportController@showReportDo');
	Route::get('exceldo/{start}/{end}/{product}','ReportController@downloadDoExcel');
	Route::get('reportdoremaining', 'ReportController@showReportDoRemaining');
	Route::get('reportdodaily', 'ReportController@showReportDoDaily');
	Route::get('exceldodaily/{start}/{end}/{product}','ReportController@downloadDoDailyExcel');
	Route::get('reportincome', 'ReportController@showReportIncome');
	Route::get('reportexpense', 'ReportController@showReportExpense');
	Route::get('reportfaktur', 'ReportController@showReportFaktur');
	Route::get('printreportfaktur/{start}/{end}/{fakturtype}/{datatype}', 'ReportController@printReportFaktur');
	Route::get('reportfakturpo', 'ReportController@reportFakturPO');
	Route::get('reportpaymentfakturpo', 'ReportController@reportPaymentFakturPO');
	Route::get('printreportfakturpo/{start}/{end}/{fakturtype}', 'ReportController@printReportFakturPO');
	Route::get('excelpaymentfakturpo/{start}/{end}/{fakturtype}', 'ReportController@downloadPaymentFakturPoExcel');
	Route::get('reportnetsales', 'ReportController@showReportNetSales');
	Route::get('printreportnetsales/{start}/{end}', 'ReportController@printReportNetSales');
	Route::get('reportprofitloss', 'ReportController@showReportProfitLoss');
	Route::get('printreportprofitloss/{start}/{end}', 'ReportController@printReportProfitLoss');
	Route::get('excelexpense/{start}/{end}', 'ReportController@downloadExpenseExcel');
	Route::get('reportcustomerpurchase', 'ReportController@showReportCustomerPurchase');
	Route::get('getreportcustomerpurchase', 'ReportController@getReportCustomerPurchase');
	Route::get('reportremainingparangloe', 'ReportController@showReportRemainingParangloe');
	Route::get('printreportremainingparangloe', 'ReportController@printReportRemainingParangloe');

	Route::get('reportstockmovement', 'ReportController@showReportStockMovement');
	Route::get('excelstockmovement/{start}/{end}', 'ReportController@downloadStockMovementExcel');
	Route::get('unittest/truncatesales','ExtendController@truncateSales');
	Route::get('unittest/truncatepurchase','ExtendController@truncatePurchase');
});

