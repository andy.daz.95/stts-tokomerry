// NOTE : Need Jquery Initalized
function ceknumber(number,nullable){
	var nullable = nullable || 0;

	if(nullable != 'nullable' && number == "")
    {
        return false;
    }else{
        // var regex = new RegExp('^\\d+$');
        var regex = new RegExp('^\\d*\\.?\\d*$');
        return regex.test(number);
    }
}
function required(array){
	// return $('input[name=namabelakang]').val();
	var elementname = "";
	var kosong = false;
	$.each(array, function(i){
		element = $("input[name="+array[i]+"]");
		if(element.length == 0)
		{
			element = $("textarea[name="+array[i]+"]");
		}
		if(element.length == 0)
		{
			element = $("select[name="+array[i]+"]");
		}
		
		if(element.val()=="")
		{
			elementname = element.closest('.input-field').find('label').html();
			kosong = true;
			return false;
		}
	});

	if(kosong == true)
	{
		return elementname;
	}else{
		return 0;
	}
}
function cekmanynumbers(array){
	var elementname = "";
	var notnumber = false;
	$.each(array, function(i){
		element = $("input[name="+array[i]+"]");
		if(element.val() != "")
		{
			if(ceknumber(element.val()) == false){
				elementname = element.closest('.input-field').find('label').html();
				notnumber = true;
				return false;
			} 
		}
	});

	if(notnumber == true)
	{
		return elementname;
	}else{
		return 0;
	}
}

