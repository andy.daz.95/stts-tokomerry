// Materialize initialization
(function($) {
    $.fn.invisible = function() {
        return this.each(function() {
            $(this).css("visibility", "hidden");
        });
    };
    $.fn.visible = function() {
        return this.each(function() {
            $(this).css("visibility", "visible");
        });
    };
}(jQuery));

$(document).ready(function(){

  moment.locale('fr');

  $('.modal').modal();

  $(".button-collapse").sideNav();

  $('.dropdown-button').dropdown({

      inDuration: 300,

      outDuration: 225,

      constrainWidth: false, // Does not change width of dropdown to that of the activator

      hover: false, // Activate on hover

      gutter: 0, // Spacing from edge

      belowOrigin: false, // Displays dropdown below the button

      alignment: 'left', // Displays dropdown with edge aligned to the left of button

      stopPropagation: false // Stops event propagation

    }

  );

  $('.collapsible').collapsible();

/*  $('select').material_select();*/

  $('.modal').modal();

  $('ul.tabs').tabs();

});



$(document).ready(function(){

  //toastr Initialization
  // toastr.options = {
  // "closeButton": false,
  // "debug": false,
  // "newestOnTop": false,
  // "progressBar": false,
  // "positionClass": "toast-top-right",
  // "preventDuplicates": false,
  // "onclick": null,
  // "showDuration": "300",
  // "hideDuration": "1000",
  // "timeOut": "1500",
  // "extendedTimeOut": "1000",
  // "showEasing": "swing",
  // "hideEasing": "linear",
  // "showMethod": "fadeIn",
  // "hideMethod": "fadeOut"
  // }

  // initialization table
  manageUserTable = $('#manageUserTable').DataTable({ // This is for user management page

    searching: true,

    responsive: true,

    "scrollY": "400px",

    'sDom': 'ti',

    'pagingType': 'full_bers_no_ellipses'

  });

  manageRoleTable = $('#manageRoleTable').DataTable({ // This is for user management page

    searching: true,

    responsive: true,

    "scrollY": "400px",

    'sDom': 'ti',

    'pagingType': 'full_numbers_no_ellipses'

  });

  managePermissionTable = $('#managePermissionTable').DataTable({ // This is for user management page

    searching: true,

    responsive: true,

    "scrollY": "400px",

    'sDom': 'ti',

    'pagingType': 'full_numbers_no_ellipses'

  });

  // .search() customization search option findNews function




  $('#findRole').on('keyup', function () { // This is for manage role page

    manageRoleTable.search(this.value).draw();

  });



  $('#findPermission').on('keyup', function () { // This is for manage role page

    managePermissionTable.search(this.value).draw();

  });



  // page.len() customization show entries option to sort by number

  $('#newsLengthChange').change(function(){ // THis is for news page

    newsTable.page.len(this.value).draw();

  });

/*  $('#pickmydate').dcalendarpicker({

    format: 'dd-mm-yyyy'

  });*/



});



/*

  var slideIndex = 1;

  showSlides(slideIndex);



  function nextSlide(n){

    showSlides(slideIndex += n);

  }



  function prevSlide(n){

    showSlides(slideIndex = n);

  }



  function showSlides(n){

    var i;

    var slides = document.getElementById('item-card');



    if (n > slides.length){

      slideIndex = 1;

    }



    if (n < 1){

      slideIndex = slides.length;

    }



    for (i = 0; i < slides.length; i++){

      slides[i].style.display = "none";

      slides[slideIndex-1].style.display = "block";

    }

  }

*/