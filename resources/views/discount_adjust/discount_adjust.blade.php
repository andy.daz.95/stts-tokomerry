<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Diskon Penyesuaian</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l3">
                  <input id="filterPayment" type="text" class="f-input">
                  <label>Filter No Invoice</label>
                </div>
                <div class="input-field col l3">
                  <input id="filterSupplier" type="text" class="f-input">
                  <label>Filter Supplier</label>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="purchasePaymentTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th>No Pembayaran</th>
                        <th>Tanggal Pembayaran</th>
                        <th>Supplier</th>
                        <th>Total</th>
                        <th>Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($data['payment'] as $key => $value)--}}
                      {{--<tr value="{{$value->payment_purchase_id}}">--}}
                      {{--<td class="nopayment">{{$value->payment_purchase_number}}</td>--}}
                      {{--<td>{{$value->date_payment_purchase}}</td>--}}
                      {{--<td>{{$value->company_name}}</td>--}}
                      {{--<td>{{number_format($value->paid)}}</td>--}}
                      {{--<td>--}}
                      {{-- <a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->invoice_purchase_id}}"><i class="material-icons">edit</i></a> --}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Pembayaran Pembelian</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br/>
                <form id="formDiscount">
                  <div class="input-field col l4">
                    <label>No PO</label>
                    <select id="noinvoice" class="browser-default selectpicker" name="nopo" data-live-search="true" data-size="5"  >
                      <option value="0">Select Po</option>
                      @foreach($data['po'] as $key => $value)
                        <option value='{{$value->purchase_order_id}}'>{{$value->purchase_order_number.' - '.$value->supplier->company_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l4">
                    <label>Supplier</label>
                    <input id="supplier" type="text" name="supplier" class="f-input" placeholder="Supplier" disabled>
                  </div>
                  <div class="col l6 m12 s12 margin-top">
                    <div>
                      <div class="input-field col l6 no-margin">
                        <label>Jenis Diskon</label>
                        <select id="discount-type-old" type="text" class="browser-default selectpicker" disabled >
                          <option value="1">Nominal</option>
                          <option value="2">Percent</option>
                        </select>
                      </div>
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader">Nilai Diskon</th>
                          <th class="theader">Periode Diskon</th>
                        </tr>
                        </thead>
                        <tbody id="append-old-disc">
                        <tr id="no-discount-row">
                          <td colspan="2">Tidak Ada Diskon Untuk PO Ini</td>
                        </tr>
                        <tr class="old-discount-row" hidden>
                          <td><input type="text" class="f-input discount-amount-old" disabled></td>
                          <td><input type="text" class="f-input discount-period-old" disabled></td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col l6 m12 s12 margin-top">
                    <div>
                      <div class="input-field col l6 no-margin">
                        <label>Jenis Diskon</label>
                        <select id="discount-type-new" type="text" name="discounttype" class="browser-default selectpicker">
                          <option value="1">Nominal</option>
                          <option value="2">Percent</option>
                        </select>
                      </div>
                      <div class="input-field col l6 no-margin">
                        <button type="button" class="btn add-discount-row">Tambah Diskon</button>
                      </div>
                      <table id="new-discount-table" class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader">Nilai Diskon</th>
                          <th class="theader">Periode Diskon</th>
                          <th class="theader">Action</th>
                        </tr>
                        </thead>
                        <tbody id="append-new-disc">
                        <tr class="new-discount-row">
                          <td>
                            <input type="text" name="discountamount[]" class="f-input discount-amount-new">
                          </td>
                          <td>
                            <input type="text" name="discountperiod[]" class="f-input discount-period-new">
                          </td>
                          <td><button type="button" class="btn red remove-discount-row"><i class="material-icons">remove</i></button></td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col l12 s12 m12 margin-top">
                    <button type="button" id="submit-adjust" class="btn-stoko teal white-text submit">Simpan</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<div id="modal-delete-payment" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-payment" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        firstload();

        $('#noinvoice').on('change',function(event){
            event.stopImmediatePropagation();
            var id = $(this).val();
            $.ajax({
                type:"GET",
                url:"getdiskonforadjust",
                data:{id:id},
                success:function(response){

                    $original_old = $('.old-discount-row:first');
                    $original_new = $('.new-discount-row:first');
                    $('.old-discount-row').remove();
                    $('.new-discount-row').remove();

                    if(response.discount.length > 0)
                    {
                        $('#no-discount-row').attr('hidden',true);
                        $('#discount-type-old').val(response.discount_type);
                        $('#discount-type-new').val(response.discount_type);


                        $.each(response.discount, function(i,v){
                            $clone_old = $original_old.clone().removeAttr('hidden');
                            $clone_old.find('.discount-amount-old').val(v.discount);
                            $clone_old.find('.discount-period-old').val(v.discount_period);
                            $clone_old.appendTo('#append-old-disc');

                            $clone_new = $original_new.clone();
                            $clone_new.find('.discount-amount-new').val(v.discount);
                            $clone_new.find('.discount-period-new').val(v.discount_period);
                            $clone_new.appendTo('#append-new-disc');
                        });
                    }else{
                        $('#no-discount-row').removeAttr('hidden');
                        $clone_old = $original_old.clone().attr('hidden',true);
                        $clone_old.find('.discount-amount-old').val('').attr('disabled',true);
                        $clone_old.find('.discount-period-old').val('').attr('disabled',true);
                        $clone_old.appendTo('#append-old-disc');

                        $clone_new = $original_new.clone();
                        $clone_new.find('.discount-amount-new').val('');
                        $clone_new.find('.discount-period-new').val('');
                        $clone_new.appendTo('#append-new-disc');
                    }
                    $('.selectpicker').selectpicker('refresh');
                }

            });
        });

        $("#submit-adjust").on('click',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            console.log($('#formDiscount').serialize());

            $.ajax({
                type:"POST",
                url:"creatediscountadjust",
                data: $('#formDiscount').serialize(),
                success:function(response){
                    toastr.success('Sukses menyesuaikan diskon', {"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            });

        });

        $(".add-discount-row").on('click',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            $original_new = $('.new-discount-row:first');

            $clone_new = $original_new.clone();
            $clone_new.find('.discount-amount-new').val('');
            $clone_new.find('.discount-period-new').val('');
            $clone_new.appendTo('#append-new-disc');
        });

        $("#new-discount-table").on('click','.remove-discount-row',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            if($(this).closest('tbody').find('.new-discount-row:first').is($(this).closest('.new-discount-row'))
                &&
                $(this).closest('tbody').find('.new-discount-row:last').is($(this).closest('.new-discount-row'))
            )
            {
                $(this).closest('.new-discount-row').find('input[type="text"]').val('');
            }else{
                alert('remove')
                $(this).closest('.new-discount-row').remove();
            }
        })

        //function
        function firstload(){

            $('.selectpicker').selectpicker('render');

            $('#tglpayment').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tglpayment').val(moment().format('DD-MM-YYYY'));
        }
    });
</script>