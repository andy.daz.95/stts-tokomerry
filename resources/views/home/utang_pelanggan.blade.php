<ul class="collapsible" data-collapsible="accordion">
  <li>
    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">attach_money</i>Utang Pelanggan</div>
    <div class="collapsible-body">
      <div class="container-fluid">
        <div class="table-responsive margin-top padding-bottom">
          <table id="pelangganHomeTable" class="table table-bordered display nowrap dataTable dtr-inline">
            <thead>
            <tr>
              <th>Nama Pelanggan</th>
              <th>Utang Tersisa</th>
            </tr>
            </thead>
            <tbody>
            @if($customer->count() > 0)
              @foreach($customer as $key => $value)
                <tr>
                  <td>{{$value->first_name.' '.$value->last_name}}</td>
                  <td>{{number_format($value->total_balance)}}</td>
                </tr>
              @endforeach
            @endif
            </tbody>
          </table>
          <div class="so-paginator">
          </div>
        </div>
      </div>
  </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $('.number').each(function(){
            $(this).html(accounting.formatMoney($(this).html(),'Rp. ',2,',','.'));
        });

        $('#pelangganHomeTable').DataTable({
            searching: true,
            responsive: true,
            "bSort":true,
            "bLengthChange": true,
            //"aaSorting": [[ 4, "asc" ]],
            "sDom": 'tip',
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "View _MENU_ ",
                "sEmptyTable": "Tidak ada data tersedia",
                "sZeroRecords": "Data tidak ditemukan"
            },
            "sPaginationType": "full_numbers",
        });
    })
</script>