<ul class="collapsible" data-collapsible="accordion">
  <li>
    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">attach_money</i>Utang Pembelian</div>
    <div class="collapsible-body">
      <div class="container-fluid">
        <div class="table-responsive bordered margin-top padding-bottom">
          <table id="poHomeTable" class="table table-bordered display nowrap dataTable dtr-inline">
            <thead>
              <tr>
                <th>Jatuh Tempo</th>
                <th>PO</th>
                <th>Supplier</th>
                <th>Utang</th>
              </tr>
            </thead>
            <tbody>
            @if($piutang_po->count() > 0)
              @foreach($piutang_po as $row)
              <tr>
                <td class="jatuh-tempo">{{$row->jatuh_tempo}}</td>
                <td>{{$row->purchase_order_number}}</td>
                <td>{{$row->company_name}}</td>
                <td style='text-align:right;' val='{{$row->remaining_payable}}'>{{number_format($row->remaining_payable,2,",",".")}}</td>
              </tr>
              @endforeach
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </li>
</ul>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });
      $('#poHomeTable').DataTable({
          searching: true,
          responsive: true,
          "bSort":true,
          "bLengthChange": true,
          "sDom": 'tip',
          "oLanguage": {
              "sSearch": "",
              "sLengthMenu": "View _MENU_ ",
              "sEmptyTable": "Tidak ada data tersedia",
              "sZeroRecords": "Data tidak ditemukan"
          },
          "sPaginationType": "full_numbers",
      });

    // $('.jatuh-tempo').each(function(key,value){
    //   var jatuhtempo = moment($(this).html(), 'YYYY-MM-DD').format('YYYY, MM, DD');
    //   var now = moment().format('YYYY, MM, DD');

    //   console.log(jatuhtempo);
    //   console.log(now);

    //   var a = moment(jatuhtempo);
    //   var b = moment(now);
    //   var diff = a.diff(b, 'days');
      
    //   if(diff < 7)
    //   {
    //     $(this).closest('tr').css({'background-color':'#E53935', 'color':'white'});
    //   }else{
    //     $(this).closest('tr').css({'background-color':'#E53935', 'color':'white'});
    //   }
    // })
  })
</script>
