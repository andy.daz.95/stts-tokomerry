<ul class="collapsible" data-collapsible="accordion">
  <li>
    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">attach_money</i>Piutang Penjualan</div>
    <div class="collapsible-body">
      <div class="container-fluid">
        <div class="table-responsive margin-top padding-bottom">
          <table id="soHomeTable" class="table table-bordered display nowrap dataTable dtr-inline">
            <thead>
            <tr>
              <th>Jatuh Tempo</th>
              <th>Tanggal SO</th>
              <th>Pelanggan</th>
              <th>Total SO</th>
            </tr>
            </thead>
            <tbody>
            @if($piutang_so->count() > 0)
              @foreach($piutang_so as $key => $value)
                @php
                  $originalDate1 = $value->date_sales_order;
                  $term = $value->term_payment;
                  $jatuhtempo = date('Y-m-d', strtotime('+'.$term.' day',strtotime($value->date_sales_order)));
                @endphp
                <tr>
                  <td>{{$jatuhtempo}}</td>
                  <td>{{$value->date_sales_order}}</td>
                  <td>{{$value->first_name.' '.$value->last_name}}</td>
                  <td class="number">{{$value->grand_total_idr}}</td>
                </tr>
              @endforeach
            @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </li>
</ul>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $('#soHomeTable').DataTable({
            searching: true,
            responsive: true,
            "bSort":true,
            "bLengthChange": true,
            "sDom": 'tip',
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "View _MENU_ ",
                "sEmptyTable": "Tidak ada data tersedia",
                "sZeroRecords": "Data tidak ditemukan"
            },
            "sPaginationType": "full_numbers",
        });
    })
</script>