<div class="row">

<div class="col l12 m12 s12">
  <div id="news" class="col l6 m6 s12">

  </div>
  <div id="utang-pelanggan" class="col l6 m6 s12">

  </div>
</div>


<div class="col l12 m12 s12">
  @if(Session('roles')->name == 'master' || Session('roles')->name == 'Toko Partai')
  <div id="piutang-so">

  </div>
  @endif

  <div id="piutang-po">

  </div>

  <div id="target">

  </div>

</div>
</div>
<script type="text/javascript">

    $(document).ready(function(){
        loadnews();
        loadpiutangpo();
        loadpiutangso();
        loadutangpelanggan();

        $(document).on('click', '.po-paginator .pagination a', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                type:"Get",
                url:url,
                success:function(response){
                    $('#piutang-po').html(response);
                }
            });
        });

        $(document).on('click', '.so-paginator .pagination a', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                type:"Get",
                url:url,
                success:function(response){
                    $('#piutang-so').html(response);
                }
            });
        });
    });

    $.ajax({
        method:"GET",
        url:"show_grafik_target",
        async: "true",
        success:function(response){
            $('#target').html(response);
        }
    })

    function loadnews(){
        $.ajax({
            type:'GET',
            url: "home/news",
            datatype:"html",
            data :{},
            success:
                function(response){
                    $('#news').html(response);
                }
        });
    }

    function loadpiutangso(){
        $.ajax({
            type:'GET',
            url: "home/piutang-so",
            datatype:"html",
            data :{},
            success:
                function(response){
                    $('#piutang-so').html(response);
                }
        });
    }

    function loadpiutangpo(){
        $.ajax({
            type:'GET',
            url: "home/piutang-po",
            datatype:"html",
            data :{},
            success:
                function(response){
                    $('#piutang-po').html(response);
                }
        });
    }

    function loadutangpelanggan(){
        $.ajax({
            type:'GET',
            url: "home/utang-pelanggan",
            datatype:"html",
            data :{},
            success:
                function(response){
                    $('#utang-pelanggan').html(response);
                }
        });
    }
</script>





