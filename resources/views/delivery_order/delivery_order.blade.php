<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Pengeluaran Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l3">
                  <input id="filterDoNumber" type="text" class="f-input">
                  <label>Filter DO</label>
                </div>
                <div class="input-field col l3">
                  <input id="filterPelanggan" type="text" class="f-input">
                  <label>Filter Pelanggan</label>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="deliveryTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th>No Delivery</th>
                        <th>No Draft</th>
                        <th>Dibuat Oleh</th>
                        <th>Tanggal</th>
                        <th>Pelanggan</th>
                        <th>Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($do as $key => $value)--}}
                      {{--<tr value="{{$value->delivery_order_id}}">--}}
                      {{--<td class="nodo">{{$value->delivery_order_number}}</td>--}}
                      {{--<td>{{$value->draft->draft_delivery_order_number}}</td>--}}
                      {{--<td>{{$value->draft->date_draft_delivery}}</td> --}}
                      {{--<td>--}}
                      {{--@if($value->draft->so)--}}
                      {{--{{$value->draft->so->customer->first_name.' '.$value->draft->so->customer->last_name}}--}}
                      {{--@elseif($value->draft->retur)--}}
                      {{--{{$value->draft->retur->so->customer->first_name.' '.$value->draft->retur->so->customer->last_name}}--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      {{--@if($value->draft->draft_type == 1)--}}
                      {{--<td>SO</td>--}}
                      {{--@else--}}
                      {{--<td>Retur</td>--}}
                      {{--@endif--}}
                      {{--<td>--}}
                      {{--@if(Session('roles')->name == 'master')--}}
                      {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->delivery_order_id}}"><i class="material-icons">edit</i></a>--}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Pengeluaran Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formDo">
                  <div class="input-field col l3">
                    <input id="donumber" type="text" class="f-input" name="donumber" disabled>
                    <input id="iddo" type="text" class="f-input" name="iddo" disabled hidden>
                    <label>No. Do</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="draft" name="draft" class="selectpicker browser-default" data-live-search="true" data-size="5"  >
                      <option value="0">Select Draft</option>
                      @foreach($draft as $key =>$value)
                        <option value="{{$value->draft_delivery_order_id}}">
                          {{$value->draft_delivery_order_number.' / '}}
                          @if($value->shipping_term_id == 1)
                            DO {{$value->warehouse_name}}
                          @elseif($value->shipping_term_id == 2)
                            Surat Antaran
                          @elseif($value->shipping_term_id == 3)
                            Draft Retur
                          @endif
                          {{' / '}}
                          {{$value->first_name.' '.$value->last_name}}
                        </option>
                      @endforeach
                    </select>
                    <label>No Draft</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="shippingterm" name="shippingterm" class="selectpicker browser-default" data-live-search="true" data-size="5"  >
                      <option value="0">Select Shipping Term</option>
                      @foreach($shippingterm as $key => $value)
                        <option value="{{$value->shipping_term_id}}">{{$value->shipping_term_name}}</option>
                      @endforeach
                    </select>
                    <label>Shipping Term</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="pelanggan" name="pelanggan" type="text" class="f-input" disabled>
                    <label>Nama Pelanggan</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="tgldo" type="text" name="tgldo" class="f-input">
                    <label>Tanggal</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="gudang" type="text" name="gudang" class="selectpicker browser-default" data-live-search="true" data-size="5">
                      <option value="0">Pilih Gudang</option>
                      @foreach($warehouse as $key => $value)
                        <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                      @endforeach
                    </select>
                    <label>Gudang</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="notes" name="notes" type="text" class="f-input">
                    <label>Notes</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="driver" name="driver" type="text" class="f-input">
                    <label>Nama Supir</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="nopol" name="nopol" type="text" class="f-input">
                    <label>No Kendaraan</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="user" name="user" class="selectpicker browser-default" data-live-search="true" data-size="5">
                      <option value="0">Select User</option>
                      @foreach($account as $key => $value)
                        <option value="{{$value->account_id}}">{{$value->full_name}}</option>
                      @endforeach
                    </select>
                    <label>User</label>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader">Nama Barang</th>
                          <th class="theader">Jumlah Order</th>
                          <th class="theader" style="width: 100px">Unit</th>
                          {{--<th class="retur-header" hidden>Jumlah Retur</th>--}}
                          <th class="theader" style="width:75px">Jumlah Terkirim</th>
                          <th class="theader" style="width:75px">Sisa Yang Harus Dikirim</th>
                          <th class="theader">Stok Di Gudang</th>
                          <th class="theader" style="width:75px">Akan Dikirim</th>
                          <th class="theader" colspan="3">Action</th>
                        </tr>
                        </thead>
                        <tbody id="barang-data">
                        <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                        <tr class="barang-row" hidden>
                          <td>
                            <div class="input-field">
                              <input id="id-1" type="text" class="f-input idbarang" name="idbarang[]" hidden>
                              <input id="nama-1" type="text" class="f-input" name="namabarang[]">
                              <input id="detail-so-1" type="text" class="f-input detailso" name="detailso[]" hidden>
                              <input id="detail-draft-1" type="text" class="f-input detaildraft" name="detaildraft[]" hidden>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="order-qty-1" type="text" class="f-input orderqty" name="orderqty[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="unit-code-1" type="text" class="f-input">
                              <input id="unit-1" type="text" class="f-input unit" name="unit[]" hidden>
                              <input id="unit-type-1" type="text" class="f-input unittype" name="unittype[]" hidden>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="delivered-qty-1" type="text" class="f-input deliveredqty" name="delivered[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="remaining-qty-1" type="text" class="f-input remainingqty" name="remainingqty[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="stock-1" type="text" class="f-input stock" name="stock[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="send-qty-1" type="text" class="f-input sendqty" name="sendqty[]">
                            </div>
                          </td>
                          <td>
                            <input type="checkbox" class="filled-in delivery" id="checkbox-delivery-1" name="check[]" />
                            <label id="label-delivery-1" for="checkbox-delivery-1"></label>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="input-field right">
                      <a id="clear" href="" class="btn-stoko orange">Clear</a>
                      <a id="cetak-do" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak DO</a>
                      <a id="submit-do" href="" class="btn-stoko btn-stoko-primary" mode="save">Submit DO</a>
                      <a id="edit-do" href="" class="btn-stoko btn-stoko-primary" mode="edit" hidden>Edit DO</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-tambah-gudang" class="modal">
  <form id="form-list-gudang">
    <div class="modal-content">
      <h4>Pilih Gudang</h4>
      <div class="row">
        <table>
          <thead>
          <th>Nama</th>
          <th>Qty</th>
          <th>Gudang</th>
          <th>check</th>
          </thead>
          <tbody id="modal-barang-data">
          </tbody>
        </table>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-barang">OK</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>

<!-- Modal Tambah Delivery Order -->
<div id="tambah_do" class="modal">
  <form>
    <div class="modal-content">
      <div class="row margin-top">
        <div class="col l6">
          <div class="row">
            <div class="input-field col l12 m12 s12">
              <select class="f-select browser-default">
                <option value="">Select Order ID</option>
                <option value="">Order 001</option>
                <option value="">Order 002</option>
                <option value="">Order 003</option>
              </select>
              <label>Order ID</label>
            </div>
            <div class="input-field col l12 m12 s12">
              <input type="text" class="f-input">
              <label>Status Pembayaran</label>
            </div>
          </div>
        </div>
        <div class="col l6">
          <div class="row">
            <div class="input-field col l6 m12 s12">
              <input type="text" class="f-input">
              <label>Nama Barang</label>
            </div>
            <div class="input-field col l6">
              <input type="text" class="f-input datepicker">
              <label>Tanggal</label>
            </div>
            <div class="input-field col l12 m12 s12">
              <input type="text" class="f-input">
              <label>Notes</label>
            </div>
          </div>
        </div>
        <div class="col l12 m12 s12">
          <div class="table-responsive">
            <table class="stoko-table">
              <tr>
                <th class="white">Nama Barang</th>
                <th class="white">Qty</th>
                <th class="white">Berat</th>
                <th class="white">Notes</th>
              </tr>
              <tr>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <h5>Tambah Delivery Order</h5>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
      <a href="#!" onclick="Materialize.toast('Berhasil menambahkan delivery order', 4000)" class="modal-action modal-close waves-effect btn-stoko teal white-text">tambah</a>
    </div>
  </form>
</div>

<!-- Modal Rubah Delivery Order -->
<div id="rubah_do" class="modal">
  <form>
    <div class="modal-content">
      <h5>Rubah Delivery Order</h5>
      <div class="row margin-top">
        <div class="col l6">
          <div class="row">
            <div class="input-field col l12 m12 s12">
              <select class="f-select browser-default">
                <option value="">Select Order ID</option>
                <option value="">Order 001</option>
                <option value="">Order 002</option>
                <option value="">Order 003</option>
              </select>
              <label>Order ID</label>
            </div>
            <div class="input-field col l12 m12 s12">
              <input type="text" class="f-input">
              <label>Status Pembayaran</label>
            </div>
          </div>
        </div>
        <div class="col l6">
          <div class="row">
            <div class="input-field col l6 m12 s12">
              <input type="text" class="f-input">
              <label>Nama Pelanggan</label>
            </div>
            <div class="input-field col l6">
              <input type="text" class="f-input datepicker">
              <label>Tanggal</label>
            </div>
            <div class="input-field col l12 m12 s12">
              <input type="text" class="f-input">
              <label>Notes</label>
            </div>
          </div>
        </div>
        <div class="col l12 m12 s12">
          <div class="table-responsive">
            <table class="stoko-table">
              <tr>
                <th class="white">Nama Barang</th>
                <th class="white">Qty</th>
                <th class="white">Berat</th>
                <th class="white">Notes</th>
              </tr>
              <tr>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
      <a href="#!" class="modal-action waves-effect btn-stoko btn-stoko-primary">cetak</a>
      <a href="#!" onclick="Materialize.toast('Berhasil merubah barang', 4000)" class="modal-action modal-close waves-effect btn-stoko teal white-text">simpan</a>
    </div>
  </form>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-do" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-do" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#draft').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getdraftfordelivery",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    if(response['draft'].details.length == 0)
                    {
                        $original.clone();
                        $original.attr('hidden',true)
                        $original.appendTo('#barang-data');
                        $('#formDo').find("input[type=text], textarea").val("");
                        $('#no-item').removeAttr('hidden');
                    }
                    else{
                        $('#shippingterm').val(response['draft'].shipping_term_id).attr('disabled',true);
                        if(response['draft'].shipping_term_id == 1){
                            $('#gudang').val(response['draft'].warehouse_id).attr('disabled',true);
                        }else if (response['draft'].shipping_term_id == 2){
                            $('#gudang').val(response['draft'].warehouse_id).removeAttr('disabled');
                        }

                        $('#notes').val(response['draft'].notes).attr('disabled',true);
                        if(response['draft'].so){
                            $('#pelanggan').val(response['draft'].so.customer.first_name+" "+response['draft'].so.customer.last_name);
                        }else{
                            $('#pelanggan').val(response['draft'].retur.so.customer.first_name+" "+response['draft'].retur.so.customer.last_name);
                        }
                        for (var i=0; i<response['draft'].details.length; i++)
                        {
                            var newid = "id-"+(i+1);
                            var newnama = "nama-"+(i+1);
                            var newdetailso = "detail-so-"+(i+1);
                            var newdetaildraft = "detail-draft-"+(i+1);
                            var neworderqty = "order-qty-"+(i+1);
                            var newunit = "unit-" + (i + 1);
                            var newunitcode = "unit-code-" + (i + 1);
                            var newunittype = "unit-type-" + (i + 1);
                            var newreturqty = "retur-qty-"+(i+1);
                            var newdeliveredqty = "delivered-qty-"+(i+1);
                            var newremainingqty = "remaining-qty-"+(i+1);
                            var newsendqty = "send-qty-"+(i+1);
                            var newstock = "stock-"+(i+1);
                            var newnote = "note-"+(i+1);
                            var newharga = "harga-"+(i+1);
                            var newLabelDelivery = "label-delivery-"+(i+1);
                            var newDelivery = "checkbox-delivery-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#nama-1').attr('id',newnama);
                            $temp.find('#detail-so-1').attr('id',newdetailso);
                            $temp.find('#detail-draft-1').attr('id',newdetaildraft);
                            $temp.find('#send-qty-1').attr('id',newsendqty);
                            $temp.find('#retur-qty-1').attr('id',newreturqty);
                            $temp.find('#delivered-qty-1').attr('id',newdeliveredqty);
                            $temp.find('#remaining-qty-1').attr('id',newremainingqty);
                            $temp.find('#order-qty-1').attr('id',neworderqty);
                            $temp.find('#unit-1').attr('id',newunit);
                            $temp.find('#unit-code-1').attr('id',newunitcode);
                            $temp.find('#unit-type-1').attr('id', newunittype);
                            $temp.find('#stock-1').attr('id',newstock);
                            $temp.find('#note-1').attr('id',newnote);
                            $temp.find('#harga-1').attr('id',newharga);
                            $temp.find('#label-delivery-1').attr('id',newLabelDelivery).attr('for',"checkbox-delivery-"+(i+1));
                            $temp.find('#checkbox-delivery-1').attr('id',newDelivery);
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(response['draft'].details[i].product.product_id);
                            $('#nama-'+(i+1)).val(response['draft'].details[i].product.product_name).attr('disabled',true);
                            $('#detail-so-'+(i+1)).val(response['draft'].details[i].sales_order_details_id).attr('disabled',true);
                            $('#detail-draft-'+(i+1)).val(response['draft'].details[i].draft_delivery_order_details_id).attr('disabled',true);
                            $('#order-qty-'+(i+1)).val(response['draft'].details[i].quantity).attr('disabled',true);
                            $('#stock-'+(i+1)).val(0).attr('disabled',true);
                            $('#checkbox-delivery-'+(i+1)).val(response['draft'].details[i].product_id);
                            $('#delivered-qty-'+(i+1)).val(response['draft'].details[i].sent).attr('disabled',true);
                            $('#unit-type-' + (i + 1)).val(response['draft'].details[i].unit_type).attr('disabled', true);
                            if(response['draft'].details[i].unit_type == 'child'){
                                $('#unit-code-' + (i + 1)).val(response['draft'].details[i].product.satuan.unit_child.unit_child_code).attr('disabled', true);
                                $('#unit-' + (i + 1)).val(response['draft'].details[i].product.satuan.unit_child.unit_child_id).attr('disabled', true);
                            }else if (response['draft'].details[i].unit_type == 'main'){
                                $('#unit-code-' + (i + 1)).val(response['draft'].details[i].product.satuan.unit_code).attr('disabled', true);
                                $('#unit-' + (i + 1)).val(response['draft'].details[i].product.satuan.unit_id).attr('disabled', true);
                            }

                            var delivered = response['draft'].details[i].sent;
                            var order = response['draft'].details[i].quantity;
                            $('#remaining-qty-'+(i+1)).val(order - delivered).attr('disabled',true);
                            //kalau sisanya 0 checkbox di disabled
                            if(order - delivered == 0){
                                $('#checkbox-delivery-'+(i+1)).attr('disabled',true);
                                $('#send-qty-'+(i+1)).attr('disabled',true);
                            }else{
                                $('#checkbox-delivery-'+(i+1)).removeAttr('disabled');
                                $('#send-qty-'+(i+1)).removeAttr('disabled');
                            }
                        }
                        $('#no-item').attr('hidden',true);
                        $('.barang-row').removeAttr('hidden');
                        $('.selectpicker').selectpicker('refresh');
                        $('#gudang').trigger('change');
                    }
                }
            });
        });

        $('#submit-do, #edit-do').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);

            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            var kode = $('#donumber').val();

            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "createdeliveryorder";
                var successmessage1 = 'Delivery Order ';
                var successmessage2 = ' telah berhasil dibuat!';
            }else{
                var url = "updatedeliveryorder";
                var successmessage1 = 'Delivery Order ';
                var successmessage2 =' telah berhasil diubah!';
            }

            var validqty = [];
            var lebihqty = [];
            var stockkurang = [];

            $('.barang-row').each(function() {
                var deliveredqty = parseFloat($(this).find('.deliveredqty').val());
                var sendqty = parseFloat($(this).find('.sendqty').val());
                var orderqty = parseFloat($(this).find('.orderqty').val());
                var returqty = parseFloat($(this).find('.returqty').val());
                var stock = parseFloat($(this).find('.stock').val());

                if ($(this).find('.delivery').is(':checked')) {
                    if ($(this).find('.sendqty').val() != '' && parseFloat($(this).find('.sendqty').val()) !== 0) {
                        validqty.push(ceknumber($(this).find('.sendqty').val()));
                    } else {
                        validqty.push(false)
                    }
                }

                if ($(this).find('.delivery').is(':checked')) {
                    if ($(event.currentTarget).attr('mode') == "save") {
                        if (deliveredqty + sendqty > orderqty) {
                            lebihqty.push(false);
                        } else {
                            lebihqty.push(true);
                        }
                    } else {
                        if (sendqty > orderqty) {
                            lebihqty.push(false);
                        } else {
                            lebihqty.push(true);
                        }
                    }
                }

                if ($(this).find('.delivery').is(':checked')) {
                    if ($(event.currentTarget).attr('mode') == "save") {
                        if (sendqty > stock) {
                            stockkurang.push(false);
                        } else {
                            stockkurang.push(true);
                        }
                    }else{
                        if (sendqty - deliveredqty > stock) {
                            stockkurang.push(false);
                        } else {
                            stockkurang.push(true);
                        }
                    }
                }
            });

            var checked = 0;
            $('.delivery').each(function(){
                if($(this).is(':checked')){
                    checked++;
                }
            });

            if($('#so').val() == 0){
                toastr.warning('Anda Belum Memilih SO!');
            }else if(checked == 0){
                toastr.warning('Anda Belum Memilih Barang!');
            }else if($('#gudang').val() == 0){
                toastr.warning('Anda Belum Memilih Gudang!');
            }else if($('#shippingterm').val() == 0){
                toastr.warning('Anda Belum Memilih Shipping Term!');
            }else if(validqty.indexOf(false) > -1){
                toastr.warning('Quantity harus berupa angka dan tidak boleh kosong!');
            }else if(stockkurang.indexOf(false) > -1){
                toastr.warning('Stock Di Gudang Tidak Cukup!');
            }else if(lebihqty.indexOf(false) > -1){
                toastr.warning('Quantity melebihi jumlah order!');
            }else if($('#user').val() == 0){
                toastr.warning('Anda Belum Memilih User!');
            }else{
                me.invisible();
                me.data('requestRunning', true);

                $('#donumber, #tgldo , #iddo , .sendqty, .weight, #gudang, .unit, .unittype, .detailso, .detaildraft').removeAttr('disabled');
                $.ajax({
                    type:"POST",
                    url:url,
                    data:$(this).closest('#formDo').serialize(),
                    success:function(response){
                        toastr.success(successmessage1+response+successmessage2,{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                })
            }
        })

        $('#gudang').on('change', function(){
            var idgudang = $(this).val();
            var idproduct = [];
            $('.idbarang').each(function(i){
                idproduct.push($(this).val());
            });

            $.ajax({
                type:"GET",
                url:"getstockfordraft",
                data: {idproduct:idproduct, idgudang:idgudang},
                success:function(response){
                    // for (var i=0; i<response.length; i++)
                    // {
                    //     $('#stock-'+(i+1)).val(response[i].quantity).attr('disabled',true);
                    // }
                    for (var i = 0; i < response.length; i++) {
                        $('.barang-row').each(function (j, v) {
                            var row = $(this);
                            var product = $(this).find('.idbarang').val();
                            var type = $(this).find('.unittype').val();
                            if (response[i].product_id == product && type == 'main') {
                                $('#stock-' + (j + 1)).val(response[i].quantity).attr('disabled', true);
                            }else if(response[i].product_id == product && type == 'child'){
                                $('#stock-' + (j + 1)).val(response[i].qty_child).attr('disabled', true);
                            }
                        });

                        // for (var i = 0; i < response.length; i++) {
                        //     if(response[i].product_id == product && type == 'main'){
                        //         row.find('.stock').val(response[i].quantity).attr('disabled', true);
                        //     }else{
                        //         row.find('.stock').val(response[i].qty_child).attr('disabled', true);
                        //     }
                        // }
                    }
                }
            })
        });

        $('body').on('click', '#deliveryTable tr, #deliveryTable .edit', function(event){
            event.stopImmediatePropagation();
            var mode = $(event.currentTarget).attr('mode');
            id = $(this).attr('value');
            $.ajax({
                type:"GET",
                url:"getdeliveryorder",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    /*name="donumber"name="so"name="shippingterm"name="status"name="pelanggan"name="tgldo"name="notes"*/
                    $('#draft').append('<option class="remove-when-clear" value="'+response['draft'].draft_delivery_order_id+'" selected="selected">'+response['draft'].draft_delivery_order_number+'</option>').attr('disabled',true);
                    $('#donumber').val(response.delivery_order_number).attr('disabled',true);
                    $('#draft').val();
                    $('#iddo').val(response.delivery_order_id).attr('disabled',true);
                    if(response['draft'].so){
                        $('#pelanggan').val(response['draft'].so.customer.first_name+" "+response['draft'].so.customer.last_name);
                    }else{
                        $('#pelanggan').val(response['draft'].retur.so.customer.first_name+" "+response['draft'].retur.so.customer.last_name);
                    }
                    if(response.draft.warehouse_id != null)
                    {
                        $('#gudang').val(response.draft.warehouse_id).attr('disabled',true);
                    }else{
                        $('#gudang').val(response.warehouse_id).attr('disabled',true);
                    }
                    $('#tgldo').val(response.date_delivery).attr('disabled',true);
                    $('#notes').val(response.dnotes).attr('disabled',true);
                    $('#shippingterm').val(response.draft.shipping_term_id).attr('disabled',true);
                    $('#driver').val(response.driver_name).attr('disabled',true);
                    $('#nopol').val(response.vehicle_number).attr('disabled',true);
                    $('#user').val(response.user_id).attr('disabled',true);
                    for (var i=0; i<response.details.length; i++)
                    {
                        var newid = "id-"+(i+1);
                        var newnama = "nama-"+(i+1);
                        var newdetailso = "detail-so-"+(i+1);
                        var newdetaildraft = "detail-draft-"+(i+1);
                        var neworderqty = "order-qty-"+(i+1);
                        var newunit = "unit-"+(i+1);
                        var newunitcode = "unit-code-"+(i+1);
                        var newunittype = "unit-type-"+(i+1);
                        var newdeliveredqty = "delivered-qty-"+(i+1);
                        var newremainingqty = "remaining-qty-"+(i+1);
                        var newsendqty = "send-qty-"+(i+1);
                        var newsentqty = "sent-qty-"+(i+1);
                        var newstock = "stock-"+(i+1);
                        var newharga = "harga-"+(i+1);
                        var newLabelDelivery = "label-delivery-"+(i+1);
                        var newDelivery = "checkbox-delivery-"+(i+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#nama-1').attr('id',newnama);
                        $temp.find('#detail-so-1').attr('id',newdetailso);
                        $temp.find('#detail-draft-1').attr('id',newdetaildraft);
                        $temp.find('#sent-qty-1').attr('id',newsentqty);
                        $temp.find('#send-qty-1').attr('id',newsendqty);
                        $temp.find('#stock-1').attr('id',newstock);
                        $temp.find('#delivered-qty-1').attr('id',newdeliveredqty);
                        $temp.find('#remaining-qty-1').attr('id',newremainingqty);
                        $temp.find('#order-qty-1').attr('id',neworderqty);
                        $temp.find('#unit-1').attr('id',newunit);
                        $temp.find('#unit-code-1').attr('id',newunitcode);
                        $temp.find('#unit-type-1').attr('id',newunittype);
                        $temp.find('#harga-1').attr('id',newharga);
                        $temp.find('#label-delivery-1').attr('id',newLabelDelivery).attr('for',"checkbox-delivery-"+(i+1));
                        $temp.find('#checkbox-delivery-1').attr('id',newDelivery);
                        $temp.appendTo('#barang-data');
                        $('#id-'+(i+1)).val(response.details[i].product.product_id);
                        $('#nama-'+(i+1)).val(response.details[i].product.product_name).attr('disabled',true);
                        $('#detail-draft-'+(i+1)).val(response.details[i].draft_delivery_order_details_id).attr('disabled',true);
                        $('#detail-so-'+(i+1)).val(response.details[i].sales_order_details_id).attr('disabled',true);
                        //filter details sesuai product
                        var product = response.details[i].product.product_id;
                        var details = response.draft.details;
                        var result = details.filter(detail => detail.product_id == product);
                        var orderqty = result[0].quantity;
                        var sentqty = response.details[i].sent;
                        $('#order-qty-'+(i+1)).val(orderqty).attr('disabled',true);
                        $('#stock-'+(i+1)).val(0).attr('disabled',true);

                        $('#unit-type-' + (i + 1)).val(response.details[i].unit_type).attr('disabled', true);
                        if(response.details[i].unit_type == 'child'){
                            $('#unit-code-' + (i + 1)).val(response.details[i].product.satuan.unit_child.unit_child_code).attr('disabled', true);
                            $('#unit-' + (i + 1)).val(response.details[i].product.satuan.unit_child.unit_child_id).attr('disabled', true);

                        }else if (response.details[i].unit_type == 'main'){
                            $('#unit-code-' + (i + 1)).val(response.details[i].product.satuan.unit_code).attr('disabled', true);
                            $('#unit-' + (i + 1)).val(response.details[i].product.satuan.unit_id).attr('disabled', true);
                        }

                        $('#delivered-qty-'+(i+1)).val(sentqty).attr('disabled',true);
                        $('#remaining-qty-'+(i+1)).val(orderqty - sentqty).attr('disabled',true);
                        $('#send-qty-'+(i+1)).val(response.details[i].quantity_sent).attr('disabled',true);
                        $('#checkbox-delivery-'+(i+1)).val(response['draft'].details[i].product_id).attr('disabled',true);
                    }
                    $('.selectpicker').selectpicker('refresh');
                    $('#no-item').attr('hidden',true);
                    $('#barang-row').removeAttr('hidden');
                    $('#gudang').trigger('change');
                },complete:function(){
                    $('#submit-do').attr('hidden',true);
                    $('#cetak-do').removeAttr('hidden').attr('href',"downloaddeliveryorder/"+id);
                    if(mode == "edit")
                    {
                        $('#edit-do').removeAttr('hidden');
                        $('#tgldo, #user, .sendqty, .delivery').removeAttr('disabled');
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            })
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        $('body').on('click','#deliveryTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var nodo = $(this).closest('tr').find('.nodo').html();
            var iddo = $(this).closest('tr').attr('value');
            $('#confirm-delete-do').attr('value',iddo).attr('nomor',nodo);
            $("#delete-message").html("Yakin ingin menghapus data "+nodo+" ?")
            $('#modal-delete-do').modal('open');
        });

        $('#confirm-delete-do').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-do').modal('close');
            var id = $(this).attr('value');
            var nodo = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletedeliveryorder",
                data:{id:id},
                success:function(response){
                    toastr.success('Delivery Order '+nodo+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        //function
        function firstload(){
            $('#tgldo').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });
            $('#tgldo').val(moment().format('DD-MM-YYYY'));
            // $.ajax({
            //   type:"GET",
            //   url:"lastdeliveryordernumber",
            //   success:function(response){
            //     $('#donumber').val(response);
            //   }
            // })

            $('.selectpicker').selectpicker('render');

            $('.number').each(function(){
                $(this).html(accounting.formatMoney($(this).html(),'Rp. ',2,',','.'));
            });

            var deliveryTable = $('#deliveryTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getdeliverytable',
                    data : function (d){
                        d.nodo = $('#filterDoNumber').val();
                        d.customer = $('#filterPelanggan').val();
                    }
                },
                rowId : 'delivery_order_id',
                columns: [
                    { data: 'delivery_order_number', name: 'delivery_order_number', class:'nodo'},
                    { data: 'draft_delivery_order_number', name: 'draft_delivery_order_number' },
                    { data: 'full_name', name: 'user' },
                    { data: 'date_delivery', name: 'date_delivery' },
                    { data: 'customer_name', name: 'customer_name' },
                    { data: 'action', name: 'action' },
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterDoNumber').on('keyup', function () { // This is for news page
                deliveryTable.column(0).search(this.value).draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                deliveryTable.column(3).search(this.value).draw();
            });
        }
    });
</script>