<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">
  table.table {
    border-collapse: collapse;
  }
  table.table, table.table th, table.table td{
    border: 1px solid black;
  }
  .red-color{
    color : red;
  }
  body {
    font-size: 9px;
  }
</style>
@if($do->draft->so)
  @php $so = $do->draft->so; @endphp
@elseif($do->draft->retur->so)
  @php $so = $do->draft->retur->so; @endphp
@endif
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">Surat Pengeluaran Gudang</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style="width: 100%">
    <tr >
      <td style="width: 20%">NO DO</td>
      <td style="width: 30%">: {{$do->delivery_order_number}}</td>
      <td style="width: 20%">NO SO</td>
      <td style="width: 30%">: {{$do->draft->so->sales_order_number}}</td>
    </tr>
    <tr>
      <td>Tanggal DO</td>
      <td>: {{$do->date_delivery}}</td>
      <td>Kepada</td>
      <td>: {{$so->customer->first_name.' '.$so->customer->last_name}}</td>
    </tr>
    <tr>
      <td>Notes</td>
      <td>: {{$do->note}}</td>
      <td>Nama Supir</td>
      <td>: {{$do->driver_name}}</td>
    </tr>
    <tr>
      <td>No Kendaraan</td>
      <td>: {{$do->vehicle_number}}</td>
      <td></td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
    <tr style="background-color: #8B8C89">
      <th class="color" style="text-align: center;">No</th>
      <th class="color" style="text-align: center;">Nama Barang</th>
      <th class="color" style="text-align: center;">Quanitiy Draft</th>
      <th class="color" style="text-align: center;">Quantity Pengiriman</th>
      <th class="color" style="text-align: center;">Quantity Tersisa</th>
    </tr>
    </thead>
    <tbody >
    @foreach($do->details as $key => $value)
      <tr >
        <td>
          <span>{{$key+1}}</span>
        </td>
        <td>
          <span>{{$value->product->product_name}}</span>
        </td>
        <td>
          <span>{{$value->quantity_draft}}</span>
          @if($value->unit_type == 'main')
            @php
              $satuan = App\satuan::where('unit_id', $value->unit_id)->first()->unit_code;
              $firstnumber = App\Traits\UnitTrait::my_offset($satuan);
              $satuan = substr($satuan,0,$firstnumber)
            @endphp
            <span>{{$satuan}}</span>
          @elseif($value->unit_type == 'child')
            <span>{{$value->product->satuan->unit_child->unit_child_code}}</span>
          @endif
        </td>
        <td>
          <span>{{$value->quantity_sent}}</span>
          @if($value->unit_type == 'main')
            <span>{{$value->product->satuan->unit_code}}</span>
          @elseif($value->unit_type == 'child')
            <span>{{$value->product->satuan->unit_child->unit_child_code}}</span>
          @endif
        </td>
        <td>
          <span>{{$value->quantity_draft - $value->total_sent}}</span>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
  @if($so->customer->total_balance <= 0)
    <table style ="width:100%">
      <tr>
        <td style="width: 25%">Sisa Hutang</td>
        <td style="width: 25%">: {{$so->customer->total_balance}}</td>
        <td></td>
      </tr>
    </table>
  @endif
  @if(count($retur) > 0 )
    <table style ="width:100%; margin-top:10px">
      <tr>
        <td class="red-color">Barang Retur Yang Harus Diambil :</td>
      </tr>
    </table>
    @foreach($retur as $key => $value)
      <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
        <tr>
          <th class="red-color">No Retur</th>
          <th class="red-color">Nama Barang</th>
          <th class="red-color">Quantity</th>
        </tr>
        @foreach($value->details as $key => $value2)
          <tr>
            <td class="red-color">{{$value->retur_number}}</td>
            <td class="red-color">{{$value2->product->product_name}}</td>
            <td class="red-color">{{$value2->qty}}</td>
          </tr>
        @endforeach
      </table>
    @endforeach
  @endif
</div>