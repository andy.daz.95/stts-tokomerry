<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
</style>
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">Invoice Pembelian</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 25%">NO</td>
      <td style="width: 25%">: {{$invoiceheader->invoice_purchase_number}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Supplier</td>
      <td>: {{$invoiceheader->company_name}}</td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr style="background-color: #8B8C89">
        <th class="color" style="text-align: center;">No</th>
        <th class="color" style="text-align: center;">Kode</th>
        <th class="color" style="text-align: center;">Satuan</th>
        <th class="color" style="text-align: center;">Nama Barang</th>
        <th class="color" style="text-align: center;">Quantity</th>
        <th class="color" style="text-align: center;">Harga Satuan</th>
        <th class="color" style="text-align: center;">Sub Total</th>
      </tr>
    </thead>
    <tbody >
      @foreach($podetail as $key => $value)
      <tr>
        <td>
          <span>{{$key+1}}</span>
        </td>
        <td>
          <span>{{$value->product_code}}</span>
        </td>
        <td>
          <span>{{$value->unit_description}}</span>
        </td>
        <td>
          <span>{{$value->product_name}}</span>
        </td>
        <td>
          <span>{{$value->quantity}}</span>
        </td>
        <td>
          <span>{{number_format($value->price)}}</span>
        </td>
        <td>
          <span>{{number_format($value->sub_total)}}</span>
        </td>
      </tr>
      @endforeach
      @if($invoiceheader->reduction > 0)
      <tr>
        <td colspan="6">Potongan Invoice</td>
        <td>{{'-'.number_format($invoiceheader->reduction)}}</td>
      </tr>
      @endif
      @if($invoiceheader->prev_invoice_reduction > 0)
      <tr>
        <td colspan="6">Potongan Invoice Sebelumnya</td>
        <td>{{'-'.number_format($invoiceheader->prev_invoice_reduction)}}</td>
      </tr>
      @endif
      <tr>
        <td colspan="6">Total</td>
        <td>{{number_format($invoiceheader->total_amount)}}</td>
      </tr>
      @if($discount)
        @foreach($discount as $key => $value)
          <tr>
            @if($invoiceheader->discount_type == 1) // nominal
            <td colspan ="7">{{"Discount Term [Rp ".number_format($value->discount)."/".$value->discount_period."]"}}</td>
            @else
            <td colspan ="7">{{"Discount Term [".$value->discount."%/".$value->discount_period."]"}}</td>
            @endif
          </tr>
        @endforeach
      @endif
    </tbody>
  </table>
  
</div>
</div>