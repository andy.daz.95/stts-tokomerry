<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Invoice</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <div class="input-field col l3">
                  <input id="filterInvoice" type="text" class="f-input">
                  <label>Filter Purchase Invoice No.</label>
                </div>
                <div class="input-field col l3">
                  <input id="filterPo" type="text" class="f-input">
                  <label>Filter PO</label>
                </div>
                <div class="input-field col l3">
                  <input id="filterSupplier" type="text" class="f-input">
                  <label>Filter Supplier</label>
                </div>
                <div class="col l12 m12 s12">
                  <table id="purchaseInvoiceTable" class="highlight table display nowrap dataTable dtr-  inline">
                    <thead>
                    <tr>
                      <th>No. Invoice</th>
                      <th>PO. Number</th>
                      <th>Tanggal PO</th>
                      <th>Supplier</th>
                      <th>Total PO</th>
                      <th>Nilai Invoice</th>
                      <th>Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--@foreach($data['invoice'] as $key => $value)--}}
                    {{--<tr mode="view" value="{{$value->invoice_purchase_id}}">--}}
                    {{--<td class="noinvoice">{{$value->invoice_purchase_number}}</td>--}}
                    {{--<td>{{$value->purchase_order_number}}</td>--}}
                    {{--<td>{{$value->date_purchase_order}}</td>--}}
                    {{--<td>{{$value->company_name}}</td>--}}
                    {{--<td class="number">{{$value->grand_total_idr}}</td>--}}
                    {{--<td class="number">{{$value->total_amount}}</td>--}}
                    {{--<td>--}}
                    {{-- <a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->invoice_purchase_id}}"><i class="material-icons">edit</i></a> --}}
                    {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                    {{--</td>--}}
                    {{--</tr>--}}
                    {{--@endforeach--}}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Invoice</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formInvoice">
                  <div class="input-field col l4 m2 s12">
                    <input id="noinvoice" name="noinvoice" type="text" class="f-input" placeholder="No. Invoice" disabled>
                    <input id="idinvoice" name="id" type="text" class="f-input" placeholder="No. Invoice" hidden>
                    <label>No Invoice</label>
                  </div>
                  <div class="input-field col l4 m2 s12">
                    <select id="nopo" name="nopo" class="selectpicker browser-default" name="nopo" data-live-search="true" data-size="5"  >
                      <option value='0'>Choose Po</option>
                      @foreach($data['po'] as $key => $value )
                        <option value="{{$value->purchase_order_id}}">{{$value->purchase_order_number.' - '.$value->supplier->company_name}}</option>
                      @endforeach
                    </select>
                    <label>No Po</label>
                  </div>
                  <div class="input-field col l4 m2 s12">
                    <input id="tglinvoice" type="text" name="tglinvoice" class="f-input" placeholder="Tanggal Tanda Terima">
                    <label>Tanggal Tanda Terima</label>
                  </div>
                  <div class="input-field col l4 m2 s12">
                    <input id="supplier" name="supplier" type="text" class="f-input" placeholder="Customer" disabled>
                    <label>Supplier</label>
                  </div>
                  <div class="input-field col l4 m2 s12">
                    <input id="paymenttype" type="text" name="paymenttype" class="f-input" placeholder="Payment Type" disabled>
                    <label>Payment Type</label>
                  </div>
                  <div class="input-field col l4 m2 s12">
                    <input id="tglpo" type="text" name="tglpo" class="f-input" placeholder="Tanggal SO" disabled>
                    <label>Tanggal PO</label>
                  </div>
                  <div class="col l12 m12 s12">
                    <table class="table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th>No Po</th>
                        <th>Total PO</th>
                        <th class="retur-header" hidden>Return Value</th>
                        {{-- <th>Sisa Pembayaran</th> --}}
                        <th>Potongan Invoice Sebelumnya</th>
                        <th>Nilai Invoice</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr id="no-item"><td colspan="5"><span> No Item Selected</span></td></tr>
                      <tr id="invoice-row" hidden>
                        <td>
                          <input id="tablenopo" type="text" name="tablenopo" class="f-input" disabled="disabled">
                        </td>
                        <td>
                          <input id="total" type="text" name="total" class="f-input" disabled="disabled">
                        </td>
                        <td class="retur-data" hidden>
                          <input id="retur" type="text" name="retur" class="f-input" disabled="disabled">
                        </td>
                        {{-- <td>
                          <input id="remaining" type="text" name="remaining" class="f-input" disabled="disabled">
                        </td> --}}
                        <td>
                          <input id="prev" type="text" class="f-input" disabled="disabled">
                          <input id="prev-hidden" type="text" name="prev" class="f-input" hidden>
                        </td>
                        <td>
                          <input id="payment" type="text" class="f-input">
                          <input id="payment-hidden" type="text" name="payment" class="f-input" hidden>
                        </td>
                      </tr>
                      </tbody>
                      <tfoot id="append-discount">
                      <tr id="discount-hidden" class="discount-row" hidden>
                        <td colspan="3"><label style="font-weight: bold" id="label-1">Discount</label></td>
                        <td>
                          <input id="discount-1" type="text" class="f-input" disabled>
                        </td>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div class="row">
                    <div class="col l5 m5 s12 input-field">
                      <a id="submit-invoice" href="#" class="btn-stoko light-blue" mode="save">Submit</a>
                      <a id="edit-invoice" href="#" class="btn-stoko light-blue" mode="edit" hidden>Edit</a>
                      <!-- <a href="#" class="btn btn-raised grey lighten-4 grey-text text-darken-4"><i class="material-icons">delete</i></a> -->
                      <a id="clear" href="" class="btn-stoko btn-stoko-primary orange">Clear</a>
                      <a id="cetak-invoice" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Invoice</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<div id="modal-delete-invoice" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-invoice" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        // $.ajax({
        //     url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
        //     dataType: "script",
        // });
        firstload();

        $('#nopo').on('change',function(){
            var id = $(this).val()
            $.ajax({
                type:"Get",
                url:"getpoforinvoice",
                data:{id:id},
                success:function(response){
                    if (response.length == 0) {
                        $('#formBpb').find("input[type=text], textarea").val("");
                        $('#invoice-row').attr('hidden',true);
                        $('#no-item').removeAttr('hidden');
                    }else{
                        var remaining = response.po.grand_total_idr - response.po.reduction - response.po.next_invoice_reduction;
                        $('#noinvoice').val(response.po.purchase_order_number);
                        $('#tablenopo').val(response.po.purchase_order_number);
                        $('#supplier').val(response.po.company_name);
                        $('#tglpo').val(response.po.date_purchase_order);
                        $('#paymenttype').val(response.po.payment_description);
                        $('#total').val(accounting.formatMoney(response.po.grand_total_idr,'Rp. ',2,',','.'));
                        $('#remaining').val(accounting.formatMoney(remaining,'Rp. ',2,',','.'));
                        // alert(response.next_invoice_reduction);
                        $('#prev').val(accounting.formatMoney(response.po.next_invoice_reduction,'Rp. ',2,',','.'));
                        $('#prev-hidden').val(response.po.next_invoice_reduction);
                        $('#no-item').attr('hidden',true);
                        $('#invoice-row').removeAttr('hidden');
                        $('#payment').val(accounting.formatMoney(remaining,'Rp. ',2,',','.')).attr('disabled',true);
                        $('#payment-hidden').val(remaining);
                        console.log(response.discount.length);
                        if(response.discount.length > 0)
                        {
                            getDiscount(response.discount, response.po.grand_total_idr, response.po.discount_type);
                        }else{
                            var $original = $('.discount-row:first');
                            $('.discount-row').remove();
                            $original.find("input[type=text], textarea").val("");
                            $original.find("label").html("");
                            $original.find("label").html("");
                            $original.attr('hidden',true);
                            $original.appendTo('#append-discount')
                        }
                        if(response.po.reduction > 0)
                        {
                            $('.retur-header').removeAttr('hidden');
                            $('.retur-data').removeAttr('hidden');
                            $('#retur').val(accounting.formatMoney(response.po.reduction, 'Rp. ',2,',','.'));
                        }else{
                            $('.retur-header').attr('hidden',true);
                            $('.retur-data').attr('hidden',true);
                        }
                    }
                }
            });
        });

        $('#submit-invoice, #edit-invoice').on('click',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            var kode = $('#noinvoice').val();
            mode = $(event.currentTarget).attr('mode');
            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "createpurchaseinvoice";
                var successmessage = 'Purchase Invoice '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatepurchaseinvoice";
                var successmessage = 'Purchase Invoice '+kode+' telah berhasil diubah!';
            }
            if($('#nopo').val() == 0){
                toastr.warning('Anda Belum Memilih Purchase Order!');
            }else{
                me.data('requestRunning', true);
                me.invisible();

                $('#nopo, #noinvoice, #idinvoice, #payment').removeAttr('disabled');
                $.ajax({
                    type:"post",
                    url:url,
                    data: $('#formInvoice').serialize(),
                    success:function(response){
                        toastr.success(successmessage, {"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                });
            }
        });

        $('body').on('click','#purchaseInvoiceTable tr, #purchaseInvoiceTable .edit', function(event){
            event.stopImmediatePropagation();
            var id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            $.ajax({
                type:"Get",
                url:"getpurchaseinvoice",
                data:{id:id},
                success:function(response){
                    $('#noinvoice').val(response.invoice.invoice_purchase_number).attr('disabled',true);
                    $('#idinvoice').val(response.invoice.invoice_purchase_id).attr('disabled',true);
                    $('#nopo').append('<option class="remove-when-clear" value="'+response.invoice.purchase_order_id+'" selected="selected">'+response.invoice.purchase_order_number+'</option>').attr('disabled',true);
                    $('#tablenopo').val(response.invoice.purchase_order_number);
                    $('#supplier').val(response.invoice.company_name);
                    $('#tglpo').val(response.invoice.date_purchase_order);
                    $('#tglinvoice').val(response.invoice.date_invoice_purchase).attr('disabled',true);
                    $('#note').val(response.invoice.note).attr('disabled',true);
                    $('#paymenttype').val(response.invoice.payment_description);
                    $('#total').val(accounting.formatMoney(response.invoice.grand_total_idr,'Rp. ',2,',','.'));
                    $('#remaining').val(accounting.formatMoney((response.invoice.grand_total_idr - response.invoice.reduction - response.invoice.total),'Rp. ',2,',','.'));
                    $('#payment').val(accounting.formatMoney((response.invoice.total_amount),'Rp. ',2,',','.')).attr('disabled',true);
                    $('#prev').val(accounting.formatMoney((response.invoice.prev_invoice_reduction),'Rp. ',2,',','.')).attr('disabled',true);
                    $('.selectpicker').selectpicker('refresh');
                    $('#no-item').attr('hidden',true);
                    $('#invoice-row').removeAttr('hidden');
                    if(parseInt(response.invoice.reduction) > 0)
                    {
                        $('.retur-header').removeAttr('hidden');
                        $('.retur-data').removeAttr('hidden');
                        $('#retur').val(accounting.formatMoney(response.invoice.reduction, 'Rp. ',2,',','.'));
                    }else{
                        $('.retur-header').attr('hidden',true);
                        $('.retur-data').attr('hidden',true);
                    }
                    if(response.discount.length > 0)
                    {
                        getDiscount(response.discount, response.invoice.grand_total_idr, response.invoice.discount_type);
                    }else{
                        var $original = $('.discount-row:first');
                        $('.discount-row').remove();
                        $original.find("input[type=text], textarea").val("");
                        $original.find("label").html("");
                        $original.find("label").html("");
                        $original.attr('hidden',true);
                        $original.appendTo('#append-discount')
                    }
                },complete:function(){
                    if(mode == "edit"){
                        $('#edit-invoice').removeAttr('hidden');
                        $('#payment').removeAttr('disabled');
                    }else{
                        $('#cetak-invoice').removeAttr('hidden').attr('href',"downloadpurchaseinvoice/"+id);
                    }
                    $('#submit-invoice').attr('hidden',true);
                }
            });
        });

        $('#payment').on('focusout', function(){
            $('#payment-hidden').val($(this).val());
            $(this).val(accounting.formatMoney($(this).val(),'Rp. ',2,',','.'))
        });
        $('#payment').on('focusin', function(){
            $(this).val("");
        })

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        $('body').on('click','#purchaseInvoiceTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var noinvoice = $(this).closest('tr').find('.noinvoice').html();
            var idinvoice = $(this).closest('tr').attr('value');
            $('#confirm-delete-invoice').attr('value',idinvoice).attr('nomor',noinvoice);
            $("#delete-message").html("Yakin ingin menghapus data "+noinvoice+" ?")
            $('#modal-delete-invoice').modal('open');
        });

        $('#confirm-delete-invoice').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-invoice').modal('close');
            var id = $(this).attr('value');
            var noinvoice = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepurchaseinvoice",
                data:{id:id},
                success:function(response){
                    toastr.success('Purchase Invoice '+noinvoice+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        //function
        function firstload()
        {
            $('#tglso').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tglinvoice').datepicker({
                dateFormat: 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tglinvoice').val(moment().format('DD-MM-YYYY'));

            // $.ajax({
            //   type:"GET",
            //     url:"lastpurchaseinvoicenumber",
            //     success:function(response){
            //       $('#noinvoice').val(response);
            //     }
            // })

            $('.selectpicker').selectpicker('render');

            $('.number').each(function(){
                $(this).html(accounting.formatMoney($(this).html(),'',2,',','.'));
            });


            var invoiceTable = $('#purchaseInvoiceTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getpurchaseinvoicetable',
                    data : function (d){
                        d.noinvoice = $('#filterinvoice').val();
                        d.nopo = $('#filterPo').val();
                        d.supplier = $('#filterSupplier').val();
                    }
                },
                rowId : 'invoice_purchase_id',
                columns: [
                    { data: 'invoice_purchase_number', name: 'invoice_purchase_number', class:'noinvoice'},
                    { data: 'purchase_order_number', name: 'purchase_order_number'},
                    { data: 'date_purchase_order', name: 'date_purchase_order'},
                    { data: 'company_name', name: 'company_name'},
                    { data: 'grand_total_idr', name: 'grand_total_idr'},
                    { data: 'total_amount', name: 'total_amount'},
                    { data: 'action', name: 'action'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterInvoice').on('keyup', function () { // This is for news page
                invoiceTable.column(0).search(this.value).draw();
            });
            $('#filterPo').on('keyup', function () { // This is for news page
                invoiceTable.column(1).search(this.value).draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                invoiceTable.column(3).search(this.value).draw();
            });

        }

        function getDiscount(discount, total, $type){
            $('#discount-term').removeAttr('hidden');
            var $original = $('.discount-row:first');
            var $cloned = $original.clone();
            $('.discount-row').remove();

            for(var i=0; i<discount.length; i++){
                var newlabel = "label-"+(i+1);
                var newdiscount = "discount-"+(i+1);
                $temp = $original.clone();
                $temp.removeAttr('hidden');
                $temp.find('#label-1').attr('id',newlabel);
                $temp.find('#discount-1').attr('id',newdiscount);
                $temp.appendTo('#append-discount');
                if($type == 1) // nominal
                {
                    $('#label-'+(i+1)).html("Discount Term [Rp "+discount[i].discount+"/"+discount[i].discount_period+"]");
                    $('#discount-'+(i+1)).val(accounting.formatMoney(discount[i].discount, 'Rp. ',2,',','.'));
                }else{
                    $('#label-'+(i+1)).html("Discount Term ["+discount[i].discount+"%/"+discount[i].discount_period+"]");
                    $('#discount-'+(i+1)).val(accounting.formatMoney(discount[i].discount / 100 * total, 'Rp. ',2,',','.'));
                }
            }
        }
    });
</script>
