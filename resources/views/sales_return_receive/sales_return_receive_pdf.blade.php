<style type="text/css">
    table.table {
        border-collapse: collapse;
    }
    table.table, table.table th, table.table td{
        border: 1px solid black;
    }
</style>
<div>
    <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
    <p style="text-align: center; margin:0px; font-weight: 600">Bukti Penerimaan Retur Penjualan</p>
    <p style="text-align: center; margin:0px">0853-9978-7515</p>
    <hr>
    <table style ="width:100%">
        <tr>
            <td style="width: 25%">NO Penerimaan Retur</td>
            <td style="width: 25%">: {{$returreceive->sales_retur_receive_number}}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 25%">NO Retur</td>
            <td style="width: 25%">: {{$returreceive->retur->retur_number}}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 25%">Nama Pelanggan</td>
            <td style="width: 25%">: {{$returreceive->retur->so->customer->first_name.' '.$returreceive->retur->so->customer->last_name }}</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Tgl Penerimaan Retur</td>
            <td>: {{$returreceive->date_sales_retur_receive}}</td>
            <td></td>
            <td></td>
        </tr><tr>
            <td>Gudang</td>
            <td>: {{$returreceive->warehouse->warehouse_name}}</td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
        <thead>
        <tr style="background-color: #8B8C89">
            <th class="color" style="text-align: center;">Nama</th>
            <th class="color" style="width: 100px; text-align: center;">Retur Qty</th>
            <th class="color" style="width: 100px; text-align: center;">Qty Diterima</th>
        </tr>
        </thead>
        <tbody >
        @foreach($returreceive->details as $key => $value)
            <tr>
                <td>
                    <span>{{$value->product->product_name}}</span>
                </td>
                <td>
                    <span>{{$value->retur_qty}}</span>
                </td>
                <td>
                    <span>{{$value->quantity}}</span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</div>