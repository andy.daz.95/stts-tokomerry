<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Sales Return</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br/>
                <div class="input-field col l3">
                  <label>Filter No Penerimaan Retur</label>
                  <input id="filterReturReceiveNumber" type="text" class="f-input" placeholder="Filter Retur" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Pelanggan</label>
                  <input id="filterPelanggan" type="text" class="f-input" placeholder="Filter Pelanggan" />
                </div>
              </div>
              <div class="row">
                <div class="col l12 m12 s12">
                  <div class="table-responsive">
                    <table id="returReceiveTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th class="theader">No Penerimaan Retur </th>
                        <th class="theader">No Retur</th>
                        <th class="theader">Pelanggan</th>
                        <th class="theader">Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($data['returreceive'] as $key => $value)--}}
                      {{--<tr value="{{$value->sales_retur_receive_id}}">--}}
                      {{--<td class="noreturreceive">{{$value->sales_retur_receive_number}}</td>--}}
                      {{--<td>{{$value->retur->retur_number}}</td>--}}
                      {{--<td>{{$value->retur->so->customer->first_name.' '.$value->retur->so->customer->last_name}}</td>--}}
                      {{--<td>--}}
                      {{--@if(Session('roles')->name == 'master')--}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                    </br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir Pengembalian Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br/>
                <form id="formPenerimaanRetur">
                  <div class="input-field col l3">
                    <label>No Pengembalian Retur Penjualan</label>
                    <input id="noreturreceive" type="text" name="noreturreceive" class="f-input" disabled>
                    <input id="idreturreceive" type="text" name="idreturreceive" class="f-input" disabled hidden>
                  </div>
                  <div class="input-field col l3">
                    <label>No retur</label>
                    <select id="noretur" name="noretur" class="selectpicker browser-default" data-live-search="true" data-size="5"  >
                      <option value="">Pilih No retur</option>
                      @foreach($data['retur'] as $key => $value)
                        <option value="{{$value->sales_retur_id}}">{{$value->retur_number.' - '.$value->so->customer->first_name.' '.$value->so->customer->last_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Pelanggan</label>
                    <input id="customer" type="text" name="customer" class="f-input" disabled>
                  </div>
                  <div class="input-field col l3">
                    <label>Tanggal Retur</label>
                    <input id="tglpenerimaanretur" type="text" name="tglpenerimaanretur" class="f-input" placeholder="Tanggal Penerimaan Retur">
                  </div>
                  <div id="gudang-div" class="input-field col l3">
                    <label>Gudang</label>
                    <select id="gudang" name="gudang" class="selectpicker browser-default" data-live-search="true" data-size="5">
                      <option value="0">Select Gudang</option>
                      @foreach($data['warehouse'] as $key => $value)
                        <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader">Kode Barang</th>
                          <th class="theader">Nama Barang</th>
                          <th class="theader">Unit</th>
                          <th class="theader">Retur Qty</th>
                          <th class="theader">Total Qty Diterima</th>
                          <th class="theader">Qty Diterima</th>
                        </tr>
                        </thead>
                        <tbody id="barang-data">
                        <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                        <tr class="barang-row" hidden>
                          <td>
                            <div class="input-field">
                              <input id="id-1" name="id[]" type="text" class="f-input" hidden>
                              <input id="kode-1" name="kode[]" type="text" class="f-input kode">
                              <input id="detail-retur-1" name="detailretur[]" type="text" class="f-input" hidden>
                              <input id="detail-retur-receive-1" name="detailreturreceive[]" type="text" class="f-input" hidden>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="nama-1" name="namabarang[]" type="text" class="f-input">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="unit-1" name="unit[]" type="text" class="f-input">
                              <input id="unit-id-1" name="unitid[]" type="text" class="f-input unitid" hidden>
                              <input id="unit-type-1" name="unittype[]" type="text" class="f-input unittype" hidden>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="retur-qty-1" name="returqty[]" type="text" class="f-input returqty" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="total-received-qty-1" name="totalreceivedqty[]" type="text" class="f-input totalreceivedqty" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="receive-qty-1" name="receiveqty[]" type="text" class="f-input receiveqty">
                            </div>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                      </br>
                    </div>
                  </div>
                  <div class="col l12 s12 m12 margin-top">
                    <a id="submit-retur" href="#" class="btn-stoko teal white-text submit" mode="save">simpan</a>
                    <a id="edit-retur" href="#" class="btn-stoko teal white-text" mode="edit" hidden>edit</a>
                    <a href="#" id="clear" class="btn-stoko">batal</a>
                    <a id="cetak-penerimaan-return" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Retur</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-retur" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-retur" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#noretur').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getreturforreceive",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    if(response.length == 0){
                        $original.clone();
                        $original.attr('hidden',true)
                        $original.appendTo('#barang-data');
                        $('.barang-row').attr('hidden',true);
                        $('#returtype').attr('disabled',true);
                        $('#no-item').removeAttr('hidden');
                    }else{
                        $('#customer').val(response.so.customer.first_name+' '+response.so.customer.last_name);
                        $.each(response.details, function(i, v){
                            var newid = "id-"+(i+1);
                            var newdetailretur = "detail-retur-"+(i+1);
                            var newdetailreturreceive = "detail-retur-receive-"+(i+1);
                            var newkode = "kode-"+(i+1);
                            var newnama = "nama-"+(i+1);
                            var newunit = "unit-"+(i+1);
                            var newunitid = "unit-id-"+(i+1);
                            var newunittype = "unit-type-"+(i+1);
                            var newreturqty = "retur-qty-"+(i+1);
                            var newtotalreceivedqty = "total-received-qty-"+(i+1);
                            var newreceiveqty = "receive-qty-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#detail-retur-1').attr('id',newdetailretur);
                            $temp.find('#kode-1').attr('id',newkode);
                            $temp.find('#nama-1').attr('id',newnama);
                            $temp.find('#unit-1').attr('id',newunit);
                            $temp.find('#unit-id-1').attr('id',newunitid);
                            $temp.find('#unit-type-1').attr('id',newunittype);
                            $temp.find('#retur-qty-1').attr('id',newreturqty);
                            $temp.find('#total-received-qty-1').attr('id',newtotalreceivedqty);
                            $temp.find('#receive-qty-1').attr('id',newreceiveqty);
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(v.product_id);
                            $('#detail-retur-'+(i+1)).val(v.sales_retur_details_id);
                            $('#kode-'+(i+1)).val(v.product.product_code).attr('disabled',true);
                            $('#nama-'+(i+1)).val(v.product.product_name).attr('disabled',true);
                            if(v.unit_type == "child")
                            {
                                $('#unit-'+(i+1)).val(v.unit_child.unit_child_name).attr('disabled',true);
                            }else{
                                $('#unit-'+(i+1)).val(v.unit.unit_description).attr('disabled',true);
                            }
                            $('#unit-id-'+(i+1)).val(v.unit_id).attr('disabled',true);
                            $('#unit-type-'+(i+1)).val(v.unit_type).attr('disabled',true);
                            $('#retur-qty-'+(i+1)).val(v.qty);
                            $('#total-received-qty-'+(i+1)).val(v.received_qty);
                            if(v.received_qty >= v.qty){
                                $('#receive-qty-'+(i+1)).val(0).attr('disabled',true);
                            }else{
                                $('#receive-qty-'+(i+1)).val(0);
                            }
                        });
                        $('.selectpicker').selectpicker('refresh');
                        $('#no-item').attr('hidden',true);
                        $('.barang-row').removeAttr('hidden');
                        $('#returtype').trigger('change');
                    }
                }
            });
        });

        $('body').on('click', '#returReceiveTable tr', function(event){
            event.stopImmediatePropagation();
            id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            $.ajax({
                type:"GET",
                url:"getsalesreturnreceive",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('#idreturreceive').val(response.sales_retur_receive_id).attr('disabled',true);
                    $('#noretur').append('<option class="remove-when-clear" value="'+response.retur.sales_retur_id+'" selected="selected">'+response.retur.retur_number+'</option>').attr('disabled',true);
                    $('#customer').val(response.retur.so.customer.first_name+' '+response.retur.so.customer.last_name).attr('disabled',true);
                    $('#tglpenerimaanretur').val(response.date_sales_retur_receive).attr('disabled',true);
                    $('#gudang').val(response.warehouse_id).attr('disabled',true);
                    $.each(response.details, function(i, v){
                        var newid = "id-"+(i+1);
                        var newdetailretur = "detail-retur-"+(i+1);
                        var newdetailreturreceive = "detail-retur-receive-"+(i+1);
                        var newkode = "kode-"+(i+1);
                        var newnama = "nama-"+(i+1);
                        var newunit = "unit-"+(i+1);
                        var newunittype = "unit-type-"+(i+1);
                        var newunitid = "unit-id-"+(i+1);
                        var newreturqty = "retur-qty-"+(i+1);
                        var newtotalreceivedqty = "total-received-qty-"+(i+1);
                        var newreceiveqty = "receive-qty-"+(i+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#detail-retur-1').attr('id',newdetailretur);
                        $temp.find('#kode-1').attr('id',newkode);
                        $temp.find('#nama-1').attr('id',newnama);
                        $temp.find('#unit-1').attr('id',newunit);
                        $temp.find('#unit-id-1').attr('id',newunitid);
                        $temp.find('#unit-type-1').attr('id',newunittype);
                        $temp.find('#retur-qty-1').attr('id',newreturqty);
                        $temp.find('#total-received-qty-1').attr('id',newtotalreceivedqty);
                        $temp.find('#receive-qty-1').attr('id',newreceiveqty);
                        $temp.appendTo('#barang-data');
                        $('#id-'+(i+1)).val(v.product_id);
                        $('#kode-'+(i+1)).val(v.product.product_code).attr('disabled',true);
                        $('#nama-'+(i+1)).val(v.product.product_name).attr('disabled',true);
                        if( v.unit_type == "child"){
                            $('#unit-'+(i+1)).val(v.unit_child.unit_child_name).attr('disabled',true);
                        }else{
                            $('#unit-'+(i+1)).val(v.unit.unit_description).attr('disabled',true);
                        }
                        $('#unit-type-'+(i+1)).val(v.unit_type).attr('disabled',true);
                        $('#unit-id-'+(i+1)).val(v.unit_id).attr('disabled',true);
                        $('#retur-qty-'+(i+1)).val(v.retur_qty);
                        $('#total-received-qty-'+(i+1)).val(v.received_qty);
                        $('#receive-qty-'+(i+1)).val(v.quantity).attr('disabled',true);
                    });
                    $('#no-item').attr('hidden',true);
                    $('.barang-row').removeAttr('hidden');
                    $('.selectpicker').selectpicker('refresh');
                },complete:function(){
                    if(mode=="edit")
                    {
                        $('#submit-retur').attr('hidden',true);
                        $('#edit-retur').removeAttr('hidden');
                        $('#tglretur, .qty').removeAttr('disabled');
                    }else{
                        $('#cetak-penerimaan-return').removeAttr('hidden').attr('href',"downloadsalesreturnreceive/"+id);
                    }
                }
            })
        });

        $('#formPenerimaanRetur #submit-retur, #formPenerimaanRetur #edit-retur').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            var kode = $('#noretur').val();
            mode = $(event.currentTarget).attr('mode');
            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "createpenerimaanreturn";
                var successmessage = 'Penerimaan Return '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatepenerimaanreturn";
                var successmessage = 'Penerimaan Return '+kode+' telah berhasil diubah!';
            }

            var validqty = [];
            var totalqty = 0;
            var maxretur = 0

            $('.barang-row').each(function(){
                var returqty = parseInt($(this).find('.receiveqty').val());
                validqty.push(ceknumber($(this).find('.receiveqty').val()));
                totalqty += returqty;
                maxretur += parseInt($(this).find('.returqty').val());
            });

            if($('#noso').val() ==0){
                toastr.warning('Anda Belum Memilih SO!');
            }else if($('#gudang').val() == 0){
                toastr.warning('Anda Belum Memilih Gudang');
            }else if(validqty.indexOf(false) > -1){
                toastr.warning('Quantity harus Angka!');
            }else if(totalqty <= 0){
                toastr.warning('Anda Belum Memasukkan quantity!');
            }else if(totalqty > maxretur){
                toastr.warning('Quantity Penerimaan Tidak Boleh Melebihi Quantity Retur!');
            }else {
                me.invisible();
                me.data('requestRunning', true);
                $('#noretur, .price , .kode, .receiveqty, #idretur, #gudang, #returtype, .unittype, .unitid').removeAttr('disabled');
                $.ajax({
                    type:"POST",
                    url:url,
                    data:$(this).closest('#formPenerimaanRetur').serialize(),
                    success:function(response){
                        toastr.success(successmessage,{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                })
            }
        })

        $('body').on('click','#returReceiveTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var noreturreceive = $(this).closest('tr').find('.noreturreceive').html();
            var idretur = $(this).closest('tr').attr('value');
            $('#confirm-delete-retur').attr('value',idretur).attr('nomor',noreturreceive);
            $("#delete-message").html("Yakin ingin menghapus data "+noreturreceive+" ?")
            $('#modal-delete-retur').modal('open');
        });

        $('#confirm-delete-retur').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-retur').modal('close');
            var id = $(this).attr('value');
            var noreturreceive = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepenerimaanreturn",
                data:{id:id},
                success:function(response){
                    toastr.success('Sales Return '+noretur+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        //function
        function firstload()
        {
            $.ajax({
                type:"GET",
                url:"lastsalesreturnnumber",
                success:function(response){
                    $('#noretur').val(response);
                }
            })

            $('.selectpicker').selectpicker('render');

            $('#tglpenerimaanretur').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            setToday();

            var returReceiveTable = $('#returReceiveTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getsalesreturnreceivetable',
                    data : function (d){
                        d.noreturreceive = $('#filterReturReceiveNumber').val();
                        d.customer = $('#filterPelanggan').val();
                    }
                },
                rowId : 'sales_retur_receive_id',
                columns: [
                    { data: 'sales_retur_receive_number', name: 'sales_retur_receive_number', class:'noreturreceive'},
                    { data: 'retur_number', name: 'retur_number' },
                    { data: 'customer_name', name: 'customer_name' },
                    { data: 'action', name: 'action' },
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterReturReceiveNumber').on('keyup', function () { // This is for news page
                returReceiveTable.draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                returReceiveTable.draw();
            });
        }
        function setToday(){
            $('#tglpenerimaanretur').val(moment().format('DD-MM-YYYY'));
        }
    });
</script>
