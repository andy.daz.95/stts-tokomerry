<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
  border: 1px solid black;
}
html { margin: 20px 15px 5px 15px}
body {
  font-size: 14px;
  letter-spacing: 1px;
}
.txt-lg{
  font-size: 16px;
}
.txt-header{
  font-size: 30px;
}
</style>
<div>
  <table style ="width:100%">
    <tr>
      <td style="text-align: left"><span>Gunung Sari Jaya Bulukumba</span></td>
      <td style="text-align: right"><span>0853-9978-7515</span></td>
    </tr>
    <tr>
      <td colspan="2" style="text-align: center"><span class="txt-header">SALES ORDER</span></td>
    </tr>
  </table>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 50%">Kepada YTH,</td>
      <td style="width: 25%">SO Number</td>
      <td>: {{$soheader->sales_order_number}}</td>
    </tr>
    <tr>
      <td>{{$soheader->first_name.' '.$soheader->last_name}}</td>
      <td>Tanggal SO</td>
      <td>: {{$soheader->date_sales_order}}</td>
    </tr>
    <tr>
      <td>{{$soheader->address}}</td>
      <td>Payment Type</td>
      <td>: {{$soheader->payment_description}}</td>
    </tr>
    <tr>
      <td>{{$soheader->postal_code}}</td>
      <td></td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr>
        <td class="color" style="width: 375px; text-align: center;">Nama</td>
        <td class="color" style="text-align: center;">Keterangan</td>
        <td class="color" style="width: 75px; text-align: center;">Qty</td>
        <td class="color" style="width: 100px; text-align: center;">Unit</td>
        <td class="color" style="text-align: center;">Harga</td>
        <td class="color" style="text-align: center;">Sub Total</td>
      </tr>
    </thead>
    <tbody >
      @foreach($sodetail as $key => $value)
      <tr >
        <td>
          <span>{{$value->p_name}}</span>
        </td>
        <td>
          <span>{{$value->detail_note}}</span>
        </td>
        <td style="text-align: center;">
          <span >{{$value->quantity}}</span>
        </td>
        <td style="text-align: center;">
          <span>
            @if($value->unit_type == 'main')
            @php
            $satuan = App\satuan::where('unit_id', $value->unit_id)->first()->unit_code;
            $firstnumber = App\Traits\UnitTrait::my_offset($satuan);
            $satuan = substr($satuan,0,$firstnumber)
            @endphp
            {{$satuan}}
            @else
            {{App\unitchild::where('unit_child_id', $value->unit_id)->first()->unit_child_code}}
            @endif
          </span>
        </td>
        <td>
          <span style="display: inline-block; float:right">{{number_format($value->price)}}</span>
        </td>
        <td>
          <span style="display: inline-block; float:right; clear:both;">{{number_format($value->sub_total)}}</span>
        </td>
      </tr>
      @endforeach
      <tr style="line-height: 25px">
        <td colspan="1"><span>Grand Total</span></td>
        <td colspan="5">
          <span class="txt-lg" style="float:right;">Rp. {{number_format($soheader->grand_total_idr)}}</span>
        </td>
      </tr>
    </tbody>
  </table>
  <table style ="width:100%">
    <tr>
      <td style="width:25%">Sales Name</td>
      <td style="width:25%">: {{$soheader->full_name}}</td>
      <td></td>
    </tr>
  </table>
</div>
</div>