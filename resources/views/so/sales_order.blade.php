<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Sales Order</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <div class="input-field col l3">
                  <label>Filter SO</label>
                  <input id="filterSoNumber" type="text" class="f-input" placeholder="Filter SO" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Pelanggan</label>
                  <input id="filterPelanggan" type="text" class="f-input" placeholder="Filter Pelanggan" />
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="SalesTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th class="theader">No SO</th>
                        <th class="theader">Pelanggan</th>
                        <th class="theader">Tanggal</th>
                        <th class="theader">Tipe Pembayaran</th>
                        <th class="theader">Catatan</th>
                        <th class="theader">Total</th>
                        <th class="theader">Jatuh Tempo</th>
                        <th class="theader">Dibuat Oleh</th>
                        <th class="theader">Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{-- Untuk Datatable Server Side --}}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Sales Order</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formSo">
                  <div class="input-field col l3">
                    <label>No Sales Order</label>
                    <input id="nosalesorder" type="text" class="f-input" name="nosalesorder" disabled>
                    <input id="idsalesorder" type="text" class="f-input" name="idso" disabled hidden>
                  </div>
                  <div class="input-field col l3">
                    <label>Pelanggan</label>
                    <select id="pelanggan" class="selectpicker browser-default" data-live-search="true" data-size="5"   name="pelanggan">
                      <option value="0">Select Pelanggan</option>
                      @foreach($customer as $key => $value)
                        <option value="{{$value->customer_id}}"}}">{{$value->first_name.' '.$value->last_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Pembayaran</label>
                    <select id="pembayaran" class="selectpicker browser-default" data-live-search="true" data-size="5"   name="pembayaran">
                      @foreach($payment_term as $key => $value)
                        <option value="{{$value->payment_type_id}}">{{$value->payment_description}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Terms</label>
                    <input id="terms" type="text" class="f-input" placeholder="Terms" name="terms" disabled>
                  </div>
                  <div class="input-field col l3">
                    <label>Jenis Pengiriman</label>
                    <select id="deliverytype" class="selectpicker browser-default" data-live-search="true" data-size="5"  name="deliverytype">
                      <option value="0">Pilih Jenis Pengiriman</option>
                      @foreach($shippingterm as $key => $value)
                        <option value="{{$value->shipping_term_id}}">{{$value->shipping_term_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Limit Kredit</label>
                    <input id="limitkredit" type="text" class="f-input" name="limitkredit" disabled>
                  </div>
                  <div class="input-field col l3">
                    <label>Utang Pelanggan</label>
                    <input id="utang" type="text" class="f-input" name="utang" disabled>
                  </div>
                  <div class="input-field col l3">
                    <label>Notes</label>
                    <input id="notes" type="text" class="f-input" name="notes">
                  </div>
                  <div class="input-field col l3">
                    <label>Tgl SO</label>
                    <input id="tglso" type="text" class="f-input" name="tglso" placeholder="Tanggal SO">
                  </div>
                  <div class="input-field col l3">
                    <label>Sales</label>
                    <select id="sales" class="selectpicker browser-default" data-live-search="true" data-size="5"   name="sales">
                      <option value="0">Pilih Sales</option>
                      @foreach($sales as $key => $value)
                        <option value="{{$value->account_id}}">{{$value->full_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Jenis Harga</label>
                    <select id="pricetype" class="selectpicker browser-default" data-live-search="true" data-size="5"  name="pricetype">
                      <option value="0">Pilih Jenis Harga</option>
                      <option value="1">Harga Eceran</option>
                      <option value="2">Harga Partai</option>
                    </select>
                  </div>
                  <div class="input-field col l3 right">
                    <button id="tambah-barang" data-target="modal-tambah-barang" {{-- href="#tambah-barang" --}} class="btn-stoko btn-stoko-primary right">Tambah Barang</button>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader" style="width: 300px">Nama</th>
                          <th class="theader">Keterangan</th>
                          <th class="theader" >Tipe </br> Faktur</th>
                          <th class="theader" style="width: 75px">Qty</th>
                          <th class="theader" style="width: 100px">Unit</th>
                          <th class="theader">Harga Barang</th>
                          <th class="theader">Potongan Harga</th>
                          <th class="theader">Harga satuan</th>
                          <th class="theader">Sub Total</th>
                          <th class="theader" style="width: 75px">Tindakan</th>
                        </tr>
                        </thead>
                        <tbody id="barang-data">
                        <tr id="no-item"><td colspan="7"><span> No Item Selected</span></td></tr>
                        <tr class="barang-row" hidden>
                          <td>
                            <div class="input-field">
                              <input id="id-1" type="text" class="f-input idproduct" name="idbarang[]" hidden>
                              <input id="stock-1" type="text" class="f-input stock" hidden>
                              <input id="nama-1" type="text" class="f-input nama">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <textarea id="detail-note-1" class="detailnote f-txt-sm" name="detailnote[]"></textarea>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="faktur-type-1" type="text" class="f-input fakturtype" name="fakturtype[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="qty-1" type="text" class="f-input qty" name="qty[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <select id="unit-1" type="text" class="f-select browser-default unit" name="unit[]">
                              </select>
                              <input id="unit-type-1" type="text" name="type[]" class="f-input unittype" hidden>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="harga-1" type="text" class="f-input harga" name="harga[]">
                              <input id="harga-modal-1" type="text" class="f-input hargamodal" name="hargamodal[]" hidden>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="potongan-1" type="text" class="f-input potongan" name="potongan[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="hargasatuan-1" type="text" class="f-input hargasatuan" name="hargasatuan[]" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="subtotal-1" type="text" class="f-input subtotal" name="subtotal[]">
                            </div>
                          </td>
                          <td>
                            <a class="btn btn-sm white grey-text copy-detail" style="margin: 5px"><i class="material-icons">content_copy</i></a>
                            <a class="btn btn-sm white grey-text delete-detail"><i class="material-icons">delete</i></a>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                      <br>
                      <div class="input-field" class="">
                        <input id="grandtotal" type="text" class="f-input" disabled>
                        <input id="grandtotal-hidden" type="text" class="f-input" name="grandtotal" hidden>
                        <label>Grand Total</label>
                      </div>
                    </div>
                    <div class="input-field right">
                      <a id="clear" href="" class="btn-stoko orange">Clear</a>
                      <a id="create-do" href="" class="btn-stoko btn-stoko-primary green">Buat Do</a>
                      <a id="create-antaran" href="" class="btn-stoko btn-stoko-primary green" hidden>Buat Antaran</a>
                      <a id="submit-so" href="" class="btn-stoko btn-stoko-primary" mode="save" hidden>Submit So</a>
                      <a id="edit-so" href="" class="btn-stoko btn-stoko-primary" mode="edit" hidden>Edit So</a>
                      <a id="cetak-so" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak So</a>
                      <a id="cetak-so-kecil" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak So Kecil</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-tambah-barang" class="modal">
  <form id="form-list-barang">
    <div class="modal-content">
      <h4>Tambah Barang</h4>
      <div class="row" id="append-barang">
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-barang">OK</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>

<div id="modal-create-do" class="modal">
  <div class="modal-header text-center">
    <h2>Buat DO</h2>
  </div>
  <div class="modal-content">
    <form id="formCreateDo">
      <table class="stoko-table no-border">
        <tr>
          <th style="width: 350px">Nama Barang</th>
          <th>Quantity</th>
          <th>DO Toko</th>
          <th>DO Pasar</th>
          <th>DO Ponci</th>
        </tr>
        <tbody id="append-do-row">
        <tr class="do-row">
          <td>
            <div class="input-field">
              <input id='id-product-do-1' type="text" class="f-input idbarang" name="idbarangdo[]" disabled hidden>
              <input type="text" class="f-input nama" name="namado[]" disabled>
            </div>
          </td>
          <td>
            <div class="input-field">
              <input id='qty-do-1' type="text" class="f-input qtydo" name="qtydo[]" disabled>
            </div>
          </td>
          <td>
            <div class="input-field">
              <span id='qty-w-toko-1' warehouse_name="{{$warehouse[2]->warehouse_name}}"></span>
              <input id='qty-toko-do-1' type="text" class="f-input qtydotoko" name="qtytoko[]" >
            </div>
          </td>
          <td>
            <div class="input-field">
              <span id='qty-w-pasar-1' warehouse_name="{{$warehouse[0]->warehouse_name}}"></span>
              <input id='qty-pasar-do-1' type="text" class="f-input qtydopasar" name="qtypasar[]">
            </div>
          </td>
          <td>
            <div class="input-field">
              <span id='qty-w-ponci-1' warehouse_name="{{$warehouse[1]->warehouse_name}}"></span>
              <input id='qty-ponci-do-1' type="text" class="f-input qtydoponci" name="qtyponci[]">
            </div>
          </td>
        </tr>
        </tbody>
      </table>
    </form>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-create-do" mode="save" class="modal-action waves-effect btn-stoko blue white-text">Buat Do</a>
  </div>
</div>

<div id="modal-create-antaran" class="modal">
  <div class="modal-header text-center">
    <h2>Buat Antaran</h2>
  </div>
  <div class="modal-content">
    <form id="formCreateAntaran">
      <div class="input-field">
        <label>Alamat Antaran</label>
        <textarea id="alamatantaran" type="text" class="f-input alamatantaran" name="alamatantaran"></textarea>
      </div>
      <table class="stoko-table no-border">
        <tr>
          <th>Total Harga</th>
          <th>Bayar</th>
        </tr>
        <tbody>
        <tr class="antaran-row">
          <td>
            <div class="input-field">
              <input id='totalsoantaran' type="text" class="f-input totalsoantaran" name="totalsoantaran" disabled>
            </div>
          </td>
          <td>
            <div class="input-field">
              <input id='totalbayarantaran' type="text" class="f-input totalbayarantaran" name="totalbayarantaran">
            </div>
          </td>
        </tr>
        </tbody>
      </table>
      <div class="input-field">
        <label>Sisa Bayar</label>
        <input id="sisabayarantaran" type="text" class="f-input sisabayarantaran" name="sisabayarantaran" disabled>
      </div>
    </form>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-create-antaran" mode="save" class="modal-action waves-effect btn-stoko blue white-text">Buat Do</a>
  </div>
</div>

<div id="modal-delete-so" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-so" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<div id="modal-additional-info" class="modal">
  <div class="modal-content">
    <textarea id="prevInfo" name="prevInfo" class="f-textarea" style="height:400px; position:relative" disabled></textarea>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
  </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#tambah-barang').on('click', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var jumlah = $('.barang-row').length;
            var barang = [];
            var src = "so";
            for (var i = 0; i < jumlah; i++) {
                var id = $('#id-' + (i + 1)).val();
                barang.push(id);
            }
            $.ajax({
                type: "GET",
                url: "modalListBarang",
                data: {barang: barang, src: src},
                success: function (response) {
                    $('#append-barang').html(response);
                },
                complete: function () {
                    $('#modal-tambah-barang').modal('open');
                }
            })
        });

        $('.submit-barang').on('click', function (event) {
            event.stopImmediatePropagation();
            modalBarangTable.search('').draw();
            var barang = [];
            modalBarangTable.rows().nodes().to$().find('input[type="checkbox"]:checked').each(function () {
                console.log($(this).closest('tr').attr('value'));
                barang.push($(this).closest('tr').attr('value'));
            });
            $.ajax({
                type: "GET",
                url: "getBarangModal",
                data: {barang: barang, objectArr: getSalesBarangObject()},
                success: function (response) {
                    var object = response['objectArr'];
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    var array = response.barang;
                    console.log(array.length);
                    if (array.length > 0) {
                        for (var i = 0; i < array.length; i++) {
                            var newid = "id-" + (i + 1);
                            var newstock = "stock-" + (i + 1);
                            var newnama = "nama-" + (i + 1);
                            var newqty = "qty-" + (i + 1);
                            var newunit = "unit-" + (i + 1);
                            var newunittype = "unit-type-" + (i + 1);
                            var newpotongan = "potongan-" + (i + 1);
                            var newdetailnote = "detail-note-" + (i + 1);
                            var newfakturtype = "faktur-type-" + (i + 1);
                            var newhargasatuan = "hargasatuan-" + (i + 1);
                            var newharga = "harga-" + (i + 1);
                            var newhargamodal = "harga-modal-" + (i + 1);
                            var newsubtotal = "subtotal-" + (i + 1);
                            $temp = $original.clone();
                            $temp.find('textarea:enabled, input:enabled').val('');
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id', newid);
                            $temp.find('#stock-1').attr('id', newstock);
                            $temp.find('#nama-1').attr('id', newnama);
                            $temp.find('#qty-1').attr('id', newqty);
                            $temp.find('#unit-1').attr('id', newunit);
                            $temp.find('#unit-type-1').attr('id', newunittype);
                            $temp.find('#detail-note-1').attr('id', newdetailnote);
                            $temp.find('#faktur-type-1').attr('id', newfakturtype);
                            $temp.find('#potongan-1').attr('id', newpotongan);
                            $temp.find('#harga-1').attr('id', newharga);
                            $temp.find('#harga-modal-1').attr('id', newhargamodal);
                            $temp.find('#hargasatuan-1').attr('id', newhargasatuan);
                            $temp.find('#subtotal-1').attr('id', newsubtotal);
                            $temp.appendTo('#barang-data')

                            $('#id-' + (i + 1)).val(response.id[i]);
                            $('#nama-' + (i + 1)).val(response.nama[i]).attr('disabled', true).attr('title', response.nama[i] + ' - Stok : ' + response.stock[i]);
                            $('#unit-' + (i + 1)).html('').attr('disabled', true);
                            $('#unit-' + (i + 1)).append("<option value= '" + response.unit[i].unit_id + "' type='main'>" + response.unit[i].unit_code + "</option>");
                            if (response.unitchild[i] != null) {
                              $('#unit-' + (i + 1)).append("<option value= '" + response.unitchild[i].unit_child_id + "' type='child'>" + response.unitchild[i].unit_child_code + "</option>")
                              $('#unit-' + (i + 1)).removeAttr('disabled');
                            }
                            $('#stock-' + (i + 1)).val(response.stock[i]);

                            flag = object.findIndex(function(val) {
                              if(typeof val != 'undefined')
                                return JSON.parse(val).id == response.id[i]
                            });
                            if (flag != -1)
                            {
                              $('#detail-note-' + (i + 1)).val(JSON.parse(object[flag]).note);
                              $('#unit-' + (i + 1)).val(JSON.parse(object[flag]).unit);
                              $('#unit-type-' + (i + 1)).val(JSON.parse(object[flag]).unittype);
                              $('#qty-' + (i + 1)).val(JSON.parse(object[flag]).qty);
                              $('#harga-modal-' + (i + 1)).val(JSON.parse(object[flag]).hargamodal);
                              $('#harga-' + (i + 1)).val(JSON.parse(object[flag]).harga);
                              $('#potongan-' + (i + 1)).val(JSON.parse(object[flag]).potongan);
                              delete object[flag]
                            }else{
                              $('#subtotal-' + (i + 1)).attr('disabled', true);
                              if ($('#pricetype').val() == 1) { //eceran
                                $('#harga-' + (i + 1)).val(response.harga[i]);
                                $('#harga-modal-' + (i + 1)).val(response.harga[i]);
                              } else {
                                $('#harga-' + (i + 1)).val(response.hargapartai[i]);
                                $('#harga-modal-' + (i + 1)).val(response.hargapartai[i]);
                              }
                              $('#unit-' + (i + 1)).trigger('change');
                              // $('#unit-'+(i+1)).val(response.unit[i]).attr('disabled',true);

                              if (response.fakturtype[i] == 1) {
                                $('#faktur-type-' + (i + 1)).val('P').attr('disabled', true);
                              } else {
                                $('#faktur-type-' + (i + 1)).val('-').attr('disabled', true);
                              }
                            }
                            $('#no-item').attr('hidden', true);
                        }
                    } else {
                        $original.clone();
                        $original.attr('hidden', true)
                        $original.appendTo('#barang-data');
                        $('#formSo').find("input[type=text], textarea").val("");
                        $('#no-item').removeAttr('hidden');
                    }
                    $('#grandtotal').val(0);
                    $('#grandtotal-hidden').val(0);
                    $('#modal-tambah-barang').modal('close');
                }
            });
        });

        $('#create-do').click(function (event) {
            event.preventDefault();
            var $original = $('.do-row:first');
            $('.do-row').remove();


            $('.barang-row').each(function (i, v) {
                $temp = $original.clone();
                var newid = "id-product-do-" + (i + 1);
                var newqty = "qty-do-" + (i + 1);
                var newqtypasar = "qty-pasar-do-" + (i + 1);
                var newqtytoko = "qty-toko-do-" + (i + 1);
                var newqtyponci = "qty-ponci-do-" + (i + 1);
                var newqtywtoko = "qty-w-toko-" + (i + 1);
                var newqtywpasar = "qty-w-pasar-" + (i + 1);
                var newqtywponci = "qty-w-ponci-" + (i + 1);
                $temp.find('#id-product-do-1').attr('id', newid);
                $temp.find('#qty-do-1').attr('id', newqty);
                $temp.find('#qty-pasar-do-1').attr('id', newqtypasar);
                $temp.find('#qty-ponci-do-1').attr('id', newqtyponci);
                $temp.find('#qty-toko-do-1').attr('id', newqtytoko);

                $temp.find('.idbarang').val($(this).find('.idproduct').val());
                $temp.find('.nama').val($(this).find('.nama').val());
                $temp.find('.qtydo').val($(this).find('.qty').val());

                $.ajax({
                    method: "GET",
                    url: 'getQtyByProduct',
                    data: { product : $(this).find('.idproduct').val(),
                            unit: $(this).find('.unit').val(),
                            unittype: $(this).find('.unittype').val(),
                    },
                    async: false,
                    success: function(response){
                        $temp.find('#qty-w-toko-1').attr('id', newqtywtoko).html("Qty Toko: "+response[$temp.find('#'+newqtywtoko).attr("warehouse_name")]);
                        $temp.find('#qty-w-pasar-1').attr('id', newqtywpasar).html("Qty Pasar: "+response[$temp.find('#'+newqtywpasar).attr("warehouse_name")]);
                        $temp.find('#qty-w-ponci-1').attr('id', newqtywponci).html("Qty Ponci: "+response[$temp.find('#'+newqtywponci).attr("warehouse_name")]);
                    }
                });

                $temp.appendTo('#append-do-row');
            });
            $('#modal-create-do').modal('open');
        });

        $('#create-antaran').click(function (event) {
            event.preventDefault();
            $('#totalsoantaran').val($('#grandtotal').val());
            $('#totalbayarantaran').trigger('keyup');
            $('#modal-create-antaran').modal('open');
        });

        $('#confirm-create-do').click(function (event) {
            var mode = $(event.currentTarget).attr('mode');
            validqty = []
            $('.do-row').each(function (i, v) {
                qty = $(this).find('.qtydo').val();

                if ($(this).find('.qtydotoko').val() == '') {
                    $(this).find('.qtydotoko').val(0);
                }
                if ($(this).find('.qtydopasar').val() == '') {
                    $(this).find('.qtydopasar').val(0);
                }
                if ($(this).find('.qtydoponci').val() == '') {
                    $(this).find('.qtydoponci').val(0);
                }

                qtydotoko = $(this).find('.qtydotoko').val();
                qtydoponci = $(this).find('.qtydoponci').val();
                qtydopasar = $(this).find('.qtydopasar').val();

                if (qty != Number(qtydotoko) + Number(qtydoponci) + Number(qtydopasar)) {
                    validqty.push('false');
                } else {
                    validqty.push('true');
                }
            });

            if (validqty.indexOf('false') > -1) {
                toastr.warning('Quantity DO harus sesaui dengan Quantity Pemesanan!');
            } else {
                $('#modal-create-do').modal('close');
                if (mode == 'save') {
                    $('#submit-so').trigger('click');
                } else if (mode == 'edit') {
                    $('#edit-so').trigger('click');
                }
            }
        });

        $('#totalbayarantaran').on('keyup', function () {
            bayar = accounting.unformat($(this).val());
            totalso = accounting.unformat($('#totalsoantaran').val());

            $('#sisabayarantaran').val(accounting.formatMoney(totalso - bayar, 'Rp ', 0, ',', '.'));
            $(this).val(accounting.formatMoney($(this).val(), 'Rp ', 0, ',', '.'))
        });

        $('#confirm-create-antaran').click(function (event) {
            if ($('#alamatantaran').val() == '') {
                toastr.warning('Alamat Belum Diisi');
            } else {
                totalbayar = accounting.unformat($('#totalbayarantaran').val());
                $('#totalbayarantaran').val(totalbayar);
                $('#submit-so').trigger('click');
                $('#modal-create-antaran').modal('close');

                if (mode == 'save') {
                    $('#submit-so').trigger('click');
                } else if (mode == 'edit') {
                    $('#edit-so').trigger('click');
                }
            }
        });

        $('#submit-so, #edit-so').click(function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var me = $(this);
            // prevent multiple request at once
            if (me.data('requestRunning')) {
                return;
            }
            //
            mode = $(event.currentTarget).attr('mode');
            var kode = $('#nosalesorder').val();


            if ($(event.currentTarget).attr('mode') == 'save') {
                var url = "createsalesorder";
                var successmessage1 = 'Sales Order ';
                var successmessage2 = ' telah berhasil dibuat!';
            } else {
                var url = "updatesalesorder";
                var successmessage1 = 'Sales Order ';
                var successmessage2 = ' telah berhasil diubah!';
            }

            var validqty = [];
            var validmodal = [];
            var qtynull = [];
            var qtyover = [];
            var qtyoverflag = [];
            $('.barang-row').each(function () {
                validqty.push(ceknumber($(this).find('.qty').val()));
                if ($(this).find('.qty').val() == 0) {
                    qtynull.push(true);
                }

                if (parseInt(accounting.unformat($(this).find('.harga').val())) < parseInt($(this).find('.hargamodal').val())) {
                    var arrayInfo = [];
                    arrayInfo['nama'] = $(this).find('.nama').val();
                    arrayInfo['harga'] = accounting.unformat($(this).find('.harga').val());
                    arrayInfo['hargamodal'] = $(this).find('.hargamodal').val();

                    validmodal.push(arrayInfo);
                }

                if (parseInt($(this).find('.qty').val()) > parseInt($(this).find('.stock').val())) {
                    qtyover['nama'] = $(this).find('.nama').val();
                    qtyover['stock'] = $(this).find('.stock').val();
                    qtyoverflag.push(true);
                } else {
                    qtyoverflag.push(false);
                }
            });

            if ($('.barang-row:first').attr('hidden') == 'hidden') {
                toastr.warning('Anda Belum Memilih Barang!');
            } else if (qtynull.indexOf(true) > -1) {
                toastr.warning('Qty tidak boleh kosong!');
            } else if (validmodal.length > 0) {
                console.log(validmodal);
                toastr.warning('Harga ' + validmodal[0]['nama'] + ' tidak boleh dibawah harga Master ' + validmodal[0]['hargamodal'] + ' !');
            }
            // else if(qtyoverflag.indexOf(true) > -1){
            //     toastr.warning('stock '+qtyover['nama']+' tidak mencukupi!');
            // }
            else if ($('#pelanggan').val() == 0) {
                toastr.warning('Anda Belum Memilih Pelanggan!');
            } else if ($('#sales').val() == 0) {
                toastr.warning('Anda Belum Memilih Sales!');
            } else if (validqty.indexOf(false) > -1) {
                toastr.warning('Quantity harus berupa angka!');
            } else if (
              mode == "save"
              && $('#limitkredit').val() != 0
              && $('#pembayaran').val() == 2
              && -Math.abs($('#limitkredit').val()) > parseInt($('#utang').val())
                - parseInt($('#grandtotal-hidden').val())
              )
            {
              toastr.warning('Utang Pelanggan melebihi Limit!');
            } else {
                me.invisible();
                me.data('requestRunning', true);
                $('.harga, #pricetype, #deliverytype, .hargasatuan, .potongan, #idsalesorder , #nosalesorder, #tglso, .subtotal, #terms, #pembayaran, #notes, .unit, #grandtotal, .detailnote').removeAttr('disabled');
                numberunformat();
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $('#formSo, #formCreateDo, #formCreateAntaran').serialize(),
                    success: function (response) {
                        toastr.success(successmessage1 + response + successmessage2, {
                            "onShow": setTimeout(function () {
                                $('.side-nav .active a').click();
                            }, 2600)
                        });
                    },
                    complete: function () {
                        me.data('requestRunning', false);
                    }
                })
            }
        });

        $('body').on('keyup', '#formSo .harga, #formSo .potongan', function (event) {
            $(this).val(accounting.formatMoney($(this).val(), '', 0, ',', '.'));
            calculatesubtotal();
            calculategrandtotal();
        });

        $('body').on('keyup', '#formSo .qty', function (event) {
            calculatesubtotal();
            calculategrandtotal();
        });

        $('body').on('change', '#pembayaran', function (event) {
            event.stopImmediatePropagation();
            if ($(this).val() == 1) {
                $('#terms').attr('disabled', true);
                $('#terms').val(0);
            } else {
                $('#terms').removeAttr('disabled');
            }
        });

        $('body').on('change', '#deliverytype', function (event) {
            if ($(this).val() == 1) {
                $('#create-do').show();
                $('#create-antaran').hide();
            } else {
                $('#create-antaran').show();
                $('#create-do').hide();
            }
        });

        $('body').on('change', '#formSo .unit', function (event) {
            event.stopImmediatePropagation();
            var product = $(this).closest('tr').find('.idproduct').val();
            var type = $('option:selected', this).attr('type');
            var pricetype = $('#pricetype').val();
            var unit = $(this).val();
            var row = $(this).closest('tr');
            $.ajax({
                type: "GET",
                url: "getPriceByUnit",
                data: {product: product, type: type, pricetype: pricetype, unit: unit},
                success: function (response) {
                    if ($('#pricetype').val() == 1) { //eceran
                        row.find('.harga').val(response.price_sale);
                        row.find('.hargamodal').val(response.price_sale);
                    } else {
                        row.find('.harga').val(response.price_wholesale);
                        row.find('.hargamodal').val(response.price_wholesale);
                    }
                },
                complete:function(){
                    row.find('.harga').trigger('keyup');
                }
            });

            if (type == 'main') {
                row.find('.unittype').val('main');
            } else {
                row.find('.unittype').val('child');
            }
        });

        $('body').on('click','.copy-detail',function(event){
            event.stopImmediatePropagation();
            var $original = $(this).closest('.barang-row');
            var $cloned = $original.clone();
            $(this).closest('.barang-row').after($cloned);

        });

        $('body').on('click', '#SalesTable.highlight tr, #SalesTable.highlight .edit', function (event) {
            event.stopImmediatePropagation();
            var id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            $.ajax({
                type: "GET",
                url: "getsalesorder",
                data: {id: id},
                success: function (response) {
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    if (response.length > 0) {
                        $('#nosalesorder').val(response[0].sales_order_number).attr('disabled', true);
                        $('#idsalesorder').val(response[0].sales_order_id).attr('disabled', true);
                        $('#pelanggan').val(response[0].customer_id).attr('disabled', true);
                        $('#pricetype').val(response[0].price_type).attr('disabled', true);
                        $('#deliverytype').val(response[0].shipping_term_id).attr('disabled', true);
                        $('#terms').val(response[0].term_payment).attr('disabled', true);
                        $('#pembayaran').val(response[0].payment_type_id).attr('disabled', true);
                        $('#tglso').val(response[0].date_sales_order).attr('disabled', true);
                        $('#sales').val(response[0].sales_id).attr('disabled', true);
                        $('#notes').val(response[0].note).attr('disabled', true);
                        if (response[0].is_pkp == 0) {
                            $('#nonpkp').attr('checked', true)
                        } else {
                            $('#pkp').attr('checked', true)
                        }
                        $('#grandtotal').val(accounting.formatMoney(response[0].grand_total_idr, 'Rp. ', 2, ',', '.'));
                        $('#grandtotal-hidden').val(response[0].grand_total_idr);
                        $("input[name=ispkp]").attr('disabled', true);
                        for (var i = 0; i < response.length; i++) {
                            var newid = "id-" + (i + 1);
                            var newnama = "nama-" + (i + 1);
                            var newdetailnote = "detail-note-" + (i + 1);
                            var newfakturtype = "faktur-type-" + (i + 1);
                            var newqty = "qty-" + (i + 1);
                            var newunit = "unit-" + (i + 1);
                            var newharga = "harga-" + (i + 1);
                            var newpotongan = "potongan-" + (i + 1);
                            var newhargasatuan = "hargasatuan-" + (i + 1);
                            var newsubtotal = "subtotal-" + (i + 1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id', newid);
                            $temp.find('#nama-1').attr('id', newnama);
                            $temp.find('#detail-note-1').attr('id', newdetailnote);
                            $temp.find('#faktur-type-1').attr('id', newfakturtype);
                            $temp.find('#qty-1').attr('id', newqty);
                            $temp.find('#unit-1').attr('id', newunit);
                            $temp.find('#harga-1').attr('id', newharga);
                            $temp.find('#potongan-1').attr('id', newpotongan);
                            $temp.find('#hargasatuan-1').attr('id', newhargasatuan);
                            $temp.find('#subtotal-1').attr('id', newsubtotal);
                            $temp.appendTo('#barang-data')
                            $('#id-' + (i + 1)).val(response[i].product_id);
                            $('#subtotal-' + (i + 1)).val(numberformat(response[i].sub_total)).attr('disabled', true);
                            $('#nama-' + (i + 1)).val(response[i].p_name).attr('disabled', true).attr('title', response[i].p_name);
                            $('#detail-note-' + (i + 1)).val(response[i].detail_note).attr('disabled', true);
                            $('#qty-' + (i + 1)).val(response[i].quantity).attr('disabled', true);

                            $.ajax({
                                url: 'getUnitByProduct',
                                data: {product: response[i].product_id},
                                async: false,
                                success: function (r) {
                                    $('#unit-' + (i + 1)).html('').attr('disabled', true);
                                    $('#unit-' + (i + 1)).append("<option value= '" + r.satuan.unit_id + "' type='main'>" + r.satuan.unit_code + "</option>");
                                    if (r.satuan.unit_child) {
                                        $('#unit-' + (i + 1)).append("<option value= '" + r.satuan.unit_child.unit_child_id + "' type='child'>" + r.satuan.unit_child.unit_child_code + "</option>")
                                    }
                                }
                            });

                            if (response[i].unit_type == 'main') {
                                $('#unit-' + (i + 1) + ' option[value="' + response[i].unit_id + '"][type="main"]').attr('selected', 'selected');
                                $('#unit-' + (i + 1)).closest('td').find('.unittype').val('main');
                            } else {
                                $('#unit-' + (i + 1) + ' option[value="' + response[i].unit_id + '"][type="child"]').attr('selected', 'selected');
                                $('#unit-' + (i + 1)).closest('td').find('.unittype').val('child');
                            }

                            $('#harga-' + (i + 1)).val(numberformat(response[i].starting_price)).attr('disabled', true);
                            $('#potongan-' + (i + 1)).val(numberformat(response[i].price_reduction)).attr('disabled', true);
                            $('#hargasatuan-' + (i + 1)).val(numberformat(response[i].price)).attr('disabled', true);
                            $('#no-item').attr('hidden', true);
                            if (response[i].is_faktur == 1) {
                                $('#faktur-type-' + (i + 1)).val('P').attr('disabled', true);
                            } else {
                                $('#faktur-type-' + (i + 1)).val('-').attr('disabled', true);
                            }
                        }
                        $('#tambah-barang').attr('disabled', true).css('cursor', 'not-allowed');
                        $('.selectpicker').selectpicker('refresh');
                    } else {
                        $original.clone();
                        $original.attr('hidden', true)
                        $original.appendTo('#barang-data');
                        $('#formSo').find("input[type=text], textarea").val("");
                    }
                },
                complete: function () {
                    $('#submit-so').attr('hidden', true);
                    $('#create-do').attr('hidden', true);
                    $('#create-antaran').attr('hidden', true);

                    if (mode == "edit") {
                        $('#confirm-create-do').attr('mode', 'edit');
                        $('#confirm-create-antaran').attr('mode', 'edit');
                        $("input[name=ispkp]").removeAttr('disabled');
                        $('.qty, #tglso, #sales, .potongan, #notes, #pelanggan, .harga, .detailnote, .unit').removeAttr('disabled');
                        $('#tambah-barang').removeAttr('disabled').css('cursor', 'pointer');
                        $('.selectpicker').selectpicker('refresh');
                        if ($('#pembayaran') != 1) {
                            $('#terms').removeAttr('disabled');
                        }
                        if ($('#deliverytype').val() == 1) {
                            $('#create-do').removeAttr('hidden');
                        } else {
                            $('#create-antaran').removeAttr('hidden');
                        }
                        $('#cetak-so, #cetak-so-kecil').attr('hidden');
                    } else {
                        $('#cetak-so').removeAttr('hidden').attr('href', "downloadsalesorder/" + id);
                        $('#cetak-so-kecil').removeAttr('hidden').attr('href', "downloadsalesorder/" + id+'/small');
                    }
                }
            });
        })

        $('#clear').on('click', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        $('body').on('click', '#SalesTable tbody .delete-modal', function (event) {
            event.stopImmediatePropagation();
            var noso = $(this).closest('tr').find('.noso').html();
            var idso = $(this).closest('tr').attr('value');
            $('#confirm-delete-so').attr('value', idso).attr('nomor', noso);
            $("#delete-message").html("Yakin ingin menghapus data " + noso + " ?")
            $('#modal-delete-so').modal('open');
        });

        $('#confirm-delete-so').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-so').modal('close');
            var id = $(this).attr('value');
            var noso = $(this).attr('nomor');
            $.ajax({
                type: "POST",
                url: "deletesalesorder",
                data: {id: id},
                success: function (response) {
                    toastr.success('Sales Order ' + noso + ' telah berhasil Dihapus!', {
                        "onShow": setTimeout(function () {
                            $('.side-nav .active a').click();
                        }, 2600)
                    });
                }
            })
        });

        $('#pelanggan').on('change', function (event) {
            event.stopPropagation();
            event.stopImmediatePropagation();
            if ($(this).val() == 2) { //Tanpa Nama
                $('#pembayaran option[value=2]').attr('disabled', true);
            } else {
                $('#pembayaran option[value=2]').removeAttr('disabled');
            }

            $.ajax({
                type: "GET",
                url: "getlimitkredit",
                data: {id: $(this).val()},
                success: function (response) {
                  $("#limitkredit").val(response["limit"])
                  $("#utang").val(response["payable"])
                }
            })
            $('.selectpicker').selectpicker('refresh');
        });

        $('#pricetype').on('change', function (event) {
            event.stopPropagation();
            event.stopImmediatePropagation();
            if ($(this).val() == 0) {
                $('#tambah-barang').attr('disabled', true).css('cursor', 'not-allowed');
            } else {
                $('#tambah-barang').removeAttr('disabled', true).css('cursor', 'pointer');
            }

            $('.selectpicker').selectpicker('refresh');
        });

        $('body').on('click', '.delete-detail', function (event) {
            event.stopImmediatePropagation();
            if ($('.barang-row').length > 1) {
                $(this).closest('tr').remove();
            } else {
                $(this).closest('tr').find('.nama').val("").removeAttr('disabled');
                $(this).closest('tr').find('.id').val("").removeAttr('disabled');
            }
            calculatesubtotal();
            calculategrandtotal();
        });

        $('body').on('click','.modal-info',function (event){
            id = $(this).closest('tr').attr('value');
            getLastProductInfo(id);
            $('#modal-additional-info').modal('open') ;
        });

        //function
        function firstload() {
            $('#modalBarangTable').attr('datatable', false);

            $('#terms').val(0);

            $('.selectpicker').selectpicker('render');

            $('#tambah-barang').attr('disabled', true).css('cursor', 'not-allowed');

            $('#tglso').datepicker({
                dateFormat: 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tglso').val(moment().format('DD-MM-YYYY'));

            $('.number').each(function () {
                $(this).html(accounting.formatMoney($(this).html(), 'Rp. ', 2, ',', '.'));
            });

            var SalesTable = $('#SalesTable').DataTable({
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url: 'getsalestable',
                    data: function (d) {
                        d.noso = $('#filterSoNumber').val();
                        d.customer = $('#filterPelanggan').val();
                    }
                },
                rowId: 'sales_order_id',
                columns: [
                    {data: 'sales_order_number', name: 'sales_order_number', class: 'noso'},
                    {data: 'fullname', name: 'fullname'},
                    {data: 'date_sales_order', name: 'date_sales_order'},
                    {data: 'payment_description', name: 'payment_type_description'},
                    {data: 'note', name: 'note'},
                    {data: 'grand_total_idr', name: 'grand_total_idr'},
                    {data: 'jatuh_tempo', name: 'jatuh_tempo'},
                    {data: 'sales_name', name: 'sales_name'},
                    {data: 'action', name: 'action'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterSoNumber').on('keyup', function () { // This is for news page
                SalesTable.draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                SalesTable.draw();
            });
        }

        function calculategrandtotal() {
            var sum = 0;
            $('.subtotal').each(function () {
                var nominal = accounting.unformat($(this).val());
                if (nominal > 0) {
                    sum += parseFloat(nominal)
                }
            });

            $('#grandtotal-hidden').val(sum);
            $('#grandtotal').val(accounting.formatMoney(sum, 'Rp ', 2, ',', '.'));
            $('#totalsoantaran').val(accounting.formatMoney(sum, 'Rp ', 2, ',', '.'));
        }

        function calculatesubtotal() {
            $('.subtotal').each(function (i, v) {
                var row = $(this).closest('tr');
                var qty = parseFloat(row.find('.qty').val());
                var potongan = accounting.unformat(row.find('.potongan').val());
                var price = accounting.unformat(row.find('.harga').val());
                row.find('.hargasatuan').val(accounting.formatMoney(price - potongan, '', 0, ',', '.'));
                row.find('.subtotal').val(accounting.formatMoney((price - potongan) * qty, '', 0, ',', '.'));
            });
        }

        function numberunformat() {
            $('.harga').each(function () {
                $(this).val(accounting.unformat($(this).val()));
            });

            $('.subtotal').each(function () {
                $(this).val(accounting.unformat($(this).val()));
            });

            $('.potongan').each(function () {
                $(this).val(accounting.unformat($(this).val()));
            });

            $('.hargasatuan').each(function () {
                $(this).val(accounting.unformat($(this).val()));
            });
        }

        function numberformat(val) {
            return accounting.formatMoney(val, '', 0, ',', '.');
        }

        function getLastProductInfo(id) {
            $.ajax({
                method: 'get',
                data: {id: id},
                url: 'getadditionalinfodata',
                success: function (response) {
                    if (response) {
                        var line1 = response.date;
                        var line2 = response.product.product_name;
                        var line3 = response.warehouse.warehouse_name;
                        var line4 = '';
                        var total = 0;

                        $.each(response.owner, function (i, v) {
                            var owner = v.name + '\n';
                            var detail = '';
                            $.each(v.detail, function (j, x) {
                                var ukuran = x.ukuran;
                                var lembar = x.lembar;
                                var temptotal = ukuran * lembar;
                                total = total + temptotal;
                                detail = detail + ukuran + ' Meter X ' + lembar + ' Lembar = ' + temptotal + ' Meter \n';
                            });
                            line4 = line4 + owner + detail;
                        });
                        var text = line1 + '\n' + line2 + '\n' + line3 + '\n' + line4 + '\nTotal = ' + total + ' Meter';
                        $('#prevInfo').val(text);
                    }
                }
            });
        }

        function getSalesBarangObject()
        {
          objectArr = [];
          $('.idproduct').each(function(k,v){
            // assign object
              var id = $(this).val();
              var stock = $(this).closest('tr').find('.stock').val();
              var keterangan = $(this).closest('tr').find('.detailnote').val();
              var fakturtype = $(this).closest('tr').find('.fakturtype').val();
              var qty = $(this).closest('tr').find('.qty').val();
              var unit = $(this).closest('tr').find('.unit').val();
              var unittype = $(this).closest('tr').find('.unittype').val();
              var harga = $(this).closest('tr').find('.harga').val();
              var hargamodal = $(this).closest('tr').find('.hargamodal').val();
              var potongan = $(this).closest('tr').find('.potongan').val();
              var hargasatuan = $(this).closest('tr').find('.hargasatuan').val();
              var subtotal = $(this).closest('tr').find('.subtotal').val();

              var object = {id: id, stock:stock, note: keterangan, fakturtype:fakturtype, qty:qty, unit: unit, unittype: unittype, harga: harga, hargamodal: hargamodal, potongan: potongan, hargasatuan: hargasatuan, subtotal:subtotal}
              objectArr.push(JSON.stringify(object));
          });
          console.log(objectArr);
          return objectArr;
        }
    });
</script>