<script type="text/javascript">

$(document).ready(function(){
	var data= [];
	var date = new Date(), y = date.getFullYear(), m = date.getMonth();
	var firstDay = new Date(y, m, 1);
	var lastDay = new Date(y, m + 1, 0);

	firstDay = moment(firstDay).format('YYYY-MM-DD');
	lastDay = moment(lastDay).format('YYYY-MM-DD'); 
	$.ajax({
		method:"GET",
		url:"grafik_penjualan/"+firstDay+"/"+lastDay,
		success:function(response){
			console.log(response);
			var data = [];
			$.each(response, function(key,value){
				var object = { "label":value.product_name, "y":parseInt(value.product_sales)};
				data.push(object);
			});
			var chart = new CanvasJS.Chart("chartContainer", {
				animationEnabled: true,
				theme: "dark1",
				title:{
					text: "Grafik Penjualan"              
				},
				axisY: {
					title: "Total Penjualan in Rupiah" 
				},
				data: [              
				{
					// Change type to "doughnut", "line", "splineArea", etc.
					type: "column",
					dataPoints: data,
				}
				]
			});
			chart.render();
		}
	})
})

</script>
<div id="chartContainer" style="height: 300px; width: 100%;"></div>
