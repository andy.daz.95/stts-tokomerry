<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/toastr.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Invoice</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="input-field col l3">
                                    <label>Filter Sales Invoice No.</label>
                                    <input id="filterInvoiceNumber" type="text" class="f-input" placeholder="Filter Sales Invoice">
                                </div>
                                <div class="input-field col l4">
                                    <label>Filter Pelanggan</label>
                                    <input id="filterPelanggan" type="text" class="f-input" placeholder="Filter Pelanggan">
                                </div>
                                <div class="col l12 m12 s12">
                                    <table id="salesInvoiceTable" class="highlight table table-bordered display nowrap dataTable dtr-  inline">
                                        <thead>
                                        <tr>
                                            <th>No. Invoice</th>
                                            <th>SO. Number</th>
                                            <th>Tanggal SO</th>
                                            <th>pelanggan</th>
                                            <th>Total SO</th>
                                            <th>Nilai Invoice</th>
                                            <th>Tindakan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{--@foreach($invoice as $key => $value)--}}
                                        {{--<tr mode="view" value="{{$value->invoice_sales_id}}">--}}
                                        {{--<td>{{$value->invoice_sales_number}}</td>--}}
                                        {{--<td class="noinvoice">{{$value->sales_order_number}}</td>--}}
                                        {{--<td>{{$value->date_sales_order}}</td>--}}
                                        {{--<td>{{$value->first_name.' '.$value->last_name}}</td>--}}
                                        {{--<td class="number">{{$value->grand_total_idr}}</td>--}}
                                        {{--<td class="number">{{$value->total_amount}}</td>--}}
                                        {{--<td>--}}
                                        {{-- <a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->invoice_sales_id}}"><i class="material-icons">edit</i></a> --}}
                                        {{--@if(Session('roles')->name == 'master')--}}
                                        {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                                        {{--@endif--}}
                                        {{--</td>--}}
                                        {{--</tr>--}}
                                        {{--@endforeach--}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Invoice</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <form id="formInvoice">
                                    <div class="input-field col l4 m2 s12">
                                        <input id="noinvoice" name="noinvoice" type="text" class="f-input" placeholder="No. Invoice">
                                        <input id="idinvoice" name="idinvoice" type="text" class="f-input" placeholder="No. Invoice" hidden>
                                        <label>No Invoice</label>
                                    </div>
                                    <div class="input-field col l4 m2 s12">
                                        <select id="noso" name="noso" class="selectpicker browser-default" name="noso" data-live-search="true" data-size="5"  >
                                            <option value='0'>Choose So</option>
                                            @foreach($so as $key => $value)
                                                <option value='{{$value->sales_order_id}}'>{{$value->sales_order_number.' - '.$value->customer->first_name.' '.$value->customer->last_name}}</option>
                                            @endforeach
                                        </select>
                                        <label>No So</label>
                                    </div>
                                    <div class="input-field col l4 m2 s12">
                                        <input id="idcustomer" name="idcustomer" type="text" class="f-input" hidden>
                                        <input id="customer" type="text" class="f-input" placeholder="Pelanggan" disabled>
                                        <label>Pelanggan</label>
                                    </div>
                                    <div class="input-field col l4 m2 s12">
                                        <input id="paymenttype" type="text" name="paymenttype" class="f-input" placeholder="Payment Type" disabled>
                                        <label>Tipe Pembayaran</label>
                                    </div>
                                    <div class="input-field col l4 m2 s12">
                                        <input id="tglso" type="text" name="tglso" class="f-input" placeholder="Tanggal SO" disabled>
                                        <label>Tanggal SO</label>
                                    </div>
                                    <div class="col l12 m12 s12">
                                        <table class="table table-bordered display nowrap dataTable dtr-inline">
                                            <thead>
                                            <tr>
                                                <th>No So</th>
                                                <th>Total SO</th>
                                                <th>Nilai Retur</th>
                                                <th>Potongan Invoice Sebelumnya</th>
                                                <th>Total</th>
                                                <th>Nilai Invoice</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                                            <tr id="invoice-row" hidden>
                                                <td>
                                                    <input id="tablenoso" type="text" name="tablenoso" class="f-input" disabled="disabled">
                                                </td>
                                                <td>
                                                    <input id="total" type="text" name="ttoal" class="f-input" disabled="disabled">
                                                </td>
                                                <td>
                                                    <input id="returreduction" type="text" name="returreduction" class="f-input" disabled="disabled">
                                                </td>
                                                <td>
                                                    <input id="prev" type="text" class="f-input" disabled="disabled">
                                                    <input id="prev-hidden" type="text" name="prev" class="f-input" hidden>
                                                </td>
                                                <td>
                                                    <input id="remaining" type="text" name="remaining" class="f-input" disabled="disabled">
                                                </td>
                                                <td>
                                                    <input id="payment" type="text" class="f-input" disabled>
                                                    <input id="payment-hidden" type="text" name="payment" class="f-input" hidden>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row">
                                        <div class="col l5 m5 s12 input-field">
                                            <a id="submit-invoice" href="#" class="btn-stoko light-blue darken-2" mode="save">Submit</a>
                                            <a id="edit-invoice" href="#" class="btn-stoko light-blue darken-2" mode="edit" hidden>Edit</a>
                                            <!-- <a href="#" class="btn btn-raised grey lighten-4 grey-text text-darken-4"><i class="material-icons">delete</i></a> -->
                                            <a id="clear" href="" class="btn-stoko btn-stoko-primary orange">Clear</a>
                                            <a id="cetak-invoice" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Invoice</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-invoice" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-invoice" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });
        firstload();

        $('#noso').on('change',function(){
            var id = $(this).val()
            $.ajax({
                type:"Get",
                url:"getsoforinvoice",
                data:{id:id},
                success:function(response){
                    if(response.length == 0){
                        $('#no-item').removeAttr('hidden');
                        $('#invoice-row').attr('hidden',true);
                    }else{
                        $remaining = response.grand_total_idr - response.reduction - response.total - response.next_invoice_reduction;
                        $('#tablenoso').val(response.sales_order_number);
                        $('#idcustomer').val(response.customer_id);
                        $('#customer').val(response.first_name+' '+response.last_name);
                        $('#tglso').val(response.date_sales_order);
                        $('#returreduction').val(accounting.formatMoney(response.reduction,'Rp. ',2,',','.'));
                        $('#prev').val(accounting.formatMoney(response.next_invoice_reduction,'Rp. ',2,',','.'));
                        $('#prev-hidden').val(response.next_invoice_reduction);
                        $('#paymenttype').val(response.payment_description);
                        $('#total').val(accounting.formatMoney(response.grand_total_idr,'Rp. ',2,',','.'));
                        $('#remaining').val(accounting.formatMoney($remaining, 'Rp. ',2,',','.'));
                        $('#payment').val(accounting.formatMoney($remaining, 'Rp. ',2,',','.'));
                        $('#payment-hidden').val($remaining);
                        $('#no-item').attr('hidden',true);
                        $('#invoice-row').removeAttr('hidden');
                    }
                }
            });
        });

        $('#submit-invoice, #edit-invoice').on('click',function(event){
            event.stopImmediatePropagation();
            var kode= $('#noinvoice').val();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "createsalesinvoice";
                var successmessage = 'Sales Invocie '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatesalesinvoice";
                var successmessage = 'Sales Invoice '+kode+' telah berhasil diubah!';
            }

            if($('#noso').val() == 0){
                toastr.warning('Anda Belum Memilih SO');
            }else{
                me.invisible();
                me.data('requestRunning', true);
                var id = $('#noso').val();
                $('#customer, #idinvoice, #idcustomer').removeAttr('disabled');
                $.ajax({
                    type:"post",
                    url:url,
                    data: $('#formInvoice').serialize()+"&id="+id+"&number="+kode,
                    success:function(response){
                        toastr.success(successmessage,{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                });
            }
        });

        $('body').on('click','#salesInvoiceTable tr, #salesInvoiceTable .edit', function(event){
            event.stopImmediatePropagation();
            var mode = $(event.currentTarget).attr('mode');
            var id = $(this).attr('value');
            $.ajax({
                type:"Get",
                url:"getsalesinvoice",
                data:{id:id},
                success:function(response){
                    $('#noinvoice').val(response.invoice_sales_number).attr('disabled',true);
                    $('#idinvoice').val(response.invoice_sales_id).attr('disabled',true);
                    $('#noso').append('<option class="remove-when-clear" value="'+response.sales_order_id+'" selected="selected">'+response.sales_order_number+'</option>').attr('disabled',true);
                    $('#tablenoso').val(response.sales_order_number);
                    $('#idcustomer').val(response.customer_id);
                    $('#customer').val(response.first_name+' '+response.last_name);
                    $('#tglso').val(response.date_sales_order);
                    $('#paymenttype').val(response.payment_description);
                    $('#total').val(accounting.formatMoney(response.grand_total_idr,'Rp. ',2,',','.'));
                    $('#prev').val(accounting.formatMoney(response.prev_invoice_reduction,'Rp. ',2,',','.'));
                    $('#returreduction').val(accounting.formatMoney(response.reduction,'Rp. ',2,',','.'));
                    $('#remaining').val(accounting.formatMoney((response.grand_total_idr - response.reduction - response.prev_invoice_reduction),'Rp. ',2,',','.'));
                    $('#payment').val(accounting.formatMoney((response.total_amount),'Rp. ',2,',','.')).attr('disabled',true);
                    $('#payment-hidden').val(response.total_amount).attr('disabled',true);
                    $('#no-item').attr('hidden',true);
                    $('#invoice-row').removeAttr('hidden');
                    $('.selectpicker').selectpicker('refresh');
                },complete:function(){
                    if(mode=="edit")
                    {
                        $('#edit-invoice').removeAttr('hidden');
                        $('#submit-invoice').attr('hidden',true);
                        $('#payment').removeAttr('disabled').trigger('change');
                    }else{
                        $('#cetak-invoice').removeAttr('hidden').attr('href',"downloadsalesinvoice/"+id);
                    }
                }
            });
        });

        $('#payment').on('focusout', function(){
            $('#payment-hidden').val($(this).val());
            $(this).val(accounting.formatMoney($(this).val(),'Rp. ',2,',','.'))
        });
        $('#payment').on('focusin', function(){
            $(this).val("");
        })

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        $('body').on('click','#salesInvoiceTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var noinvoice = $(this).closest('tr').find('.noinvoice').html();
            var idinvoice = $(this).closest('tr').attr('value');
            $('#confirm-delete-invoice').attr('value',idinvoice).attr('nomor',noinvoice);
            $("#delete-message").html("Yakin ingin menghapus data "+noinvoice+" ?")
            $('#modal-delete-invoice').modal('open');
        });

        $('#confirm-delete-invoice').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-invoice').modal('close');
            var id = $(this).attr('value');
            var noinvoice = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletesalesinvoice",
                data:{id:id},
                success:function(response){
                    toastr.success('Penerimaan Barang '+noinvoice+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        //function
        function firstload()
        {
            $('#tglso').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $.ajax({
                type:"GET",
                url:"lastsalesinvoicenumber",
                success:function(response){
                    $('#noinvoice').val(response);
                }
            })

            $('.selectpicker').selectpicker('render');

            $('.number').each(function(){
                $(this).html(accounting.formatMoney($(this).html(),'',2,',','.'));
            });

            var salesInvoiceTable = $('#salesInvoiceTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getsalesinvoicetable',
                    data : function (d){
                        d.noinvoice = $('#filterInvoiceNumber').val();
                        d.customer = $('#filterPelanggan').val();
                    }
                },
                rowId : 'invoice_sales_id',
                columns: [
                    { data: 'invoice_sales_number', name: 'invoice_sales_number', class:'noinvoice'},
                    { data: 'sales_order_number', name: 'sales_order_number' },
                    { data: 'date_sales_order', name: 'date_sales_order' },
                    { data: 'customer_name', name: 'customer_name' },
                    { data: 'grand_total_idr', name: 'grand_total_idr' },
                    { data: 'total_amount', name: 'total_amount' },
                    { data: 'action', name: 'action' },
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterInvoiceNumber').on('keyup', function () { // This is for news page
                salesInvoiceTable.draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                salesInvoiceTable.draw();
            });
        }
    });
</script>
