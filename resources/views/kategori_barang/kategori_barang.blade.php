<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l4 m4 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Kategori Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formKategori">
                <div class="row margin-top">
                  <div class="col l12">
                    <div class="input-field col l12 m12 s12">
                      <input id="id" type="text" class="f-input" name="id" hidden>
                      <input id="kategori" name="kategori" type="text" class="f-input">
                      <label>Kategori Barang</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col l12 m12 s12">
                    <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                    <a href="#" class="btn btn-sm btn-raised grey lighten-4 grey-text text-darken-4 reset">cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l8 m8 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Kategori</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l6">
                  <input id="filterKategori" type="text" class="f-input">
                  <label>Filter Kategori</label>
                </div>
                <div class="col l12 m12 s12">
                  <table id="kategoriTable" class="table table-bordered display nowrap dataTable dtr-inline">
                    <thead>
                      <tr>
                        <th>Kategori</th>
                        <th>Tindakan</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($kategori as $key => $value)
                      <tr value="{{$value->product_category_id}}">
                        <td>{{$value->description}}</td>
                        <td>
                         <a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
                         <a href="#delete_data" class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                       </td>
                     </tr>
                     @endforeach
                   </tbody>
                 </table>
                 <ul class="pagination">
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
</div>
<!-- Modal Delete Satuan -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="row">
      <div class="col l12">
        <table id="modal-delete-data">

        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text delete">Hapus</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    kategoriTable = $('#kategoriTable').DataTable({ // This is for home page
      searching: true,
      responsive: true,
      'sDom': 'ti',
      'pagingType': 'numbers',
      language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
    });

    $('#filterKategori').on('keyup', function () { // This is for news page
      kategoriTable.search($(this).val()).draw();
    });

    $('#formKategori').submit(function(event){
      event.preventDefault();
      var mode = $(this).find('#btn-submit').attr('mode');
      var empty = required(['kategori']);
      if(empty != '0')
      {
        toastr.warning(empty+' tidak boleh kosong!');
      }else{
        if (mode == 'save')
        {
          var id = $(this).find('#id').val();
          $.ajax({
            type:"POST",
            url:"createKategoriBarang",
            data: $("#formKategori").serialize(),
            success:function(response){
              toastr.success('Kategori Barang Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
            }
          });
        }else if(mode == 'edit'){
          var id = $(this).find('#id').val();
          $.ajax({
            type:"POST",
            url:"updateKategoriBarang",
            data: $("#formKategori").serialize()+"&id="+id,
            success:function(response){
              toastr.success('Kategori Barang Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
            }
          });
        }
      }
    });

    $('body').on('click','#formKategori .reset', function(event){
      event.stopImmediatePropagation();
      $(this).closest('#formKategori').find("input[type=text], textarea").val("");
      $('#btn-submit').attr('mode', 'save');
    });

    $('body').on('click','#kategoriTable .edit', function(event){
      event.stopImmediatePropagation();
      var id = $(this).closest('tr').attr('value');
      $.ajax({
        type:"GET",
        url:"getKategoriData",
        data: {id:id},
        success:function(response){
          $('#id').val(response.product_category_id);
          $('#kategori').val(response.description);
        }
      });
      $('#btn-submit').attr('mode', 'edit');
    });

    $('body').on('click',"#kategoriTable .delete-modal",function(event){
      event.preventDefault();
      var id = $(this).closest('tr').attr('value');

      $.ajax({
        type:"GET",
        url:"modalKategoriData",
        async:false,
        data:{
          id:id,
        },
        success:function(response){
          $('#modal-delete-data').html(response);
        }
      });
      $('#delete_data').modal('open');
    });

    $(".delete").on('click',function(event){
      var id = $(this).closest('.modal').find('#id-delete').attr('value');
      console.log(id);
      $.ajax({
        type:"Get",
        url:"deleteKategoriBarang",
        data:{id:id},
        success:function(response){
          toastr.success('Berhasil Menghapus Kategori Barang!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
        }
      });
    });
  })
</script>
