@if(Session('roles')->name == 'master')
<ul id="slide-out" class="side-nav fixed">
  <li>
    <div class="userView">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#!user">
        <img class="circle" @if(Session('user')->profile_picture) src="{{'uploads/avatars/'.Session('user')->profile_picture}}" @else src="uploads/avatars/profile.jpg" @endif>
      </a>
      <a><h6 class="white-text">{{Session('user')->full_name}} - {{Session('roles')->name}}</h6></a>
      <a href="#!email" data-activates="email-drop" class="dropdown-button"><span class="white-text email">{{Session('user')->email}}<i class="right material-icons">arrow_drop_down</i></span></a>
    </div>
    <ul id="email-drop" class="dropdown-content">
      <li><a href="logout">Logout</a></li>
    </ul>
  </li>
  <li><a class="subheader">List Aplikasi</a></li>
  <li class="nav-item active"><a url="homes"><i class="material-icons">home</i><p>Halaman Utama</p></a></li>
  <li><a href="user_profile"><i class="material-icons">person_pin</i><p>Profil Pengguna</p></a></li>
  <li><a href="change_password"><i class="material-icons">person_pin</i><p>Ubah Password</p></a></li>
  <li><a href="manage_user"><i class="material-icons">accessibility</i><p>Atur Pengguna</p></a></li>
  <li><a href="logout" class="nav-item"><i class="material-icons">exit_to_app</i><p>Logout / Keluar</p></a></li>
  <li class="divider"></li>
  <li class="nav-item"><a class="subheader">Master</a></li>
  <li class="nav-item"><a url="pelanggan" ><i class="material-icons">people</i><p>Pelanggan</p></a></li>
  <li class="nav-item"><a url="supplier" ><i class="material-icons">add_shopping_cart</i><p>Pemasok</p></a></li>
  <li class="nav-item"><a url="kategori_barang"><i class="material-icons">donut_large</i><p>Kategori Barang</p></a></li>
  <li class="nav-item"><a url="barang"><i class="material-icons">layers</i><p>Barang</p></a></li>
  <li class="nav-item"><a url="spandek"><i class="material-icons">layers</i><p>Spandek</p></a></li>
  <li class="nav-item"><a url="satuan"><i class="material-icons">view_module</i><p>Satuan</p></a></li>
  <li class="nav-item"><a url="anaksatuan"><i class="material-icons">view_module</i><p>Anak Satuan</p></a></li>
  <li class="nav-item"><a url="news"><i class="material-icons">trending_up</i><p>Pengaturan Berita</p></a></li>
  <li class="nav-item"><a url="bank"><i class="material-icons">attach_money</i><p>Bank</p></a></li>
  <li class="nav-item"><a url="gudang"><i class="material-icons">store</i><p>Gudang</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Pembelian</a></li>
  {{--<li class="nav-item"><a url="unittest/truncatepurchase"><i class="material-icons">track_changes</i><p>Hapus Data PO</p></a></li>--}}
  <li class="nav-item"><a url="purchaseorder"><i class="material-icons">attach_money</i><p>Pembelian / PO</p></a></li>
  <li class="nav-item"><a url="penerimaanbarang"><i class="material-icons">arrow_back</i><p>Penerimaan Barang</p></a></li>
  <li class="nav-item"><a url="purchaseinvoice"><i class="material-icons">assignment</i><p>Invoice Pembelian</p></a></li>
  <li class="nav-item"><a url="discountadjust"><i class="material-icons">assignment</i><p>Penyesuaian Diskon</p></a></li>
  <li class="nav-item"><a url="purchasepayment"><i class="material-icons">assignment</i><p>Pembayaran Pembelian</p></a></li>
  <li class="nav-item"><a url="purchasereturn"><i class="material-icons">arrow_back</i><p>Retur Pembelian</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Penjualan</a></li>
  {{--<li class="nav-item"><a url="unittest/truncatesales"><i class="material-icons">track_changes</i><p>Hapus Data SO</p></a></li>--}}
  <li class="nav-item"><a url="salesorder"><i class="material-icons">account_box</i><p>Penjualan / SO</p></a></li>
  <li class="nav-item"><a url="draftdelivery"><i class="material-icons">account_box</i><p>Delivery Order</p></a></li>
  <li class="nav-item"><a url="deliveryorder"><i class="material-icons">account_box</i><p>Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="historydo"><i class="material-icons">account_box</i><p>Histori Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="salesinvoice"><i class="material-icons">account_box</i><p>Invoice Penjualan</p></a></li>
  <li class="nav-item"><a url="salespayment"><i class="material-icons">account_box</i><p>Pembayaran Penjualan</p></a></li>
  <li class="nav-item"><a url="salesreturn"><i class="material-icons">arrow_back</i><p>Retur Penjualan</p></a></li>
  <li class="nav-item"><a url="salesreturnreceive"><i class="material-icons">arrow_back</i><p>Penerimaan Barang Retur Penjualan</p></a></li>
  <li class="nav-item"><a url="refundhistory"><i class="material-icons">arrow_back</i><p>Histori Refund Penjualan</p></a></li>
  <li class="nav-item"><a url="existingreceiveable"><i class="material-icons">attach_money</i><p>Pembayaran Utang Diluar Sistem</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Stok</a></li>
  <li class="nav-item"><a url="stockoverview"><i class="material-icons">import_contacts</i><p>Peninjauan Stok</p></a></li>
  <li class="nav-item"><a url="additionalinfo"><i class="material-icons">import_contacts</i><p>Informasi Spandek</p></a></li>
  <li class="nav-item"><a url="stocktransfer/parangloe"><i class="material-icons">local_shipping</i><p>Pemindahan Stok Parangloe</p></a></li>
  <li class="nav-item"><a url="stocktransfer"><i class="material-icons">local_shipping</i><p>Pemindahan Stok</p></a></li>
  <li class="nav-item"><a url="stockcorrection"><i class="material-icons">import_contacts</i><p>Koreksi Stok</p></a></li>
  <li class="nav-item"><a url="kartustock"><i class="material-icons">credit_card</i><p>Kartu Stok</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Laporan</a></li>
  <li class="nav-item"><a url="reportpo"><i class="material-icons">book</i><p>Laporan PO</p></a></li>
  <li class="nav-item"><a url="reportpobyproduct"><i class="material-icons">book</i><p>Laporan PO Per Barang</p></a></li>
  <li class="nav-item"><a url="reportpaymentpo"><i class="material-icons">book</i><p>Laporan Pembayaran PO</p></a></li>
  <li class="nav-item"><a url="reportunsettledpo"><i class="material-icons">book</i><p>Laporan Utang PO</p></a></li>
  <li class="nav-item"><a url="reportso"><i class="material-icons">book</i><p>Laporan SO</p></a></li>
  <li class="nav-item"><a url="reportpaymentso"><i class="material-icons">book</i><p>Laporan Pembayaran SO</p></a></li>
  <li class="nav-item"><a url="reportunsettledso"><i class="material-icons">book</i><p>Laporan Utang SO</p></a></li>
  <li class="nav-item"><a url="reportdo"><i class="material-icons">book</i><p>Laporan Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="reportdoremaining"><i class="material-icons">book</i><p>Laporan Sisa Pengeluaran</p></a></li>
  <li class="nav-item"><a url="reportdodaily"><i class="material-icons">book</i><p>Laporan Pengeluaran Barang Harian</p></a></li>
  <li class="nav-item"><a url="reportfakturpo"><i class="material-icons">book</i><p>Laporan Faktur PO</p></a></li>
  <li class="nav-item"><a url="reportpaymentfakturpo"><i class="material-icons">book</i><p>Laporan Pembayaran Faktur PO</p></a></li>
  <li class="nav-item"><a url="reportfaktur"><i class="material-icons">book</i><p>Laporan Faktur SO</p></a></li>
  <li class="nav-item"><a url="reportincome"><i class="material-icons">timeline</i><p>Laporan Pemasukkan</p></a></li>
  <li class="nav-item"><a url="reportexpense"><i class="material-icons">timeline</i><p>Laporan Expense</p></a></li>
  <li class="nav-item"><a url="reportnetsales"><i class="material-icons">timeline</i><p>Pendapatan Bersih</p></a></li>
  <li class="nav-item"><a url="reportprofitloss"><i class="material-icons">timeline</i><p>Laba Rugi</p></a></li>
  <li class="nav-item"><a url="reportremainingparangloe"><i class="material-icons">timeline</i><p>Sisa Parangloe</p></a></li>
  <li class="nav-item"><a url="grafik_penjualan"><i class="material-icons">timeline</i><p>Grafik Penjualan Barang</p></a></li>
  <li class="nav-item"><a url="reportcustomerpurchase"><i class="material-icons">timeline</i><p>Laporan Pembelian Customer</p></a></li>
  <li class="nav-item"><a url="reportstockmovement"><i class="material-icons">book</i><p>Report Stock Transfer</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Lain Lain</a></li>
  <li class="nav-item"><a url="log"><i class="material-icons">short_text</i><p>Log</p></a></li>
  <li class="nav-item"><a url="expense"><i class="material-icons">short_text</i><p>Expense</p></a></li>
  <li class="nav-item"><a url="balance"><i class="material-icons">account_balance</i><p>Saldo Pelanggan</p></a></li>
  <li class="nav-item"><a url="target"><i class="material-icons">track_changes</i><p>Atur Target Pembelian</p></a></li>
  <br>
  <br>
  <br>
</ul>
@endif
@if(Session('roles')->name == 'Gudang Makassar')
<ul id="slide-out" class="side-nav fixed">
  <li>
    <div class="userView">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#!user">
        <img class="circle" @if(Session('user')->profile_picture) src="{{'uploads/avatars/'.Session('user')->profile_picture}}" @else src="uploads/avatars/profile.jpg" @endif>
      </a>
      <a><h6 class="white-text">{{Session('user')->full_name}} - {{Session('roles')->name}}</h6></a>
      <a href="#!email" data-activates="email-drop" class="dropdown-button"><span class="white-text email">{{Session('user')->email}}<i class="right material-icons">arrow_drop_down</i></span></a>
    </div>
    <ul id="email-drop" class="dropdown-content">
      <li><a href="logout">Logout</a></li>
    </ul>
  </li>
  <li><a class="subheader">List Aplikasi</a></li>
  <li class="nav-item active"><a url="homes"><i class="material-icons">home</i>Halaman Utama</a></li>
  <li><a href="user_profile"><i class="material-icons">person_pin</i>Profil Pengguna</a></li>
  <li><a href="change_password"><i class="material-icons">person_pin</i>Ubah Password</a></li>
  <li><a href="logout" class="nav-item"><i class="material-icons">exit_to_app</i>Logout / Keluar</a></li>
  <li class="divider"></li>
  <li class="nav-item"><a class="subheader">Master</a></li>
  <li class="nav-item"><a url="supplier" ><i class="material-icons">add_shopping_cart</i><p>Pemasok</p></a></li>
  <li class="nav-item"><a url="barang"><i class="material-icons">layers</i>Barang</a></li>
  <li class="nav-item"><a url="spandek"><i class="material-icons">layers</i><p>Spandek</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Pembelian</a></li>
  <li class="nav-item"><a url="purchaseorder"><i class="material-icons">attach_money</i><p>Pembelian / PO</p></a></li>
  <li class="nav-item"><a url="penerimaanbarang"><i class="material-icons">arrow_back</i><p>Penerimaan Barang</p></a></li>
  <li class="nav-item"><a url="purchasereturn"><i class="material-icons">arrow_back</i><p>Retur Pembelian</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Stok</a></li>
  <li class="nav-item"><a url="stockoverview"><i class="material-icons">import_contacts</i>Peninjauan Stok</a></li>
  <li class="nav-item"><a url="stocktransfer"><i class="material-icons">local_shipping</i>Pemindahan Stok</a></li>
  <li class="nav-item"><a url="kartustock"><i class="material-icons">credit_card</i>Kartu Stok</a></li>
  <li class="divider"></li>
  <li><a class="subheader">Laporan</a></li>
  <li class="nav-item"><a url="reportdo"><i class="material-icons">book</i><p>Laporan Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="reportdodaily"><i class="material-icons">book</i><p>Laporan Pengeluaran Barang Harian</p></a></li>
  <br>
  <br>
  <br>
</ul>
@endif
@if(Session('roles')->name == 'Gudang Besar')
<ul id="slide-out" class="side-nav fixed">
  <li>
    <div class="userView">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#!user">
        <img class="circle" @if(Session('user')->profile_picture) src="{{'uploads/avatars/'.Session('user')->profile_picture}}" @else src="uploads/avatars/profile.jpg" @endif>
      </a>
      <a><h6 class="white-text">{{Session('user')->full_name}} - {{Session('roles')->name}}</h6></a>
      <a href="#!email" data-activates="email-drop" class="dropdown-button"><span class="white-text email">{{Session('user')->email}}<i class="right material-icons">arrow_drop_down</i></span></a>
    </div>
    <ul id="email-drop" class="dropdown-content">
      <li><a href="logout">Logout</a></li>
    </ul>
  </li>
  <li><a class="subheader">List Aplikasi</a></li>
  <li class="nav-item active"><a url="homes"><i class="material-icons">home</i>Halaman Utama</a></li>
  <li><a href="user_profile"><i class="material-icons">person_pin</i>Profil Pengguna</a></li>
  <li><a href="change_password"><i class="material-icons">person_pin</i>Ubah Password</a></li>
  <li><a href="logout" class="nav-item"><i class="material-icons">exit_to_app</i>Logout / Keluar</a></li>
  <li><a class="subheader">Penjualan</a></li>
  <li class="nav-item"><a url="deliveryorder"><i class="material-icons">account_box</i>Pengeluaran Barang</a></li>
  <li class="nav-item"><a url="historydo"><i class="material-icons">account_box</i><p>Histori Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="salesreturn"><i class="material-icons">arrow_back</i>Retur Penjualan</a></li>
  <li class="nav-item"><a url="salesreturnreceive"><i class="material-icons">arrow_back</i><p>Penerimaan Barang Retur Penjualan</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Stok</a></li>
  <li class="nav-item"><a url="stockoverview"><i class="material-icons">import_contacts</i>Peninjauan Stok</a></li>
  <li class="nav-item"><a url="additionalinfo"><i class="material-icons">import_contacts</i><p>Informasi Spandek</p></a></li>
  <li class="nav-item"><a url="stocktransfer"><i class="material-icons">local_shipping</i>Pemindahan Stok</a></li>
  <li class="nav-item"><a url="kartustock"><i class="material-icons">credit_card</i>Kartu Stok</a></li>
  <li class="divider"></li>
  <li><a class="subheader">Laporan</a></li>
  <li class="nav-item"><a url="reportdo"><i class="material-icons">book</i><p>Laporan Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="reportdodaily"><i class="material-icons">book</i><p>Laporan Pengeluaran Barang Harian</p></a></li>
  <br>
  <br>
  <br>
</ul>
@endif
@if(Session('roles')->name == 'Gudang Kecil')
<ul id="slide-out" class="side-nav fixed">
  <li>
    <div class="userView">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#!user">
        <img class="circle" @if(Session('user')->profile_picture) src="{{'uploads/avatars/'.Session('user')->profile_picture}}" @else src="uploads/avatars/profile.jpg" @endif>
      </a>
      <a><h6 class="white-text">{{Session('user')->full_name}} - {{Session('roles')->name}}</h6></a>
      <a href="#!email" data-activates="email-drop" class="dropdown-button"><span class="white-text email">{{Session('user')->email}}<i class="right material-icons">arrow_drop_down</i></span></a>
    </div>
    <ul id="email-drop" class="dropdown-content">
      <li><a href="logout">Logout</a></li>
    </ul>
  </li>
  <li><a class="subheader">List Aplikasi</a></li>
  <li class="nav-item active"><a url="homes"><i class="material-icons">home</i>Halaman Utama</a></li>
  <li><a href="user_profile"><i class="material-icons">person_pin</i>Profil Pengguna</a></li>
  <li><a href="change_password"><i class="material-icons">person_pin</i>Ubah Password</a></li>
  <li><a href="logout" class="nav-item"><i class="material-icons">exit_to_app</i>Logout / Keluar</a></li>
  <li class="divider"></li>
  <li><a class="subheader">Penjualan</a></li>
  <li class="nav-item"><a url="deliveryorder"><i class="material-icons">account_box</i>Pengeluaran Barang</a></li>
  <li class="nav-item"><a url="historydo"><i class="material-icons">account_box</i><p>Histori Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="salesreturn"><i class="material-icons">arrow_back</i>Retur Penjualan</a></li>
  <li class="nav-item"><a url="salesreturnreceive"><i class="material-icons">arrow_back</i><p>Penerimaan Barang Retur Penjualan</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Stok</a></li>
  <li class="nav-item"><a url="stockoverview"><i class="material-icons">import_contacts</i>Peninjauan Stok</a></li>
  <li class="nav-item"><a url="stocktransfer"><i class="material-icons">local_shipping</i>Pemindahan Stok</a></li>
  <li class="nav-item"><a url="kartustock"><i class="material-icons">credit_card</i>Kartu Stok</a></li>
  <li class="divider"></li>
  <li><a class="subheader">Laporan</a></li>
  <li class="nav-item"><a url="reportdo"><i class="material-icons">book</i><p>Laporan Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="reportdodaily"><i class="material-icons">book</i><p>Laporan Pengeluaran Barang Harian</p></a></li>
  <br>
  <br>
  <br>
</ul>
@endif
@if(Session('roles')->name == 'Komputer 123')
<ul id="slide-out" class="side-nav fixed">
  <li>
    <div class="userView">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#!user">
        <img class="circle" @if(Session('user')->profile_picture) src="{{'uploads/avatars/'.Session('user')->profile_picture}}" @else src="uploads/avatars/profile.jpg" @endif>
      </a>
      <a><h6 class="white-text">{{Session('user')->full_name}} - {{Session('roles')->name}}</h6></a>
      <a href="#!email" data-activates="email-drop" class="dropdown-button"><span class="white-text email">{{Session('user')->email}}<i class="right material-icons">arrow_drop_down</i></span></a>
    </div>
    <ul id="email-drop" class="dropdown-content">
      <li><a href="logout">Logout</a></li>
    </ul>
  </li>
  <li><a class="subheader">List Aplikasi</a></li>
  <li class="nav-item active"><a url="homes"><i class="material-icons">home</i>Halaman Utama</a></li>
  <li><a href="user_profile"><i class="material-icons">person_pin</i>Profil Pengguna</a></li>
  <li><a href="change_password"><i class="material-icons">person_pin</i>Ubah Password</a></li>
  <li><a href="logout" class="nav-item"><i class="material-icons">exit_to_app</i>Logout / Keluar</a></li>
  <li class="divider"></li>
  <li class="nav-item"><a class="subheader">Master</a></li>
  <li class="nav-item"><a url="barang"><i class="material-icons">layers</i>Barang</a></li>
  <li class="divider"></li>
  <li><a class="subheader">Penjualan</a></li>
  <li class="nav-item"><a url="salesorder"><i class="material-icons">account_box</i>Penjualan / SO</a></li>
  <li class="nav-item"><a url="draftdelivery"><i class="material-icons">account_box</i><p>Delivery Order</p></a></li>
  <li class="nav-item"><a url="deliveryorder"><i class="material-icons">account_box</i><p>Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="historydo"><i class="material-icons">account_box</i><p>Histori Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="salesreturn"><i class="material-icons">arrow_back</i>Retur Penjualan</a></li>
  <li class="divider"></li>
  <li><a class="subheader">Stok</a></li>
  <li class="nav-item"><a url="stockoverview"><i class="material-icons">import_contacts</i>Peninjauan Stok</a></li>
  <li class="divider"></li>
  <br>
  <br>
  <br>
</ul>
@endif
@if(Session('roles')->name == 'Toko Partai')
<ul id="slide-out" class="side-nav fixed">
  <li>
    <div class="userView">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#!user">
        <img class="circle" @if(Session('user')->profile_picture) src="{{'uploads/avatars/'.Session('user')->profile_picture}}" @else src="uploads/avatars/profile.jpg" @endif>
      </a>
      <a><h6 class="white-text">{{Session('user')->full_name}} - {{Session('roles')->name}}</h6></a>
      <a href="#!email" data-activates="email-drop" class="dropdown-button"><span class="white-text email">{{Session('user')->email}}<i class="right material-icons">arrow_drop_down</i></span></a>
    </div>
    <ul id="email-drop" class="dropdown-content">
      <li><a href="logout">Logout</a></li>
    </ul>
  </li>
  <li><a class="subheader">List Aplikasi</a></li>
  <li class="nav-item active"><a url="homes"><i class="material-icons">home</i>Halaman Utama</a></li>
  <li><a href="user_profile"><i class="material-icons">person_pin</i>Profil Pengguna</a></li>
  <li><a href="change_password"><i class="material-icons">person_pin</i>Ubah Password</a></li>
  <li><a href="logout" class="nav-item"><i class="material-icons">exit_to_app</i>Logout / Keluar</a></li>
  <li class="divider"></li>
  <li class="nav-item"><a class="subheader">Master</a></li>
  <li class="nav-item"><a url="pelanggan" ><i class="material-icons">people</i><p>Pelanggan</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Penjualan</a></li>
  <li class="nav-item"><a url="salesorder"><i class="material-icons">account_box</i><p>Penjualan / SO</p></a></li>
  <li class="nav-item"><a url="draftdelivery"><i class="material-icons">account_box</i><p>Delivery Order</p></a></li>
  <li class="nav-item"><a url="historydo"><i class="material-icons">account_box</i><p>Histori Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="salesinvoice"><i class="material-icons">account_box</i><p>Invoice Penjualan</p></a></li>
  <li class="nav-item"><a url="salespayment"><i class="material-icons">account_box</i><p>Pembayaran Penjualan</p></a></li>
  <li class="nav-item"><a url="salesreturn"><i class="material-icons">arrow_back</i><p>Retur Penjualan</p></a></li>
  <li class="nav-item"><a url="salesreturnreceive"><i class="material-icons">arrow_back</i><p>Penerimaan Barang Retur Penjualan</p></a></li>
  <li class="nav-item"><a url="refundhistory"><i class="material-icons">arrow_back</i><p>Histori Refund Penjualan</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Stock</a></li>
  <li class="nav-item"><a url="stockoverview"><i class="material-icons">import_contacts</i>Peninjauan Stok</a></li>
  <li class="divider"></li>
  <br>
  <br>
  <br>
</ul>
@endif
@if(Session('roles')->name == 'Atas')
<ul id="slide-out" class="side-nav fixed">
  <li>
    <div class="userView">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#!user">
        <img class="circle" @if(Session('user')->profile_picture) src="{{'uploads/avatars/'.Session('user')->profile_picture}}" @else src="uploads/avatars/profile.jpg" @endif>
      </a>
      <a><h6 class="white-text">{{Session('user')->full_name}} - {{Session('roles')->name}}</h6></a>
      <a href="#!email" data-activates="email-drop" class="dropdown-button"><span class="white-text email">{{Session('user')->email}}<i class="right material-icons">arrow_drop_down</i></span></a>
    </div>
    <ul id="email-drop" class="dropdown-content">
      <li><a href="logout">Logout</a></li>
    </ul>
  </li>
  <li><a class="subheader">List Aplikasi</a></li>
  <li class="nav-item active"><a url="homes"><i class="material-icons">home</i>Halaman Utama</a></li>
  <li><a href="user_profile"><i class="material-icons">person_pin</i>Profil Pengguna</a></li>
  <li><a href="change_password"><i class="material-icons">person_pin</i>Ubah Password</a></li>
  <li><a href="logout" class="nav-item"><i class="material-icons">exit_to_app</i>Logout / Keluar</a></li>
  <li class="divider"></li>
  <li class="nav-item"><a class="subheader">Master</a></li>
  <li class="nav-item"><a url="barang"><i class="material-icons">layers</i><p>Barang</p></a></li>
  <li><a class="subheader">Pembelian</a></li>
  <li class="nav-item"><a url="purchaseorder"><i class="material-icons">attach_money</i><p>Pembelian / PO</p></a></li>
  <li class="nav-item"><a url="penerimaanbarang"><i class="material-icons">arrow_back</i><p>Penerimaan Barang</p></a></li>
  <li class="nav-item"><a url="purchaseinvoice"><i class="material-icons">assignment</i><p>Invoice Pembelian</p></a></li>
  <li class="nav-item"><a url="discountadjust"><i class="material-icons">assignment</i><p>Penyesuaian Diskon</p></a></li>
  <li class="nav-item"><a url="purchasepayment"><i class="material-icons">assignment</i><p>Pembayaran Pembelian</p></a></li>
  <li class="nav-item"><a url="purchasereturn"><i class="material-icons">arrow_back</i><p>Retur Pembelian</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Penjualan</a></li>
  <li class="nav-item"><a url="salesorder"><i class="material-icons">account_box</i><p>Penjualan / SO</p></a></li>
  <li class="nav-item"><a url="draftdelivery"><i class="material-icons">account_box</i><p>Delivery Order</p></a></li>
  <li class="nav-item"><a url="historydo"><i class="material-icons">account_box</i><p>Histori Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="salesinvoice"><i class="material-icons">account_box</i><p>Invoice Penjualan</p></a></li>
  <li class="nav-item"><a url="salespayment"><i class="material-icons">account_box</i><p>Pembayaran Penjualan</p></a></li>
  <li class="nav-item"><a url="salesreturn"><i class="material-icons">arrow_back</i><p>Retur Penjualan</p></a></li>
  <li class="nav-item"><a url="salesreturnreceive"><i class="material-icons">arrow_back</i><p>Penerimaan Barang Retur Penjualan</p></a></li>
  <li class="nav-item"><a url="refundhistory"><i class="material-icons">arrow_back</i><p>Histori Refund Penjualan</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Stok</a></li>
  <li class="nav-item"><a url="stockoverview"><i class="material-icons">import_contacts</i><p>Peninjauan Stok</p></a></li>
  <li class="nav-item"><a url="additionalinfo"><i class="material-icons">import_contacts</i><p>Informasi Spandek</p></a></li>
  <li class="nav-item"><a url="stocktransfer"><i class="material-icons">local_shipping</i><p>Pemindahan Stok</p></a></li>
  <li class="nav-item"><a url="kartustock"><i class="material-icons">credit_card</i><p>Kartu Stok</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Laporan</a></li>
  <li class="nav-item"><a url="reportpo"><i class="material-icons">book</i><p>Laporan PO</p></a></li>
  <li class="nav-item"><a url="reportso"><i class="material-icons">book</i><p>Laporan SO</p></a></li>
  <li class="nav-item"><a url="reportdo"><i class="material-icons">book</i><p>Laporan Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="reportfakturpo"><i class="material-icons">book</i><p>Laporan Faktur PO</p></a></li>
  <li class="nav-item"><a url="reportfaktur"><i class="material-icons">book</i><p>Laporan Faktur SO</p></a></li>
  <li class="nav-item"><a url="reportincome"><i class="material-icons">timeline</i><p>Laporan Pemasukkan</p></a></li>
  <li class="nav-item"><a url="reportexpense"><i class="material-icons">timeline</i><p>Laporan Expense</p></a></li>
  <li class="nav-item"><a url="reportnetsales"><i class="material-icons">timeline</i><p>Pendapatan Bersih</p></a></li>
  <li class="nav-item"><a url="reportprofitloss"><i class="material-icons">timeline</i><p>Laba Rugi</p></a></li>
  <li class="nav-item"><a url="grafik_penjualan"><i class="material-icons">timeline</i><p>Grafik Penjualan Barang</p></a></li>
  <li class="nav-item"><a url="reportcustomerpurchase"><i class="material-icons">timeline</i><p>Laporan Pembelian Customer</p></a></li>
  <br>
  <br>
  <br>
</ul>
@endif
@if(Session('roles')->name == 'Kantor')
<ul id="slide-out" class="side-nav fixed">
  <li>
    <div class="userView">
      <div class="background">
        <img src="images/office.jpg">
      </div>
      <a href="#!user">
        <img class="circle" @if(Session('user')->profile_picture) src="{{'uploads/avatars/'.Session('user')->profile_picture}}" @else src="uploads/avatars/profile.jpg" @endif>
      </a>
      <a><h6 class="white-text">{{Session('user')->full_name}} - {{Session('roles')->name}}</h6></a>
      <a href="#!email" data-activates="email-drop" class="dropdown-button"><span class="white-text email">{{Session('user')->email}}<i class="right material-icons">arrow_drop_down</i></span></a>
    </div>
    <ul id="email-drop" class="dropdown-content">
      <li><a href="logout">Logout</a></li>
    </ul>
  </li>
  <li><a class="subheader">List Aplikasi</a></li>
  <li class="nav-item active"><a url="homes"><i class="material-icons">home</i>Halaman Utama</a></li>
  <li><a href="user_profile"><i class="material-icons">person_pin</i>Profil Pengguna</a></li>
  <li><a href="change_password"><i class="material-icons">person_pin</i>Ubah Password</a></li>
  <li><a href="logout" class="nav-item"><i class="material-icons">exit_to_app</i>Logout / Keluar</a></li>
  <li class="divider"></li>
  <li><a class="subheader">Penjualan</a></li>
  <li class="nav-item"><a url="historydo"><i class="material-icons">account_box</i><p>Histori Pengeluaran Barang</p></a></li>
  <li class="nav-item"><a url="salespayment"><i class="material-icons">account_box</i><p>Pembayaran Penjualan</p></a></li>
  <li class="nav-item"><a url="salesreturnreceive"><i class="material-icons">arrow_back</i><p>Penerimaan Barang Retur Penjualan</p></a></li>
  <li class="divider"></li>
  <li><a class="subheader">Stock</a></li>
  <li class="nav-item"><a url="stockoverview"><i class="material-icons">import_contacts</i>Peninjauan Stok</a></li>
  <li class="nav-item"><a url="additionalinfo"><i class="material-icons">import_contacts</i><p>Informasi Spandek</p></a></li>
  <li class="nav-item"><a url="stocktransfer"><i class="material-icons">local_shipping</i>Pemindahan Stok</a></li>
  <li class="nav-item"><a url="kartustock"><i class="material-icons">credit_card</i>Kartu Stok</a></li>
  <br>
  <br>
  <br>
</ul>
@endif