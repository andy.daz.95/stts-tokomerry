<style type="text/css">
    table.table {
        border-collapse: collapse;
    }
    table.table th{
        border-top: 1px solid black;
        border-bottom: 1px solid black;
    }
    p , span{
        font-size: 12px;
    }
    body{
        margin:0px;
    }
    html { margin: 2cm 1cm 0cm 1cm}

</style>
<body>
<div style="">
    <table style ="width:100%">
        <tr>
            <td style="width: 2.5cm"><span></span></td>
            <td style="width: 11cm"><span>{{$transaction->transaction_number}}</span></td>
            <td style="width: 2.2cm"><span></span></td>
            <td><span>{{$transaction->transaction_date}}</span></td>
        </tr>
        <tr>
            <td ><span></span></td>
            <td ><span>{{$transaction->customer->first_name.' '.$transaction->customer->last_name}}</span></td>
            <td ><span></span></td>
            <td ><span>IDR (Rupiah)</span></td>
        </tr>
        <tr>
            <td ><span></span></td>
            <td ><span>{{$transaction->customer->phone}}</span></td>
            <td ><span></span></td>
            <td ><span>{{$transaction->method->bank->bank_name.' '.$transaction->method->payment_method_type->description}}</span></td>
        </tr>
        <tr>
            <td style="width: 2.5cm"><span></span></td>
            <td colspan="3"><span>{{$transaction->customer->address}}</span></td>
        </tr>
    </table>
    <div>
    </div>
    <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
        <thead>
        <tr>
            <th class="color" style="text-align: center;"><span>No</span></th>
            <th class="color" style="text-align: center;"><span>Nama Barang</span></th>
             <th class="color" style="text-align: center;"><span>Saldo Hutang</span></th>
            <th class="color" style="text-align: center;"><span>Dibayar</span></th>
            <th class="color" style="text-align: center;"><span>Sub Total</span></th>
        </tr>
        </thead>
        <tbody >
            <tr style="text-align: center">
                <td>
                    <span>{{1}}</span>
                </td>
                <td>
                    <span>{{$transaction->product_name}}</span>
                </td>
                <td>
                    <span>{{number_format($transaction->receiveable_before_paid)}}</span>
                </td>
                <td>
                    <span>{{number_format($transaction->payment)}}</span>
                </td>
                <td>
                    <span>{{number_format($transaction->receiveable_before_paid - $transaction->payment)}}</span>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div style="position: fixed; top:68mm; left:155mm" >
    <p style="margin: 1.5mm; padding: 0">{{number_format($transaction->receiveable_before_paid)}}</p>
    <p style="margin: 1.5mm; padding: 0">{{number_format($transaction->payment)}}</p>
    <p style="margin: 1.5mm; padding: 0">{{number_format($transaction->receiveable_before_paid - $transaction->payment)}}</p>
</div>
</body>
