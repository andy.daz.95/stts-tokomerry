<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Existing Receiveable</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="customerTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th>Nama Customer</th>
                        <th>Jumlah Utang</th>
                      </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Existing Receiveable</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="existingReceiveableTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th>No Pembayaran</th>
                        <th>Pelanggan</th>
                        <th>Tanggal</th>
                        <th>Tipe Pembayaran</th>
                        <th>Nominal Pembayaran</th>
                        <th>Notes</th>
                        <th>Action</th>
                      </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Transaksi Saldo</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <form id="formPayment">
                  <br/>
                  <div class="input-field col l3">
                    <label>No Pembayaran</label>
                    <input id="nopayment" type="text" name="nopayment" class="f-input" placeholder="No Pembayaran">
                  </div>
                  <div class="input-field col l3">
                    <label>Customer</label>
                    <select id="customer" class="browser-default selectpicker"  data-live-search="true" name="customer">
                      <option value="0">Select Customer</option>
                      @foreach($data['customer'] as $key => $value)
                        <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Tgl Transaksi</label>
                    <input id="tgltransaction" type="text" name="tgltransaction" class="f-input" placeholder="">
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader">Total Utang</th>
                          <th class="theader">Bayar</th>
                          <th class="theader">Payment Method</th>
                          <th class="theader">Notes</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="no-item"><td colspan="2"><span> No Item Selected</span></td></tr>
                        <tr id="payment-row" hidden>
                          <td>
                            <div class="input-field">
                              <input id="idst" name="id" type="text" class="f-input" hidden disabled>
                              <input id="existingreceiveable" name="existingreceiveable" type="text" class="f-input" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="pembayaran" name="pembayaran" type="text" class="f-input">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <select id="paymentmethod" name="paymentmethod" class="f-select">
                                @foreach($data['paymentmethod'] as $key => $value)
                                  <option value="{{$value->payment_method_id}}">{{$value->payment_method_name}}</option>
                                @endforeach
                              </select>
                              <select id="bank" name="bank" class="f-select" disabled hidden>
                                @foreach($data['bank'] as $key => $value)
                                  <option value="{{$value->bank_id}}">{{$value->bank_name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="notes" name="notes" type="text" class="f-input">
                            </div>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col l12 s12 m12 main-button-group margin-top">
                    <a id="submit-st" href="" class="btn-stoko btn-stoko-primary" mode="save">Submit</a>
                    <a id="edit-st" href="" class="btn-stoko btn-stoko-primary" mode="edit" hidden>Edit</a>
                    <a id="clear" href="#" class="btn-stoko orange">batal</a>
                    <a id="cetak-st" href="#" target="_blank" class="btn-stoko" hidden>print</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-transaction" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-transaction" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $("#submit-st, #edit-st").on('click',function(event){
            event.preventDefault();

            var mode = $(this).attr('mode');
            alert(mode);
            if (mode == 'save') {
                var url = "createexistingreceiveable";
            } else {
                var url = "updateexistingreceiveable";
            }

            if($('#payment-row').attr('hidden') == "hidden")
            {
                toastr.warning('Anda Belum Memilih Customer');
            }else if($('#nopayment').val() == ""){
                toastr.warning('No Pembayaran tidak boleh kosong');
            }else if($('#pembayaran').val() == ""){
                toastr.warning('Nominal Pembayaran tidak boleh kosong ');
            }else{
                hidemainbuttongroup();
                var unformat = accounting.unformat($('#existingreceiveable').val());
                $('#existingreceiveable').val(unformat);
                $('#idst, #customer, #existingreceiveable, #pembayaran, #bank').removeAttr('disabled');
                $('#pembayaran').val(accounting.unformat($('#pembayaran').val()));
                var kode = $('#nopayment').val();
                $.ajax({
                    type:"POST",
                    url:url,
                    data: $('#formPayment').serialize(),
                    success:function(response){
                        toastr.success('Transaction has been Created!',{"onShow":setTimeout(function(){$('.side-nav .nav-item.active a').click();}, 2600)});
                    }
                });
            }
        });

        $('#paymentmethod').on('change',function(){
            if( $(this).val() == 2)
            {
                $('#bank').removeAttr('hidden disabled');
            }else{
                $('#bank').attr('hidden', true).attr('disabled',true);
            }
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .nav-item.active a').click()
        });

        $('#pembayaran').on('keyup', function(event){
            if(event.keyCode !== 189 && event.keyCode !== 187)
            $(this).val(accounting.formatMoney($(this).val(), 'Rp ' ,0, ',','.'));
        })

        $('#pembayaran').on('focusin', function(){
            $(this).val("");
        });

        $('#customer').on('change', function(event){
            event.stopImmediatePropagation();
            $.ajax({
                type:"GET",
                url:"getcustomerexistingreceiveable",
                data:{id:$(this).val()},
                success:function(response){
                    if(response == ""){
                        $('#payment-row').attr('hidden',true);
                        $('#no-item').removeAttr('hidden');
                    }else{
                        $('#booknumber').val(response.book_number);
                        $('#existingreceiveable').val(accounting.formatMoney(response.existing_receiveable,'Rp ',0,',','.'));
                        $('#total').val(accounting.formatMoney(response,'Rp ' ,2,',','.'));
                        $('#payment-row').removeAttr('hidden');
                        $('#no-item').attr('hidden',true);
                    }
                }
            });
        });

        $('#existingReceiveableTable').on('click','tr, tr .edit', function(event){
            event.stopImmediatePropagation();
            var mode = $(this).attr('mode');
            var id = $(this).attr('value');
            $.ajax({
                type:"GET",
                url:"getexistingreceiveable",
                data:{id:id},
                success:function(response){
                    if(response == ""){
                        $('#payment-row').attr('hidden',true);
                        $('#no-item').removeAttr('hidden');
                    }else{
                        $('#idst').val(id).attr('disabled',true);
                        $('#nopayment').val(response.transaction_number).attr('disabled',true);
                        $('#customer').val(response.customer_id).attr('disabled',true);
                        $('#tgltransaction').val(response.transaction_date).attr('disabled',true);
                        $('#existingreceiveable').val(response.receiveable_before_paid).attr('disabled',true);
                        $('#pembayaran').val(accounting.formatMoney(response.payment,'Rp ',2,',','.')).attr('disabled',true);
                        $('#notes').val(response.notes).attr('disabled',true);
                        $('#paymentmethod').val(response.payment_method_id).attr('disabled',true);
                        $('#payment-row').removeAttr('hidden');
                        $('#no-item').attr('hidden',true);
                        $('.selectpicker').selectpicker('refresh');
                        $('#cetak-st').attr('href','downloadexistingreceiveable/'+id).removeAttr('hidden');
                        $('.submit').attr('hidden',true);
                    }
                },complete: function(){
                  $('#submit-st').attr('hidden',true);
                  $('#cetak-st').removeAttr('hidden').attr('href','downloadexistingreceiveable/'+id);

                  if(mode == "edit"){
                    $('#tgltransaction, #pembayaran, #notes, #paymentmethod').removeAttr('disabled');
                    $('#edit-st').removeAttr('hidden');
                    $('#submit-st').attr('hidden',true);
                  }
                }
            });
        });

        $('#existingReceiveableTable').on('click','.delete-modal', function (event) {
            event.stopImmediatePropagation();
            var notransaction = $(this).closest('tr').find('.notransaction').html();
            var idtransaction = $(this).closest('tr').attr('value');
            $('#confirm-delete-transaction').attr('value', idtransaction).attr('nomor', notransaction);
            $("#delete-message").html("Yakin ingin menghapus data " + notransaction + " ?")
            $('#modal-delete-transaction').modal('open');
        });

        $('#confirm-delete-transaction').on('click', function (event) {
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-transaction').modal('close');
            var id = $(this).attr('value');
            var notransaction = $(this).attr('nomor');
            $.ajax({
                type: "POST",
                url: "deleteexistingreceiveable",
                data: {id: id},
                success: function (response) {
                    toastr.success('Trasaksi ' + notransaction + ' telah berhasil Dihapus!', {
                        "onShow": setTimeout(function () {
                            $('.side-nav .nav-item.active a').click();
                        }, 2600)
                    });
                }
            })
        });

        //function
        function firstload(){
            $('#tgltransaction').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tgltransaction').val(moment().format('DD-MM-YYYY'));

            $('.selectpicker').selectpicker('render');

            var customertable = $('#customerTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getcustomerexistingreceiveabletable',
                    data : function (d){
                    }
                },
                rowId : 'customer_id',
                columns: [
                    { data: 'customer_name', name: 'customer_name'},
                    { data: 'existing_receiveable', name: 'existing_receiveable'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });


            var existingReceiveabletable = $('#existingReceiveableTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getexistingreceiveabletable',
                    data : function (d){
                    }
                },
                rowId : 'existing_receiveable_transaction_id',
                columns: [
                    { data: 'transaction_number', name: 'transaction_number', class:'notransaction'},
                    { data: 'customer_name', name: 'customer_name'},
                    { data: 'transaction_date', name: 'transaction_date'},
                    { data: 'method.payment_method_name', name: 'payment_method'},
                    { data: 'payment', name: 'payment'},
                    { data: 'notes', name: 'notes'},
                    { data: 'action', name: 'action'}
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

        }
        function hidemainbuttongroup(){
            $('.main-button-group a').attr('hidden',true);
        }


    });
</script>