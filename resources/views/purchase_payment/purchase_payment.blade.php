<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Pembayaran Purchase</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row margin-top">
                                <div class="input-field col l3">
                                    <input id="filterPayment" type="text" class="f-input">
                                    <label>Filter Payment No.</label>
                                </div>
                                <div class="input-field col l3">
                                    <input id="filterSupplier" type="text" class="f-input">
                                    <label>Filter Supplier</label>
                                </div>
                                <div class="col l12 m12 s12 margin-top">
                                    <div class="table-responsive">
                                        <table id="purchasePaymentTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                                            <thead>
                                            <tr>
                                                <th>No Pembayaran</th>
                                                <th>Tanggal Pembayaran</th>
                                                <th>Supplier</th>
                                                <th>Total</th>
                                                <th>Tindakan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@foreach($data['payment'] as $key => $value)--}}
                                            {{--<tr value="{{$value->payment_purchase_id}}">--}}
                                            {{--<td class="nopayment">{{$value->payment_purchase_number}}</td>--}}
                                            {{--<td>{{$value->date_payment_purchase}}</td>--}}
                                            {{--<td>{{$value->company_name}}</td>--}}
                                            {{--<td>{{number_format($value->paid)}}</td>--}}
                                            {{--<td>--}}
                                            {{-- <a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->invoice_purchase_id}}"><i class="material-icons">edit</i></a> --}}
                                            {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                                            {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--@endforeach--}}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Pembayaran Pembelian</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <form id="formPayment">
                                    <br/>
                                    <div class="input-field col l4">
                                        <label>No Pembayaran</label>
                                        <input id="nopayment" type="text" name="nopayment" class="f-input" placeholder="No Pembayaran" disabled>
                                        <input id="idpayment" type="text" name="id" class="f-input" placeholder="No Pembayaran" hidden>
                                    </div>
                                    <div class="input-field col l4">
                                        <label>No Invoice</label>
                                        <select id="noinvoice" class="browser-default selectpicker" name="noinvoice" data-live-search="true" data-size="5"  >
                                            <option value="0">Select Invoice</option>
                                            @foreach($data['invoice'] as $key => $value)
                                                <option value='{{$value->invoice_purchase_id}}'>{{$value->invoice_purchase_number.' - '.$value->po->supplier->company_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input-field col l4">
                                        <label>Supplier</label>
                                        <input id="supplier" type="text" name="supplier" class="f-input" placeholder="Supplier" disabled>
                                    </div>
                                    <div class="input-field col l4">
                                        <label>Tanggal Pembayaran</label>
                                        <input id="tglpayment" type="text" name="tglpayment" class="f-input" placeholder="tanggal Pembayaran">
                                    </div>
                                    <div class="input-field col l4">
                                        <label>Tanggal Tanda Terima</label>
                                        <input id="tglinvoice" type="text" name="tglinvoice" class="f-input" placeholder="tanggal Tanda Terima" disabled>
                                    </div>
                                    <div class="input-field col l4">
                                        <label>Jenis PKP</label>
                                      <select id="pkp" class="selectpicker browser-default" data-live-search="true" data-size="5" name="pkp">
                                        <option value="0">Non</option>
                                        <option value="1">PKP</option>
                                      </select>
                                    </div>
                                    <div class="input-field col l4" hidden>
                                        <label>Discount Term</label>
                                        <input id="discount-term" type="text" name="discountterm" class="f-input" placeholder="Discount Term" disabled >
                                    </div>
                                    <div class="col l12 m12 s12 margin-top">
                                        <div class="table-responsive">
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Total Invoice</th>
                                                    <th class="theader">Diskon Pembayaran</th>
                                                    <th class="theader">Nominal Pembayaran</th>
                                                    <th class="theader">Tipe Pembayaran</th>
                                                    <th class="theader sudahbayar">Sudah Bayar</th>
                                                    <th class="theader">Bayar</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                                                <tr id="payment-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="total" name="total" type="text" class="f-input" disabled>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="iddiskon" type="text" name="iddiskon" class="f-input" hidden disabled>
                                                            <input id="diskon" type="text" class="f-input" disabled>
                                                            <input id="diskon-hidden" name="diskon" type="text" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="totalbayar" type="text" class="f-input" disabled>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="paymenttype" type="text" class="f-input" disabled>
                                                            <input id="ptid" type="text" name="paymenttype" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                    <td class="sudahbayar">
                                                        <div class="input-field">
                                                            <input id="paid" type="text" class="f-input" disabled>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="pembayaran" type="text" class="f-input">
                                                            <input id="pembayaran-hidden" name="pembayaran" type="text" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col l12 s12 m12 margin-top">
                                        <a id="submit-payment" href="#" class="btn-stoko teal white-text">simpan</a>
                                        <a id="clear" href="#" class="btn-stoko orange">batal</a>
                                        <a id="cetak-payment" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Payment</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="modal-delete-payment" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-payment" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        // $.ajax({
        //     url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
        //     dataType: "script",
        // });
        firstload();

        $('#noinvoice').on('change',function(event){
            event.stopImmediatePropagation();
            var id = $(this).val();
            $.ajax({
                type:"GET",
                url:"getpurchaseinvoiceforpayment",
                data:{id:id},
                success:function(response){
                    if(response.length == 0){
                        $('#formPayment').find("input[type=text], textarea").val("");
                        $('#payment-row').attr('hidden',true);
                        $('#no-item').removeAttr('hidden');
                    }else{
                        var total = response.invoice.t_amount;
                        var paid = response.invoice.total;
                        var bayar = 0;

                        if (response.selected.discount != 0) {
                            $('#discount-term').closest('.input-field').removeAttr('hidden');
                            $('#iddiskon').val(response.selected.id);
                            var diskon = response.selected.discount;
                            if(response.selected.type == 1) //nominal
                            {
                                $('#discount-term').val("Rp "+response.selected.discount+"/"+response.selected.period);
                                var diskonnominal = diskon;
                            }else{
                                $('#discount-term').val(response.selected.discount+"%/"+response.selected.period);
                                var diskonnominal = diskon / 100 * response.invoice.grand_total_idr;
                            }
                            var bayar = total - diskonnominal;
                        }else{
                            var bayar = total;
                        }
                        $('#total').val(accounting.formatMoney(response.invoice.grand_total_idr - response.invoice.reduction - response.invoice.prev_invoice_reduction,'Rp ',2,',','.'));
                        $('#paid').val(accounting.formatMoney(paid,'Rp ',2,',','.'));
                        $('#no-item').attr('hidden',true);
                        $('#payment-row').removeAttr('hidden');
                        $('#supplier').val(response.invoice.company_name);
                        $('#pkp').val(response.invoice.is_pkp).attr('disabled',true);
                        $('#paymenttype').val(response.invoice.payment_description);
                        $('#tglinvoice').val(response.invoice.date_invoice_purchase);
                        $('#ptid').val(response.invoice.payment_type_id);
                        $('#diskon').val(accounting.formatMoney(diskonnominal,'Rp ',2,',','.'));
                        $('#diskon-hidden').val(diskonnominal);
                        $('#totalbayar').val(accounting.formatMoney(bayar,'Rp ',2,',','.'));
                        $('#pembayaran').val(accounting.formatMoney(bayar-paid,'Rp ',2,',','.'));
                        $('#pembayaran-hidden').val(bayar-paid);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
        })

        $('#pembayaran').on('keyup', function(){
            $('#pembayaran-hidden').val($(this).val());
        });

        $('#pembayaran').on('focusout', function(){
            $(this).val(accounting.formatMoney($(this).val(),'Rp ',2,',','.'))
        });

        $('#pembayaran').on('focusin', function(){
            $(this).val("");
        });

        $('#purchasePaymentTable').on('click','tr', function(){
            var id = $(this).attr('value');
            $.ajax({
                type:"GET",
                url:"getpurchasepayment",
                data: {id:id},
                success:function(response){
                    $('#noinvoice').append('<option class="remove-when-clear" value="'+response.invoice_purchase_id+'" selected="selected">'+response.invoice_purchase_number+'</option>').attr('disabled',true);
                    $('#nopayment').val(response.payment_purchase_number).attr('disabled',true);
                    $('#supplier').val(response.company_name);
                    $('#pkp').val(response.is_pkp).attr('disabled',true);
                    $('#tglpayment').val(response.date_payment_purchase).attr('disabled',true);
                    $('#tglinvoice').val(response.date_invoice_purchase);
                    $('#total').val(accounting.formatMoney(response.grand_total_idr - response.reduction - response.prev_invoice_reduction,'Rp ',2,'.','.'));
                    $('#paid').val(accounting.formatMoney(response.paid,'Rp ',2,',','.'));
                    $('#diskon').val(accounting.formatMoney(response.discount_nominal,'Rp ',2,',','.'));
                    $('#paymenttype').val(response.payment_description);
                    $('#ptid').val(response.payment_type_id);
                    $('#totalbayar').val(accounting.formatMoney(response.grand_total_idr - response.reduction -  response.discount_nominal - response.prev_invoice_reduction,'Rp ',2,',','.'));
                    $('#pembayaran').val(accounting.formatMoney(response.paid,'Rp ',2,',','.')).attr('disabled',true);
                    $('#pembayaran-hidden').val(response.paid);
                    $('#no-item').attr('hidden',true);
                    $('#payment-row').removeAttr('hidden');
                    $('.sudahbayar').attr('hidden',true);
                    $('#cetak-payment').removeAttr('hidden').attr('href',"downloadpurchasepayment/"+id);
                    $('#submit-payment').attr('hidden',true);
                    $('.selectpicker').selectpicker('refresh');
                }
            });
        });

        $('#tglpayment').on('change',function(){
            var date = $(this).val();
            var invoice = $('#noinvoice').val();

            $.ajax({
                type:"GET",
                url:"getdiscountforpayment",
                data: {date:date, invoice:invoice},
                success:function(response){
                    alert(response.discount);
                    var total = accounting.unformat($('#total').val());

                    if(response.discount != 0) {
                        $('#discount-term').closest('.input-field').removeAttr('hidden');
                        $('#iddiskon').val(response.id);
                        var diskon = response.discount;

                        var bayar = 0;

                        if(response.type == 1 ) //nominal
                        {
                            $('#discount-term').val("Rp "+response.discount+"/"+response.period);
                            var diskonnominal = diskon;
                        }else{
                            $('#discount-term').val(response.discount+"%/"+response.period);
                            var diskonnominal = diskon / 100 * total;
                        }
                        var bayar = total - diskonnominal;
                    }else{
                        var bayar = total;
                    }

                    $('#diskon').val(accounting.formatMoney(diskonnominal,'Rp ',2,',','.'));
                    $('#diskon-hidden').val(diskonnominal);
                    $('#totalbayar').val(accounting.formatMoney(bayar,'Rp ',2,',','.'));
                    $('#pembayaran').val(accounting.formatMoney(bayar,'Rp ',2,',','.'));
                    $('#pembayaran-hidden').val(bayar);
                }
            })
        });

        $("#submit-payment").on('click',function(event){
            /*$('#pembayaran').val(accounting.unformat($(this).val()));*/
            event.preventDefault();
            event.stopImmediatePropagation();


            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }

            if($('#noinvoice').val()== 0){
                toastr.warning('Anda Belum Memilih Invoice!');
            }else{
                me.invisible();
                me.data('requestRunning', true);

                $('#customer, #nopayment, #tglpayment, #total, #pembayaran, #iddiskon').removeAttr('disabled');

                var kode = $('#nopayment').val()
                $.ajax({
                    type:"POST",
                    url:"createpurchasepayment",
                    data: $('#formPayment').serialize(),
                    success:function(response){

                        // $('.side-nav .active a').click();
                        toastr.success('Purchase Payment '+kode+' has been Created!', {"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    }
                });
            }
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        $('#purchasePaymentTable').on('click','.delete-modal', function(event){
            event.stopImmediatePropagation();
            var nopayment = $(this).closest('tr').find('.nopayment').html();
            var idpayment = $(this).closest('tr').attr('value');
            $('#confirm-delete-payment').attr('value',idpayment).attr('nomor',nopayment);
            $("#delete-message").html("Yakin ingin menghapus data "+nopayment+" ?")
            $('#modal-delete-payment').modal('open');
        });

        $('#confirm-delete-payment').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-payment').modal('close');
            var id = $(this).attr('value');
            var nopayment = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepurchasepayment",
                data:{id:id},
                success:function(response){
                    toastr.success('Purchase Payment '+nopayment+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        //function
        function firstload(){

            $('.selectpicker').selectpicker('render');

            $('#tglpayment').datepicker({
                dateFormat : 'dd-mm-yyyy',
                // maxDate: new Date(),
                language: 'en',
                onSelect: function() {
                    $("#tglpayment").trigger('change');
                }
            });
            $('#tglpayment').val(moment().format('DD-MM-YYYY'));

            $.ajax({
                type:"GET",
                url:"lastdeliveryordernumber",
                success:function(response){
                    $('#donumber').val(response);
                }
            })

            $.ajax({
                type:"GET",
                url:"lastpurchasepaymentnumber",
                success:function(response){
                    $('#nopayment').val(response);
                }
            });

            var purchasePaymentTable = $('#purchasePaymentTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getpurchasepaymenttable',
                    data : function (d){
                        d.nopayment = $('#filterPayment').val();
                        d.supplier = $('#filterSupplier').val();
                    }
                },
                rowId : 'payment_purchase_id',
                columns: [
                    { data: 'payment_purchase_number', name: 'payment_purchase_number', class:'nopayment'},
                    { data: 'date_payment_purchase', name: 'date_payment_purchase'},
                    { data: 'company_name', name: 'company_name'},
                    { data: 'paid', name: 'paid'},
                    { data: 'action', name: 'action'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterPayment').on('keyup', function () { // This is for news page
                purchasePaymentTable.draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                purchasePaymentTable.draw();
            });
        }
    });
</script>