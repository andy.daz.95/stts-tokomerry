<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
</style>
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">Nota Pembayaran Pembelian</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 25%">NO</td>
      <td style="width: 25%">: {{$paymentheader->payment_purchase_number}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Tanggal Tanda Terima</td>
      <td>: {{Carbon\Carbon::parse($paymentheader->date_invoice_purchase)->format('d-m-Y') }}</td>
      <td></td>
    </tr>
    <tr>
      <td>Supplier</td>
      <td>: {{$paymentheader->company_name}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Tanggal Pembayaran</td>
      <td>: {{Carbon\Carbon::parse($paymentheader->date_payment_purchase)->format('d-m-Y') }}</td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr style="background-color: #8B8C89">
        <th class="color" style="text-align: center;">Keterangan</th>
        <th class="color" style="text-align: center;">Total Invoice</th>
        <th class="color" style="text-align: center;">Diskon</th>
        <th class="color" style="text-align: center;">Nominal</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Pembayaran Atas Invoice dengan No {{$paymentheader->invoice_purchase_number}} Berdasarkan Kepada No Purchase Order {{$paymentheader->purchase_order_number}} Dengan Nominal Tertera Disamping Ini</td>
        <td>{{number_format($paymentheader->total_amount)}}</td>
        <td>{{number_format($paymentheader->discount_nominal)}}</td>
        <td>{{number_format($paymentheader->paid)}}</td>
      </tr>
    </tbody>
  </table>
  <table style ="width:100%">
    <tr>
      <td style="width: 25%">Sisa Pembayaran</td>
      <td style="width: 25%">: Rp. {{number_format($remaining)}}</td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </table>
</div>
</div>