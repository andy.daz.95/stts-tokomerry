<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/toastr.css">
<!-- Latest compiled and minified JavaScript -->

<!-- <link rel="stylesheet" href="bootstrap/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.css">
<style type="text/css">
  /*  .canvasjs-chart-canvas{
      position: inherit !important;
    }*/
  /*  .__web-inspector-hide-shortcut__
    {
      position: absolute !important;
    }
  */
</style>
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Set Target</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3 right">
                  <a id="add-target" href="#modal-tambah-target" class="btn-stoko btn-stoko-primary modal-trigger right">Tambah Target</a>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="stockTransferTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th class="theader">Nama Supplier</th>
                        <th class="theader">Product</th>
                        <th class="theader">Target Nominal</th>
                        <th class="theader">Tanggal Awal</th>
                        <th class="theader">Tanggal Akhir</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($target as $key => $value)
                        <tr value="{{$value->target_id}}">
                          <td>{{$value->supplier->company_name}}</td>
                          <td>{{$value->product->product_name}}</td>
                          <td>{{number_format($value->target_nominal)}}</td>
                          <td>{{$value->date_start}}</td>
                          <td>{{$value->date_end}}</td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
        </li>
      </ul>
    </div>

    <div id="modal-tambah-target" class="modal">
      <form id="form-add-target">
        <div class="modal-content">
          <h4>Tambah Target</h4>
          <br>
          <div class="row">
            <div class="input-field col l12 m12 s12">
              <label>Supplier</label>
              <select id="supplier" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="supplier">
                <option value="0">Select Supplier</option>
                @foreach($supplier as $key => $value)
                  <option value="{{$value->supplier_id}}">{{$value->company_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="input-field col l12 m12 s12">
              <label>Barang</label>
              <select id="product" class="browser-default selectpicker" data-live-search="true" data-size="5" name="product">
                <option value="0">Select Barang</option>
                @foreach($product as $key => $value)
                  <option value="{{$value->product_id}}">{{$value->product_name}}</option>
                @endforeach
              </select>
            </div>
            <div class="input-field col l12 m12 s12">
              <label>Target Nominal</label>
              <input id="nominal" type="text" class="f-input" name="nominal">
            </div>
            <div class="input-field col l12 l12 m12 s12">
              <label>Tgl Awal</label>
              <input id="tglawal" type="text" class="f-input" name="tglawal">
            </div>
            <div class="input-field col l12 l12 m12 s12">
              <label>Tgl Akhir</label>
              <input id="tglakhir" type="text" class="f-input" name="tglakhir">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-target">OK</a>
          <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
        </div>
      </form>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Target Pembelian</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <table>
                  <tr>
                    <div id="append-canvas" class="row">
                      <div class="canvas col l4 m4 s4">
                        <h4 class="title-top" style="text-align: center; position: relative; top:15px">Target Pembelian Supplier X</h4>
                        <div id="chartContainer" class="container chart" style="min-height: 180px">

                        </div>
                        <h4 class="title-bottom" style="text-align: center">100000000 out of 200000000000</h4>
                      </div>
                    </div>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <script type="text/javascript" src="js/toastr.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: 'js/materialize.min.js',
                dataType: "script",
            });

            $.ajax({
                url: 'js/custom.js',
                dataType: "script",
            });

            $.ajax({
                url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
                dataType: "script",
            });

            $.ajax({
                method:"GET",
                url:"grafik_target",
                success:function(response){
                    var $orginal = $('.canvas:first');
                    $('.canvas:first').remove();
                    $.each(response, function(key,value){
                        var target_nominal = value.target_nominal;
                        var pembelian = value.pembelian ? value.pembelian : 0  ;
                        var percent = (parseInt(pembelian) / parseInt(target_nominal)* 100).toFixed(2);
                        if(percent > 100)
                        {
                            percent = 100;
                        }
                        var data = [];
                        var $newId = 'chartContainer'+key;
                        var $cloned = $orginal.clone().addClass('cloned');
                        // alert($newId);
                        // alert($cloned.find('#chartContainer').attr('id'));
                        $cloned.find('#chartContainer').attr('id',$newId);
                        $cloned.find('.title-top').html('Target Pembelian Supplier </br>'+value.supplier.company_name+'</br> untuk barang '+value.product.product_name);
                        $cloned.find('.title-bottom').html(CanvasJS.formatNumber(pembelian, '#,###,###')+' out of '+ CanvasJS.formatNumber(target_nominal, '#,###,###'));
                        $cloned.appendTo('#append-canvas');
                        var object = { "label":value.product.product_name, "y":parseInt(value.pembelian)};
                        data.push(object);
                        var chart = new CanvasJS.Chart("chartContainer"+key, {
                            animationEnabled: true,
                            backgroundColor: "rgba(0, 0, 0, 0)",
                            title: {
                                fontColor: "#848484",
                                fontSize: 42,
                                horizontalAlign: "center",
                                text: percent+"%",
                                verticalAlign: "center"
                            },
                            toolTip: {
                                backgroundColor: "#ffffff",
                                borderThickness: 0,
                                cornerRadius: 0,
                                fontColor: "#424242"
                            },
                            data: [
                                {
                                    explodeOnClick: false,
                                    innerRadius: "90%",
                                    radius: "90%",
                                    startAngle: 270,
                                    type: "doughnut",
                                    dataPoints: [
                                        { y: percent, color: "#c70000", toolTipContent: "Pembelian: Rp <span>" + CanvasJS.formatNumber(pembelian, '#,###,###') + "</span>" },
                                        { y: (100 - percent), color: "#424242", toolTipContent: null }
                                    ]
                                }
                            ]
                        });
                        chart.render();
                    });
                }
            })


            // $(".bootstrap-select").click(function (event) {
            //     event.preventDefault();
            //     event.stopImmediatePropagation();
            //     alert('test');
            //     $(this).addClass("open");
            //     $('.selectpicker').selectpicker('toogle');
            //     $(this).find('.dropdown-toggle').attr('aria-expanded',true);
            // });



            // $('#gudangfrom').change(function(event){
            //   event.stopImmediatePropagation();
            //   var idgudang = $(this).val();
            //   var temp = $('#gudangfrom').html();
            //   $('#gudangto').html(temp);
            //   $('#gudangto option').each(function (){
            //     if($(this).val() == idgudang)
            //     {
            //       $(this).remove();
            //     }
            //   })
            //   $("#gudangto option:selected").prop("selected", false);
            //   $("#gudangto option:first").prop("selected", "selected");
            //   getBarang();
            // })

            // $('#gudangfrom').on('change', function(){
            //   var idgudang = $(this).val();
            //   var temp = $('#gudangfrom').html();
            //   $('#gudangto').html(temp);
            //   $('#gudangto option').each(function (){
            //     if($(this).val() == idgudang)
            //     {
            //       $(this).remove();
            //     }
            //   })
            //   $("#gudangto option:selected").prop("selected", false);
            //   $("#gudangto option:first").prop("selected", "selected");
            //   $('.selectpicker').selectpicker('refresh');
            //   getBarang();
            // });

            // firstload();
            // $('.selectpicker').selectpicker('render');

            // $('.bootstrap-select>.dropdown-toggle').on('click',function(event){
            //   event.preventDefault();
            //   event.stopImmediatePropagation();
            //   $(this).closest('.bootstrap-select').addClass("open");
            //   $(this).attr('aria-expanded',true);
            // })

            // $('#barang').change(function(event){
            //   event.stopImmediatePropagation();
            //   getBarang();
            // });

            $('#nominal').on('keyup',function(){
                $(this).val(accounting.formatMoney($(this).val(),'',0,',','.'));
            });

            $('.submit-target').click(function(event){
                event.preventDefault();
                event.stopImmediatePropagation();
                if($('#supplier').val() == 0){
                    toastr.warning('Anda Belum Memilih Supplier');
                }else{
                    $('#nominal').val(accounting.unformat($('#nominal').val()));
                    $.ajax({
                        type:"POST",
                        url:"createtarget",
                        data:$(this).closest('#form-add-target').serialize(),
                        success:function(response){
                            $('#modal-tambah-target').modal('close');
                            toastr.success('Target  has been Created!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                        }
                    })
                }
            })

            // $('.highlight tr').on('click',function(event){
            //   event.stopImmediatePropagation();
            //   var id = $(this).attr('value');
            //   $.ajax({
            //     type:"GET",
            //     url:"getstocktransfer",
            //     data:{id:id},
            //     success:function(response){
            //       console.log(response);
            //       $('#barang').val(response.product_id).attr('disabled',true);
            //       $('#gudangfrom').val(response.warehouse_from).attr('disabled',true);
            //       $('#gudangfrom').trigger('change');
            //       $('#gudangto').val(response.warehouse_to).attr('disabled',true);
            //       $('#no-item').attr('hidden',true);
            //       $('#id').val(response.product_id);
            //       $('#nama').val(response.product_name).attr('disabled',true);
            //       $('#qty').val(response.quantity).attr('disabled',true);
            //       $('.barang-row').removeAttr('hidden');
            //       $('.selectpicker').selectpicker('refresh');
            //     }
            //   });
            // })

            // $('.trigger-submit').change(function(event){
            //   event.stopImmediatePropagation();
            //   getBarang();
            // });

            // $('body').on('click','.bootstrap-select>.dropdown-toggle',function(event){
            //   event.stopPropagation();
            //   event.stopImmediatePropagation();
            //   alert('test');
            //   // $('#barang').selectpicker('toggle');
            // })

            //function
            firstload();

            function firstload()
            {
                $('#tglawal, #tglakhir').datepicker({
                    dateFormat : 'dd-mm-yyyy',
                    autoClose: true,
                    language: 'en',
                });
                $('#tglawal').val(moment().format('DD-MM-YYYY'));
                $('#tglakhir').val(moment().add('years', 1).format('DD-MM-YYYY'));

                $('#gudangfrom').trigger('change');
                $('.selectpicker').selectpicker('render');

                stockTransferTable = $('#stockTransferTable').DataTable({ // This is for home page
                    searching: true,
                    responsive: true,
                    'sDom':'tip',
                    "bPaginate":true,
                    "bFilter": false,
                    "sPaginationType": "full_numbers",
                    "iDisplayLength": 10,
                    language: {
                        "sProcessing":   "Sedang proses...",
                        "sLengthMenu":   "Tampilan _MENU_ entri",
                        "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                        "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                        "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
                        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                        "sInfoPostFix":  "",
                        "sSearch":       "Cari:",
                        "sUrl":          "",
                        "oPaginate": {
                            "sFirst":    "Awal",
                            "sPrevious": "Balik",
                            "sNext":     "Lanjut",
                            "sLast":     "Akhir"
                        }
                    },
                });
            }

            // function sortgudang(id){
            //   var temp = $('#gudangfrom').html(q4);
            //   $('#gudangto').html(temp);
            //   $('#gudangto option').each(function (){
            //     if($(this).val() == id)
            //     {
            //       $(this).remove();
            //     }
            //   })
            //   $("#gudangto option:selected").prop("selected", false);
            //   $("#gudangto option:first").prop("selected", "selected");
            //   $('.selectpicker').selectpicker('refresh');
            // }

            // function getBarang()
            // {
            //   var id = $('#barang').val();
            //   if (id !== '0')
            //   {
            //     $.ajax({
            //       type:"GET",
            //       url:"getBarangTransfer",
            //       data: $('#formStocktransfer').serialize()+"&id="+id,
            //       success:function(response){
            //         $('#no-item').attr('hidden',true);
            //         $('#id').val(response.product.product_id);
            //         $('#nama').val(response.product.product_name).attr('disabled',true);
            //         $('#from').val(response.qtyfrom).attr('disabled',true);
            //         $('#to').val(response.qtyto).attr('disabled',true);
            //         $('.barang-row').removeAttr('hidden');
            //       }
            //     });
            //   }
            // }
        });
    </script>