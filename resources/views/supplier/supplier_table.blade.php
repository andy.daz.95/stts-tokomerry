<table id="supplierTable" class="table table-bordered display nowrap dataTable dtr-inline">
	<thead>
		<tr>
			<th>Supplier</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody id="supplierData">
		@foreach($supplier as $row => $value)
		<tr value="{{$value->supplier_id}}">
			<td>{{$value->company_name}}</td>
			<td>
				<a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
				<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{$supplier->links()}}
<script type="text/javascript">
	 supplierTable = $('#supplierTable').DataTable({ // This is for home page
      searching: true,
      responsive: true,
      'sDom': 'ti',
      'pagingType': 'full_numbers',
      language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
    });
</script>