<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l6 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Supplier</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formSupplier">
                <div class="row margin-top">
                  <div class="col l6">
                    <div class="row">
                      <div class="input-field col l12 m12 s12">
                        <input id="kode" type="text" class="f-input" name="kode" placeholder="Kode" disabled>
                        <label>Kode</label>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <input id="firstname" type="text" class="f-input" name="firstname">
                        <label>Nama Depan</label>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <select id="bank" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="bank">
                          <option value="">Pilih Bank</option>
                          @foreach($bank as $key => $value)
                          <option value="{{$value->bank_id}}">{{$value->bank_name}}</option>
                          @endforeach
                        </select>
                        <label>Bank</label>
                      </div>
                    </div>
                  </div>
                  <div class="col l6">
                    <div class="row">
                      <div class="input-field col l12 m12 s12">
                        <input id="perusahaan" type="text" class="f-input" name="perusahaan">
                        <label>Perusahaan</label>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <input id="lastname" type="text" class="f-input" name="lastname">
                        <label>Nama Belakang</label>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <input id="atasnama" type="text" class=f-input name="atasnama">
                        <label>Atas Nama</label>
                      </div>
                    </div>
                  </div>

                  <div class="col l12">
                      <div class="input-field col l12 m12 s12">
                        <input id="norek" type="text" class="f-input" name="norek">
                        <label>No Rekening</label>
                      </div>
                  </div>

                  <div class="col l6">
                    <div class="input-field col l12 m12 s12">
                      <select id="provinsi" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="provinsi">
                        <option value="">Pilih Provinsi</option>
                        @foreach($provinsi as $key => $value)
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endforeach
                      </select>
                      <label>Provinsi</label>
                    </div>
                  </div>
                  <div class="col l6">
                    <div class="input-field col l12 m12 s12">
                      <select id="kota" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="kota">
                        <option value="">Pilih Kota</option>
                      </select >
                      <label>Kota</label>
                    </div>
                  </div>

                  <div class="col l12">
                    <div class="input-field col l12 m12 s12">
                      <textarea id="alamat" class="f-txt" name="alamat"></textarea>
                      <label>Alamat</label>
                    </div>
                  </div>
                </div>

                <div class="col l12 m12 s12">
                  <div class="row">
                    <div class="input-field col l4 m4 s12">
                      <input id="telp1" type="text" class="f-input" name="telp1">
                      <label>Telp 1</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                      <input id="telp2" type="text" class="f-input" name="telp2">
                      <label>Telp 2</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                      <input id="fax" type="text" class="f-input" name="fax">
                      <label>Fax 1</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col l12 m12 s12">
                     <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                    <a href="#" class="btn btn-raised btn-sm orange reset">batal</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
     {{--<div class="col l6 m12 s12">--}}
      {{--<ul class="collapsible" data-collapsible="accordion">--}}
        {{--<li>--}}
          {{--<div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Import Supplier</div>--}}
          {{--<div class="collapsible-body">--}}
            {{--<div class="container-fluid">--}}
              {{--<div class="row margin-top">--}}
                {{--<form id="importSupplier" enctype="multipart/form-data">--}}
                  {{--<div class="col l10">--}}
                    {{--<div class="input-field">--}}
                      {{--<input id="file" type="file" name="file" class="f-input">--}}
                    {{--</div>--}}
                  {{--</div>--}}
                  {{--<div class="col-l2">--}}
                    {{--<button id="submit" type="button" class="btn btn-raised light-blue darken-2">save</button>--}}
                  {{--</div>--}}
                {{--</form>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</li>--}}
      {{--</ul>--}}
    {{--</div>--}}
    <div class="col l6 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Supplier</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="col l10">
                  <div class="input-field col l6">
                    <input id="filterNama" type="text" class="f-input">
                    <label>Filter Nama</label>
                  </div>
                  <div class="input-field col l6">
                    <input id="filterPerusahaan" type="text" class="f-input">
                    <label>Filter Perusahaan</label>
                  </div>
                  {{-- <div class=" input-field col l6">
                    <a id="btn-search" class="btn btn-sm light-blue">Search</a>
                    <label></label>
                  </div> --}}
                </div>
                <div id="append-table" class="col l12 m12 s12">
                  <table id="supplierTable" class="table table-bordered display nowrap dataTable dtr-inline">
                    <thead>
                      <tr>
                        <th>Supplier</th>
                        <th>Nama Perusahaan</th>
                        <th>Tindakan</th>
                      </tr>
                    </thead>
                    <tbody id="supplierData">
                      @foreach($supplier as $row => $value)
                      <tr value="{{$value->supplier_id}}">
                        <td>{{$value->first_name.' '.$value->last_name}}</td>
                        <td>{{$value->company_name}}</td>
                        <td>
                          <a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
                          <a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Modal Delete Supplier -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="row">
      <div class="col l12">
       <table id="modal-delete-data">

       </table>
     </div>
   </div>
 </div>
 <div class="modal-footer">
  <form>
    <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius delete">Hapus</a>
    <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
  </form>
</div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
 $('#btn-submit').attr('mode', 'save');
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    // $.ajax({
    //   url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
    //   dataType: "script",
    // });

    supplierTable = $('#supplierTable').DataTable({ // This is for home page
      searching: true,
      responsive: true,
      'sDom': 'tip',
      'pagingType': 'numbers',
      language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
    });


    $('#filterNama').on('keyup', function () { // This is for news page
       supplierTable.column(0).search(this.value).draw();
    });
    $('#filterPerusahaan').on('keyup', function () { // This is for news page
       supplierTable.column(1).search(this.value).draw();
    });


    $('#formSupplier').submit(function(event){
      event.preventDefault();
      var mode = $(this).find('#btn-submit').attr('mode');
      console.log(mode);
      var empty = required(['firstname','lastname','alamat','provinsi','kota','telp1']);
      var validangka = cekmanynumbers(['telp1','telp2','norek','fax']);
      var validbank = 0;
      if($('#bank').val() != "")
      {
        var validbank = required(['atasnama','bank','norek']);
      }

      if(empty != '0')
      {
        toastr.warning(empty+' tidak boleh kosong!');
      }else if(validangka != '0'){
        toastr.warning(validangka+' harus angka');
      }else if(validbank != '0'){
        toastr.warning(validbank+' tidak boleh kosong');
      }else{
        if (mode == 'save')
        {
          $.ajax({
            type:"POST",
            url:"createSupplier",
            data: $("#formSupplier").serialize(),
            success:function(response){
              toastr.success('Supplier Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
            }
          });
        }else if(mode == 'edit'){
          var id = $(this).find('#kode').val();
          $.ajax({
            type:"POST",
            url:"updateSupplier",
            data: $("#formSupplier").serialize()+"&id="+id,
            success:function(response){
              toastr.success('Supplier Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
            }
          });
        }
      }
    });

    $('#importSupplier #submit').on('click', function(event){
      event.stopImmediatePropagation()
      event.preventDefault();

      var data = new FormData();
      jQuery.each(jQuery('#file')[0].files, function(i, file) {
        data.append('file', file);
      });

      $.ajax({
        url: 'importSupplier',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        success: function(data){
          console.log(data);
          toastr.success("Success",{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
        }
      });
    });

    $('body').on('click','#formSupplier .reset', function(event){
      event.stopImmediatePropagation();
      $(this).closest('#formSupplier').find("input[type=text], textarea").val("");
      $('#btn-submit').attr('mode', 'save');
    });

    $('#provinsi').on('change',function(){
      var provinsi = $('#provinsi option:selected').val();
      getCity(provinsi);
    });

    $('body').on('click','#supplierTable .edit', function(event){
      event.stopImmediatePropagation();
      var id = $(this).closest('tr').attr('value');
      $.ajax({
        type:"GET",
        url:"getSupplierData",
        data: {id:id},
        success:function(response){
          console.log(response);
          $('#provinsi').val(response.supplier.provinsi);
          $('#kode').val(response.supplier.supplier_id);
          $('#nama').val(response.supplier.nama);
          $('#telp1').val(response.supplier.phone);
          $('#firstname').val(response.supplier.first_name);
          $('#lastname').val(response.supplier.last_name);
          $('#telp2').val(response.supplier.phone2);
          $('#fax').val(response.supplier.fax);
          $('#perusahaan').val(response.supplier.company_name);
          $('#alamat').val(response.supplier.address);
          if(typeof response.bank != "undefined" && response.bank != null && response.bank > 0)
          {
            $('#bank').val(response.bank.bank_id);
            $('#atasnama').val(response.bank.on_behalf_of);
            $('#norek').val(response.bank.bank_account);
          }
          getCity(response.supplier.provinsi, response.supplier.city_id);
        }
      });
      $('#btn-submit').attr('mode', 'edit');
    });

    $('body').on('click',"#supplierTable .delete-modal",function(event){
      event.preventDefault();
      var id = $(this).closest('tr').attr('value');
      $.ajax({
        type:"GET",
        url:"modalSupplierData",
        async:false,
        data:{
          id:id,
        },
        success:function(response){
          $('#modal-delete-data').html(response);
        }
      });
      $('#delete_data').modal('open');
    });

    $(".delete").on('click',function(event){
      var id = $(this).closest('.modal').find('#id-delete').attr('value');
      console.log(id);
      $.ajax({
        type:"Get",
        url:"deleteSupplier",
        data:{id:id},
        success:function(response){
          toastr.success('Berhasil menghapus pelanggan!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
        }
      });
    });

    $('.selectpicker').selectpicker('render');

    //function
    function getCity(provinsi,city)
    {
      city = city || 0;
      $.ajax({
        type:"GET",
        url:"cities",
        data:{
          provinsi:provinsi, city:city,
        },
        success:function(response){
          $('#kota').html(response);
        },
        complete:function(){
          $('.selectpicker').selectpicker('refresh');
        }
      });
    }
  })
</script>