<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Beban / Biaya</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l3">
                  <input id="filterExpenseNumber" type="text" class="f-input">
                  <label>Filter No. Beban</label>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="expenseTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th>No Beban</th>
                          <th>Tanggal Beban</th>
                          <th>Deskripsi</th>
                          <th>Nominal Beban</th>
                          <th>Tindakan</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data['expense'] as $key => $value)
                          <tr value="{{$value->expense_id}}">
                            <td class="noexpense">{{$value->expense_number}}</td>
                            <td>{{$value->date_expense}}</td>
                            <td>{{$value->description}}</td>
                            <td>{{$value->nominal}}</td>
                            <td>
                              @if(Session('roles')->name == 'master')
                              <a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->expense_id}}"><i class="material-icons">edit</i></a>
                              <a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                              @endif
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Beban</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <form id="formExpense">
                  <br/>
                  <div class="input-field col l3">
                    <label>No Beban</label>
                    <input id="noexpense" type="text" name="noexpense" class="f-input" placeholder="No Beban" disabled>
                    <input id="idexpense" type="text" name="idexpense" class="f-input" hidden>
                  </div>
                  <div class="input-field col l3">
                    <label>Tanggal Beban</label>
                    <input id="tglexpense" type="text" name="tglexpense" class="f-input" placeholder="Tanggal Expense">
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                          <tr>
                            <th class="theader">Deskripsi</th>
                            <th class="theader">Nominal Beban</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr id="payment-row">
                            <td>
                              <div class="input-field">
                                <input id="description" name="description" type="text" class="f-input">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="nominal" type="text" class="f-input">
                                <input id="nominal-hidden" name="nominal" type="text" class="f-input" hidden>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="col l12 s12 m12 margin-top">
                    <a id="submit-expense" href="#" class="btn-stoko teal white-text" mode="save">Simpan</a>
                    <a id="edit-expense" href="#" class="btn-stoko teal white-text" mode="edit" hidden >Edit</a>
                    <a id="clear" href="#" class="btn-stoko orange">batal</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-expense" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-expense" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $.ajax({
      url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
      dataType: "script",
    });

    firstload();

    $("#submit-expense, #edit-expense").on('click',function(event){
      event.preventDefault();
      
      var me = $(this);
      // prevent multiple request at once
      if ( me.data('requestRunning') ) {
        return;
      }
      //

      var kode = $('#noexpense').val();

      if($(event.currentTarget).attr('mode') == 'save')
      {
        var url = "createexpense";
        var successmessage = 'Beban '+kode+' telah berhasil dibuat!';
      }else{
        var url = "updateexpense";
        var successmessage = 'Beban '+kode+' telah berhasil diubah!';
      }

      if(ceknumber($('#nominal-hidden').val()) == false)
      {
        toastr.warning('Masukkan nominal dengan benar');
      }else{
        me.data('requestRunning', true);
        $('#noexpense').removeAttr('disabled')
        $.ajax({
          type:"POST",
          url:url,
          data: $('#formExpense').serialize(),
          success:function(response){
            toastr.success(successmessage,{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
          },
          complete: function() {
            me.data('requestRunning', false);
          }
        });
      }
    });

    $('body').on('click', '#expenseTable tr, #expenseTable .edit', function(event){
      event.stopImmediatePropagation();
      var id = $(this).attr('value');
      var mode = $(event.currentTarget).attr('mode');
      $.ajax({
        type:"GET",
        url:"getexpense",
        data: {id:id},
        success:function(response){
          $('#noexpense').val(response.expense_number).attr('disabled',true);
          $('#idexpense').val(response.expense_id);
          $('#description').val(response.description).attr('disabled',true);
          $('#tglexpense').val(response.date_expense).attr('disabled',true);
          $('#nominal').val(accounting.formatMoney(response.nominal,'Rp. ',2,'.',',')).attr('disabled',true);
          $('#nominal-hidden').val(response.nominal);
        },complete:function(){
          if(mode=="edit"){
            $('#submit-expense').attr('hidden',true);
            $('#edit-expense').removeAttr('hidden');
            $('#nominal, #nominal-hidden, #tglexpense, #description').removeAttr('disabled');
          }
          $('.selectpicker').selectpicker('refresh');
        }
      });
    });

    $('#nominal').on('focusout', function(){
      $('#nominal-hidden').val($(this).val());
      $(this).val(accounting.formatMoney($(this).val(),'Rp. ',2,',','.'))
    });

    $('#pembayaran').on('keyup', function(){
      $('#pembayaran-hidden').val($(this).val());
    });

    $('#nominal').on('focusin', function(){
      $(this).val("");
    });

    $('body').on('click','#expenseTable .delete-modal', function(event){
      event.stopImmediatePropagation();
      var noexpense = $(this).closest('tr').find('.noexpense').html();
      var idexpense = $(this).closest('tr').attr('value');
      $('#confirm-delete-expense').attr('value',idexpense).attr('nomor',noexpense);
      $("#delete-message").html("Yakin ingin menghapus data "+noexpense+" ?")
      $('#modal-delete-expense').modal('open');
    });

    $('#confirm-delete-expense').on('click', function(event){
      event.stopImmediatePropagation();
      event.preventDefault();
      $('#modal-delete-expense').modal('close');
      var id = $(this).attr('value');
      var noexpense = $(this).attr('nomor');
      $.ajax({
        type:"POST",
        url:"deleteexpense",
        data:{id:id},
        success:function(response){
          toastr.success('Expense '+noexpense+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
        }
      })
    });

    //function
    function firstload(){

      $('#tglexpense').datepicker({
        dateFormat : 'dd-mm-yyyy',
        maxDate: new Date(),
        language: 'en',
      });

      $('#tglexpense').val(moment().format('DD-MM-YYYY'));

      $('.selectpicker').selectpicker('render');
      $.ajax({
        type:"GET",
        url:"lastexpensenumber",
        success:function(response){
          $('#noexpense').val(response);
        }
      });

      expenseTable = $('#expenseTable').DataTable({ // This is for home page
        searching: true, 
        responsive: true,
        'sDom':'tip',
        "bPaginate":true,
        "bFilter": false,
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });

      $('#filterExpenseNumber').on('keyup', function () { // This is for news page
        expenseTable.column(0).search(this.value).draw();
      });
    }
  });
</script>