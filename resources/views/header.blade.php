<?
$date = date("d F Y, H:i");
?>
<script type="text/javascript">
  function updateclock() {
    $('#clock').html(moment().format('D. MMMM YYYY H:mm:ss'));
  }
  setInterval(updateclock, 1000);
</script>
<nav class="red darken-1">
  <div class="container-fluid">
    <div class="nav-wrapper">
      <a href="#" data-activates="slide-out" class="button-collapse left"><i class="material-icons">menu</i></a>
      <a href="#" class="brand-logo center">Sistem Toko</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li id="clock"></li>
      </ul>
    </div>
  </div>
</nav>