<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Log</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="StockOverviewTable" style="width: 100%" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th class="theader">Kode Log</th>
                          <th class="theader">Description</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($log as $key => $value)
                          <tr>
                          <td>{{$value->log_id}}</td>
                          <td>{{$value->actor.' '.$value->activity.' '.$value->object.' dengan Kode '.$value->object_details}}</td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.3/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });
  });
</script>