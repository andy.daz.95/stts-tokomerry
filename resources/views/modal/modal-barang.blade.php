<table id="modalBarangTable">
	<thead>
		<th>Kode</th>
		<th>Nama</th>
		@if($src !== 'po')
		<th>Qty</th>
		@endif
		<th>Harga Eceran</th>
		<th>check</th>
	</thead>
	<tbody id="modal-barang-data">
		@foreach($barang as $key => $value)
		<tr value="{{$value->product_id}}">
			<td>{{$value->product_code}}</td>
			<td>{{$value->product_name}}</td>
			@if($src !== 'po')
			<td>{{number_format($value->total_qty)}}</td>
			@endif
			<td>{{number_format($value->price_sale)}}</td>
			<td>
				@php
				$check = 0;
				@endphp
				@for($i=0; $i<sizeof($reqbarang); $i++)
				@if($value->product_id == $reqbarang[$i])
				@php $check++; @endphp
				@endif
				@endfor
				@if($check > 0)
				<input type="checkbox" id="{{'myCheckbox'.$key}}" class="filled-in" value="{{$value->product_id}}" name="barang[]" checked/>
				<label for="{{'myCheckbox'.$key}}"></label>
				@else
				<input type="checkbox" id="{{'myCheckbox'.$key}}" class="filled-in" value="{{$value->product_id}}" name="barang[]" />
				<label for="{{'myCheckbox'.$key}}"></label>
				@endif
        @if(sizeof($value->last_product_info) > 0)
          <button class="modal-info" type="button">Lihat Detail</button>
        @endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<script type="text/javascript">
	$(document).ready(function(){
		modalBarangTable = $('#modalBarangTable').DataTable({
            'sDom':'lftirp',
            "destroy": true,
            language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
          });
	});
</script>
