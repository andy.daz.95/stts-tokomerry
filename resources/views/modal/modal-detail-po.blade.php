<table id="modalPoTable">
	<thead>
		<th>No PO</th>
		<th>Nama</th>
		<th>Total Order</th>
		<th>Qty Belum Transfer</th>
		<th>Keterangan</th>
		<th>Pilih</th>
	</thead>
	<tbody id="modal-barang-data">
		@foreach($detailpo as $key => $value)
		<tr value="{{$value->purchase_order_details_id}}">
			<td>{{$value->po->purchase_order_number}}</td>
			<td>{{$value->product->product_name}}</td>
			@php $total = $value->quantity + $value->free_quantity @endphp
			<td>{{$total}}</td>
			<td>{{$total - $value->detailsm->sum('quantity')}}</td>
			<td>{{$value->detail_note}}</td>
			<td>
				<input type="radio" id="{{'myCheckbox'.$key}}" class="with-gap radio"	 value="{{$value->product_id}}" name="po[]" />
				<label for="{{'myCheckbox'.$key}}"></label>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<script type="text/javascript">
	$(document).ready(function(){
		modalPoTable = $('#modalPoTable').DataTable({
            'sDom':'lftirp',
            "destroy": true,
            language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
          });
	});
</script>
