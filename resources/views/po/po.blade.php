<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Purchase Order</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="input-field col l3">
                                    <label>Filter PO</label>
                                    <input id="filterPo" type="text" class="f-input" placeholder="Filter PO">
                                </div>
                                <div class="input-field col l3">
                                    <label>Filter Supplier</label>
                                    <input id="filterSupplier" type="text" class="f-input" placeholder="Filter Supplier">
                                </div>
                                <div class="col l12 m12 s12">
                                    <div class="table-responsive">
                                        <table id="poTable" class="table highlight table-bordered display nowrap dataTable dtr-inline">
                                            <thead>
                                            <tr>
                                                <th class="theader">No PO</th>
                                                <th class="theader">Tanggal PO</th>
                                                <th class="theader">Supplier</th>
                                                <th class="theader">Jenis Pembayaran</th>
                                                <th class="theader">Total</th>
                                                <th class="theader">Jatuh Tempo</th>
                                                <th class="theader">Tipe PKP</th>
                                                <th class="theader">Tindakan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@foreach($data['po'] as $key => $value)--}}
                                                {{--<tr value="{{$value->purchase_order_id}}" mode="view">--}}
                                                    {{--<td class="nopo">{{$value->purchase_order_number}}</td>--}}
                                                    {{--<td>{{$value->date_purchase_order}}</td>--}}
                                                    {{--<td>{{$value->s_name}}</td>--}}
                                                    {{--<td>{{$value->p_desc}}</td>--}}
                                                    {{--<td class='number'>{{$value->grand_total_idr}}</td>--}}
                                                    {{--<td>{{$value->jatuh_tempo}}</td>--}}
                                                    {{--<td>--}}
                                                        {{--@if($value->is_pkp == 0)--}}
                                                            {{-----}}
                                                        {{--@else--}}
                                                            {{--P--}}
                                                        {{--@endif--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--@if(Session('roles')->name == 'master')--}}
                                                            {{--@if($value->edit_status == 'editable')--}}
                                                                {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->purchase_order_id}}"><i class="material-icons">edit</i></a>--}}
                                                                {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                                                            {{--@endif--}}
                                                        {{--@endif--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                            </tbody>
                                        </table>
                                        </br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir PO</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br>
                                <div class="col l12 m12 s12">
                                    <button id="tambah-barang" data-target="modal-tambah-barang" class="btn-stoko btn-stoko-primary modal-trigger">Tambah Barang</button>
                                    <button id="tambah-discount" data-target="modal-tambah-discount" class="btn-stoko btn-stoko-primary modal-trigger">Tambah Discount</button>
                                </div>
                                <br><br>
                                <form id="formPo">
                                    <div class="input-field col l3 m12 s12">
                                        <label>No PO</label>
                                        <input id="idpurchaseorder" name="id" type="text" class="f-input" hidden disabled>
                                        <input id="nopurchaseorder" name="nopo" type="text" class="f-input" placeholder="No PO">
                                    </div>
                                    <div class="input-field col l3 m12 s12">
                                        <label>Supplier</label>
                                        <select id="supplier" name="supplier" class="browser-default selectpicker" data-live-search="true" data-size="5">
                                            <option value="0">Select Supplier</option>
                                            @foreach($data['supplier'] as $key => $value)
                                                <option value="{{$value->supplier_id}}">{{$value->company_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input-field col l3 m12 s12">
                                        <label>Pembayaran</label>
                                        <select id="pembayaran" class="selectpicker browser-default" data-live-search="true" data-size="5"   name="pembayaran">
                                            @foreach($data['payment_term'] as $key => $value)
                                                <option value="{{$value->payment_type_id}}">{{$value->payment_description}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input-field col l3 m12 s12">
                                        <label>Jatuh Tempo</label>
                                        <input id="terms" name="terms" type="text" class="f-input" placeholder="Terms">
                                    </div>
                                    <div class="input-field col l3 m12 s12">
                                        <label>Tgl PO</label>
                                        <input id="tglpo" name="tglpo" type="text" class="f-input" placeholder="Tgl PO">
                                    </div>
                                    <div class="input-field col l3 m12 s12">
                                        <label>Notes</label>
                                        <input id="notes" name="notes" type="text" class="f-input" placeholder="Notes">
                                    </div>
                                    <div class="input-field col l3 m12 s12">
                                        <label>Discount</label>
                                        <input id="discount-type" type="text" class="f-input" name="discounttype" value="0" hidden>
                                        <input id="discount" type="text" class="f-input" name="discount" placeholder="Discount" disabled>
                                    </div>
                                    <div class="input-field col l3 m12 s12">
                                        <label>Jenis PKP</label>
                                        <select id="pkp" class="selectpicker browser-default" data-live-search="true" data-size="5" name="pkp">
                                            <option value="0">Non</option>
                                            <option value="1">PKP</option>
                                        </select>
                                    </div>
                                    <div class="col l12 m12 s12">
                                        <div class="table-responsive">
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader" style="width: 220px">Nama</th>
                                                    <th class="theader" style="width: 100px">Keterangan</th>
                                                    <th class="theader" style="width: 60px">Qty</th>
                                                    <th class="theader" style="width: 60px">Qty Gratis</th>
                                                    <th class="theader">Harga</th>
                                                    <th class="theader" style="width: 50px">D1 (%)</th>
                                                    <th class="theader" style="width: 50px">D2 (%)</th>
                                                    <th class="theader" style="width: 50px">D3 (%)</th>
                                                    <th class="theader" style="width: 80px">D4 <br> (Rp)</th>
                                                    <th class="theader">Sub Total</th>
                                                    <th class="theader" style="width: 30px">T</th>
                                                </tr>
                                                </thead>
                                                <tbody id="barang-data">
                                                <tr id="no-item"><td colspan="11"><span> No Item Selected</span></td></tr>
                                                <tr class="barang-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="detail-po-1" type="text" class="f-input" name="detailpo[]" hidden>
                                                            <input id="id-1" type="text" class="f-input id" name="idbarang[]" hidden>
                                                            <input id="nama-1" type="text" class="f-input nama">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="detail-note-1" type="text" class="f-input detailnote" name="detailnote[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="qty-1" type="text" class="f-input qty" name="qty[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="free-1" type="text" class="f-input free" name="free[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="harga-1" type="text" class="f-input harga" name="harga[]">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="disc1-1" type="text" class="f-input disc1" name="disc1[]" value="0">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="disc2-1" type="text" class="f-input disc2" name="disc2[]" value="0">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="disc3-1" type="text" class="f-input disc3" name="disc3[]" value="0">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="disc-nominal-1" type="text" class="f-input discnominal" name="discnominal[]" value="0">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input type="text" class="f-input subtotal-show">
                                                            <input id="subtotal-1" type="text" class="f-input subtotal" name="subtotal[]" hidden>
                                                        </div>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <a class="btn btn-sm white grey-text copy-detail" style="margin: 5px"><i class="material-icons">content_copy</i></a>
                                                        <a class="btn btn-sm white grey-text delete-detail" style="margin: 5px"><i class="material-icons">delete</i></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <br>
                                            <div class="input-field" class="">
                                                <input id="grandtotal" type="text" class="f-input" disabled>
                                                <input id="grandtotal-hidden" type="text" class="f-input" name="grandtotal" hidden>
                                                <label>Grand Total</label>
                                            </div>
                                            <div id="append-total-after-disc">
                                                <div class="input-field total-after-disc-row" hidden>
                                                    <input id="total-after-disc-1" type="text" class="f-input" disabled>
                                                    <label id="label-total-after-disc-1">Total After Discount 1</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col l4 margin-top">
                                        <a id="submit-po" href="#!" mode="save" class="waves-effect btn-stoko teal white-text">simpan</a>
                                        <a id="edit-po" href="#!" mode="edit" class="waves-effect btn-stoko teal white-text" hidden>edit</a>
                                        <a id="clear" href="#!" class="waves-effect btn-stoko orange white-text">Batal</a>
                                        <a id="cetak-po" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak PO</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<div id="modal-tambah-barang" class="modal">
    <form id="form-list-barang">
        <div class="modal-content">
            <h4>Tambah Barang</h4>
            <div id="append-barang" class="row">

            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-barang">OK</a>
            <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
        </div>
    </form>
</div>

<div id="modal-tambah-discount" class="modal">
    <form id="form-discount">
        <div id="append-disc" class="modal-content">
            <h4>Tambah Discount</h4>
            <div class="row">
                <div class="col l3 m12 s12">
                    <p class="disc-type" style="font-size: 18px; margin-top: 20px">Discount Type</p>
                </div>
                <div class="input-field  col l2">
                    <input name="type" type="radio" id="typeNominal" value="1"/>
                    <label for="typeNominal">Nominal</label>
                </div>
                <div class="input-field  col l2">
                    <input name="type" type="radio" id="typePercent" value="2"/>
                    <label for="typePercent">Percent</label>
                </div>
            </div>
            <div class="row disc-row">
                <tr>
                    <div class="col l3 m12 s12">
                        <p class="disc-no" style="font-size: 18px; margin-top: 20px">Discount 1</p>
                    </div>
                    <div class="input-field col l3 m12 s12">
                        <label>Discount</label>
                        <input id="discount-1" name="discount[]" type="text" class="f-input discount" placeholder="discount">
                    </div>
                    <div class="input-field col l3 m12 s12">
                        <label>Discount Period</label>
                        <input id="period-1" name="period[]" type="text" class="f-input period" placeholder="Period">
                    </div>
                    <div class="input-field col l3 m12 s12">
                        <label></label>
                        <a href="#!" class="add-disc btn-flat green white-text no-border-radius">Add</a>
                        <a href="#!" class="remove-disc btn-flat red white-text no-border-radius" style="display: none;">Remove</a>
                    </div>
                </tr>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="waves-effect btn-flat red white-text no-border-radius submit-disc">OK</a>
            <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
        </div>
    </form>
</div>

<!-- Modal Delete Purchase Request -->
<div id="modal-delete-po" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-po" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        firstload();

        $('.bootstrap-select>.dropdown-toggle').on('click',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $(this).closest('.bootstrap-select').addClass("open");
            $(this).attr('aria-expanded',true);
        });

        $('#tambah-barang').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var jumlah = $('.barang-row').length;
            var barang = [];
            var jenispkp = $('#pkp').val();
            var src = "po";
            $('.id').each(function(k,v){
                var id = $(this).val();
                barang.push(id);
            });
            $.ajax({
                type:"GET",
                url:"modalListBarang",
                data:{barang:barang, src:src, jenispkp:jenispkp},
                success:function(response){
                    $('#append-barang').html(response);
                },
                complete:function(){
                    $('#modal-tambah-barang').modal('open');
                }
            })
        });

        $('#pkp').on('change',function(){
            var $original = $('.barang-row:first');
            var $cloned = $original.clone();
            $('.barang-row').remove();
            $cloned.attr('hidden',true);
            $cloned.find('input[type="text"]').val('');
            $cloned.appendTo('#barang-data');
            $('#no-item').removeAttr('hidden');
        });

        $('body').on('click','#poTable.highlight tr, #poTable .edit',function(event){
            event.stopImmediatePropagation();
            console.log($(event.currentTarget).attr('mode'));
            var mode = $(event.currentTarget).attr('mode');
            var id = $(this).attr('value');
            $.ajax({
                type:"GET",
                url:"getpurchaseorder",
                data:{id:id},
                success:function(response){
                    console.log(response);
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('#idpurchaseorder').val(response.po[0].purchase_order_id).attr('disabled',true);
                    $('#nopurchaseorder').val(response.po[0].purchase_order_number).attr('disabled',true);
                    $('#tglpo').val(moment(response.po[0].date_purchase_order,'YYYY-MM-DD').format('DD-MM-YYYY')).attr('disabled',true);
                    $('#pkp').val(response.po[0].is_pkp).attr('disabled',true);
                    $('#supplier').val(response.po[0].supplier_id).attr('disabled',true);
                    $('#notes').val(response.po[0].note).attr('disabled',true);
                    $('#pembayaran').val(response.po[0].payment_type_id).attr('disabled',true);
                    $('#terms').val(response.po[0].payment_term).attr('disabled',true);
                    $('#grandtotal-hidden').val(response.po[0].grand_total_idr);
                    $('#tambah-barang').attr('disabled',true).css('cursor','not-allowed');
                    $('#tambah-discount').attr('disabled',true).css('cursor','not-allowed');
                    for(var i=0; i<response.po.length; i++){
                        var newid = "id-"+(i+1);
                        var newnama = "nama-"+(i+1);
                        var newdetailnote = "detail-note-"+(i+1);
                        var newqty = "qty-"+(i+1);
                        var newfree = "free-"+(i+1);
                        var newweight = "weight-"+(i+1);
                        var newharga = "harga-"+(i+1);
                        var newdisc1 = "disc1-"+(i+1);
                        var newdisc2 = "disc2-"+(i+1);
                        var newdisc3 = "disc3-"+(i+1);
                        var newdiscnominal = "disc-nominal-"+(i+1);
                        var newsubtotal = "subtotal-"+(i+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#nama-1').attr('id',newnama);
                        $temp.find('#detail-note-1').attr('id',newdetailnote);
                        $temp.find('#free-1').attr('id',newfree);
                        $temp.find('#qty-1').attr('id',newqty);
                        $temp.find('#weight-1').attr('id',newweight);
                        $temp.find('#harga-1').attr('id',newharga);
                        $temp.find('#disc1-1').attr('id',newdisc1);
                        $temp.find('#disc2-1').attr('id',newdisc2);
                        $temp.find('#disc3-1').attr('id',newdisc3);
                        $temp.find('#disc-nominal-1').attr('id',newdiscnominal);
                        $temp.find('#subtotal-1').attr('id',newsubtotal);
                        $temp.appendTo('#barang-data')
                        $('#id-'+(i+1)).val(response.po[i].product_id);
                        $('#subtotal-'+(i+1)).val(response.po[i].sub_total).attr('disabled',true);
                        $('#nama-'+(i+1)).val(response.po[i].p_name).attr('disabled',true);
                        $('#detail-note-'+(i+1)).val(response.po[i].detail_note).attr('disabled',true);
                        $('#free-'+(i+1)).val(response.po[i].free_qty?response.po[i].free_qty:0).attr('disabled',true);
                        $('#qty-'+(i+1)).val(response.po[i].quantity).attr('disabled',true);
                        $('#weight-'+(i+1)).val(response.po[i].weight).attr('disabled',true);
                        $('#harga-'+(i+1)).val(response.po[i].price).attr('disabled',true);
                        $('#disc1-'+(i+1)).val(response.po[i].discount_1).attr('disabled',true);
                        $('#disc2-'+(i+1)).val(response.po[i].discount_2).attr('disabled',true);
                        $('#disc3-'+(i+1)).val(response.po[i].discount_3).attr('disabled',true);
                        $('#disc-nominal-'+(i+1)).val(response.po[i].discount_nominal).attr('disabled',true);
                        $('#no-item').attr('hidden',true);
                        $('.subtotal').each(function(){
                            $(this).closest('tr').find('.subtotal-show').val(accounting.formatMoney($(this).val(),'Rp. ',2,',','.')).attr('disabled',true);
                        })
                    }
                    if (response.discount) {
                        var text = "";
                        var discount = [];
                        $('#discount-type').val(response.po[0].discount_type);
                        $('input[name="type"][value="'+response.po[0].discount_type+'"]').attr("checked",true);
                        var $disc = $('.disc-row:first');
                        $('.disc-row').remove();
                        for(var i=0; i<response.discount.length; i++){
                            var $newdiscount = "discount-"+(i+1);
                            var $newPeriod = "period-"+(i+1);
                            var $cloned = $disc.clone().addClass('cloned');
                            $cloned.find("#discount-1").attr('id',$newdiscount);
                            $cloned.find("#period-1").attr('id',$newPeriod);
                            $cloned.find("#discount-"+(i+1)).val(response.discount[i].discount);
                            $cloned.find("#period-"+(i+1)).val(response.discount[i].discount_period);
                            $cloned.find(".add-disc").remove();
                            $cloned.find(".remove-disc").css('display','inline-block');
                            $cloned.appendTo('#append-disc');

                            if(response.po[0].discount_type == 1)
                            {
                                text += "Rp"+response.discount[i].discount+"/"+response.discount[i].discount_period;
                            }else{
                                text += response.discount[i].discount+"%/"+response.discount[i].discount_period;
                            }
                            if(response.discount.length-1){
                                text +="; ";
                            }

                            discount.push(response.discount[i].discount);
                        }
                        $('#discount').val(text);
                        getTotalDiscount(discount);
                    }
                    else{
                        $('#discount').val('no discount');
                        $('.total-after-disc-row').attr('hidden',true);
                    }
                    $('.selectpicker').selectpicker('refresh');
                },
                complete:function(){
                    var sum = 0;
                    $('.subtotal').each(function(){
                        if ($(this).val() > 0)
                        {
                            sum += parseFloat($(this).val())
                        }
                    });
                    $('#grandtotal').val(accounting.formatMoney(sum,'Rp. ',2,',','.'));
                    $('#grandtotal-hidden').val(sum);
                    $('#submit-po').attr('hidden',true);
                    $('#cetak-po').removeAttr('hidden').attr('href',"downloadpurchaseorder/"+id);
                    if(mode == "edit")
                    {
                        $('.disc1, .disc2, .disc3 , .discnominal').removeAttr('disabled');
                        $('#nopurchaseorder ,#supplier, #pembayaran, #tglpo, #tambah-barang, #tambah-discount, .free, .harga, .qty, .detailnote').removeAttr('disabled');
                        $("input[name=ispkp]").removeAttr('disabled');
                        $('#tambah-discount').css('cursor','pointer');
                        $('#pembayaran').trigger('change');
                        $('#edit-po').removeAttr('hidden');
                        $('#submit-po').attr('hidden',true);
                        $('#tambah-barang').attr('hidden',true);
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
        })

        $('.submit-barang').on('click', function(event){
            event.stopImmediatePropagation();
            modalBarangTable.search('').draw();
            var barang =[];
            modalBarangTable.rows().nodes().to$().find('input[type="checkbox"]:checked').each(function(){
                console.log($(this).closest('tr').attr('value'));
                barang.push($(this).closest('tr').attr('value'));
            });
            $.ajax({
                type:"GET",
                url:"getBarangModal",
                data: {barang:barang},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    var array = response.barang;
                    console.log(array.length);
                    if(array.length > 0)
                    {
                        for(var i=0; i<array.length; i++){
                            var newid = "id-"+(i+1);
                            var newnama = "nama-"+(i+1);
                            var newdetailnote = "detail-note-"+(i+1);
                            var newfree = "free-"+(i+1);
                            var newqty = "qty-"+(i+1);
                            var newdisc1 = "disc1-"+(i+1);
                            var newdisc2 = "disc2-"+(i+1);
                            var newdisc3 = "disc3-"+(i+1);
                            var newdiscnominal = "disc-nominal-"+(i+1);
                            var newweight = "weight-"+(i+1);
                            var newharga = "harga-"+(i+1);
                            var newsubtotal = "subtotal-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('.id').attr('id',newid);
                            $temp.find('.nama').attr('id',newnama);
                            $temp.find('.detailnote').attr('id',newdetailnote);
                            $temp.find('.free').attr('id',newfree);
                            $temp.find('.qty').attr('id',newqty);
                            $temp.find('.harga').attr('id',newharga);
                            $temp.find('.disc1').attr('id',newdisc1);
                            $temp.find('.disc2').attr('id',newdisc2);
                            $temp.find('.disc3').attr('id',newdisc2);
                            $temp.find('.discnominal').attr('id',newdiscnominal);
                            $temp.find('.subtotal').attr('id',newsubtotal);
                            $temp.appendTo('#barang-data')
                            $('#id-'+(i+1)).val(response.id[i]);
                            $('#subtotal-'+(i+1)).attr('disabled',true);
                            $('#nama-'+(i+1)).val(response.nama[i]).attr('disabled',true).closest('td').attr('title',response.nama[i]);
                            $('#weight-'+(i+1)).val(response.weight[i]).attr('disabled',true);
                            $('#no-item').attr('hidden',true);
                        }
                    }else{
                        $original.clone();
                        $original.attr('hidden',true)
                        $original.appendTo('#barang-data');
                        $('#formSo').find("input[type=text], textarea").val("");
                        $('#no-item').removeAttr('hidden');
                    }
                    $('#grandtotal').val(0);
                    $('#modal-tambah-barang').modal('close');
                }
            });
        });

        $('#submit-po, #edit-po').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            var kode = $('#nopurchaseorder').val();
            var validqty = [];
            var validharga = [];
            var url = "";
            var mode = $(event.currentTarget).attr('mode');
            if( mode == 'save')
            {
                var url = "createpurchaseorder";
                var successmessage = 'Purchase Order '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatepurchaseorder";
                var successmessage = 'Purchase Order '+kode+' telah berhasil diubah!';
            }

            $('.barang-row').each(function(){
                validqty.push(ceknumber($(this).find('.qty').val(),false));
                validharga.push(ceknumber($(this).find('.harga').val(),false));
            });

            var validnopo = "true";
            $.ajax({
                type:"GET",
                url:"checkponumber",
                async:false,
                data:{nopo:$('#nopurchaseorder').val()},
                success:function(response){
                    validnopo = response;
                }
            });


            if($('.barang-row:first').attr('hidden') == 'hidden' && $('.barang-row:first').attr('deleted') != 'true')
            {
                toastr.warning('Anda Belum Memilih Barang!');
            }else if(validnopo == "false" && mode == 'save'){
                toastr.warning('No Po sudah pernah dibuat sebelumnya!');
            }else if($('#nopurchaseorder').val() == ""){
                toastr.warning('No Pembelian Harus di Isi!');
            }else if($('#supplier').val() == 0){
                toastr.warning('Anda Belum Memilih Supplier!');
            }else if(validqty.indexOf(false) > -1){
                toastr.warning('Quantity harus berupa angka!');
            }else if(validharga.indexOf(false) > -1){
                toastr.warning('Masukkan Harga dengan Benar!');
            }else if($('#pembayaran').val() == 2 && ceknumber($('#terms').val()) == false){
                toastr.warning('Jatuh Tempo harus angka')
            }else if($('#pembayaran').val() == 2 && ceknumber($('#terms').val()) == false){
                toastr.warning('Jatuh Tempo harus angka')
            }
            else{ // validasi sukses
                me.invisible();
                me.data('requestRunning', true);
                $('.harga, #idpurchaseorder, #pembayaran, #nopurchaseorder, .subtotal, #terms, #discount').removeAttr('disabled');
                $.ajax({
                    type:"POST",
                    url:url,
                    data:$(this).closest('#formPo').serialize(),
                    success:function(response){
                        toastr.success(successmessage ,{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                })
            }
        });

        $('body').on('change', '#pembayaran', function(event){
            event.stopImmediatePropagation();
            console.log('test');
            if($(this).val() == 1){
                $('#terms').attr('disabled',true);
                $('#terms').val(1);
                $('#tambah-discount').attr('disabled',true).css('cursor','not-allowed');
            }else{
                $('#terms').removeAttr('disabled');
                $('#tambah-discount').removeAttr('disabled').css('cursor','pointer');
            }
        })

        $('body').on('keyup', '#formPo .qty, .harga, .disc1 , .disc2, .disc3, .discnominal', function(event){
            event.stopImmediatePropagation();
            var qty = $(this).closest('tr').find('.qty').val();
            var price = $(this).closest('tr').find('.harga').val();
            var disc1 = $(this).closest('tr').find('.disc1').val() != '' ? $(this).closest('tr').find('.disc1').val(): 0;
            var disc2 = $(this).closest('tr').find('.disc2').val() != '' ? $(this).closest('tr').find('.disc2').val() : 0;
            var disc3 = $(this).closest('tr').find('.disc3').val() != '' ? $(this).closest('tr').find('.disc3').val() : 0;
            var discnominal = $(this).closest('tr').find('.discnominal').val() != '' ? parseInt($(this).closest('tr').find('.discnominal').val()) : 0;
            console.log(discnominal);

            var discountedprice = price - (disc1 /100 * price);
            discountedprice = discountedprice - (disc2 / 100 * discountedprice);
            discountedprice = discountedprice - (disc3 / 100 * discountedprice);
            discountedprice = discountedprice - discnominal;

            $(this).closest('tr').find('.subtotal').val(discountedprice*qty);
            $(this).closest('tr').find('.subtotal-show').val(accounting.formatMoney(discountedprice*qty,'Rp. ',2,',','.')).attr('disabled',true);

            var sum = 0;
            $('.subtotal').each(function(){
                if ($(this).val() > 0)
                {
                    sum += parseFloat($(this).val())
                }
            })
            $('#grandtotal-hidden').val(sum);
            $('#grandtotal').val(accounting.formatMoney(sum,'Rp. ',2,',','.'));
            getTotalDiscount();
        })

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click();
        });

        $('body').on('click','.add-disc',function(event){
            event.stopImmediatePropagation();
            var length = $(".disc-row").length;
            initNumber = length+1;
            var $newPercent = "percent-"+initNumber;
            var $newPeriod = "period-"+initNumber;

            if(length < 3)
            {
                var $original = $('.disc-row:first');
                var $cloned = $original.clone().addClass('cloned');
                $cloned.find("#percent-1").attr('id',$newPercent);
                $cloned.find("#period-1").attr('id',$newPeriod);
                $cloned.find(".add-disc").remove();
                $cloned.find(".remove-disc").css('display','inline-block');
                console.log($original);
                $cloned.appendTo('#append-disc');
            }

            $('.disc-row').each(function(i,v){
                $(this).find('.disc-no').html('Discount'+(i+1));
            })
        });

        $('body').on('click','.remove-disc',function(event){
            event.stopImmediatePropagation();
            $(this).closest('.disc-row').remove();
            $('.disc-row').each(function(i,v){
                $(this).find('.disc-no').html('Discount'+(i+1));
            })
        });

        $('.submit-disc').on('click',function(event){
            event.preventDefault();
            var length = $('.disc-row').length;
            var text ="";
            var discounttype = $("input[type=radio][name=type]:checked").attr('value');
            var firstperiod = $('.period:first').val();
            var firstdiscount = $('.discount:first').val();
            var validdiscount = [];
            var validperiod = [];
            var periodarray = [];
            $('.disc-row').each(function(){
                periodarray.push(parseInt($(this).find('.period').val()));
                validdiscount.push(ceknumber($(this).find('.discount').val(), "nullable"));
                validperiod.push(ceknumber($(this).find('.period').val()), "nullable");
            });

            if(length == 1 && firstdiscount == "" && firstperiod =="")
            {
                $('#modal-tambah-discount').modal('close');
                $('#pembayaran').val(1).removeAttr('disabled').trigger('change');
                $('#discount-type').val(0);
                $('.selectpicker').selectpicker('refresh');
            }
            else if(validperiod.indexOf(false) > -1){
                toastr.warning('Periode harus angka')
            }else if(validdiscount.indexOf(false) > -1){
                toastr.warning('Diskon harus angka')
            }else{
                $('.disc-row').each(function(i,v){
                    var diskon = $(this).find('.discount').val();
                    var period = $(this).find('.period').val();
                    if(discounttype == 1)
                    {
                        text += "Rp"+diskon+"/"+period;
                    }else{
                        text += diskon+"%/"+period;
                    }
                    if(i<length-1){
                        text +="; ";
                    }
                })
                var highestperiod = Math.max.apply(Math, periodarray);
                $('#terms').val(highestperiod);
                $('#discount').val(text);
                $('#discount-type').val(discounttype);
                $('#pembayaran').val(2).attr('disabled',true).trigger('change');
                $('.selectpicker').selectpicker('refresh');
                $('#modal-tambah-discount').modal('close');
                getTotalDiscount();
            }
        });

        $('body').on('click','.delete-detail',function(event){
            event.stopImmediatePropagation();
            if($('.barang-row').length > 1)
            {
                $(this).closest('tr').remove();
            }else{
                $(this).closest('tr').find('.nama').val("").removeAttr('disabled');
                $(this).closest('tr').find('.id').val("").removeAttr('disabled');
            }
        });

        $('body').on('click','.copy-detail',function(event){
            event.stopImmediatePropagation();
            var $original = $(this).closest('.barang-row');
            var $cloned = $original.clone();
            console.log($cloned);
            $(this).closest('.barang-row').after($cloned);

        });

        $('body').on('click','#poTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var nopo = $(this).closest('tr').find('.nopo').html();
            var idpo = $(this).closest('tr').attr('value');
            $('#confirm-delete-po').attr('value',idpo).attr('nomor',nopo);
            $("#delete-message").html("Yakin ingin menghapus data "+nopo+" ?")
            $('#modal-delete-po').modal('open');
        });

        $('#confirm-delete-po').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-po').modal('close');
            var id = $(this).attr('value');
            var nopo = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepurchaseorder",
                data:{id:id},
                success:function(response){
                    toastr.success('Purchase Order '+nopo+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        function firstload()
        {

            $('#modalBarangTable').attr('datatable', false);

            $('#terms').val(0).attr('disabled',true);

            $('.selectpicker').selectpicker('render');

            $('#pembayaran').trigger('change');

            $('#tglpo').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tglpo').val(moment().format('DD-MM-YYYY'));

            $('.number').each(function(){
                console.log($(this).html());
                $(this).html(accounting.formatMoney($(this).html(),'Rp. ',2,',','.'));
            });
            /*$('.total-col').html(accounting.formatMoney($(this).html(),'Rp.',2,',','.'));*/
            /*$('#nosalesorder').val("SO/"+moment().format('DDMMYY')+)*/
            // $.ajax({
            //   type:"GET",
            //   url:"lastpurchaseordernumber",
            //   success:function(response){
            //     $('#nopurchaseorder').val(response);
            //   }
            // })

            var poTable = $('#poTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getpurchaseordertable',
                    data : function (d){
                        d.nopo = $('#filterPo').val();
                        d.supplier = $('#filterSupplier').val();
                    }
                },
                rowId : 'purchase_order_id',
                columns: [
                    { data: 'purchase_order_number', name: 'purchase_order_number', class:'nopo'},
                    { data: 'date_purchase_order', name: 'date_purchase_order'},
                    { data: 'company_name', name: 'company_name'},
                    { data: 'payment_description', name: 'payment_description'},
                    { data: 'grand_total_idr', name: 'grand_total_idr'},
                    { data: 'jatuh_tempo', name: 'jatuh_tempo'},
                    { data: 'is_pkp', name: 'is_pkp'},
                    { data: 'action', name: 'action'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterPo').on('keyup', function () { // This is for news page
                poTable.draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                poTable.draw();
            });

        }

        function getTotalDiscount(discount){
            var $original = $('.total-after-disc-row:first');
            var $cloned = $original.clone();
            $('.total-after-disc-row').remove();

            var disc = [];
            $('.discount').each(function(i,v){
                disc.push($(this).val());
            });
            disc = discount || disc;
            var type = $('input[name="type"]:checked').val();
            var total = $('#grandtotal-hidden').val();

            for(var i=0; i<disc.length; i++){
                var newlabel = "label-total-after-disc-"+(i+1);
                var newdiscount = "total-after-disc-"+(i+1);
                $temp = $original.clone();
                $temp.removeAttr('hidden');
                $temp.find('#label-total-after-disc-1').attr('id',newlabel);
                $temp.find('#total-after-disc-1').attr('id',newdiscount);
                $temp.appendTo('#append-total-after-disc');
                if(type == 1) // nominal
                {
                    $('#label-total-after-disc-'+(i+1)).html("Grand Total After Disc "+(i+1));
                    $('#total-after-disc-'+(i+1)).val(accounting.formatMoney(total - disc[i], 'Rp. ',2,',','.'));
                }else{
                    $('#label-total-after-disc-'+(i+1)).html("Grand Total After Disc "+(i+1));
                    $('#total-after-disc-'+(i+1)).val(accounting.formatMoney(total - (disc[i] / 100 * total), 'Rp. ',2,',','.'));
                }
            }
        }
    });

</script>