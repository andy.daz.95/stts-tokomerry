<br>
<table class="stoko-table no-border">
	<thead>
		<tr>
			<th class="theader">Nama Barang</th>
			<th class="theader">Quantity Penerimaan</th>
			<th class="theader" style="width:200px">Gudang</th>
		</tr>
	</thead>
	<tbody id="barang-data">
		@if($historygr->count() < 1)
		<tr id="no-history"><td colspan="3"><span> No History</span></td></tr>
		@else
		@foreach($historygr as $key => $value)
		<tr>
			<td colspan='3'>{{$value->good_receive_code}}</td>
			@foreach($value->details as $key=>$value)
			<tr>
				<td>{{$value->product->product_name}}</td>
				<td>{{$value->quantity_received}}</td>
				<td>{{$value->warehouse->warehouse_name}}</td>
			</tr>
			@endforeach
		</tr>
		@endforeach
		@endif
	</tbody>
</table>