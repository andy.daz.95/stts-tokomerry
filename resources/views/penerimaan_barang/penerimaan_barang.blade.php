<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Penerimaan Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l3">
                  <input id="filterPenerimaan" type="text" class="f-input">
                  <label>Filter Penerimaan No</label>
                </div>
                <div class="input-field col l3">
                  <input id="filterPo" type="text" class="f-input">
                  <label>Filter PO</label>
                </div>
                <div class="input-field col l3">
                  <input id="filterSupplier" type="text" class="f-input">
                  <label>Filter Supplier</label>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="penerimaanTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th>No BPB</th>
                        <th>No Purchase Order</th>
                        <th>Tanggal</th>
                        <th>Supplier</th>
                        <th>Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($data['bpb'] as $key => $value)--}}
                      {{--<tr mode="view" value="{{$value->good_receive_id}}">--}}
                      {{--<td class="nobpb">{{$value->good_receive_code}}</td>--}}
                      {{--<td>{{$value->purchase_order_number}}</td>--}}
                      {{--<td>{{$value->good_receive_date}}</td>--}}
                      {{--<td>{{$value->company_name}}</td>--}}
                      {{--<td>--}}
                      {{--@if(Session('roles')->name == 'master')--}}
                      {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->good_receive_id}}"><i class="material-icons">edit</i></a>--}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                      {{--</td>--}}
                      {{--@endif--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                    </br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Penerimaan Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formBpb">
                  <div class="input-field col l3">
                    <input id="bpbnumber" type="text" class="f-input" name="donumber" disabled>
                    <input id="idbpb" name="id" type="text" class="f-input" hidden disabled>
                    <label>No. Penerimaan Barang</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="nopo" name="po" class="selectpicker browser-default" data-live-search="true" data-size="5"  data-size="5"  data-size="5" >
                      <option value="0">Select PO</option>
                      @foreach($data['po'] as $key => $value)
                        <option value="{{$value->purchase_order_id}}">{{$value->purchase_order_number.' - '.$value->supplier->company_name}}</option>
                      @endforeach
                    </select>
                    <label>No Purchase Order</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="supplier" name="supplier" type="text" class="f-input" disabled>
                    <label>Nama Supplier</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="tglbpb" type="text" name="tglbpb" class="f-input">
                    <label>Tanggal</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="notes" name="notes" type="text" class="f-input">
                    <label>Notes</label>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader">Nama Barang</th>
                          <th class="theader">Quantity Pemesanan</th>
                          <th class="theader">Quantity Gratis</th>
                          <th class="retur-header" hidden>Quantity Retur</th>
                          <th class="theader">Quantity Telah Diterima</th>
                          <th class="theader" style="width:150px">Sisa Yang Harus Diterima</th>
                          <th class="theader">Quantity Penerimaan</th>
                          <th class="theader" style="width:200px">Gudang</th>
                          {{-- <th class="theader">Tindakan</th> --}}
                        </tr>
                        </thead>
                        <tbody id="barang-data">
                        <tr id="no-item"><td colspan="7"><span> No Item Selected</span></td></tr>
                        <tr class="barang-row" hidden>
                          <td>
                            <div class="input-field">
                              <input id="id-1" type="text" class="f-input" name="idbarang[]" hidden>
                              <input id="detail-po-1" type="text" class="f-input" name="detailpo[]" hidden>
                              <input id="detail-bpb-1" type="text" class="f-input" name="detailbpb[]" hidden>
                              <input id="nama-1" type="text" class="f-input" name="namabarang[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="order-qty-1" type="text" class="f-input orderqty" name="orderqty[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="free-qty-1" type="text" class="f-input freeqty" name="freeqty[]">
                            </div>
                          </td>
                          <td class="retur-data" hidden>
                            <div class="input-field">
                              <input id="retur-qty-1" type="text" class="f-input returqty" name="returqty[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="received-qty-1" type="text" class="f-input receivedqty" name="receivedqty[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="remaining-qty-1" type="text" class="f-input remainingqty" name="remainingqty[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="earned-qty-1" type="text" class="f-input earnedqty" name="earnedqty[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <select id="gudang-1" class="f-select gudang" name="gudang[]">
                                @foreach($data['warehouse'] as $key => $value)
                                  <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                    <br>
                    <ul class="collapsible" data-collapsible="accordion">
                      <li>
                        <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>History Penerimaan Barang</div>
                        <div class="collapsible-body">
                          <div class="container-fluid">
                            <div class="row append-history">

                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <div class="input-field right">
                      <a id="clear" href="" class="btn-stoko btn-stoko-primary orange">Batal</a>
                      <a id="edit-bpb" href="" mode="edit" class="btn-stoko btn-stoko-primary" hidden>Edit BPB</a>
                      <a id="submit-bpb" href="" mode="save" class="btn-stoko btn-stoko-primary">Submit BPB</a>
                      <a id="cetak-gr" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Penerimaan</a>

                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-bpb" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-bpb" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        // $.ajax({
        //     url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
        //     dataType: "script",
        // });

        firstload();

        $('.bootstrap-select>.dropdown-toggle').on('click',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $(this).closest('.bootstrap-select').addClass("open");
            $(this).attr('aria-expanded',true);
        })

        $('#nopo').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getpoforpenerimaan",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    if (response.length == 0) {
                        $original.clone();
                        $('.barang-row').remove();
                        $original.attr('hidden',true);
                        $original.appendTo('#barang-data');
                        $('#formBpb').find("input[type=text], textarea").val("");
                        $('#no-item').removeAttr('hidden');
                        reset();
                    }else{
                        $('#supplier').val(response['podata'].s_name);
                        var $cloned = $original.clone();
                        $('.barang-row').remove();
                        $('.append-history').html('');
                        $('.append-history').append(response['history']);
                        for (var i=0; i<response['podetail'].length; i++)
                        {
                            var newid = "id-"+(i+1);
                            var newnama = "nama-"+(i+1);
                            var neworderqty = "order-qty-"+(i+1);
                            var newdetailpo = "detail-po-"+(i+1);
                            var newfreeqty = "free-qty-"+(i+1);
                            var newreturqty = "retur-qty-"+(i+1);
                            var newreceivedqty = "received-qty-"+(i+1);
                            var newremainingqty = "remaining-qty-"+(i+1);
                            var newearnedqty = "earned-qty-"+(i+1);
                            var newsendqty = "send-qty-"+(i+1);
                            var newgudang = "gudang-"+(i+1);
                            var newharga = "harga-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#detail-po-1').attr('id',newdetailpo);
                            $temp.find('#nama-1').attr('id',newnama);
                            $temp.find('#send-qty-1').attr('id',newsendqty);
                            $temp.find('#received-qty-1').attr('id',newreceivedqty);
                            $temp.find('#remaining-qty-1').attr('id',newremainingqty);
                            $temp.find('#retur-qty-1').attr('id',newreturqty);
                            $temp.find('#earned-qty-1').attr('id',newearnedqty);
                            $temp.find('#order-qty-1').attr('id',neworderqty);
                            $temp.find('#free-qty-1').attr('id',newfreeqty);
                            $temp.find('#gudang-1').attr('id',newgudang);
                            $temp.find('#harga-1').attr('id',newharga);
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(response['podetail'][i].product_id);
                            $('#detail-po-'+(i+1)).val(response['podetail'][i].purchase_order_details_id);
                            $('#nama-'+(i+1)).val(response['podetail'][i].p_name).attr('disabled',true).attr('title',response['podetail'][i].p_name);
                            $('#order-qty-'+(i+1)).val(response['podetail'][i].quantity).attr('disabled',true);
                            $('#free-qty-'+(i+1)).val(response['podetail'][i].free_qty).attr('disabled',true);
                            if(response['grdetail'].length == 0 || !response['grdetail'][i])
                            {
                                $('#received-qty-'+(i+1)).val(0).attr('disabled',true);
                            }
                            else{
                                $('#received-qty-'+(i+1)).val(response['grdetail'][i].sum).attr('disabled',true);
                            }
                            //validasi apakah ada retur barang atau gak kalau ada baru dimunculkan retur quantity nya.
                            if(response['returdetail'].length > 0)
                            {
                                $('.retur-header').removeAttr('hidden');
                                $('.retur-data').removeAttr('hidden');
                                $('#retur-qty-'+(i+1)).val(response['returdetail'][i].sum).attr('disabled', true);
                            }else{
                                $('#retur-qty-'+(i+1)).val(0).attr('disabled', true);
                                $('.retur-header').attr('hidden',true);
                                $('.retur-data').attr('hidden',true);
                            }
                            //di cek apakah barang sudah dikirim semua, kalau sudah di disabled dan valuenya kosong
                            if(parseFloat($('#received-qty-'+(i+1)).val()) >= parseFloat($('#order-qty-'+(i+1)).val()) + parseFloat($('#free-qty-'+(i+1)).val()) + parseFloat($('#retur-qty-'+(i+1)).val())) {
                                $('#earned-qty-'+(i+1)).val(0).attr('disabled',true);
                            }else{
                                $('#earned-qty-'+(i+1)).val(0).removeAttr('disabled');
                            }

                            $('#remaining-qty-'+(i+1)).val(parseFloat($('#order-qty-'+(i+1)).val()) + parseFloat($('#free-qty-'+(i+1)).val()) + parseFloat($('#retur-qty-'+(i+1)).val()) - parseFloat($('#received-qty-'+(i+1)).val())).attr('disabled',true);


                            $('#no-item').attr('hidden',true);

                            // var podate = moment(response['podata'].date_purchase_order, "YYYY-MM-DD").format("MM/DD/YYYY");
                            // $('#tglbpb').attr('data-mindate', podate);
                            var tglbpb = $('#tglbpb').datepicker().data('datepicker');
                            tglbpb.update('minDate', new Date(response['podata'].date_purchase_order));
                            tglbpb.selectDate(new Date(response['podata'].date_purchase_order));
                            // $('#tglbpb').datepicker();
                            $('.selectpicker').selectpicker('refresh');

                        }
                    }
                }
            });
        });

        $('#penerimaanTable').on('click', 'tr, .edit', function(event){
            event.stopImmediatePropagation();
            id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            $.ajax({
                type:"GET",
                url:"getpenerimaan",
                data:{id:id},
                success:function(response){
                    $('#supplier').val(response.company_name);
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    console.log(response);
                    console.log(response['bpb']);
                    $('#bpbnumber').val(response['bpb'][0].good_receive_code).attr('disabled',true);
                    $('#idbpb').val(response['bpb'][0].good_receive_id).attr('disabled',true);
                    $('#nopo').append('<option class="remove-when-clear" value="'+response['bpb'][0].purchase_order_id+'" selected="selected">'+response['bpb'][0].purchase_order_number+'</option>').attr('disabled',true);
                    $('#tglbpb').val(response['bpb'][0].good_receive_date).attr('disabled',true);
                    $('#supplier').val(response['bpb'][0].company_name).attr('disabled',true);
                    $('#notes').val(response['bpb'][0].note).attr('disabled',true);
                    $('#no-item').attr('hidden',true);
                    $('#barang-row').removeAttr('hidden');
                    for (var i=0; i<response.bpb.length; i++)
                    {
                        var newid = "id-"+(i+1);
                        var newdetail = "detail-bpb-"+(i+1);
                        var newdetailpo = "detail-po-"+(i+1);
                        var newnama = "nama-"+(i+1);
                        var neworderqty = "order-qty-"+(i+1);
                        var newfreeqty = "free-qty-"+(i+1);
                        var newreceivedqty = "received-qty-"+(i+1);
                        var newremainingqty = "remaining-qty-"+(i+1);
                        var newearnedqty = "earned-qty-"+(i+1);
                        var newreturqty = "retur-qty-"+(i+1);
                        var newgudang = "gudang-"+(i+1);
                        var newharga = "harga-"+(i+1);
                        var newgudang = "gudang-"+(i+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#detail-bpb-1').attr('id',newdetail);
                        $temp.find('#detail-po-1').attr('id',newdetailpo);
                        $temp.find('#nama-1').attr('id',newnama);
                        $temp.find('#earned-qty-1').attr('id',newearnedqty);
                        $temp.find('#received-qty-1').attr('id',newreceivedqty);
                        $temp.find('#remaining-qty-1').attr('id',newremainingqty);
                        $temp.find('#order-qty-1').attr('id',neworderqty);
                        $temp.find('#free-qty-1').attr('id',newfreeqty);
                        $temp.find('#retur-qty-1').attr('id',newreturqty);
                        $temp.find('#gudang-1').attr('id',newgudang);
                        $temp.find('#harga-1').attr('id',newharga);
                        $temp.find('#gudang-1').attr('id',newgudang);
                        $temp.appendTo('#barang-data');
                        $('#id-'+(i+1)).val(response.bpb[i].product_id);
                        $('#detail-bpb-'+(i+1)).val(response.bpb[i].good_receive_details_id);
                        $('#detail-po-'+(i+1)).val(response.bpb[i].purchase_order_details_id);
                        $('#nama-'+(i+1)).val(response.bpb[i].product_name).attr('disabled',true).attr('title',response.bpb[i].product_name);
                        $('#order-qty-'+(i+1)).val(response.bpb[i].quantity).attr('disabled',true);
                        $('#free-qty-'+(i+1)).val(response.bpb[i].free_qty?response.bpb[i].free_qty:0).attr('disabled',true);
                        $('#received-qty-'+(i+1)).val(response.bpb[i].total_receive).attr('disabled',true);
                        $('#earned-qty-'+(i+1)).val(response.bpb[i].quantity_received).attr('disabled',true);
                        $('#gudang-'+(i+1)).val(response.bpb[i].warehouse_id).attr('disabled',true);
                        if(response.bpb[i].total_retur > 0)
                        {
                            $('.retur-header').removeAttr('hidden');
                            $('.retur-data').removeAttr('hidden');
                            $('#retur-qty-'+(i+1)).val(response.bpb[i].total_retur).attr('disabled', true);
                        }else{
                            $('#retur-qty-'+(i+1)).val(0).attr('disabled', true);
                            $('.retur-header').attr('hidden',true);
                            $('.retur-data').attr('hidden',true);
                        }

                        $('#remaining-qty-'+(i+1)).val(parseFloat($('#order-qty-'+(i+1)).val()) + parseFloat($('#free-qty-'+(i+1)).val()) + parseFloat($('#retur-qty-'+(i+1)).val()) - parseFloat($('#received-qty-'+(i+1)).val())).attr('disabled',true);
                    }



                    $('.append-history').html('');
                    $('.append-history').append(response['history']);
                    $('.selectpicker').selectpicker('refresh');
                },complete:function(){
                    if(mode == "edit"){
                        $('#edit-bpb').removeAttr('hidden');
                        $('#tglbpb, .earnedqty, .gudang').removeAttr('disabled');
                    }else{
                        $('#cetak-gr').removeAttr('hidden').attr('href',"downloadpenerimaanbarang/"+id);
                    }
                    $('#submit-bpb').attr('hidden',true);
                }
            })
        });

        $('#submit-bpb, #edit-bpb').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            $('#bpbnumber,#idbpb, #nopo, .earnedqty').removeAttr('disabled');
            var kode = $('#bpbnumber').val();
            mode = $(event.currentTarget).attr('mode');
            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "createpenerimaanbarang";
                var successmessage = 'Penerimaan Barang '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatepenerimaanbarang";
                var successmessage = 'Penerimaan Barang '+kode+' telah berhasil diubah!';
            }

            var validqty = [];
            var lebihqty = [];
            var totalqty = 0;
            $('.barang-row').each(function(){
                var earnqty = parseFloat($(this).find('.earnedqty').val());
                var receivedqty = parseFloat($(this).find('.receivedqty').val());
                var returqty = parseFloat($(this).find('.returqty').val());
                var freeqty = parseFloat($(this).find('.freeqty').val());
                var orderqty = parseFloat($(this).find('.orderqty').val());
                validqty.push(ceknumber($(this).find('.earnedqty').val()));
                if(earnqty + receivedqty > orderqty+freeqty+returqty){
                    lebihqty.push(false);
                }else{
                    lebihqty.push(true);
                }
                totalqty += earnqty;
            });

            if($('#nopo').val() == 0){
                toastr.warning('Anda Belum Memilih Purchase Order!');
            }else if(totalqty == 0){
                toastr.warning('Quantity harus di isi!');
            }else if(validqty.indexOf(false) > -1){
                toastr.warning('Quantity harus berupa angka!');
            }else if(lebihqty.indexOf(false) > -1 && mode == 'save'){
                toastr.warning('Quantity melebihi jumalh order!');
            }else{
                me.invisible();
                me.data('requestRunning', true);
                $.ajax({
                    type:"POST",
                    url:url,
                    data:$(this).closest('#formBpb').serialize(),
                    success:function(response){
                        toastr.success(successmessage, {"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                })
            }
        })

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        $('body').on('click','.delete-detail',function(event){
            event.stopImmediatePropagation();
            $(this).closest('tr').remove();
        });

        $('#penerimaanTable').on('click', '.delete-modal' ,function(event){
            event.stopPropagation();
            event.stopImmediatePropagation();
            var nobpb = $(this).closest('tr').find('.nobpb').html();
            var idbpb = $(this).closest('tr').attr('value');
            $('#confirm-delete-bpb').attr('value',idbpb).attr('nomor',nobpb);
            $("#delete-message").html("Yakin ingin menghapus data "+nobpb+" ?")
            $('#modal-delete-bpb').modal('open');
        });

        $('#confirm-delete-bpb').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-bpb').modal('close');
            var id = $(this).attr('value');
            var nobpb = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepenerimaanbarang",
                data:{id:id},
                success:function(response){
                    toastr.success('Penerimaan Barang '+nobpb+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        function firstload(){
            var tglbpb = $('#tglbpb').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                minDate: new Date('2017-12-01'),
                language: 'en',
            }).data('datepicker');

            $('#tglbpb').val(moment().format('DD-MM-YYYY'));

            $('.selectpicker').selectpicker('render');

            $.ajax({
                type:"GET",
                url:"lastbpbnumber",
                success:function(response){
                    $('#bpbnumber').val(response);
                }
            })

            $('.number').each(function(){
                $(this).html(accounting.formatMoney($(this).html(),'Rp. ',2,',','.'));
            });

            var penerimaanTable = $('#penerimaanTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getpenerimaantable',
                    data : function (d){
                        d.nobpb = $('#filterPenerimaan').val();
                        d.nopo = $('#filterPo').val();
                        d.supplier = $('#filterSupplier').val();
                    }
                },
                rowId : 'good_receive_id',
                columns: [
                    { data: 'good_receive_code', name: 'good_receive_code', class:'nobpb'},
                    { data: 'purchase_order_number', name: 'purchase_order_number'},
                    { data: 'good_receive_date', name: 'good_receive_date'},
                    { data: 'company_name', name: 'company_name'},
                    { data: 'action', name: 'action'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterPenerimaan').on('keyup', function () { // This is for news page
                penerimaanTable.draw();
            });
            $('#filterPo').on('keyup', function () { // This is for news page
                penerimaanTable.draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                penerimaanTable.draw();
            });
        }

        function reset()
        {
            $('#nopo').val(0);
            $('#supplier').val("");
            $('#notes').val("");
            $('#tglbpb').val(moment().format('DD-MM-YYYY'));
        }
    });
</script>
