<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
</style>
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">Bukti Penerimaan Barang</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style="width:100%">
    <tr>
      <td style="width: 25%">NO</td>
      <td style="width: 25%">: {{$grheader->good_receive_code}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Tanggal Penerimaan</td>
      <td>: {{$grheader->good_receive_date}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Kepada</td>
      <td>: {{$grheader->company_name}}</td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr style="background-color: #8B8C89">
        <th class="color" style="text-align: center;">No</th>
        <th class="color" style="text-align: center;">Kode</th>
        <th class="color" style="text-align: center;">Satuan</th>
        <th class="color" style="text-align: center;">Nama Barang</th>
        <th class="color" style="text-align: center;">Ke Gudang</th>
        <th class="color" style="text-align: center;">Quantity</th>
      </tr>
    </thead>
    <tbody >
      @foreach($grdetail as $key => $value)
      <tr>
        <td>
          <span>{{$key+1}}</span>
        </td>
        <td>
          <span>{{$value->product_code}}</span>
        </td>
        <td>
          <span>{{$value->unit_description}}</span>
        </td>
        <td>
          <span>{{$value->product_name}}</span>
        </td>
        <td>
          <span>{{$value->warehouse_name}}</span>
        </td>
        <td>
          <span>{{$value->quantity_received}}</span>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>