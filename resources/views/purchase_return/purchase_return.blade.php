<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
    <div class="row">
        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Pengembalian Barang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br/>
                                <div class="input-field col l3">
                                    <label>Filter Retur</label>
                                    <input id="filterReturNumber" type="text" class="f-input" placeholder="Filter Retur" />
                                </div>
                                <div class="input-field col l3">
                                    <label>Filter supplier</label>
                                    <input id="filterSupplier" type="text" class="f-input" placeholder="Filter supplier" />
                                </div>
                                <div class="input-field col l3">
                                    <label>Filter SO</label>
                                    <input id="filterPoNumber" type="text" class="f-input" placeholder="Filter SO" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col l12 m12 s12">
                                    <div class="table-responsive">
                                        <table id="returTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                                            <thead>
                                            <tr>
                                                <th class="theader">No Retur</th>
                                                <th class="theader">Tipe Retur</th>
                                                <th class="theader">supplier</th>
                                                <th class="theader">No PO</th>
                                                <th class="theader">Tindakan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {{--@foreach($retur as $key => $value)--}}
                                                {{--<tr mode="view" value="{{$value->purchase_retur_id}}">--}}
                                                    {{--<td class="noretur">{{$value->retur_number}}</td>--}}
                                                    {{--<td>{{$value->retur_type_name}}</td>--}}
                                                    {{--<td>{{$value->company_name}}</td>--}}
                                                    {{--<td>{{$value->purchase_order_number}}</td>--}}
                                                    {{--<td>--}}
                                                        {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->purchase_retur_id}}"><i class="material-icons">edit</i></a>--}}
                                                        {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="col l12 m12 s12">
            <ul class="collapsible" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir Pengembalian Barang</div>
                    <div class="collapsible-body">
                        <div class="container-fluid">
                            <div class="row">
                                <br/>
                                <form id="formRetur">
                                    <div class="input-field col l3">
                                        <label>No Retur PO</label>
                                        <input id="noretur" type="text" name="noretur" class="f-input" disabled>
                                        <input id="idretur" type="text" name="idretur" class="f-input" hidden disabled>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>No PO</label>
                                        <select id="nopo" name="nopo" class="browser-default selectpicker" data-live-search="true" data-size="5"  >
                                            <option value="0">Pilih No PO</option>
                                            @foreach($po as $key => $value)
                                                <option value="{{$value->purchase_order_id}}">{{$value->purchase_order_number}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>supplier</label>
                                        <input id="supplier" type="text" name="supplier" class="f-input" placeholder="Prima PT" disabled>
                                    </div>
                                    <div class="input-field col l3">
                                        <label>Tanggal Retur</label>
                                        <input id="tglretur" type="text" name="tglretur" class="f-input" placeholder="Tanggal Retur">
                                    </div>
                                    <div class="input-field col l3">
                                        <label>Tipe Retur</label>
                                        <select id="returtype" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="returtype" disabled>
                                            <option value="0">Select Retur Type</option>
                                            @foreach($returtype as $key => $value)
                                                <option value="{{$value->retur_type_id}}">{{$value->retur_type_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div id="gudang-div" class="input-field col l3" hidden>
                                        <label>Gudang</label>
                                        <select id="gudang" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="gudang" disabled>
                                            <option value="0">Select Gudang</option>
                                            @foreach($warehouse as $key => $value)
                                                <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col l12 m12 s12 margin-top">
                                        <div class="table-responsive">
                                            <table class="stoko-table no-border">
                                                <thead>
                                                <tr>
                                                    <th class="theader">Kode Barang</th>
                                                    <th class="theader">Nama Barang</th>
                                                    <th class="theader">Retur Quota</th>
                                                    <th class="theader">Stock di Gudang</th>
                                                    <th class="theader">Price</th>
                                                    <th class="theader">Quantity</th>
                                                </tr>
                                                </thead>
                                                <tbody id="barang-data">
                                                <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                                                <tr class="barang-row" hidden>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="id-1" name="id[]" type="text" class="f-input idbarang" hidden>
                                                            <input id="kode-1" name="kode[]" type="text" class="f-input">
                                                            <input id="detail-retur-1" name="detailretur[]" type="text" class="f-input" hidden>
                                                            <input id="detail-po-1" name="detailpo[]" type="text" class="f-input" hidden>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="nama-1" name="namabarang[]" type="text" class="f-input">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="quota-1" name="quota[]" type="text" class="f-input quota">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="stock-1" name="stock[]" type="text" class="f-input stock">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="price-1" name="price[]" type="text" class="f-input price">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-field">
                                                            <input id="qty-1" name="qty[]" type="text" class="f-input qty">
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            </br>
                                            <div class="input-field">
                                                <label>Nilai Retur</label>
                                                <input id="totalvalue" type="text" name="totalvalue" class="f-input" placeholder="Nilai Retur" value="0" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col l12 s12 m12 margin-top">
                                        <a href="#" id="submit-retur" mode="save" class="btn-stoko teal white-text">Simpan</a>
                                        <a href="#" id="edit-retur" mode="edit" class="btn-stoko teal white-text" hidden>Edit</a>
                                        <a href="#" id="clear" class="btn-stoko">batal</a>
                                        <a id="cetak-return" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Retur</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Modal Delete Delivery Order -->
<div id="modal-delete-retur" class="modal">
    <div class="modal-content">
        <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
        <a id="confirm-delete-retur" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
</div>

<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        // $.ajax({
        //     url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
        //     dataType: "script",
        // });

        firstload();

        $('#nopo').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getpoforreturn",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    if(response.length ==0)
                    {
                        $original.clone();
                        $original.attr('hidden',true)
                        $original.appendTo('#barang-data');
                        $('#no-item').removeAttr('hidden');
                        $('#gudang-div').attr('hidden',true);
                    }else{
                        // var $cloned = $original.clone();
                        // $('.barang-row').remove();
                        $('#supplier').val(response['po'][0].company_name);
                        $('#no-item').attr('hidden',true);
                        $('.barang-row').removeAttr('hidden');
                        $.each(response['po'], function(i, v){
                            var newid = "id-"+(i+1);
                            var newquota = "quota-"+(i+1);
                            var newdetailpo = "detail-po-"+(i+1);
                            var newharga = "harga-"+(i+1);
                            var newkode = "kode-"+(i+1);
                            var newnama = "nama-"+(i+1);
                            var newqty = "qty-"+(i+1);
                            var newstock = "stock-"+(i+1);
                            var newprice = "price-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#detail-po-1').attr('id',newdetailpo);
                            $temp.find('#quota-1').attr('id',newquota);
                            $temp.find('#kode-1').attr('id',newkode);
                            $temp.find('#nama-1').attr('id',newnama);
                            $temp.find('#qty-1').attr('id',newqty);
                            $temp.find('#price-1').attr('id',newprice);
                            $temp.find('#stock-1').attr('id',newstock);
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(v.product_id);
                            $('#stock-'+(i+1)).attr('disabled',true);
                            $('#detail-po-'+(i+1)).val(v.purchase_order_details_id);
                            $('#price-'+(i+1)).val(v.item_price).attr('disabled',true);
                            if (v.retur_quota==null) {
                                $('#quota-'+(i+1)).val(v.quantity).attr('disabled',true);
                            }else{
                                $('#quota-'+(i+1)).val(v.retur_quota).attr('disabled',true);
                            }
                            $('#kode-'+(i+1)).val(v.product_code).attr('disabled',true);
                            $('#nama-'+(i+1)).val(v.product_name).attr('disabled',true).attr('title',v.product_name);
                            $('#qty-'+(i+1)).val(0);
                        });

                        $('#returtype').html("");
                        $.each(response['returtype'],function(i,v){
                            $('#returtype').append("<option value='"+v.retur_type_id+"''>"+v.retur_type_name+"</option>").removeAttr('disabled');
                        });
                        $('.selectpicker').selectpicker('refresh');
                        $('#returtype').trigger('change');


                        var tglretur = $('#tglretur').datepicker().data('datepicker');
                        tglretur.update('minDate', new Date(response['good_receive'].good_receive_date));
                        tglretur.selectDate(new Date(response['good_receive'].good_receive_date));
                    }
                }
            });
        });

        $('#returtype').on('change', function(){
            if($(this).val() == 4){
                $('#gudang-div').removeAttr('hidden');
                $('#gudang').removeAttr('disabled');
                $('.selectpicker').selectpicker('refresh');
            }else{
                $('#gudang').val(0).attr('disabled',true);
                $('#gudang-div').attr('hidden',true);
                $('.selectpicker').selectpicker('refresh');
            }
        })


        $('body').on('keyup','#formRetur .qty', function(event){
            event.stopImmediatePropagation();
            calculatetotal();
        });

        $('body').on('click','#returTable tr, #returTable .edit', function(event){
            event.stopImmediatePropagation();
            var mode = $(event.currentTarget).attr('mode');
            id = $(this).attr('value');
            $.ajax({
                type:"GET",
                url:"getpurchasereturn",
                data:{id:id},
                success:function(response){
                    console.log(response);
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('#nopo').append('<option class="remove-when-clear" value="'+response[0].purchase_order_id+'" selected="selected">'+response[0].purchase_order_number+'</option>').attr('disabled',true);
                    $('#idretur').val(response[0].purchase_retur_id).attr('disabled',true);
                    $('#noretur').val(response[0].retur_number).attr('disabled',true);
                    $('#supplier').val(response[0].company_name).attr('disabled',true);
                    $('#tglretur').val(response[0].date_retur).attr('disabled',true);
                    $('#returtype').val(response[0].retur_type_id).attr('disabled',true);
                    //cek apakah retur barang atau tidak, kalau iya makan gudang ditampilkan.
                    if(response[0].retur_type_id == 4){
                        $('#gudang').val(response[0].warehouse_id);
                        $('#gudang-div').removeAttr('hidden');
                    }else{
                        $('#gudang-div').removeAttr('hidden');
                    }
                    for (var i=0; i<response.length; i++)
                    {
                        var newid = "id-"+(i+1);
                        var newdetail = "detail-retur-"+(i+1);
                        var newquota = "quota-"+(i+1);
                        var newharga = "harga-"+(i+1);
                        var newkode = "kode-"+(i+1);
                        var newnama = "nama-"+(i+1);
                        var newqty = "qty-"+(i+1);
                        var newstock = "stock-"+(i+1);
                        var newprice = "price-"+(i+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#detail-retur-1').attr('id',newdetail);
                        $temp.find('#stock-1').attr('id',newstock);
                        $temp.find('#harga-1').attr('id',newharga);
                        $temp.find('#quota-1').attr('id',newquota);
                        $temp.find('#kode-1').attr('id',newkode);
                        $temp.find('#nama-1').attr('id',newnama);
                        $temp.find('#qty-1').attr('id',newqty);
                        $temp.find('#price-1').attr('id',newprice);
                        $temp.appendTo('#barang-data');
                        $('#id-'+(i+1)).val(response[i].product_id);
                        $('#detail-retur-'+(i+1)).val(response[i].purchase_retur_details_id);
                        $('#kode-'+(i+1)).val(response[i].product_code).attr('disabled',true);
                        $('#nama-'+(i+1)).val(response[i].product_name).attr('disabled',true).attr('title',response[i].product_name);
                        $('#qty-'+(i+1)).val(response[i].qty).attr('disabled',true);
                        $('#stock-'+(i+1)).attr('disabled',true);
                        $('#price-'+(i+1)).val(response[i].item_price).attr('disabled',true);
                        $('#quota-'+(i+1)).val(response[i].retur_quota).attr('disabled',true);
                    }
                    calculatetotal();
                    $('.selectpicker').selectpicker('refresh');
                    $('#no-item').attr('hidden',true);
                    $('#barang-row').removeAttr('hidden');
                },complete:function(){
                    if(mode == "edit"){
                        $('#edit-retur').removeAttr('hidden');
                        $('#submit-retur').attr('hidden',true);
                        $('#tglretur, .qty').removeAttr('disabled');
                    }else{
                        $('#cetak-return').removeAttr('hidden').attr('href',"downloadpurchasereturn/"+id);
                    }
                }
            })
        });

        $('#formRetur #submit-retur, #formRetur #edit-retur').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            var validqty = [];
            var lebihqty = [];
            var stockkurang = [];
            var kode = $('#noretur').val();
            mode = $(event.currentTarget).attr('mode');

            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "createpurchasereturn";
                var successmessage = 'Purchase Return '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatepurchasereturn";
                var successmessage = 'Purchase Return '+kode+' telah berhasil diubah!';
            }


            $('.barang-row').each(function(){
                var qty = parseInt($(this).find('.qty').val());
                var quota = parseInt($(this).find('.quota').val());
                validqty.push(ceknumber($(this).find('.qty').val()));
                if(qty > quota){
                    lebihqty.push(false);
                }else{
                    lebihqty.push(true);
                }

                if($('#returtype').val() == 4){ // return barang
                    var stock = $(this).find('.stock').val();
                    stock = stock == "" ? 0 : stock;
                    stock = parseInt(stock);
                    if(qty > stock){
                        stockkurang.push(false)
                    }else{
                        stockkurang.push(true)
                    }
                }
            });

            if($('#nopo').val()==0)
            {
                toastr.warning('Anda Belum Memilih Purchase Order!');
            }else if($('#returtype').val() == 0){
                toastr.warning('Anda Belum Memilih Retur Type!');
            }else if(validqty.indexOf(false) > -1){
                toastr.warning('Quantity harus berupa angka!');
            }else if(lebihqty.indexOf(false) > -1 && mode=="save"){
                toastr.warning('Quantity melebihi quota!');
            }else if($('#returtype').val() == 4 && stockkurang.indexOf(false) > -1){
                toastr.warning('Stock Di Gudang Tidak Mencukupi!');
            }else{
                me.invisible();
                me.data('requestRunning', true);
                $('#noretur, #gudang, #idretur, #returtype, .price').removeAttr('disabled');
                $.ajax({
                    type:"POST",
                    url:url,
                    data:$(this).closest('#formRetur').serialize(),
                    success:function(response){
                        toastr.success(successmessage,{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                })
            }
        });

        $('body').on('click','#returTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var noretur = $(this).closest('tr').find('.noretur').html();
            var idretur = $(this).closest('tr').attr('value');
            $('#confirm-delete-retur').attr('value',idretur).attr('nomor',noretur);
            $("#delete-message").html("Yakin ingin menghapus data "+noretur+" ?")
            $('#modal-delete-retur').modal('open');
        });

        $('#confirm-delete-retur').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-retur').modal('close');
            var id = $(this).attr('value');
            var noretur = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepurchasereturn",
                data:{id:id},
                success:function(response){
                    toastr.success('Purchase Return '+noretur+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        $('#gudang').on('change', function(){
            var idgudang = $(this).val();
            var idproduct = [];
            $('.idbarang').each(function(i){
                idproduct.push($(this).val());
            });
            $.ajax({
                type:"GET",
                url:"getStockInWarehouse",
                data: {idproduct:idproduct, idgudang:idgudang},
                success:function(response){
                    for (var i=0; i<response.length; i++)
                    {
                        // $('#stock-'+(i+1)).val(response[i].quantity).attr('disabled',true);
                        $('.idbarang').each(function(j,v){
                            if($(this).val() == response[i].product_id){
                                $(this).closest('tr').find('.stock').val(response[i].quantity)
                            }
                        });
                    }
                }
            })
        });


        //function
        function firstload()
        {
            $.ajax({
                type:"GET",
                url:"lastpurchasereturnnumber",
                success:function(response){
                    $('#noretur').val(response);
                }
            })

            $('.selectpicker').selectpicker('render');

            $('#tglretur').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tglretur').val(moment().format('DD-MM-YYYY'));

            var returTable = $('#returTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getpurchasereturntable',
                    data : function (d){
                        d.noretur = $('#filterReturNumber').val();
                        d.supplier = $('#filterSupplier').val();
                        d.nopo = $('#filterPoNumber').val();
                    }
                },
                rowId : 'purcahse_retur_id',
                columns: [
                    { data: 'retur_number', name: 'retur_number', class:'noretur'},
                    { data: 'retur_type_name', name: 'retur_type_name' },
                    { data: 'company_name', name: 'company_name' },
                    { data: 'purchase_order_number', name: 'purchase_order_number' },
                    { data: 'action', name: 'action'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterReturNumber').on('keyup', function () { // This is for news page
                returTable.draw();
            });
            $('#filterSupplier').on('keyup', function () { // This is for news page
                returTable.draw();
            });
            $('#filterPoNumber').on('keyup', function () { // This is for news page
                returTable.draw();
            });
        }

        function calculatetotal(){
            var total = 0;
            $('.barang-row').each(function(key, value){
                totalqty = 0;
                $(this).find('.qty').each(function(key,value){
                    totalqty += parseInt($(this).val());
                })
                total += totalqty * $(this).find('.price').val();
            });
            $('#totalvalue').val(total);
        }
    });
</script>
