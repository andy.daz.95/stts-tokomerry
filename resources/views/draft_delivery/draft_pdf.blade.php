<style type="text/css">
  table.table {
    border-collapse: collapse;
  }
  table.table, table.table th, table.table td{
    border: 1px solid black;
  }
  .red-color{
    color : red;
  }
  html { margin: 20px 15px 5px 14px}
  body {
    font-size: 14px;
    letter-spacing: 1px;
  }
  .txt-lg{
    font-size: 16px;
  }
  .txt-header{
    font-size: 30px;
  }
</style>
@if($draft->so)
  @php $so = $draft->so; @endphp
@elseif($draft->retur->so)
  @php $so = $draft->retur->so; @endphp
@endif
<div>
  <p style="text-align: center; margin:0px" class="txt-header">
    @if($draft->shipping_term_id == 1)
      DO {{$draft->warehouse->warehouse_name}}
    @elseif($draft->shipping_term_id == 2)
      Surat Antaran
    @elseif($draft->shipping_term_id == 3)
      Draft Retur
    @endif
  </p>
  <hr>
  <table style ="width:100%">`
    <tr>
      <td style="width: 25%">NO</td>
      <td>: {{$draft->draft_delivery_order_number}}</td>
      <td></td>
    </tr>
    <tr>
      <td style="width: 25%">NO Nota Penjualan</td>
      <td>: {{$draft->so->sales_order_number}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Tanggal</td>
      <td>: {{$draft->date_draft_delivery}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Pelanggan</td>
      <td>: {{$so->customer->first_name.' '.$so->customer->last_name}}</td>
      <td></td>
    </tr>
    @if($draft->shipping_term_id == 1)
      <tr>
        <td>Dari Gudang</td>
        <td colspan="2">: {{$draft->warehouse->warehouse_name}}</td>
      </tr>
    @endif
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
    <tr>
      <td class="color" style="text-align: center;">No</td>
      <td class="color" style="text-align: center;">Nama Barang</td>
      <td class="color" style="text-align: center;">Keterangan</td>
      @if($draft->draft_type == 1)
      <td class="color" style="text-align: center;">Quantity Kirim</td>
      @else
      <td class="color" style="text-align: center;">Quantity Retur</td>
      @endif
    </tr>
    </thead>
    <tbody>
    @php($row=0)
      @php
        if($order != null)
        {
          $details = $draft->details->sortBy(function($model) use ($order){
          return array_search($model->getKey(), $order);
          });
        }else{
          $details = $draft->details;
        }
      @endphp
    @foreach($details as $key => $value)
      @if($value->quantity > 0)
      <tr>
        <td>
          <span>{{$row+1}}</span>
        </td>
        <td>
          <span>{{$value->product->product_name}}</span>
        </td>
        <td>
          @if($draft->draft_type == 1)
          <span>{!! nl2br($value->detailso->detail_note) !!}</span>
          @else
          <span>{!! "Barang Retur" !!}</span>
          @endif
        </td>
        <td style="text-align: center;">
          <span>{{$value->quantity . " (". App\Traits\NumberToLetterTrait::terbilang($value->quantity).")"}}</span>
          @if($value->unit_type == 'main')
            @php
              $satuan = App\satuan::where('unit_id', $value->unit_id)->first()->unit_code;
              $firstnumber = App\Traits\UnitTrait::my_offset($satuan);
              $satuan = substr($satuan,0,$firstnumber)
            @endphp
            <span>{{$satuan}}</span>
          @else
            <span>{{$value->product->satuan->unit_child->unit_child_code}}</span>
          @endif
        </td>
      </tr>
        @php($row++)
      @endif
    @endforeach
    </tbody>
  </table>
  @if($draft->shipping_term_id == 2)
    <table style ="width:100%">
      @foreach($invoicebelumlunas as $key => $value)
      <tr>
        <td style="width: 50%">Sisa Hutang Nota Penjualan {{$value->so->sales_order_number}}</td>
        <td style="width: 50%">: Rp {{number_format($value->total_amount - $value->payments->sum('total_paid'))}}</td>
        <td></td>
      </tr>
        @endforeach
    </table>
  @endif
  @if($draft->shipping_term_id == 2)
    <p style="text-transform: uppercase" class="txt-lg">Alamat:</p>
    <p style="text-transform: uppercase" class="txt-lg">{{$draft->notes}}</p>
  @endif
  @if(trim($so->note) != '')
    <p style="text-transform: uppercase" class="txt-lg">Notes SO: {{$so->note}}</p>
  @endif
  @if(count($retur) > 0 )
    <table style ="width:100%; margin-top:10px">
      <tr>
        <td class="red-color">Barang Retur Yang Harus Diambil :</td>
      </tr>
    </table>
    @foreach($retur as $key => $value)
      <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
        <tr>
          <th class="red-color">No Retur</th>
          <th class="red-color">Nama Barang</th>
          <th class="red-color">Quantity</th>
        </tr>
        @foreach($value->details as $key => $value2)
          <tr>
            <td class="red-color">{{$value->retur_number}}</td>
            <td class="red-color">{{$value2->product->product_name}}</td>
            <td class="red-color">{{$value2->qty}}</td>
          </tr>
        @endforeach
      </table>
    @endforeach
  @endif
</div>