<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Delivery Order</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l3">
                  <input id="filterDraftNumber" type="text" class="f-input">
                  <label>Filter DO</label>
                </div>
                <div class="input-field col l3">
                  <input id="filterPelanggan" type="text" class="f-input">
                  <label>Filter Pelanggan</label>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="draftDeliveryTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th>No DO</th>
                        <th>No Sales Order</th>
                        <th>Dibuat Oleh</th>
                        <th>Tanggal</th>
                        <th>Pelanggan</th>
                        <th>Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($data['draft'] as $key => $value)--}}
                      {{--<tr value="{{$value->draft_delivery_order_id}}">--}}
                      {{--<td class="nodraft">{{$value->draft_delivery_order_number}}</td>--}}
                      {{--@if($value->so)--}}
                      {{--@php --}}
                      {{--$so = $value->so;--}}
                      {{--$type = 'so'; --}}
                      {{--@endphp--}}
                      {{--@elseif($value->retur->so)--}}
                      {{--@php --}}
                      {{--$so = $value->retur->so;--}}
                      {{--$type = 'retur';--}}
                      {{--@endphp--}}
                      {{--@endif--}}

                      {{--<td>{{$type == 'retur' ? $so->sales_order_number.' (Retur)' : $so->sales_order_number}} </td>--}}
                      {{--<td>{{$value->date_draft_delivery}}</td>--}}
                      {{--<td>{{$so->customer->first_name.' '.$so->customer->last_name}}</td>--}}
                      {{--<td>--}}
                      {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->draft_delivery_order_id}}"><i--}}
                      {{--class="material-icons">edit</i></a>--}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Draft Delivery Order</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formDraftDo">
                  <div class="input-field col l3">
                    <input id="ddnumber" type="text" class="f-input" name="ddnumber" disabled>
                    <input id="iddd" type="text" class="f-input" name="iddd" hidden disabled>
                    <label>No. Do</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="so" name="so" class="selectpicker browser-default" data-live-search="true" data-size="5" disabled>
                      <option value="0">Select SO</option>
                      @foreach($data['retur'] as $key =>$value)
                        <option type="retur" value="{{$value->sales_retur_id}}">{{$value->so->sales_order_number}} (Retur)</option>
                      @endforeach
                      @foreach($data['so'] as $key =>$value)
                        <option type="so" value="{{$value->so->sales_order_id}}">{{$value->so->sales_order_number}}</option>
                      @endforeach
                    </select>
                    <label>Sales Order ID</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="shippingterm" name="shippingterm" class="selectpicker browser-default" data-live-search="true" data-size="5" disabled>
                      <option value="0">Select Shipping Term</option>
                      @foreach($data['shippingterm'] as $key => $value)
                        <option value="{{$value->shipping_term_id}}">{{$value->shipping_term_name}}</option>
                      @endforeach
                    </select>
                    <label>Shipping Term</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="pelanggan" name="pelanggan" type="text" class="f-input" disabled>
                    <label>Nama Pelanggan</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="tgldd" type="text" name="tgldd" class="f-input" disabled>
                    <label>Tanggal</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="gudang" type="text" name="gudang" class="selectpicker browser-default" data-live-search="true" data-size="5" disabled>
                      <option value="0">Pilih Gudang</option>
                      @foreach($data['warehouse'] as $key => $value)
                        <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                      @endforeach
                    </select>
                    <label>Gudang</label>
                  </div>
                  <div class="input-field col l3">
                    <input id="notes" name="notes" type="text" class="f-input" disabled>
                    <label>Notes</label>
                  </div>
                  <div class="input-field col l3">
                    <select id="user" name="user" class="selectpicker browser-default" data-live-search="true" data-size="5" disabled>
                      <option value="0">Select User</option>
                      @foreach($data['account'] as $key => $value)
                        <option value="{{$value->account_id}}">{{$value->full_name}}</option>
                      @endforeach
                    </select>
                    <label>User</label>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border row">
                        <thead>
                        <tr>
                          <th class="theader">Nama Barang</th>
                          <th id="orderheader" class="theader">Jumlah Order</th>
                          <th id="unitheader" class="theader">Unit</th>
                          <th id="stockheader" class="theader">Stok Di Gudang</th>
                          <th id="actionheader" class="theader">Action</th>
                        </tr>
                        </thead>
                        <tbody id="barang-data">
                        <tr id="no-item">
                          <td colspan="6"><span> No Item Selected</span></td>
                        </tr>
                        <tr class="barang-row" hidden>
                          <td>
                            <div class="input-field">
                              <input id="detail-1" type="text" class="f-input iddetail" name="iddetail[]" hidden>
                              <input id="id-1" type="text" class="f-input idbarang" name="idbarang[]" hidden>
                              <input id="nama-1" type="text" class="f-input" name="namabarang[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="order-qty-1" type="text" class="f-input orderqty" name="orderqty[]">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="unit-1" type="text" class="f-input unit" name="unit">
                              <input id="unit-type-1" type="text" class="f-input unittype" name="unittype" hidden>
                            </div>
                          </td>
                          <td class="stock-data">
                            <div class="input-field">
                              <input id="stock-1" type="text" class="f-input stock" name="stock[]">
                            </div>
                          </td>
                          <td class="action-data">
                            <input type="checkbox" class="filled-in checkboxdraft" id="checkbox-draft-1" name="check[]"/>
                            <label id="label-draft-1" for="checkbox-draft-1"></label>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="input-field right">
                      <a id="open-print-modal" class="btn-stoko btn-stoko-primary orange darken-4 modal-trigger" href="#modal-print" hidden>Cetak Draft</a>
                      {{--<a id="clear" href="" class="btn-stoko orange">Clear</a>--}}
                      {{-- <a id="cetak-draft" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Draft</a>
                      <a id="cetak-draft-kecil" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Draft Kecil</a> --}}
                      {{--<a id="submit-draft" href="" class="btn-stoko btn-stoko-primary" mode="save">Submit Draft</a>--}}
                      {{--<a id="edit-draft" href="" class="btn-stoko btn-stoko-primary" mode="edit" hidden>Edit Draft</a>--}}
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-print" class="modal">
  <form id="form-list-barang">
    <div class="modal-content">
      <h4>Sortir Barang</h4>
      <div class="row">
        <table class="stoko-table no-border row">
          <thead>
            <th>Nama</th>
            <th>Qty</th>
            <th>Action</th>
          </thead>
          <tbody id="modal-barang-data">
            <tr class="print-row" hidden>
              <td>
                <div class="input-field">
                  <input id="print-detail-1" type="text" class="f-input idprintdetail" name="iddetail[]" hidden>
                  <input id="print-nama-1" type="text" class="f-input" name="namabarang[]">
                </div>
              </td>
              <td>
                <div class="input-field">
                  <input id="print-order-qty-1" type="text" class="f-input orderqty" name="orderqty[]">
                </div>
              </td>
              <td>
                <a class="btn btn-sm btn-raised green move-up"><i class="material-icons">arrow_upward</i></a>
                <a class="btn btn-sm btn-raised red move-down"><i class="material-icons">arrow_downward</i></a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="modal-footer">
      <a id="cetak-draft" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Draft</a>
      <a id="cetak-draft-kecil" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Draft Kecil</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>


<!-- Modal Delete Delivery Order -->
<div id="modal-delete-draft" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-draft" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });
        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#so').on('change', function () {
            var id = $(this).val();
            var type = $('option:selected',this).attr('type');

            if(type == "so")
            {
                var url = 'getsofordraft';
            }else if(type == "retur"){
                var url = 'getreturfordraft';
            }

            $.ajax({
                type: "Get",
                url: url,
                data: {id: id},
                success: function (response) {
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('.print-row').remove();
                    if (response.details.length == 0) {
                        $original.clone();
                        $original.attr('hidden', true)
                        $original.appendTo('#barang-data');
                        $('#formDo').find("input[type=text], textarea").val("");
                        $('#no-item').removeAttr('hidden');
                    }
                    else {
                        if(type== "so"){
                            $('#pelanggan').val(response.customer.first_name + " " + response.customer.last_name);
                        }else if(type == 'retur'){
                            $('#pelanggan').val(response.so.customer.first_name + " " + response.so.customer.last_name);
                        }
                        for (var i = 0; i < response.details.length; i++) {
                            var newid = "id-" + (i + 1);
                            var newdetail = "detail-" + (i + 1);
                            var newnama = "nama-" + (i + 1);
                            var neworderqty = "order-qty-" + (i + 1);
                            var newunit = "unit-" + (i + 1);
                            var newunittype = "unit-type-" + (i + 1);
                            var newstock = "stock-" + (i + 1);
                            var newLabelDraft = "label-draft-" + (i + 1);
                            var newDraft = "checkbox-draft-" + (i + 1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id', newid);
                            $temp.find('#detail-1').attr('id', newdetail);
                            $temp.find('#nama-1').attr('id', newnama);
                            $temp.find('#unit-1').attr('id', newunit);
                            $temp.find('#unit-type-1').attr('id', newunittype);
                            $temp.find('#order-qty-1').attr('id', neworderqty);
                            $temp.find('#stock-1').attr('id', newstock);
                            $temp.find('#label-draft-1').attr('id', newLabelDraft).attr('for', "checkbox-draft-" + (i + 1));
                            $temp.find('#checkbox-draft-1').attr('id', newDraft);
                            $temp.appendTo('#barang-data');

                            $('#id-' + (i + 1)).val(response.details[i].product_id);
                            $('#nama-' + (i + 1)).val(response.details[i].product.product_name).attr('disabled', true);
                            // $('#unit-' + (i + 1)).val(response.details[i].unit_id).attr('disabled', true);
                            $('#unit-type-' + (i + 1)).val(response.details[i].unit_type).attr('disabled', true);
                            if(response.details[i].unit_type == 'child'){
                                $('#unit-' + (i + 1)).val(response.details[i].unit_child.unit_child_code).attr('disabled', true);
                            }else if (response.details[i].unit_type == 'main'){
                                $('#unit-' + (i + 1)).val(response.details[i].unit.unit_code).attr('disabled', true);
                            }

                            $('#stock-' + (i + 1)).val(0).attr('disabled', true);

                            if(type == 'so'){
                                $('#detail-' + (i + 1)).val(response.details[i].sales_order_details_id);
                                $('#order-qty-' + (i + 1)).val(response.details[i].quantity).attr('disabled', true);
                                $('#orderheader').html('Jumlah Order');
                                $('#checkbox-draft-' + (i + 1)).val(response.details[i].sales_order_details_id);
                                $('#submit-draft').attr('type', 'so')
                            }else if (type == 'retur'){
                                $('#detail-' + (i + 1)).val(response.details[i].sales_retur_details_id);
                                $('#order-qty-' + (i + 1)).val(response.details[i].qty).attr('disabled', true);
                                $('#orderheader').html('Jumlah Retur');
                                $('#checkbox-draft-' + (i + 1)).val(response.details[i].sales_retur_details_id);
                                $('#submit-draft').attr('type', 'retur');
                            }

                            if (response.details[i].draft) { // kalau sudah ada draft di disabled checkbox nya
                                $('#checkbox-draft-' + (i + 1)).attr('disabled', true);
                            } else {
                                $('#checkbox-draft-' + (i + 1)).removeAttr('disabled');
                            }
                            $('.selectpicker').selectpicker('refresh');
                        }
                        $('#no-item').attr('hidden', true);
                    }
                }, complete: function () {
                    $('#gudang').removeAttr('disabled');
                    $('.selectpicker').selectpicker('refresh');
                }
            });
        });

        $('#submit-draft, #edit-draft').click(function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);

            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //
            var type = $(this).attr('type');
            var kode = $('#donumber').val();

            if ($(event.currentTarget).attr('mode') == 'save') {
                var url = "createdraftdelivery/"+type;
                var successmessage1 = 'Draft Delivery ';
                var successmessage2 = ' telah berhasil dibuat!';
            } else {
                var url = "updatedraftdelivery";
                var successmessage1 = 'Draft Delivery ';
                var successmessage2 = ' telah berhasil diubah!';
            }

            var stockkurang = [];
            var sodetail = [];
            $('.barang-row').each(function () {
                var orderqty = parseFloat($(this).find('.orderqty').val());
                var stock = parseFloat($(this).find('.stock').val());
                if ($(this).find('.checkboxdraft').is(':checked')) {
                    if (orderqty > stock) {
                        stockkurang.push(false)
                    } else {
                        stockkurang.push(true)
                    }
                }
                sodetail.push($(this).find('.detail').val());
            });

            var checked = 0;
            $('.checkboxdraft').each(function(){
                if($(this).is(':checked')){
                    checked++;
                }
            });

            if ($('#so').val() == 0) {
                toastr.warning('Anda Belum Memilih SO!');
            }else if(checked == 0){
                toastr.warning('Anda Belum Memilih Barang!');
            }else if($('#shippingterm').val() == 0){
                toastr.warning('Anda Belum Memilih Shipping Term!');
            }else if (stockkurang.indexOf(false) > -1) {
                toastr.warning('Stock Di Gudang Tidak Mencukupi!');
            }else if($('#user').val() == 0){
                toastr.warning('Anda Belum Memilih User!');
            }else {
                me.data('requestRunning', true);
                $('.orderqty, #iddd').removeAttr('disabled');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $('#formDraftDo').serialize(),
                    success: function (response) {
                        toastr.success(successmessage1 + kode + successmessage2, {
                            "onShow": setTimeout(function () {
                                $('.side-nav .active a').click();
                            }, 2600)
                        });
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                })
            }
        });

        $('#formDraftDo').on('change', '#gudang', function () {
            var idgudang = $(this).val();
            var idproduct = [];
            $('.idbarang').each(function (i) {
                idproduct.push($(this).val());
            });
            $.ajax({
                type: "GET",
                url: "getstockfordraft",
                data: {idproduct: idproduct, idgudang: idgudang},
                success: function (response) {
                    $('.barang-row').each(function(i,v){
                        var row = $(this);
                        var product = $(this).find('.idbarang').val();
                        var type = $(this).find('.unittype').val();
                        alert(type);
                        for (var i = 0; i < response.length; i++) {
                            if(response[i].product_id == product && type == 'main'){
                                row.find('.stock').val(response[i].quantity).attr('disabled', true);
                            }else{
                                row.find('.stock').val(response[i].qty_child).attr('disabled', true);
                            }
                        }
                    });
                }
            })
        });

        $('#clear').on('click', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        $('body').on('click', '#draftDeliveryTable tr, #draftDeliveryTable .edit', function (event) {
            event.stopImmediatePropagation();
            var mode = $(event.currentTarget).attr('mode');
            id = $(this).attr('value');
            $.ajax({
                type: "GET",
                url: "getdraftdelivery",
                data: {id: id},
                success: function (response) {
                    $('#ddnumber').val(response.draft_delivery_order_number);
                    $('#iddd').val(response.draft_delivery_order_id);
                    $('#gudang').val(response.warehouse_id).attr('disabled', true);
                    $('#shippingterm').val(response.shipping_term_id).attr('disabled', true);
                    $('#notes').val(response.notes).attr('disabled', true);
                    $('#tgldd').val(response.date_draft_delivery).attr('disabled', true);
                    $('#user').val(response.user_id).attr('disabled',true);
                    if(response.so)
                    {
                        $('#so').append('<option class="remove-when-clear" value="' + response.so.sales_order_id + '" selected="selected">' + response.so.sales_order_number + '</option>').attr('disabled', true);
                        $('#pelanggan').val(response.so.customer.first_name + " " + response.so.customer.last_name);
                    }else{
                        $('#so').append('<option class="remove-when-clear" value="' + response.retur.so.sales_order_id + '" selected="selected">' + response.retur.so.sales_order_number +
                            ' (Retur)</option>').attr('disabled', true);
                        $('#pelanggan').val(response.retur.so.customer.first_name + " " + response.retur.so.customer.last_name);
                    }
                    var $original = $('.barang-row:first');
                    var $print = $('.print-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('.print-row').remove();
                    for (var i = 0; i < response.details.length; i++) {
                        var newid = "id-" + (i + 1);
                        var newdetail = "detail-" + (i + 1);
                        var newnama = "nama-" + (i + 1);
                        var newunit = "unit-" + (i + 1);
                        var newunittype = "unit-type-" + (i + 1);
                        var neworderqty = "order-qty-" + (i + 1);
                        var newstock = "stock-" + (i + 1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id', newid);
                        $temp.find('#detail-1').attr('id', newdetail);
                        $temp.find('#nama-1').attr('id', newnama);
                        $temp.find('#stock-1').attr('id', newstock);
                        $temp.find('#unit-1').attr('id', newunit);
                        $temp.find('#unit-type-1').attr('id', newunittype);
                        $temp.find('#order-qty-1').attr('id', neworderqty);
                        $temp.appendTo('#barang-data');

                        // for Print Modal
                        var newprintdetail = "print-detail-" + (i + 1);
                        var newprintnama = "print-nama-" + (i + 1);
                        var newprintorderqty = "print-order-qty-" + (i + 1);
                        $printtemp = $print.clone();
                        $printtemp.removeAttr('hidden')
                        $printtemp.find('#print-detail-1').attr('id', newprintdetail);
                        $printtemp.find('#print-nama-1').attr('id', newprintnama);
                        $printtemp.find('#print-order-qty-1').attr('id', newprintorderqty);
                        $printtemp.appendTo('#modal-barang-data');

                        if(response.so != 'undefinied' || response.so != null){
                            $('#detail-' + (i + 1)).val(response.details[i].sales_order_details_id);
                        }else{
                            $('#detail-' + (i + 1)).val(response.details[i].sales_retur_details_id);
                        }

                        $('#unit-type-' + (i + 1)).val(response.details[i].unit_type).attr('disabled', true);
                        if(response.details[i].unit_type == 'child'){
                            $('#unit-' + (i + 1)).val(response.details[i].product.satuan.unit_child.unit_child_code).attr('disabled', true);
                        }else if (response.details[i].unit_type == 'main'){
                            $('#unit-' + (i + 1)).val(response.details[i].product.satuan.unit_code).attr('disabled', true);
                        }

                        $('#id-' + (i + 1)).val(response.details[i].product.product_id);
                        $('#nama-' + (i + 1)).val(response.details[i].product.product_name).attr('disabled', true);
                        $('#order-qty-' + (i + 1)).val(response.details[i].quantity).attr('disabled', true);
                        $('#stock-' + (i + 1)).val(0).attr('disabled', true);

                        // For Print Modal
                        $('#print-detail-' + (i + 1)).val(response.details[i].draft_delivery_order_details_id).attr('disabled', true);
                        $('#print-nama-' + (i + 1)).val(response.details[i].product.product_name).attr('disabled', true);
                        $('#print-order-qty-' + (i + 1)).val(response.details[i].quantity).attr('disabled', true);

                    }
                    $('.selectpicker').selectpicker('refresh');
                    $('#no-item').attr('hidden', true);
                    $('#barang-row').removeAttr('hidden');
                }, complete: function () {
                    hideStock();
                    hideAction();
                    $('#submit-draft').attr('hidden',true);
                    $('#open-print-modal').removeAttr('hidden');
                    $('#cetak-draft').removeAttr('hidden').attr('href',"downloaddraftdelivery/"+id);
                    $('#cetak-draft-kecil').removeAttr('hidden').attr('href',"downloaddraftdelivery/"+id+"/small");
                    if(mode == "edit")
                    {
                        $('#edit-draft').removeAttr('hidden');
                        $('#tgldd , #user , #notes, #shippingterm').removeAttr('disabled');
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            })
        });

        $('body').on('click','#draftDeliveryTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var nodraft = $(this).closest('tr').find('.nodraft').html();
            var iddraft = $(this).closest('tr').attr('value');
            $('#confirm-delete-draft').attr('value',iddraft).attr('nomor',nodraft);
            $("#delete-message").html("Yakin ingin menghapus data "+nodraft+" ?")
            $('#modal-delete-draft').modal('open');
        });

        $('#confirm-delete-draft').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-draft').modal('close');
            var id = $(this).attr('value');
            var nodraft = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletedraftdelivery",
                data:{id:id},
                success:function(response){
                    toastr.success('Draft Delivery '+nodraft+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        $("#modal-print").on('click', '.move-up, .move-down', function () {
          var $element = this;
          var row = $($element).parents("tr:first");

          if($(this).is('.move-up')){
           row.insertBefore(row.prev());
          }
          else{
           row.insertAfter(row.next());
          }

          order = get_print_array()
          add_url = $.param({order: order});
          current_url = $('#cetak-draft').attr('href');
          base_url = current_url.split("?")[0]

          $('#cetak-draft').removeAttr('hidden').attr('href', base_url+"?"+add_url);
          $('#cetak-draft-kecil').removeAttr('hidden').attr('href', base_url+"/small?"+add_url);
        });

        function get_print_array(){
          return $(".idprintdetail").map(function(){return $(this).val();}).get();
        }

        //function
        function firstload() {
            $('#tgldd').datepicker({
                dateFormat: 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tgldd').val(moment().format('DD-MM-YYYY'));

            $('.selectpicker').selectpicker('render');

            $('.number').each(function () {
                $(this).html(accounting.formatMoney($(this).html(), 'Rp. ', 2, ',', '.'));
            });

            var draftDeliveryTable = $('#draftDeliveryTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getdrafttable',
                    data : function (d){
                        d.nodraft = $('#filterDraftNumber').val();
                        d.customer = $('#filterPelanggan').val();
                    }
                },
                rowId : 'draft_delivery_order_id',
                columns: [
                    { data: 'draft_delivery_order_number', name: 'draft_delivery_order_number', class:'nodraft'},
                    { data: 'sales_order_number', name: 'sales_order_number' },
                    { data: 'full_name', name: 'user' },
                    { data: 'date_draft_delivery', name: 'date_draft_delivery' },
                    { data: 'customer_name', name: 'customer_name' },
                    { data: 'action', name: 'action' },
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });


            $('#filterDraftNumber').on('keyup', function () { // This is for news page
                draftDeliveryTable.draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                draftDeliveryTable.draw();
            });
        }

        function hideStock() {
            $('#stockheader, .stock-data').attr('hidden', true);
        }

        function hideAction() {
            $('#actionheader, .action-data').attr('hidden', true);
        }
    });
</script>