<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sistem Toko</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Styles -->
  <link rel="stylesheet" href="css/materialize.css">
  <link rel="stylesheet" href="css/master.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
  <div class ="row">
    <div class="col l6">
      <div class="login-card">
        <div class="card-title center">
          <h3>Grant Access</h3>
        </div>
        <div class="container">
          <div class="row">
            <form class="form-horizontal">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="email" class="col-md-4 control-label">E-Mail</label>
                <div class="col-md-6">
                  <input id="email" class="form-control f-input" name="email" value="{{ old('email') }}" required autofocus>
                    <span class="help-block">
                      <strong></strong>
                    </span>
                </div>
              </div>

              <div class="form-group">
                <label for="password" class="col-md-4 control-label">Kata Sandi</label>
                <div class="col-md-6">
                  <input id="password" type="password" class="form-control f-input" name="password" required>
                </div>
              </div>
              <div class="form-group">
                <div class="col l12">
                  <button id="grant" type="submit" class="btn btn-primary">
                    Grant Access
                  </button>
                  <span class="help-block">
                      <strong id="error" class="red-text"></strong>
                    </span>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col l6">
      <h1 id="access"></h1>
    </div>
  </div>
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="js/jquery.min.js"></script>
  <script src="js/materialize.min.js"></script>
  <script src="js/custom.js"></script>
<script>
    if(localStorage.getItem("access_sistemtoko") != 'true'){
        $("#access").html('This browser doesn\'t have access for staff');
    }else{
        $("#access").html('This browser have granted access for staff');
    }


    $('#grant').on('click',function(event){
        event.preventDefault();
        $.ajax({
            method : 'GET',
            url : 'validmaster',
            data : {email: $('#email').val(), password : $('#password').val()},
            success:function(response){
              console.log(response)
                if(response == 'false'){
                  $('#error').html('credentials Not Match');
                }else{
                    $('#error').html('');
                    localStorage.setItem("access_sistemtoko", "true");
                    setTimeout(function(){location.reload()},300)
                }
            }
        })
    });
</script>
</body>
</html>