@if($finalsaldo >0)
<tr class="ks-row">
	<td>Saldo</td>
	<td>{{$tglawal}}</td>
	<td class="in">{{$finalsaldo}}</td>
	<td class="out">0</td>
	<td class="balance"></td>
</tr>
@endif
@if(count($overall)>0)
@foreach($overall as $key => $value)
<tr class="ks-row">
	<td>{{$value->tcode}}</td>
	<td>{{$value->date}}</td>
	@if($value->stat == 'inout')
		<td class="in">{{$value->qty}}</td>
		<td class="out">{{$value->qty}}</td>
	@else
		@if($value->stat == 'in')
		<td class="in">{{$value->qty}}</td>
		@else
		<td class="in">0</td>
		@endif
		@if($value->stat == 'out')
		<td class="out">{{$value->qty}}</td>
		@else
		<td class="out">0</td>
		@endif
	@endif
	<td class="balance"></td>
</tr>
@endforeach
@endif
@if(!$finalsaldo && count($overall) == 0)
<tr>
	<td colspan="5">No Data</td>
</tr>
@endif