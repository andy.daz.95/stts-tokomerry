<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="container-fluid padding-top">
  <div class="panel-dt grey darken-3 white-text"><i class="material-icons" style="">search</i>Detail Product</div>
  <div class="card margin-zero">
    <div style="min-height: 30px;">
    </div>
    <div class="row margin-zero">
      <div class="input-field col l3">
        <label style="margin-top: -10px">Filter Kode Barang</label>
        <input id="filterKodeBarang" type="text" name="" class="f-input margin-zero" >
      </div>
      <div class="input-field col l3">
        <label style="margin-top: -10px">Filter Nama Barang</label>
        <input id="filterNamaBarang" type="text" name="" class="f-input margin-zero" >
      </div>
    </div>
    <div class="container-fluid" style="padding: 10px 20px 20px 20px;">
      <div class="col l12">
      <table id="StockOverviewTable" class="table highlight table-bordered display nowrap dataTable">
        <thead>
        <tr>
          <th>Kode Barang</th>
          <th>Nama Barang</th>
          <th>Jenis Faktur</th>
          @if(in_array(Session('roles')->name, ['Komputer 123', 'master']))
          <th>Harga Eceran</th>
          @endif
          @if(in_array(Session('roles')->name, ['Toko Partai', 'master']))
          <th>Harga Partai</th>
          @endif
          <th>Total Quantity</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data['product'] as $key => $value)
          <tr value="{{$value->product_id}}">
            <td class="noso">{{$value->product_code}}</td>
            <td>{{$value->product_name}}</td>
            <td>{{$value->is_faktur == 0 ? '-' : 'Faktur' }}</td>
            @if(in_array(Session('roles')->name, ['Komputer 123', 'master']))
            <td>{{number_format($value->price_sale)}}</td>
            @endif
            @if(in_array(Session('roles')->name, ['Toko Partai', 'master']))
            <td>{{number_format($value->price_wholesale)}}</td>
            @endif
            <td>
              @if(!$value->satuan->unit_child)
                {{$value->t_qty.' '.strtolower($value->satuan->unit_code)}}
              @else
                {{$value->t_qty.' '.strtolower($value->satuan->unit_code).' / '. $value->t_qty * $value->satuan->unit_child->multiplier.' '.strtolower($value->satuan->unit_child->unit_child_code)}}
              @endif
            </td>

          </tr>
        @endforeach
        </tbody>
      </table>
      </div>
    </div>
  </div>
  <div class="col l12 m12 s12">
    <ul class="collapsible" data-collapsible="accordion">
      <li>
        <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Detail Product</div>
        <div class="collapsible-body">
          <div class="row">
            <div class="col s12">
              <ul class="tabs">
                @foreach($data['warehouse'] as $key => $value)
                  <li class="tab col s2"><a class="" href="#{{$value->warehouse_id}}">{{$value->warehouse_name}}</a></li>
                @endforeach
              </ul>
            </div>
            @foreach($data['warehouse'] as $key => $value)
              <div id="{{$value->warehouse_id}}" class="col s12" style=" overflow:auto; height: 100px; position:relative;" >
                <table id="{{$value->warehouse_id}}Table" class="table table-bordered display nowrap dataTable dtr-inline">
                  <thead>
                  <tr>
                    <th class="theader">Nama Barang</th>
                    <th class="theader">Total Quantity</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr class="no-item"><td colspan="2"><span> No Item Selected for {{$value->warehouse_name}}</span></td></tr>
                  <tr class='detail-row' hidden>
                    <td class="detail-nama"></td>
                    <td class="quantity"></td>
                  </tr>
                  </tbody>
                </table>
              </div>
          @endforeach
          <!-- <div id="test1" class="col s12">Test 1</div>
              <div id="test2" class="col s12">Test 2</div>
              <div id="test3" class="col s12">Test 3</div>
              <div id="test4" class="col s12">Test 4</div> -->
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $('#StockOverviewTable tr').on('click', function(){
            var id = $(this).attr('value');
            $.ajax({
                url: 'detailStock',
                data: {id:id},
                success:function(response){
                    $.each(response, function(key,value){
                        $.each(value, function(key,values){
                            $('#'+values.warehouse_id+'Table').find('.detail-nama').html(values.product_name);
                            $('#'+values.warehouse_id+'Table').find('.quantity').html(values.quantity);
                            if(!values.satuan.unit_child) {
                                $('#'+values.warehouse_id+'Table').find('.quantity').html(values.quantity+' '+values.satuan.unit_code);
                            }
                            else{
                                var qtymain = values.quantity+' '+values.satuan.unit_code;
                                var qtychild = values.quantity * values.satuan.unit_child.multiplier;
                                $('#'+values.warehouse_id+'Table').find('.quantity').html(qtymain+' / '+ qtychild.toFixed(2) +' '+values.satuan.unit_child.unit_child_code);
                            }
                        });
                    });
                    $('.no-item').attr('hidden',true);
                    $('.detail-row').removeAttr('hidden');
                }
            });
        });


        var stTable = $('#StockOverviewTable').DataTable({ // This is for home page
            searching: true,
            responsive: true,
            "sScrollY": "250px",
            // "bSort":true,
            // "bAutoWidth": false,
            // "bLengthChange": true,
            "bPaginate": false,
            //"aaSorting": [[ 4, "asc" ]],
            "sDom": 'ti',
            "oLanguage": {
                "sSearch": "",
                "sLengthMenu": "View _MENU_ ",
                "sEmptyTable": "Tidak ada data tersedia",
                "sZeroRecords": "Data tidak ditemukan"
            },
            // "sPaginationType": "full_numbers",
            "bInfo": false//Showing 1 to 1 of 1 entries (filtered from 7 total entries)
        });

        // $('#StockOverviewTable').DataTable().columns.adjust().draw();

        $('#filterKodeBarang').on('keyup', function () { // This is for news page
            stTable.column(1).search(this.value).draw();
        });
        $('#filterNamaBarang').on('keyup', function () { // This is for news page
            stTable.column(1).search(this.value).draw();
        });

         $('#filterKodeBarang').trigger('keyup');
    });
</script>