<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/master.css">
<!-- Latest compiled and minified JavaScript -->

<!-- <link rel="stylesheet" href="bootstrap/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Sales Order</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Barang</label>
                  <select id="#barang" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="barang">
                    <option>Select Barang</option>
                    @foreach($data['barang'] as $key => $value)
                      <option value="{{$value->product_id}}">{{$value->product_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="input-field col l3">
                  <label>Gudang</label>
                  <select id="#gudang" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="gudang">
                    <option value="0">All Gudang</option>
                    @foreach($data['gudang'] as $key => $value)
                      <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="input-field col l3">
                  <label>Filter Periode (Awal Periode)</label>
                  <input id="tglawal" type="text" name="tglawal" class="f-input" placeholder="Awal Periode" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Periode (Akhir Periode)</label>
                  <input id="tglakhir" type="text" name="tglakhir" class="f-input" placeholder="Akhir Periode" />
                </div>
                <div class="input-field col l6">
                  <a id="preview" href="#" class="btn btn-raised blue white-text">preview</a>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Kartu Stock</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <table class="table table-bordered display nowrap dataTable dtr-inline">
                  <thead>
                    <tr>
                      <th class="theader" style="width: 300px">Transaction Code</th>
                      <th class="theader">Transaction Date</th>
                      <th class="theader">In</th>
                      <th class="theader">Out</th>
                      <th class="theader">Balance</th>
                    </tr>
                  </thead>
                  <tbody id="ks-overall">
                      
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<script src="bootstrap/js/bootstrap.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $.ajax({
      url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
      dataType: "script",
    });

    firstload();

    $('.bootstrap-select>.dropdown-toggle').on('click',function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      $(this).closest('.bootstrap-select').addClass("open");
      $(this).attr('aria-expanded',true);
    })

    $('#preview').on('click',function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      $.ajax({
        method: 'GET',
        url: 'getkartustock',
        data: {
                barang : $('select[name=barang]').val(), 
                gudang : $('select[name=gudang]').val(),
                tglawal : $('#tglawal').val(),
                tglakhir : $('#tglakhir').val(),
              },
        success : function(response){
          // console.log(response);
          $('#ks-po').html(response.viewpo);
          $('#ks-so').html(response.viewso);
          $('#ks-sm').html(response.viewsm);
          $('#ks-overall').html(response.viewoverall);
        },complete:function(){
          var rowbalance = 0;
          $('.ks-row').each(function(){
            var _in = $(this).find('.in').html();           
            var _out = $(this).find('.out').html();            
            var calculate = _in - _out;
            rowbalance = rowbalance+calculate;
            $(this).find('.balance').html(rowbalance);            
          });
        } 
      });      
    });

    function firstload()
    {
      $('#tglawal, #tglakhir').datepicker({
        dateFormat : 'dd-mm-yyyy',
        language: 'en',
      });

      var date = moment();
      var month = date.month()+1; 
      var year = date.year();
      var endofmonth = moment().endOf('month').format('DD');
      var start = '01-'+month+'-'+year;
      var end = endofmonth+'-'+month+'-'+year;
      $('#tglawal').val(start);
      $('#tglakhir').val(end);

      $('.selectpicker').selectpicker('render');
    }
  });
</script>