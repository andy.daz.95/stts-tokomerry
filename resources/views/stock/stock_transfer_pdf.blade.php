<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
</style>
<div>
  <h3 style="text-align: center;">TOKO MATERIAL BU MERRY</h3>
  <h4 style="text-align: center;">Transfer Stock</h4>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 25%">NO Transfer Stock</td>
      <td style="width: 25%">: {{$stockmovement->stock_movement_number}}</td>
      <td style="width: 25%"></td>
      <td style="width: 25%"></td>
    </tr>
    <tr>
      <td>Tgl Transfer</td>
      <td>: {{$stockmovement->date_stock_movement}}</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>Sopir</td>
      <td>: {{$stockmovement->driver_name}}</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>No Plat Kendaraan</td>
      <td>: {{$stockmovement->vehicle_number}}</td>
      <td></td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr style="background-color: #8B8C89">
        <th class="color" style="text-align: center;">Nama</th>
        <th class="color" style="text-align: center;">No Faktur</th>
        <th class="color" style="text-align: center;">Keterangan</th>
        <th class="color" style="width: 100px; text-align: center;">Dari Gudang</th>
        <th class="color" style="width: 100px; text-align: center;">Ke Gudang</th>
        <th class="color" style="width: 100px; text-align: center;">Qty</th>
      </tr>
    </thead>
    <tbody>
      @foreach($stockmovement->details as $key => $value)
      <tr>
        <td>
          <span>{{$value->product->product_name}}</span>
        </td>
        <td>
          <span>{{$value->faktur_number}}</span>
        </td>
        <td>
          <span>{{$value->note}}</span>
        </td>
        <td>
          <span>{{$stockmovement->warehouse_from_data->warehouse_name}}</span>
        </td>
        <td>
          <span>{{$stockmovement->warehouse_to_data->warehouse_name}}</span>
        </td>
        <td>
          <span>{{$value->quantity}}</span>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  
</div>
</div>