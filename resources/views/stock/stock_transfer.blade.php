<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/toastr.css">
<!-- Latest compiled and minified JavaScript -->
<!-- <link rel="stylesheet" href="bootstrap/css/bootstrap.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Stock Transfer</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Filter Periode (Awal Periode)</label>
                  <input type="text" class="f-input" placeholder="Awal Periode" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Periode (Akhir Periode)</label>
                  <input type="text" class="f-input" placeholder="Akhir Periode" />
                </div>
                <div class="input-field col l6">
                  <a href="#" class="btn btn-raised blue white-text">preview</a>
                </div>
              </div>
              <div class="row">
                <div class="input-field col l3">
                  <label>Filter PO</label>
                  <input id="findKode" type="text" class="f-input" placeholder="Filter PO" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Tgl</label>
                  <input id="findPelanggan" type="text" class="f-input" placeholder="Filter Tgl" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Pelanggan</label>
                  <input id="findSupplier" type="text" class="f-input" placeholder="Filter Pelanggan" />
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="stockTransferTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th class="theader">No Stock Transfer</th>
                        <th class="theader">Tanggal</th>
                        <th class="theader">Dari Gudang</th>
                        <th class="theader">Ke Gudang</th>
                        <th class="theader">Sopir</th>
                        <th class="theader">Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($data['stockmovement'] as $key => $value)
                        <tr value="{{$value->stock_movement_id}}" mode="view">
                          <td class="nots">{{$value->stock_movement_number}}</td>
                          <td>{{$value->created_at}}</td>
                          <td>{{$value->warehouse_from_data->warehouse_name}}</td>
                          <td>{{$value->warehouse_to_data->warehouse_name}}</td>
                          <td>{{$value->driver_name}}</td>
                          <td>
                            @if($value->is_received == 0
                                && $value->warehouse_to_data->head_warehouse_data->account_id == Session('user')->account_id)
                              <a class="btn btn-sm btn-raised green darken-2 accept-modal" value="{{$value->stock_movement_id}}"><i class="material-icons">check</i></a>
                            @endif
                            @if($value->is_received == 0)
                              <a class="btn btn-sm btn-raised light-blue darken-2 edit" value="{{$value->stock_movement_id}}" mode="edit"><i class="material-icons">edit</i></a>
                            @endif
                            <a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                          </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Stock Transfer</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="col l12 m12 s12">
                  <button id="tambah-barang" data-target="modal-tambah-barang" class="btn-stoko btn-stoko-primary modal-trigger">Tambah Barang</button>
                </div>
                <br><br>
                <form id="formStocktransfer">
                  <div class='row'>
                    <div class="input-field col l3 m12 s12">
                      <label>No Stock Transfer</label>
                      <input id="nostocktransfer" type="text" class="f-input" name="nostocktransfer" disabled>
                      <input id="idstocktransfer" type="text" class="f-input" name="idstocktransfer" disabled hidden>
                    </div>
                    <div class="input-field col l3 l3 m12 s12">
                      <label>Dari Gudang</label>
                      <select id="gudangfrom" class="browser-default selectpicker" data-live-search="true" data-size="5" name="gudangfrom" @if(isset($data['source'])) disabled= @endif>
                        @foreach($data['gudang'] as $key => $value)
                          <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="input-field col l3 l3 m12 s12">
                      <label>Ke Gudang</label>
                      <select id="gudangto" class="selectpicker browser-default trigger-submit" data-live-search="true" data-size="5"   name="gudangto">
                        @foreach($data['gudang'] as $key => $value)
                          <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="input-field col l3 l3 m12 s12">
                      <label>Tgl Stock Transfer</label>
                      <input id="tglsm" type="text" class="f-input" name="tglsm">
                    </div>
                  </div>
                  <div class="input-field col l3 l3 m12 s12">
                    <label>Nama Sopir</label>
                    <input id="sopir" type="text" class="f-input" name="sopir">
                  </div>
                  <div class="input-field col l3 l3 m12 s12">
                    <label>No Plat Kendaraan</label>
                    <input id="nopol" type="text" class="f-input" name="nopol">
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader" style="width: 300px">Nama</th>
                          <th class="theader" style="width: 100px">Stock <span id="gudangawal">Dari Gudang</span></th>
                          <th class="theader" style="width: 100px">Stock <span id="gudangakhir">Ke Gudang<span></th>
                          @if(isset($data['source'])) <th class="theader">No Faktur</th> @endif
                          <th class="theader">Keterangan</th>
                          <th class="theader" style="width: 75px">Transfer <br> Qty</th>
                          <th class="theader" style="width: 75px">Copy</th>
                        </tr>
                        </thead>
                        <tbody id="barang-data">
                        <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                        <tr class="barang-row" hidden>
                          <td>
                            <div class="input-field">
                              <input id="id" type="text" class="f-input id" name="idbarang[]" hidden>
                              <input id="detail-st" type="text" class="f-input detail" name="detailst[]" hidden>
                              <input id="detail-po" type="text" class="f-input detailpo" name="detailpo[]" hidden>
                              <input id="nama" type="text" class="f-input nama">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="from" type="text" class="f-input from" name="from">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="to" type="text" class="f-input to" name="to">
                            </div>
                          </td>
                          @if(isset($data['source']))
                          <td>
                            <div class="input-field">
                              <input id="fakturnumber" type="text" class="f-input fakturnumber" name="fakturnumber[]" style="width:75%" readonly>
                              <a class="no-border-radius btn btn-sm add-po" style="border-radius: 0px"><i class="material-icons">add</i></a>
                            </div>
                          </td>
                          @endif
                          <td>
                            <div class="input-field">
                              <input id="note" type="text" class="f-input note" name="note[]">
                            </div>
                          </td>
                          <td style="position: relative"0>
                            @if(isset($data['source']))
                            <a href="javascript:void(0);" class="totalordertooltip" title="0" style="position: absolute; right: 5px; top: 10px;"><i style="font-size: 14px;" class="material-icons">help_outline</i></a>
                            @endif
                            <div class="input-field">
                              <input id="totalorder" type="text" class="f-input totalorder" hidden>
                              <input id="qty" type="text" class="f-input qty" name="qty[]">
                            </div>
                          </td>
                          <td>
                            <a class="btn btn-sm white grey-text copy-detail" style="margin: 5px"><i class="material-icons">content_copy</i></a>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="input-field right">
                      <a id="submit-ts" href="" class="btn-stoko btn-stoko-primary" mode="save"
                      @if(isset($data['source'])) page="{{$data['source']}}" @endif>Submit</a>
                      <a id="edit-ts" href="" class="btn-stoko btn-stoko-primary" mode="edit"
                      @if(isset($data['source'])) page="{{$data['source']}}" @endif hidden>Edit</a>
                      <a id="cetak-ts" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-tambah-barang" class="modal">
  <form id="form-list-barang">
    <div class="modal-content">
      <h4>Tambah Barang</h4>
      <div id="append-barang" class="row">

      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-barang">OK</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>

<div id="modal-delete-ts" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-ts" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<div id="modal-accept-ts" class="modal">
  <div class="modal-content">
    <h5 id="accept-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-accept-ts" class="modal-action modal-close waves-effect btn-stoko green white-text">Terima</a>
  </div>
</div>

<div id="modal-list-po" class="modal">
  <form id="form-list-po">
    <div class="modal-content">
      <h4>List PO</h4>
      <div id="append-po" class="row">

      </div>
    </div>
    <div class="modal-footer">
      <a id="submit-po" href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius">OK</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        $('#tambah-barang').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var jumlah = $('.barang-row').length;
            var barang = [];
            var src = "st";

            $('.id').each(function(k,v){
                var id = $(this).val();
                barang.push(id);
            });

            $.ajax({
                type:"GET",
                url:"modalListBarang",
                data:{barang:barang, src:src},
                success:function(response){
                    $('#append-barang').html(response);
                },
                complete:function(){
                    $('#modal-tambah-barang').modal('open');
                }
            })
        });

        $('#formStocktransfer').on('click', '.add-po', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $product = $(this).closest('tr').find('.id').val();

            element = $(this);
            nopo = iddetail = totalorder = "";

            $.ajax({
                type:"GET",
                url:"getDetailPoByProduct",
                data:{product: $product},
                success:function(response){
                    $('#append-po').html(response);
                },
                complete:function(){
                    $('#modal-list-po').modal('open');
                }
            })

            $('#append-po').on('click', '.radio', function(event){
              event.stopImmediatePropagation();
              nopo = $(this).closest('tr').find('td:eq(0)').html();
              iddetail = $(this).closest('tr').attr('value');
              totalorder = $(this).closest('tr').find('td:eq(2)').html();
            });

            $('#submit-po').on('click', function(event){
              event.stopImmediatePropagation();
              $('#modal-list-po').modal('close');
              element.closest('td').find('input').val(nopo);
              element.closest('tr').find('.detailpo').val(iddetail);
              element.closest('tr').find('.totalordertooltip').attr("title", "Jumlah Order: "+totalorder);
              element.closest('tr').find('.totalorder').val(totalorder);
            });
        });

        $('#gudangfrom').on('change', function(){
            var idgudang = $(this).val();
            var temp = $('#gudangfrom').html();
            $('#gudangto').html(temp);
            $('#gudangto option').each(function (){
                if($(this).val() == idgudang)
                {
                    $(this).remove();
                }
            })
            $("#gudangto option:selected").prop("selected", false);
            $("#gudangto option:first").prop("selected", "selected");

            $('#barang-data').find('.qty').val('');
            $('.selectpicker').selectpicker('refresh');
            getBarang();
        });

        firstload();
        // $('.selectpicker').selectpicker('render');

        $('.bootstrap-select>.dropdown-toggle').on('click',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $(this).closest('.bootstrap-select').addClass("open");
            $(this).attr('aria-expanded',true);
        })

        $('#submit-ts, #edit-ts').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            var validqty = [];
            var validharga = [];
            var validstock = [];
            var validstockbyfaktur = [];
            var url = "";

            var mode = $(event.currentTarget).attr('mode');
            var page = $(event.currentTarget).attr('page');
            var kode = $('#nostocktransfer').val();

            if(mode == 'save')
            {
                var url = "createstocktransfer";
                var successmessage = 'Stock Transfer '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatestocktransfer";
                var successmessage = 'Stock Transfer '+kode+' telah berhasil diubah!';
            }

            $('.barang-row').each(function(){
                if($(this).find('.qty').val() != '')
                {
                    validqty.push(ceknumber($(this).find('.qty').val()));
                }else{
                    validqty.push(false);
                }

                if(parseInt($(this).find('.qty').val()) <= parseInt($(this).find('.from').val()))
                {
                    validstock.push(true);
                }else{
                    validstock.push(false);
                }

                if(page == "parangloe")
                {
                  if(parseInt($(this).find('.qty').val()) <= parseInt($(this).find('.totalorder').val()))
                  {
                    validstockbyfaktur.push(true);
                  }else{
                    validstockbyfaktur.push(false);
                  }
                }
            });

            if($('.barang-row:first').attr('hidden') == 'hidden' && $('.barang-row:first').attr('deleted') != 'true')
            {
                toastr.warning('Anda Belum Memilih Barang!');
            }else if(validqty.indexOf(false) > -1){
                toastr.warning('Quantity harus berupa angka!');
            }else if(validstock.indexOf(false) > -1){
                toastr.warning('Quantity melebihi stok!');
            }else if(page == "parangloe" && validstockbyfaktur.indexOf(false) > -1){
                toastr.warning('Quantity melebihi jumlah order!');
            }else{ // validasi sukses
                me.invisible();
                me.data('requestRunning', true);
                $('#gudangfrom, #nostocktransfer, #tglsm, #idstocktransfer').removeAttr('disabled');
                $.ajax({
                    type:"POST",
                    url:url,
                    data:$(this).closest('#formStocktransfer').serialize(),
                    success:function(response){
                        toastr.success(successmessage ,{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                })
            }
        })


        $('body').on('click','#stockTransferTable.highlight tr, #stockTransferTable .edit',function(event){
            event.stopImmediatePropagation();
            var mode = $(event.currentTarget).attr('mode');
            var id = $(this).attr('value');
            $.ajax({
                type:"GET",
                url:"getstocktransfer",
                data:{id:id},
                success:function(response){
                    $('#idstocktransfer').val(response.stock_movement_id).attr('disabled',true);
                    $('#barang').val(response.product_id).attr('disabled',true);
                    $('#gudangfrom').val(response.warehouse_from).attr('disabled',true);
                    $('#gudangto').val(response.warehouse_to).attr('disabled',true);
                    $('#sopir').val(response.driver_name).attr('disabled',true);
                    $('#nopol').val(response.vehicle_number).attr('disabled',true);
                    $('#qty').val(response.quantity).attr('disabled',true);
                    $('#no-item').attr('hidden',true);
                    $('#nama').val(response.product_name).attr('disabled',true);
                    $('#id').val(response.product_id);
                    $('#to').val(response.qtyto).attr('disabled',true);
                    $('#from').val(response.qtyfrom).attr('disabled',true);

                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    console.log(response.length)
                    console.log(response.details.length)
                    if(response)
                    {
                        for(var i=0; i<response.details.length; i++){
                            console.log(response.details[i].product.qty_from.quantity)
                            var newid = "id-"+(i+1);
                            var newnama = "nama-"+(i+1);
                            var newqty = "qty-"+(i+1);
                            var newfrom = "from-"+(i+1);
                            var newfaktur = "fakturnumber-"+(i+1);
                            var newtotalorder = "totalorder-"+(i+1);
                            var newnote = "note-"+(i+1);
                            var newto = "to-"+(i+1);
                            var newdetail = "detail-st-"+(i+1);
                            var newdetailpo = "detail-po-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('.id').attr('id',newid);
                            $temp.find('.nama').attr('id',newnama);
                            $temp.find('.from').attr('id',newfrom);
                            $temp.find('.to').attr('id',newto);
                            $temp.find('.fakturnumber').attr('id',newfaktur);
                            $temp.find('.note').attr('id',newnote);
                            $temp.find('.qty').attr('id',newqty);
                            $temp.find('.detail').attr('id',newdetail);
                            $temp.find('.detailpo').attr('id',newdetailpo);
                            $temp.find('.totalorder').attr('id',newtotalorder);
                            $temp.appendTo('#barang-data')
                            $('#id-'+(i+1)).val(response.details[i].product.product_id);
                            $('#detail-st-'+(i+1)).val(response.details[i].stock_movement_details_id);
                            $('#nama-'+(i+1)).val(response.details[i].product.product_name).attr('disabled',true);
                            $('#from-'+(i+1)).val(response.details[i].product.qty_from.quantity).attr('disabled',true);
                            $('#to-'+(i+1)).val(response.details[i].product.qty_to.quantity).attr('disabled',true);
                            $('#fakturnumber-'+(i+1)).val(response.details[i].faktur_number).attr('disabled',true);
                            $('#note-'+(i+1)).val(response.details[i].note).attr('disabled',true);
                            $('#qty-'+(i+1)).val(response.details[i].quantity).attr('disabled',true);
                            if(response.details[i]['detailpo'] != null)
                            {
                              $('#totalorder-'+(i+1)).val(response.details[i]['detailpo'].quantity + response.details[i]['detailpo'].free_qty);
                            }
                            $('#detail-po-'+(i+1)).val(response.details[i].purchase_order_details_id);
                            $('#no-item').attr('hidden',true);
                        }
                    }else{
                        $original.clone();
                        $original.attr('hidden',true)
                        $original.appendTo('#barang-data');
                        $('#formStocktransfer').find("input[type=text], textarea").val("");
                        $('#no-item').removeAttr('hidden');
                    }
                    $('.barang-row').removeAttr('hidden');
                    $('.selectpicker').selectpicker('refresh');
                },complete:function(){
                    $('#submit-ts').attr('hidden',true);
                    if(mode == 'edit')
                    {
                        $('#submit-ts').attr('hidden',true)
                        $('#edit-ts').removeAttr('hidden')
                        $('.qty, .note, .fakturnumber, #tglsm').removeAttr('disabled');
                    }else{
                        $('#cetak-ts').removeAttr('hidden').attr('href',"downloadstocktransfer/"+id);
                    }
                }
            });
        })

        $('.trigger-submit').change(function(event){
            event.stopImmediatePropagation();
            getBarang();
        });

        $('body').on('click','#stockTransferTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var nots = $(this).closest('tr').find('.nots').html();
            var idts = $(this).closest('tr').attr('value');
            $('#confirm-delete-ts').attr('value',idts).attr('nomor',nots);
            $("#delete-message").html("Yakin ingin menghapus data "+nots+" ?")
            $('#modal-delete-ts').modal('open');
        });


        $('#confirm-delete-ts').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-ts').modal('close');
            var id = $(this).attr('value');
            var nots = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletestocktransfer",
                data:{id:id},
                success:function(response){
                    toastr.success('Stock Transfer '+nots+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        $('.accept-modal').on('click', function(event){
            event.stopImmediatePropagation();
            var nots = $(this).closest('tr').find('.nots').html();
            var idts = $(this).closest('tr').attr('value');
            $('#confirm-accept-ts').attr('value',idts).attr('nomor',nots);
            $("#accept-message").html("Apakah Pengiriman "+nots+" Sudah Diterima ?")
            $('#modal-accept-ts').modal('open');
        });

        $('#confirm-accept-ts').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-accept-ts').modal('close');
            var id = $(this).attr('value');
            var nots = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"acceptstocktransfer",
                data:{id:id},
                success:function(response){
                    toastr.success('Stock Transfer '+nots+' telah berhasil Diterima!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        $('#formStocktransfer').on('click','.copy-detail',function(event){
            event.stopImmediatePropagation();
            var $original = $(this).closest('.barang-row');
            var $cloned = $original.clone();
            $cloned.find('input:enabled:not(.id)').val('');
            $cloned.find('.totalordertooltip').attr('title',"0");
            $(this).closest('.barang-row').after($cloned);

        });

        $('.submit-barang').on('click', function(event){
            event.stopImmediatePropagation();
            modalBarangTable.search('').draw();
            var barang =[];
            var from = $('#gudangfrom').val();
            var to = $('#gudangto').val();
            modalBarangTable.rows().nodes().to$().find('input[type="checkbox"]:checked').each(function(){
                console.log($(this).closest('tr').attr('value'));
                barang.push($(this).closest('tr').attr('value'));
            });

            $.ajax({
                type:"GET",
                url:"getBarangModalStockTransfer",
                data: {barang:barang, from:from, to:to, objectArr: getBarangObject()},
                success:function(response){
                    var object = response['objectArr'];
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    if(response.product.length > 0)
                    {
                      $.each(response.product, function(i,v){
                        var newid = "id-"+(i+1);
                        var newnama = "nama-"+(i+1);
                        var newdetailpo = "detail-po-"+(i+1);
                        var newqty = "qty-"+(i+1);
                        var newtotalorder = "totalorder-"+(i+1);
                        var newfrom = "from-"+(i+1);
                        var newto = "to-"+(i+1);
                        var newfaktur = "fakturnumber-"+(i+1);
                        var newnote = "note-"+(i+1);
                        $temp = $original.clone();
                        $temp.find('input:enabled').val('');
                        $temp.removeAttr('hidden');
                        $temp.attr('row', (i+1));
                        $temp.find('.id').attr('id',newid);
                        $temp.find('.nama').attr('id',newnama);
                        $temp.find('.from').attr('id',newfrom);
                        $temp.find('.to').attr('id',newto);
                        $temp.find('.qty').attr('id',newqty);
                        $temp.find('.totalorder').attr('id',newtotalorder);
                        $temp.find('.fakturnumber').attr('id',newfaktur);
                        $temp.find('.note').attr('id',newnote);
                        $temp.find('.detailpo').attr('id',newdetailpo);
                        $temp.appendTo('#barang-data');

                        flag = object.findIndex(function(val) {
                          if(typeof val != 'undefined')
                            return JSON.parse(val).id == v.product_id
                        });
                        if (flag != -1)
                        {
                          $('#qty-'+(i+1)).val(JSON.parse(object[flag]).qty);
                          $('#totalorder-'+(i+1)).val(JSON.parse(object[flag]).order);
                          $('#detail-po-'+(i+1)).val(JSON.parse(object[flag]).detailpo);
                          $('#fakturnumber-'+(i+1)).val(JSON.parse(object[flag]).nopo);
                          delete object[flag]
                        }

                        $('#to-'+(i+1)).val(v.qty_to.quantity).attr('disabled',true);
                        $('#from-'+(i+1)).val(v.qty_from.quantity).attr('disabled',true);
                        $('#id-'+(i+1)).val(v.product_id);
                        $('#nama-'+(i+1)).val(v.product_name).attr('disabled',true);
                        $('#no-item').attr('hidden',true);

                      });
                    }else{
                        $original.clone();
                        $original.attr('hidden',true)
                        $original.appendTo('#barang-data');
                        $('#formStocktransfer').find("input[type=text], textarea").val("");
                        $('#no-item').removeAttr('hidden');
                    }
                    $('#grandtotal').val(0);
                    $('#modal-tambah-barang').modal('close');
                }
            });
        });

        //function
        function firstload()
        {
            $.ajax({
                type:"GET",
                url:"laststocktransfernumber",
                success:function(response){
                    $('#nostocktransfer').val(response);
                }
            })

            $('#tglsm').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate : new Date(),
                language: 'en',
            });

            $('#tglsm').val(moment().format('DD-MM-YYYY'));

            $('#gudangfrom').trigger('change');
            $('.selectpicker').selectpicker('render');


            stockTransferTable = $('#stockTransferTable').DataTable({ // This is for home page
                searching: true,
                responsive: true,
                'sDom':'tip',
                "bPaginate":true,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                "aaSorting" :[],
                language: {
                    "sProcessing":   "Sedang proses...",
                    "sLengthMenu":   "Tampilan _MENU_ entri",
                    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                    "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix":  "",
                    "sSearch":       "Cari:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Awal",
                        "sPrevious": "Balik",
                        "sNext":     "Lanjut",
                        "sLast":     "Akhir"
                    }
                },
            });
        }

        function sortgudang(id){
            var temp = $('#gudangfrom').html(q4);
            $('#gudangto').html(temp);
            $('#gudangto option').each(function (){
                if($(this).val() == id)
                {
                    $(this).remove();
                }
            })
            $("#gudangto option:selected").prop("selected", false);
            $("#gudangto option:first").prop("selected", "selected");
            $('.selectpicker').selectpicker('refresh');
        }

        function getBarang()
        {
            var barang = [];
            var from = $('#gudangfrom').val();
            var to = $('#gudangto').val();

            $('.id').each(function(k,v){
                var id = $(this).val();
                if(id != "")
                {
                    barang.push(id);
                }
            });

            if(barang.length > 0)
            {
                $.ajax({
                    type:"GET",
                    url:"getBarangModalStockTransfer",
                    data: {barang:barang, from:from, to:to},
                    success:function(response){
                        var $original = $('.barang-row:first');
                        var $cloned = $original.clone();
                        $('.barang-row').remove();
                        if(response.product.length > 0)
                        {
                          $.each(response.product, function(i,v){
                            var newid = "id-"+(i+1);
                            var newnama = "nama-"+(i+1);
                            var newqty = "qty-"+(i+1);
                            var newfrom = "from-"+(i+1);
                            var newto = "to-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('.id').attr('id',newid);
                            $temp.find('.nama').attr('id',newnama);
                            $temp.find('.from').attr('id',newfrom);
                            $temp.find('.to').attr('id',newto);
                            $temp.find('.qty').attr('id',newqty);
                            $temp.appendTo('#barang-data')
                            $('#id-'+(i+1)).val(v.product_id);
                            $('#nama-'+(i+1)).val(v.product_name).attr('disabled',true);
                            $('#from-'+(i+1)).val(v.qty_from.quantity).attr('disabled',true);
                            $('#to-'+(i+1)).val(v.qty_to.quantity).attr('disabled',true);
                            $('#no-item').attr('hidden',true);
                          });
                        }else{
                            $original.clone();
                            $original.attr('hidden',true)
                            $original.appendTo('#barang-data');
                            $('#formStocktransfer').find("input[type=text], textarea").val("");
                            $('#no-item').removeAttr('hidden');
                        }
                    }
                });
            }
        }

        function getBarangObject()
        {
          objectArr = [];
          $('.id').each(function(k,v){
            // assign object

            var id = $(this).val();
            var detailpo = $(this).closest('tr').find('.detailpo').val();
            var keterangan = $(this).closest('tr').find('.note').val();
            var totalorder = $(this).closest('tr').find('.totalorder').val();
            var nopo = $(this).closest('tr').find('.fakturnumber').val();
            var qty = $(this).closest('tr').find('.qty').val();

            var object = {id: id, detailpo:detailpo, note: keterangan, nopo:nopo, order:totalorder, qty: qty}
            objectArr.push(JSON.stringify(object));
          });
          console.log(objectArr);
          return objectArr;
        }
    });
</script>