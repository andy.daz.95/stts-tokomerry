<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Stock Correction</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Filter PO</label>
                  <input id="findKode" type="text" class="f-input" placeholder="Filter PO" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Tgl</label>
                  <input id="findPelanggan" type="text" class="f-input" placeholder="Filter Tgl" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Pelanggan</label>
                  <input id="findSupplier" type="text" class="f-input" placeholder="Filter Pelanggan" />
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="stockCorrectionTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th class="theader">No Stock Transfer</th>
                        <th class="theader">Tanggal</th>
                        <th class="theader">Nama Barang</th>
                        <th class="theader">Gudang</th>
                        <th class="theader">Quantity Sistem</th>
                        <th class="theader">Quantity Aktual</th>
                        <th class="theader">Status</th>
                        <th class="theader">Notes</th>
                        <th class="theader">Dibuat Oleh</th>
                        <th class="theader">Action</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($data['stockcorrection'] as $key => $value)
                        <tr value="{{$value->stock_correction_id}}">
                          <td class="nocs">{{$value->stock_correction_number}}</td>
                          <td>{{$value->date_stock_correction}}</td>
                          <td>{{$value->product_name}}</td>
                          <td>{{$value->warehouse->warehouse_name}}</td>
                          <td>{{$value->projected_qty}}</td>
                          <td>{{$value->actual_qty}}</td>
                          <td>
                            @if($value->actual_qty > $value->projected_qty)
                              + {{$value->actual_qty - $value->projected_qty}}
                            @else
                              - {{ $value->projected_qty - $value->actual_qty}}
                            @endif
                          </td>
                          <td>{{$value->note}}</td>
                          <td>{{$value->account->full_name}}</td>
                          <td>
                            @if($value->is_approved == 0
                                && (Session('roles')->role_id == 1 || Session('roles')->name == 'master' ))
                              <a class="btn btn-sm btn-raised green darken-2 approve-modal" value="{{$value->stock_movement_id}}"><i class="material-icons">check</i></a>
                            @endif
                          </td>
                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Stock Correction</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formStockCorrection">
                <div class="row">
                  <br>
                  <div class="input-field col l3">
                    <label>Stock Correction Number</label>
                    <input id="nostockcorrection" type="text" name="nostockcorrection" class="f-input" disabled>
                  </div>
                  <div class="input-field col l3 l3 m12 s12">
                    <label>Tgl Stock Correction</label>
                    <input id="tglsc" type="text" class="f-input" name="tglsc">
                  </div>
                  <div class="input-field col l3">
                    <label>Barang</label>
                    <select id="#barang" class="browser-default selectpicker margin-zero" data-live-search="true" data-size="5"   name="barang">
                      <option>Select Barang</option>
                      @foreach($data['barang'] as $key => $value)
                        <option value="{{$value->product_id}}">{{$value->product_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Gudang</label>
                    <select id="#gudang" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="gudang">
                      <option value="0">All Gudang</option>
                      @foreach($data['gudang'] as $key => $value)
                        <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col l12">
                    <a id="preview" href="#" class="btn btn-raised blue white-text" style="margin-bottom: 15px">preview</a>
                  </div>
                  <div class="col l12 m12 s12">
                    <table class="table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th class="theader" style="width: 300px">Product Name</th>
                        <th class="theader">Quantity Sistem</th>
                        <th class="theader">Quantity Aktual</th>
                        <th class="theader">Note</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td>
                          <input id="productname" type="text" name="productname" class="f-input" disabled>
                        </td>
                        <td>
                          <input id="projectedqty" type="text" name="projectedqty" class="f-input" disabled>
                        </td>
                        <td>
                          <input id="actualqty" type="text" name="actualqty" class="f-input">
                        </td>
                        <td>
                          <input id="notes" type="text" name="notes" class="f-input">
                        </td>
                      </tr>
                      </tbody>
                    </table>
                    <div class="input-field right">
                      <a id="submit-sc" href="" class="btn-stoko btn-stoko-primary">Submit</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-approve-cs" class="modal">
  <div class="modal-content">
    <h5 id="accept-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-approve-cs" class="modal-action modal-close waves-effect btn-stoko green white-text">Terima</a>
  </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('.bootstrap-select>.dropdown-toggle').on('click',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $(this).closest('.bootstrap-select').addClass("open");
            $(this).attr('aria-expanded',true);
        })

        $('#preview').on('click',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $.ajax({
                method: 'GET',
                url: 'getstockinwarehouse',
                data: {barang : $('select[name=barang]').val(), gudang : $('select[name=gudang]').val()},
                success : function(response){
                    $('#projectedqty').val(response.quantity);
                    $('#productname').val(response.product_name);
                    $('#title').html("<b>"+response.product_name+" in "+response.warehouse_name+"</b>")
                }
            });
        });

        $('#submit-sc').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            me.data('requestRunning', true);
            $('#projectedqty, #actualqty , #nostockcorrection').removeAttr('disabled');
            var kode = $('#nostockcorrection').val();
            $.ajax({
                type:"POST",
                url:"createstockcorrection",
                data:$(this).closest('#formStockCorrection').serialize(),
                success:function(response){
                    toastr.success('Stock Correction '+kode+' has been Created!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                },
                complete: function() {
                    me.data('requestRunning', false);
                }
            })
        });

        $('.approve-modal').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var nots = $(this).closest('tr').find('.nocs').html();
            var idcs = $(this).closest('tr').attr('value');
            $('#confirm-approve-cs').attr('value',idcs).attr('nomor',nots);
            $("#accept-message").html("Apakah Anda yakin melakukan koreksi?")
            $('#modal-approve-cs').modal('open');
        });

        $('#confirm-approve-cs').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-approve-cs').modal('close');
            var id = $(this).attr('value');
            var nocs = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"approvestockcorrection"  ,
                data:{id:id},
                success:function(response){
                    toastr.success('Stock Koreksi telah berhasil!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        function firstload()
        {
            $.ajax({
                type:"GET",
                url:"laststockcorrectionnumber",
                success:function(response){
                    $('#nostockcorrection').val(response);
                }
            })

            $('#tglsc').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate : new Date(),
                language: 'en',
            });
            $('#tglsc').val(moment().format('DD-MM-YYYY'));

            $('.selectpicker').selectpicker('render');

            stockCorrectionTable = $('#stockCorrectionTable').DataTable({ // This is for home page
                searching: true,
                responsive: true,
                'sDom':'tip',
                "bPaginate":true,
                "bFilter": false,
                "sPaginationType": "full_numbers",
                "iDisplayLength": 10,
                'aaSorting' : [],
                language: {
                    "sProcessing":   "Sedang proses...",
                    "sLengthMenu":   "Tampilan _MENU_ entri",
                    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                    "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix":  "",
                    "sSearch":       "Cari:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Awal",
                        "sPrevious": "Balik",
                        "sNext":     "Lanjut",
                        "sLast":     "Akhir"
                    }
                },
            });
        }
    });
</script>