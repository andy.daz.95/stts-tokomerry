<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Informasi Produk</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <div class="input-field col l3">
                  <label>Filter Spandek</label>
                  <input id="filterSpandek" type="text" class="f-input" placeholder="Filter Spandek" />
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="InfoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th class="theader">Nama Barang</th>
                        <th class="theader">Gudang</th>
                        <th class="theader">Tanggal</th>
                        <th class="theader">Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{-- Untuk Datatable Server Side --}}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Informasi Produk</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formInfoProduct">
                  <div class="input-field col l3">
                    <label>Nama Barang</label>
                    <select id="product" class="selectpicker browser-default" data-live-search="true" data-size="5" name="product">
                      <option value="0">Pilih Barang</option>
                      @foreach($data['product'] as $key => $value)
                        <option value="{{$value->product_id}}">{{$value->product_name.' / '.$value->product_code}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Gudang</label>
                    <select id="warehouse" class="selectpicker browser-default" data-live-search="true" data-size="5"   name="sales">
                      <option value="0">Pilih Gudang</option>
                      @foreach($data['warehouse'] as $key => $value)
                        <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>User</label>
                    <select id="user" class="selectpicker browser-default" data-live-search="true" data-size="5"   name="sales">
                      <option value="0">Pilih Sales</option>
                      @foreach($data['user'] as $key => $value)
                        <option value="{{$value->account_id}}">{{$value->full_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Tgl Catatan</label>
                    <input id="tglinfo" type="text" class="f-input" name="tglinfo" placeholder="">
                  </div>
                  <div class="row">
                    <div class="input-field col l6 m12 s12" style="position:relative;">
                      <textarea id="prevInfo" name="prevInfo" class="f-textarea" style="height:400px; position:relative" disabled></textarea>
                      <button id="copy" type="button" style="position:absolute; right: 15px; top : 10px">Copy</button>
                    </div>
                    <div class="col l6 m12 s12 row">
                      <div class="group-row row">
                        <div class="row">
                        <div class="input-field col l10 m8 s8">
                          <label for="owner">Pemilik</label>
                          <input id="" name="owner[]" class="f-input owner">
                        </div>
                        <div class="input-field col l1 m4 s4 ">
                          <label></label>
                          <button class="btn btn-stoko add-group blue"><i class="material-icons">add</i></button>
                        </div>
                        <div class="input-field col l1 m4 s4">
                          <label></label>
                          <button class="btn btn-stoko remove-group red"><i class="material-icons">remove</i></button>
                        </div>
                        </div>
                        <div class="info-row row no-margin">
                          <div class="input-field col l3 m12 s12">
                            <label>M / Lembar</label>
                            <input id="" name="ukuran[]" type="text" class="f-input ukuran" placeholder="">
                          </div>
                          <div class="input-field col l3 m12 s12">
                            <label>Total Lembar</label>
                            <input id="" name="owner[]" type="text" class="f-input totallembar">
                          </div>
                          <div class="input-field col l3 m12 s12">
                            <label>Total Meter</label>
                            <input id="" name="totalmeter[]" type="text" class="f-input totalmeter" disabled>
                          </div>
                          <div class="input-field col l1 m12 s12">
                            <button class="btn btn-stoko add-info green"><i class="material-icons">add</i></button>
                          </div>
                          <div class="input-field col l1 m12 s12">
                            <button class="btn btn-stoko remove-info red"><i class="material-icons">remove</i></button>
                          </div>
                        </div>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <label>Total Meter</label>
                        <input id="grandtotalmeter" name="grandtotalmeter" class="f-input" disabled>
                      </div>
                    </div>
                  </div>
                  <div>
                    <button id="submit-catatan" type="button">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<div id="modal-info" class="modal">
  <div class="modal-content">
    <div class="input-field">
      <textarea id="InfoModalContent" name="prevInfo" class="f-textarea" style="height:400px; position:relative" disabled></textarea>
    </div>
  </div>
  <div class="modal-footer">
    <a id="delete-info" class="modal-action modal-close waves-effect btn-stoko red white-text">Hapus</a>
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">Batal</a>
  </div>
</div>
<div id="example-group" hidden>
  <div class="group-row row">
    <div class="input-field col l19 m10 s10">
      <label for="owner">Pemilik</label>
      <input id="" name="owner[]" class="f-input owner">
    </div>
    <div class="input-field col l1 m2 s2">
      <label></label>
      <button class="btn btn-stoko remove-group red"><i class="material-icons">remove</i></button>
    </div>
    <div class="info-row row no-margin">
      <div class="input-field col l3 m3 s3">
        <label>M / Lembar</label>
        <input id="" name="ukuran[]" type="text" class="f-input ukuran" placeholder="">
      </div>
      <div class="input-field col l3 m3 s3">
        <label>Total Lembar</label>
        <input id="" name="owner[]" type="text" class="f-input totallembar">
      </div>
      <div class="input-field col l3 m3 s3">
        <label>Total Meter</label>
        <input id="" name="totalmeter[]" type="text" class="f-input totalmeter" disabled>
      </div>
      <div class="input-field col l1 m1 s1">
        <button class="btn btn-stoko add-info green"><i class="material-icons">add</i></button>
      </div>
    </div>
  </div>
  <div class="group-row row">
    <div class="input-field col l10 m10 s10">
      <label for="owner">Pemilik</label>
      <input id="" name="owner[]" class="f-input owner">
    </div>
    <div class="input-field col l2 m2 s2 right">
      <label></label>
      <button class="btn btn-stoko remove-group">Hapus</button>
    </div>
    <div class="info-row row no-margin">
      <div class="input-field col l3 m3 s3">
        <label>M / Lembar</label>
        <input id="" name="ukuran[]" type="text" class="f-input ukuran" placeholder="">
      </div>
      <div class="input-field col l3 m3 s3">
        <label>Total Lembar</label>
        <input id="" name="owner[]" type="text" class="f-input totallembar">
      </div>
      <div class="input-field col l3 m3 s3">
        <label>Total Meter</label>
        <input id="" name="totalmeter[]" type="text" class="f-input totalmeter" disabled>
      </div>
      <div class="input-field col l1 m1 s1">
        <button class="btn btn-stoko remove-info red"><i class="material-icons">remove</i></button>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#formInfoProduct').on('click','.add-info',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var row = $(this).closest('.info-row');
            original = $('#example-group .info-row:last').clone();
            row.after(original.addClass('cloned'));
        });

        $('#formInfoProduct').on('click','.add-group',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var row = $(this).closest('.group-row');
            original = $('#example-group .group-row:first').clone();
            row.after(original.addClass('cloned'));
        });

        $('#formInfoProduct').on('keyup', '.ukuran, .totallembar', function(event){
            event.stopImmediatePropagation();
            var row = $(this).closest('.info-row');
            var ukuran = row.find('.ukuran').val();
            var lembar = row.find('.totallembar').val();

            if((lembar !=='' &&  lembar > 0) && (ukuran !='' &&  ukuran > 0))
            {
                var totalmeter = ukuran * lembar;
            }else{
                var totalmeter = 0;
            }
            row.find('.totalmeter').val(totalmeter);
            counttotalmeter();
        });

        $('#formInfoProduct').on('click','.remove-info',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            if ($(this).closest('.group-row').find('.info-row:last').is($(this).closest('.info-row'))
                &&
                    $(this).closest('.group-row').find('.info-row:first').is($(this).closest('.info-row'))
                ){
                $(this).closest('.info-row').find('input[type="text"]').val('');
            }else if ($(this).closest('.group-row').find('.info-row:first').is($(this).closest('.info-row'))) {
                $(this).closest('.group-row').find('.info-row:eq(1)').find('div:eq(2)').after('<div class="input-field col l1 m12 s12">\n' +
                    '                            <button class="btn btn-stoko add-info green"><i class="material-icons">add</i></button>\n' +
                    '                          </div>');
                $(this).closest('.info-row').remove();
            }else{
                $(this).closest('.info-row').remove();
            }
        });

        $('#formInfoProduct').on('click','.remove-group', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            if ($(this).closest('#formInfoProduct').find('.group-row:last').is($(this).closest('.group-row'))
                &&
                $(this).closest('#formInfoProduct').find('.group-row:first').is($(this).closest('.group-row'))
            ){
                $(this).closest('.group-row').find('input[type="text"]').val('');
            }else if ($(this).closest('#formInfoProduct').find('.group-row:first').is($(this).closest('.group-row'))) {
                $(this).closest('#formInfoProduct').find('.group-row:eq(1)').find('div:eq(0)').after('<div class="input-field col l1 m12 s12">\n' +
                    '                            <button class="btn btn-stoko add-group blue"><i class="material-icons">add</i></button>\n' +
                    '                          </div>');
                $(this).closest('.group-row').remove();
            }else{
                $(this).closest('.group-row').remove();
            }

        });

        $('#product').on('change', function (event){
            event.preventDefault();
            event.stopImmediatePropagation();
            getLastProductInfo('get');
        });

        $('#InfoTable').on('click','.open-info', function (event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $.ajax({
                method:'get',
                data:{id : $(this).closest('tr').attr('value')},
                url:'getadditionalinfodatabyid',
                success:function(response){
                    if(response) {
                        var line1 = response.date;
                        var line2 = response.product.product_name;
                        var line3 = response.warehouse.warehouse_name;
                        var line4 = '';
                        var total = 0;

                        $.each(response.owner, function(i,v){
                            var owner = v.name+'\n';
                            var detail = '';
                            $.each(v.detail, function(j,x){
                                var ukuran = x.ukuran;
                                var lembar = x.lembar;
                                var temptotal = ukuran * lembar;
                                total = total + temptotal;
                                detail = detail + ukuran +' Meter X '+ lembar + ' Lembar = '+ temptotal +' Meter \n';
                            });
                            line4 = line4 + owner + detail;
                        });
                        var text = line1 + '\n' + line2 + '\n' + line3 + '\n'+ line4 + '\nTotal = ' + total+' Meter';
                        $('#InfoModalContent').val(text);
                        $('#modal-info').modal('open');
                    }
                }
            });
        });

        $('#submit-catatan').on('click',function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var detail = {};
            $('#formInfoProduct').find('.group-row').each(function(i,v){
                detail[i] = {};
                detail[i]['owner'] = $(this).find('.owner').val();
                detail[i]['info'] = [];

                $(this).find('.info-row').each(function (j,x){
                    product = {};
                    var ukuran = $(this).find('.ukuran').val();
                    var lembar = $(this).find('.totallembar').val();

                    product['ukuran'] = ukuran;
                    product['lembar'] = lembar;

                    detail[i]['info'].push(product);
                })
            });

            main = {};
            main['product'] = $('#product').val();
            main['warehouse'] = $('#warehouse').val();
            main['user'] = $('#user').val();
            main['date'] = $('#tglinfo').val();

            $.ajax({
                method:'get',
                url:'createadditionalinfo',
                data: {main:main, detail:detail},
                success:function(){
                    toastr.success('Berhasil!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        $('#copy').on('click',function(event){
            if($('#prevInfo').val() != '')
            {
                $('.cloned').remove();
                getLastProductInfo('copy');
            }

        });

        function counttotalmeter(){
            var total = 0;
            $('#formInfoProduct .totalmeter').each(function(i,v){
                var meter = $(this).val();
                total = parseFloat(total) + parseFloat(meter);
            });

            $('#grandtotalmeter').val(total);
        }

        //function
        function firstload()
        {
            $('#user').val(17).attr('disabled',true); // Ponci
            $('#warehouse').val(6).attr('disabled',true); // Gudang Ponci

            $('.selectpicker').selectpicker('render');

            $('#tglinfo').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tglinfo').val(moment().format('DD-MM-YYYY'));

            var infoTable = $('#InfoTable').DataTable({
                processing: true,
                serverSide: true,
                "aaSorting": [],
                "dom" : "ti",
                ajax: {
                    url : 'getadditionalinfotable',
                    data: function (d) {
                        d.spandek = $('#filterSpandek').val();
                    }
                },
                rowId : 'additional_product_info_id',
                columns: [
                    { data: 'product.product_name', name: 'product_name' },
                    { data: 'warehouse.warehouse_name',  name: 'warehouse_name' },
                    { data: 'date', name: 'date'},
                    { data: 'action', name:'action'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterSpandek').on('keyup', function () { // This is for news page
                infoTable.draw();
            });
        }


        function getLastProductInfo(type){
            $.ajax({
                method:'get',
                data:{id : $('#product').val()},
                url:'getadditionalinfodata',
                success:function(response){
                    console.log(response);
                    if(response)
                    {
                        if(type== 'get')
                        {
                            var line1 = response.date;
                            var line2 = response.product.product_name;
                            var line3 = response.warehouse.warehouse_name;
                            var line4 = '';
                            var total = 0;

                            $.each(response.owner, function(i,v){
                                var owner = v.name+'\n';
                                var detail = '';
                                $.each(v.detail, function(j,x){
                                    var ukuran = x.ukuran;
                                    var lembar = x.lembar;
                                    var temptotal = ukuran * lembar;
                                    total = total + temptotal;
                                    detail = detail + ukuran +' Meter X '+ lembar + ' Lembar = '+ temptotal +' Meter \n';
                                });
                                line4 = line4 + owner + detail;
                            });
                            var text = line1 + '\n' + line2 + '\n' + line3 + '\n'+ line4 + '\nTotal = ' + total+' Meter';
                            $('#prevInfo').val(text);
                        }else if(type == 'copy'){
                            $.each(response.owner, function(i,v){
                                var row = $('.group-row:first');
                                if(i!=0) {
                                    original = $('#example-group .group-row:last').clone();
                                    original.find('.owner').val(v.name);
                                    row.after(original.addClass('cloned'));
                                }else{
                                    row.find('.owner').val(v.name);
                                }

                                var rowchild = $('.group-row').eq(i).find('.info-row');
                                $.each(v.detail, function(j,x){
                                    if( j != 0) {
                                        originalchild = $('#example-group .info-row:last').clone();
                                        originalchild.find('.ukuran').val(x.ukuran);
                                        originalchild.find('.totallembar').val(x.lembar);
                                        rowchild.after(originalchild.addClass('cloned'));
                                    }else{
                                        rowchild.find('.ukuran').val(x.ukuran);
                                        rowchild.find('.totallembar').val(x.lembar);
                                    }
                                });
                            });

                            $('#formInfoProduct .totallembar').each(function (i,v){
                                $(this).trigger('keyup');
                            });
                        }
                    }else{
                        $('#prevInfo').val('');
                    }
                }
            })
        }
    });
</script>