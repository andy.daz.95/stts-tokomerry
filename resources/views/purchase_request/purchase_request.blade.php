<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Purchase Request</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Filter PO</label>
                  <input id="findKode" type="text" class="f-input" placeholder="Filter PO">
                </div>
                <div class="input-field col l3">
                  <label>Filter Tgl</label>
                  <input id="findPelanggan" type="text" class="f-input" placeholder="Filter Tgl">
                </div>
                <div class="col l12 m12 s12">
                  <div class="table-responsive">
                    <table id="PelangganTable" class="table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th class="theader">No PR</th>
                          <th class="theader">Tanggal PO</th>
                          <th class="theader">Status</th>
                          <th class="theader">Modified By</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>PR001</td>
                          <td>30-11-2016 14:16:23</td>
                          <td>Draft</td>
                          <td>warehouse</td>
                        </tr>
                        <tr>
                          <td>PR002</td>
                          <td>26-08-2016 15:20:30</td>
                          <td>Done</td>
                          <td>warehouse</td>
                        </tr>
                        <tr>
                          <td>PR003</td>
                          <td>30-10-2016 11:11:36</td>
                          <td>Done</td>
                          <td>warehouse</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir PR</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="col l12 m12 s12">
                  <a href="#tambah_purchase_request" data-toggle="modal" class="btn-stoko btn-stoko-primary">tambah barang</a>
                </div>
                <br><br>
                <div class="input-field col l3">
                  <label>No PR</label>
                  <input type="text" class="f-input" placeholder="No PR">
                </div>
                <div class="input-field col l3">
                  <label>Tgl PO</label>
                  <input type="text" class="f-input datepicker" placeholder="Tgl PO">
                </div>
                <div class="col l12 m12 s12">
                  <div class="table-responsive">
                    <table class="table table-hover display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th class="theader">Nama Barang</th>
                          <th class="theader">Satuan</th>
                          <th class="theader">Qty</th>
                          <th class="theader">Berat</th>
                          <th class="theader" colspan="2">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <div class="input-field">
                              <input type="text" class="f-input">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input type="text" class="f-input">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input type="text" class="f-input">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input type="text" class="f-input">
                            </div>
                          </td>
                          <td>
                            <p>
                              <input type="checkbox" class="filled-in" id="filled-in-box" />
                              <label for="filled-in-box"></label>
                            </p>
                          </td>
                          <td>
                            <a href="#" class="grey-text"><span class="material-icons">delete</span></a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>

                <div class="col l4 margin-top">
                  <a href="#!" onclick="Materialize.toast('Berhasil menambahkan purchase request', 4000)" class="waves-effect btn-stoko teal white-text">simpan</a>
                  <a href="#delete_data" class="waves-effect btn-stoko red white-text">hapus</a>
                  <a href="#!" class="waves-effect btn-stoko orange white-text">batal</a>
                  <a href="#!" class="waves-effect btn-stoko">cetak</a>
                </div>
                <div class="col l3">
                  <div class="input-field">
                    <input type="text" class="f-input" placeholder="Total Qty">
                  </div>
                </div>
                <div class="col l3">
                  <div class="input-field">
                    <input type="text" class="f-input" placeholder="Total Berat">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- Modal Tambah Purchase Request -->
<div id="tambah_purchase_request" class="modal">
  <div class="modal-content">
    <h4>Tambah Purchase Request</h4>
    <div class="row">
      <div class="input-field col l6">
        <select class="f-select browser-default">
          <option value="" disabled selected>Pilih Barang</option>
          <option value="1">Barang 1</option>
          <option value="2">Barang 2</option>
          <option value="3">Barang 3</option>
          <option value="4">Barang 4</option>
          <option value="5">Barang 5</option>
          <option value="6">Barang 6</option>
        </select>
        <label>Pilih Barang</label>
      </div>
      <div class="input-field col l6">
        <select class="f-select browser-default">
          <option value="" disabled selected>Pilih Pelanggan</option>
          <option value="1">Pelanggan 1</option>
          <option value="2">Pelanggan 2</option>
          <option value="3">Pelanggan 3</option>
          <option value="4">Pelanggan 4</option>
          <option value="5">Pelanggan 5</option>
          <option value="6">Pelanggan 6</option>
        </select>
        <label>Pilih Pelanggan</label>
      </div>
      <div class="input-field col l6">
        <input type="text" class="f-input" />
        <label>Qty</label>
      </div>
      <div class="input-field col l6">
        <input type="text" class="f-input" disabled />
        <label>Harga</label>
      </div>
      <div class="input-field col l6">
        <input type="text" class="f-input datepicker" />
        <label>Expected Delivery Date</label>
      </div>
    </div>
  </div>
  <div class="modal-footer">

  </div>
</div>

<!-- Modal Rubah Purchase Request -->
<div id="rubah_purchase_request" class="modal">
  <div class="modal-content">
    <h4>Rubah Purchase Request</h4>
    <div class="row">
      <div class="input-field col l6">
        <select class="f-select browser-default">
          <option value="" disabled selected>Pilih Barang</option>
          <option value="1">Barang 1</option>
          <option value="2">Barang 2</option>
          <option value="3">Barang 3</option>
          <option value="4">Barang 4</option>
          <option value="5">Barang 5</option>
          <option value="6">Barang 6</option>
        </select>
        <label>Pilih Barang</label>
      </div>
      <div class="input-field col l6">
        <select class="f-select browser-default">
          <option value="" disabled selected>Pilih Pelanggan</option>
          <option value="1">Pelanggan 1</option>
          <option value="2">Pelanggan 2</option>
          <option value="3">Pelanggan 3</option>
          <option value="4">Pelanggan 4</option>
          <option value="5">Pelanggan 5</option>
          <option value="6">Pelanggan 6</option>
        </select>
        <label>Pilih Pelanggan</label>
      </div>
      <div class="input-field col l6">
        <input type="text" class="f-input" />
        <label>Qty</label>
      </div>
      <div class="input-field col l6">
        <input type="text" class="f-input" disabled />
        <label>Harga</label>
      </div>
      <div class="input-field col l6">
        <input type="text" class="f-input datepicker" />
        <label>Expected Delivery Date</label>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a href="#!" onclick="Materialize.toast('Berhasil merubah purchase request', 4000)" class="modal-action modal-close waves-effect btn-stoko teal white-text">simpan</a>
  </div>
</div>

<!-- Modal Delete Purchase Request -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data ini?</h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a href="#!" onclick="Materialize.toast('Berhasil menghapus purchase request', 4000)" class="modal-action modal-close waves-effect btn-stoko red white-text">hspus</a>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });
  });
</script>