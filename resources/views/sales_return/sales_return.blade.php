<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Sales Return</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br/>
                <div class="input-field col l3">
                  <label>Filter Retur</label>
                  <input id="filterReturNumber" type="text" class="f-input" placeholder="Filter Retur" />
                </div>
                <div class="input-field col l3">
                  <label>Filter Pelanggan</label>
                  <input id="filterPelanggan" type="text" class="f-input" placeholder="Filter Pelanggan" />
                </div>
                <div class="input-field col l3">
                  <label>Filter SO</label>
                  <input id="filterSoNumber" type="text" class="f-input" placeholder="Filter SO" />
                </div>
              </div>
              <div class="row">
                <div class="col l12 m12 s12">
                  <div class="table-responsive">
                    <table id="salesReturTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th class="theader">No Retur</th>
                        <th class="theader">Tipe Retur</th>
                        <th class="theader">Pelanggan</th>
                        <th class="theader">No SO</th>
                        <th class="theader">Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($retur as $key => $value)--}}
                      {{--<tr value="{{$value->sales_retur_id}}">--}}
                      {{--<td class="noretur">{{$value->retur_number}}</td>--}}
                      {{--<td>{{$value->retur_type_name}}</td>--}}
                      {{--<td>{{$value->first_name.' '.$value->last_name}}</td>--}}
                      {{--<td>{{$value->sales_order_number}}</td>--}}
                      {{--<td>--}}
                      {{--@if(Session('roles')->name == 'master')--}}
                      {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->sales_retur_id}}"><i class="material-icons">edit</i></a>--}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                    </br>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir Pengembalian Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br/>
                <form id="formRetur">
                  <div class="input-field col l3">
                    <label>No Retur SO</label>
                    <input id="noretur" type="text" name="noretur" class="f-input" disabled>
                    <input id="idretur" type="text" name="idretur" class="f-input" disabled hidden>
                  </div>
                  <div class="input-field col l3">
                    <label>No SO</label>
                    <select id="noso" name="noso" class="selectpicker browser-default" data-live-search="true" data-size="5"  >
                      <option value="">Pilih No SO</option>
                      @foreach($so as $key => $value)
                        <option value="{{$value->sales_order_id}}">{{$value->sales_order_number.' / '.$value->customer->first_name.' '.$value->customer->last_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Pelanggan</label>
                    <input id="customer" type="text" name="customer" class="f-input" disabled>
                  </div>
                  <div class="input-field col l3">
                    <label>Tanggal Retur</label>
                    <input id="tglretur" type="text" name="tglretur" class="f-input" placeholder="Tanggal Retur">
                  </div>
                  <div class="input-field col l3">
                    <label>Tipe Retur</label>
                    <select id="returtype" name="returtype" class="selectpicker browser-default" data-live-search="true" data-size="5"   disabled>
                      <option>Select Retur Type</option>
                      @foreach($returtype as $key => $value)
                        <option value="{{$value->retur_type_id}}">{{$value->retur_type_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div id="gudang-div" class="input-field col l3" hidden>
                    <label>Gudang</label>
                    <select id="gudang" name="gudang" class="selectpicker browser-default" data-live-search="true" data-size="5"   disabled>
                      <option>Select Gudang</option>
                      @foreach($warehouse as $key => $value)
                        <option value="{{$value->warehouse_id}}">{{$value->warehouse_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader">Kode Barang</th>
                          <th class="theader">Nama Barang</th>
                          <th class="theader">Retur Quota</th>
                          <th class="theader">Unit</th>
                          <th class="theader">Price</th>
                          <th class="theader">Quantity</th>
                          <th class="theader">Tindakan</th>
                        </tr>
                        </thead>
                        <tbody id="barang-data">
                        <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                        <tr class="barang-row" hidden>
                          <td>
                            <div class="input-field">
                              <input id="id-1" name="id[]" type="text" class="f-input" hidden>
                              <input id="kode-1" name="kode[]" type="text" class="f-input">
                              <input id="detail-sales-1" name="detailsales[]" type="text" class="f-input" hidden>
                              <input id="detail-retur-1" name="detailretur[]" type="text" class="f-input" hidden>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="nama-1" name="namabarang[]" type="text" class="f-input">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="quota-1" name="quota[]" type="text" class="f-input quota">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="unit-1" name="unit[]" type="text" class="f-input unit">
                              <input id="unit-id-1" name="unitid[]" type="text" class="f-input unitid" hidden>
                              <input id="unit-type-1" name="unittype[]" type="text" class="f-input unittype" hidden>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="price-1" name="price[]" type="text" class="f-input price">
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="qty-1" name="qty[]" type="text" class="f-input qty">
                            </div>
                          </td>
                          <td>
                            <a href="#" class="btn btn-sm white grey-text"><i class="material-icons">close</i></a>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                      </br>
                      <div class="input-field">
                        <label>Nilai Retur</label>
                        <input id="totalvalue" type="text" name="totalvalue" class="f-input" placeholder="Nilai Retur" value="0" disabled>
                      </div>
                    </div>
                  </div>
                  <div class="col l12 s12 m12 margin-top">
                    <a id="submit-retur" href="#" class="btn-stoko teal white-text submit" mode="save">simpan</a>
                    <a id="edit-retur" href="#" class="btn-stoko teal white-text" mode="edit" hidden>edit</a>
                    <a href="#" id="clear" class="btn-stoko">batal</a>
                    <a id="cetak-return" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Retur</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-retur" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-retur" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        $('#noso').on('change',function(){
            var id = $(this).val();
            $.ajax({
                type:"Get",
                url:"getsoforreturn",
                data:{id:id},
                success:function(response){
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    if(response.length == 0){
                        $original.clone();
                        $original.attr('hidden',true)
                        $original.appendTo('#barang-data');
                        $('.barang-row').attr('hidden',true);
                        $('#returtype').attr('disabled',true);
                        $('#no-item').removeAttr('hidden');
                    }else{
                        $('#customer').val(response['so'].customer.first_name+' '+response['so'].customer.last_name);
                        $.each(response['so'].details, function(i, v){
                            var newid = "id-"+(i+1);
                            var newdetailsalesid = "detail-sales-"+(i+1);
                            var newquota = "quota-"+(i+1);
                            var newunit = "unit-"+(i+1);
                            var newunitid = "unit-id-"+(i+1);
                            var newunittype = "unit-type-"+(i+1);
                            var newharga = "harga-"+(i+1);
                            var newkode = "kode-"+(i+1);
                            var newnama = "nama-"+(i+1);
                            var newqty = "qty-"+(i+1);
                            var newprice = "price-"+(i+1);
                            $temp = $original.clone();
                            $temp.removeAttr('hidden');
                            $temp.find('#id-1').attr('id',newid);
                            $temp.find('#detail-sales-1').attr('id',newdetailsalesid);
                            $temp.find('#quota-1').attr('id',newquota);
                            $temp.find('#unit-1').attr('id',newunit);
                            $temp.find('#unit-id-1').attr('id',newunitid);
                            $temp.find('#unit-type-1').attr('id',newunittype);
                            $temp.find('#kode-1').attr('id',newkode);
                            $temp.find('#nama-1').attr('id',newnama);
                            $temp.find('#qty-1').attr('id',newqty);
                            $temp.find('#price-1').attr('id',newprice);
                            $temp.appendTo('#barang-data');
                            $('#id-'+(i+1)).val(v.product_id);
                            $('#detail-sales-'+(i+1)).val(v.sales_order_details_id);
                            $('#price-'+(i+1)).val(v.price).attr('disabled',true);
                            $('#quota-'+(i+1)).val(v.quota).attr('disabled',true);
                            if(v.unit_type == "child")
                            {
                                unit = v.unit_child.unit_child_name;
                            }else{
                                unit = v.unit.unit_description;
                            }
                            $('#unit-'+(i+1)).val(unit).attr('disabled',true);
                            $('#unit-id-'+(i+1)).val(v.unit_id).attr('disabled',true);
                            $('#unit-type-'+(i+1)).val(v.unit_type).attr('disabled',true);
                            $('#kode-'+(i+1)).val(v.product.product_code).attr('disabled',true);
                            $('#nama-'+(i+1)).val(v.product.product_name).attr('disabled',true);
                            $('#qty-'+(i+1)).val(0);
                        });
                        $('#returtype').html("").removeAttr('disabled');
                        $.each(response['returtype'], function(k,v)
                        {
                            $('#returtype').append("<option value='"+v.retur_type_id+"'>"+v.retur_type_name+"</option>");
                        });
                        $('returtype').trigger('change');
                        $('.selectpicker').selectpicker('refresh');
                        $('#no-item').attr('hidden',true);
                        $('.barang-row').removeAttr('hidden');

                        var tglretur = $('#tglretur').datepicker().data('datepicker');
                        tglretur.update('minDate', new Date(response['so'].delivery[0].date_delivery));
                        tglretur.selectDate(new Date());

                        $('#returtype').trigger('change');
                    }
                }
            });
        });

        $('body').on('click', '#salesReturTable tr, #salesReturTable .edit', function(event){
            event.stopImmediatePropagation();
            id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            $.ajax({
                type:"GET",
                url:"getsalesreturn",
                data:{id:id},
                success:function(response){
                    console.log(response);
                    var $original = $('.barang-row:first');
                    var $cloned = $original.clone();
                    $('.barang-row').remove();
                    $('#idretur').val(response["retur"].sales_retur_id).attr('disabled',true);
                    $('#noso').val(response["retur"].sales_order_id).attr('disabled',true);
                    $('#noretur').val(response["retur"].retur_number).attr('disabled',true);
                    $('#customer').val(response["retur"].so.customer.first_name+' '+response["retur"].so.customer.last_name).attr('disabled',true);
                    $('#tglretur').val(response["retur"].date_retur).attr('disabled',true);
                    $('#returtype').val(response["retur"].retur_type_id).attr('disabled',true);
                    if(response["retur"].retur_type_id == 5){ // return barang
                        $('#gudang').val(response["retur"].warehouse_id);
                        $('#gudang-div').removeAttr('hidden');
                    }else{
                        $('#gudang-div').removeAttr('hidden');
                    }
                    $.each(response["retur_details"], function(i,v){
                      var newid = "id-"+(i+1);
                        var newdetail = "detail-retur-"+(i+1);
                        var newdetailsalesid = "detail-sales-"+(i+1);
                        var newquota = "quota-"+(i+1);
                        var newunit = "unit-"+(i+1);
                        var newunitid = "unit-id-"+(i+1);
                        var newunittype = "unit-type-"+(i+1);
                        var newharga = "harga-"+(i+1);
                        var newkode = "kode-"+(i+1);
                        var newnama = "nama-"+(i+1);
                        var newqty = "qty-"+(i+1);
                        var newprice = "price-"+(i+1);
                        $temp = $original.clone();
                        $temp.removeAttr('hidden');
                        $temp.find('#id-1').attr('id',newid);
                        $temp.find('#detail-retur-1').attr('id',newdetail);
                        $temp.find('#detail-sales-1').attr('id',newdetailsalesid);
                        $temp.find('#harga-1').attr('id',newharga);
                        $temp.find('#quota-1').attr('id',newquota);
                        $temp.find('#unit-1').attr('id',newunit);
                        $temp.find('#unit-id-1').attr('id',newunitid);
                        $temp.find('#unit-type-1').attr('id',newunittype);
                        $temp.find('#kode-1').attr('id',newkode);
                        $temp.find('#nama-1').attr('id',newnama);
                        $temp.find('#qty-1').attr('id',newqty);
                        $temp.find('#price-1').attr('id',newprice);
                        $temp.appendTo('#barang-data');
                        $('#id-'+(i+1)).val(v.product_id);
                        $('#detail-retur-'+(i+1)).val(v.sales_retur_details_id);
                        $('#detail-sales-'+(i+1)).val(v.sales_order_details_id);
                        $('#kode-'+(i+1)).val(v.product.product_code).attr('disabled',true);
                        $('#nama-'+(i+1)).val(v.product.product_name).attr('disabled',true);
                        $('#qty-'+(i+1)).val(v.qty).attr('disabled',true);
                        $('#price-'+(i+1)).val(v.price).attr('disabled',true);
                        $('#quota-'+(i+1)).val(v.quota).attr('disabled',true);
                        $('#unit-type-'+(i+1)).val(v.unit_type).attr('disabled',true);
                        $('#unit-id-'+(i+1)).val(v.unit_id).attr('disabled',true);
                        if(v.unit_type == "child"){
                            $('#unit-'+(i+1)).val(v.unit_child.unit_child_name).attr('disabled',true);
                        }else{
                            $('#unit-'+(i+1)).val(v.unit.unit_description).attr('disabled',true);
                        }
                    });
                    calculatetotal();
                    $('#no-item').attr('hidden',true);
                    $('.barang-row').removeAttr('hidden');
                    $('.selectpicker').selectpicker('refresh');
                },complete:function(){
                    if(mode=="edit")
                    {
                        $('#submit-retur').attr('hidden',true);
                        $('#edit-retur').removeAttr('hidden');
                        $('#tglretur, .qty').removeAttr('disabled');
                    }else{
                        $('#cetak-return').removeAttr('hidden').attr('href',"downloadsalesreturn/"+id);
                    }
                }
            })
        });

        $('#formRetur #submit-retur, #formRetur #edit-retur').click(function(event){
            event.preventDefault();
            event.stopImmediatePropagation();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //

            var kode = $('#noretur').val();
            mode = $(event.currentTarget).attr('mode');
            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "createreturn";
                var successmessage = 'Sales Return '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatereturn";
                var successmessage = 'Sales Return '+kode+' telah berhasil diubah!';
            }

            var validqty = [];
            var lebihqty = [];

            $('.barang-row').each(function(){
                var quota = parseInt($(this).find('.quota').val());
                var returqty = parseInt($(this).find('.qty').val());
                validqty.push(ceknumber($(this).find('.qty').val()));
                if(returqty > quota){
                    lebihqty.push(false);
                }else{
                    lebihqty.push(true);
                }
            });

            if($('#noso').val() ==0){
                toastr.warning('Anda Belum Memilih SO!');
            }else if($('#returtype').val == 0 ){
                toastr.warning('Anda Belum Memilih SO!');
            }else if(validqty.indexOf(false) > -1)
            {
                toastr.warning('Quantity harus Angka!');
            }else if(lebihqty.indexOf(false) > -1){
                toastr.warning('Quantity melebihi quota!');
            }else{
                me.invisible();
                me.data('requestRunning', true);
                $('#noretur, .price , #idretur, #gudang, #returtype, .unitid, .unittype').removeAttr('disabled');
                $.ajax({
                    type:"POST",
                    url:url,
                    data:$(this).closest('#formRetur').serialize(),
                    success:function(response){
                        toastr.success(successmessage,{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                })
            }
        })

        $('body').on('keyup','#formRetur .qty', function(event){
            event.stopImmediatePropagation();
            calculatetotal();
        });

        $('#returtype').on('change',function(event){
            event.stopImmediatePropagation();
            if($(this).val() == 1) // Kembali Uang / Refund
            {
                $('#gudang-div').removeAttr('hidden');
                $('#gudang').removeAttr('disabled').selectpicker('refresh');
            }else{
                $('#gudang-div').attr('hidden',true);
                $('#gudang').attr('disabled',true).selectpicker('refresh');;
            }
        });

        $('body').on('click','#salesReturTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var noretur = $(this).closest('tr').find('.noretur').html();
            var idretur = $(this).closest('tr').attr('value');
            $('#confirm-delete-retur').attr('value',idretur).attr('nomor',noretur);
            $("#delete-message").html("Yakin ingin menghapus data "+noretur+" ?")
            $('#modal-delete-retur').modal('open');
        });

        $('#confirm-delete-retur').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-retur').modal('close');
            var id = $(this).attr('value');
            var noretur = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletereturn",
                data:{id:id},
                success:function(response){
                    toastr.success('Sales Return '+noretur+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        //function
        function firstload()
        {
            $.ajax({
                type:"GET",
                url:"lastsalesreturnnumber",
                success:function(response){
                    $('#noretur').val(response);
                }
            })

            $('.selectpicker').selectpicker('render');

            $('#tglretur').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            setToday();

            var salesReturTable = $('#salesReturTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getsalesreturntable',
                    data : function (d){
                        d.noretur = $('#filterReturNumber').val();
                        d.customer = $('#filterPelanggan').val();
                        d.noso = $('#filterSoNumber').val();
                    }
                },
                rowId : 'sales_retur_id',
                columns: [
                    { data: 'retur_number', name: 'retur_number', class:'noretur'},
                    { data: 'retur_type_name', name: 'retur_type_name' },
                    { data: 'customer_name', name: 'customer_name' },
                    { data: 'sales_order_number', name: 'sales_order_number' },
                    { data: 'action', name: 'action'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterReturNumber').on('keyup', function () { // This is for news page
                salesReturTable.draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                salesReturTable.draw();
            });
            $('#filterSoNumber').on('keyup', function () { // This is for news page
                salesReturTable.draw();
            });

        }
        function calculatetotal(){
            var total = 0;
            $('.barang-row').each(function(key, value){
                totalqty = 0;
                $(this).find('.qty').each(function(key,value){
                    $qty = $(this).val();
                    $qty =  $qty == '' ? 0 : $qty;
                    totalqty += parseInt($qty);
                })
                total += totalqty * $(this).find('.price').val();
            });
            $('#totalvalue').val(total);
        }

        function setToday(){
            $('#tglretur').val(moment().format('DD-MM-YYYY'));
        }
    });
</script>
