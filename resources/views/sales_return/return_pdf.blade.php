<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
</style>
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">Bukti Retur Penjualan</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 25%">NO Retur</td>
      <td style="width: 25%">: {{$returheader->retur_number}}</td>
      <td style="width: 25%">No SO</td>
      <td style="width: 25%">: {{$returheader->sales_order_number}}</td>
    </tr>
    <tr>
      <td>Tgl Retur</td>
      <td>: {{$returheader->date_retur}}</td>
      <td>Tgl SO</td>
      <td>: {{$returheader->date_sales_order}}</td>
    </tr>
    <tr>
      <td>Tipe Retur</td>
      <td colspan="3">: {{$returheader->retur_type_name}}</td>
    </tr>
      <tr>
          <td>Gudang</td>
          <td colspan="3">: {{$returheader->warehouse_name ? $returheader->warehouse_name  : '-'}}</td>
      </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr style="background-color: #8B8C89">
        <th class="color" style="text-align: center;">Nama</th>
        <th class="color" style="width: 100px; text-align: center;">Order Qty</th>
        <th class="color" style="text-align: center;">Unit</th>
        <th class="color" style="width: 100px; text-align: center;">Retur Qty</th>
      </tr>
    </thead>
    <tbody >
      @foreach($returdetail as $key => $value)
      <tr>
        <td>
          <span>{{$value->p_name}}</span>
        </td>
        <td>
          <span>{{$value->orderqty}}</span>
        </td>
        <td>
          @if($value->unit_type == "child")
            <span>{{$value->unit_child->unit_child_name}}</span>
          @else
            <span>{{$value->unit->unit_description}}</span>
          @endif
        </td>
        <td>
          <span>{{$value->qty}}</span>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>