<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>History Refund</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br/>
                <div class="input-field col l3">
                  <label>Filter Pelanggan</label>
                  <input id="filterPelanggan" type="text" class="f-input" placeholder="Filter Pelanggan" />
                </div>
                <div class="input-field col l3">
                  <label>Filter SO</label>
                  <input id="filterSoNumber" type="text" class="f-input" placeholder="Filter SO" />
                </div>
              </div>
              <div class="row">
                <div class="col l12 m12 s12">
                  <div class="table-responsive">
                    <table id="refundTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th class="theader">Nama Pelanggan</th>
                          <th class="theader">No Retur</th>
                          <th class="theader">No So</th>
                          <th class="theader">Tanggal Refund</th>
                          <th class="theader">Nominal</th>
                        </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($refund as $key => $value)--}}
                        {{--<tr value="{{$value->sales_retur_id}}">--}}
                          {{--<td>{{$value->first_name.' '.$value->last_name}}</td>--}}
                          {{--<td>{{$value->sales_order_number}}</td>--}}
                          {{--<td>{{$value->date_retur}}</td>--}}
                          {{--<td>{{$value->total_price}}</td>--}}
                        {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir Pengembalian Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br/>
                <form>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                          <tr>
                            <th class="theader">Kode Barang</th>
                            <th class="theader">Nama Barang</th>
                            <th class="theader">Quantity</th>
                          </tr>
                        </thead>
                        <tbody id="barang-data">
                          <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                          <tr class="barang-row" hidden>
                            <td>
                              <div class="input-field">
                                <input id="kode-1" name="kode[]" type="text" class="f-input">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="nama-1" name="namabarang[]" type="text" class="f-input">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="qty-1" name="qty[]" type="text" class="f-input qty">
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      </br>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    firstload();

    $('body').on('click','#refundTable tr, .edit', function(event){
      event.stopImmediatePropagation();
      id = $(this).attr('value');
      var mode = $(event.currentTarget).attr('mode');
      $.ajax({
        type:"GET",
        url:"getsalesreturn",
        data:{id:id},
        success:function(response){
          var $original = $('.barang-row:first');
          var $cloned = $original.clone();
          $('.barang-row').remove();
          $('#idretur').val(response[0].sales_retur_id).attr('disabled',true);
          $('#noso').val(response[0].sales_order_id).attr('disabled',true);
          $('#noretur').val(response[0].retur_number).attr('disabled',true);
          $('#customer').val(response[0].first_name+' '+response[0].last_name).attr('disabled',true);
          $('#tglretur').val(response[0].date_retur).attr('disabled',true);
          $('#returtype').val(response[0].retur_type_id).attr('disabled',true);
           if(response[0].retur_type_id == 5){ // return barang
            $('#gudang').val(response[0].warehouse_id);
            $('#gudang-div').removeAttr('hidden');
          }else{
            $('#gudang-div').removeAttr('hidden');
          }
          for (var i=0; i<response.length; i++)
          {
            var newid = "id-"+(i+1);
            var newkode = "kode-"+(i+1);
            var newnama = "nama-"+(i+1);
            var newqty = "qty-"+(i+1);
            $temp = $original.clone();
            $temp.removeAttr('hidden');
            $temp.find('#id-1').attr('id',newid);
            $temp.find('#kode-1').attr('id',newkode);
            $temp.find('#nama-1').attr('id',newnama);
            $temp.find('#qty-1').attr('id',newqty);
            $temp.appendTo('#barang-data');
            $('#id-'+(i+1)).val(response[i].product_id);
            $('#kode-'+(i+1)).val(response[i].product_code).attr('disabled',true);
            $('#nama-'+(i+1)).val(response[i].product_name).attr('disabled',true);
            $('#qty-'+(i+1)).val(response[i].qty).attr('disabled',true);
          }
          $('#no-item').attr('hidden',true);
          $('.barang-row').removeAttr('hidden');
          $('.selectpicker').selectpicker('refresh');
        },complete:function(){
        }
      })
    });

    function firstload()
    {

        var refundTable = $('#refundTable').DataTable({ // This is for home page
            searching: true,
            processing: true,
            serverSide: true,
            ajax: {
                url : 'getrefundhistorytable',
                data : function (d){
                    d.noso = $('#filterSoNumber').val();
                    d.customer = $('#filterPelanggan').val();
                }
            },
            rowId : 'sales_retur_id',
            columns: [
                { data: 'customer_name', name: 'customer_name'},
                { data: 'retur_number', name: 'retur_number', class:'noretur'},
                { data: 'sales_order_number', name: 'sales_order_number' },
                { data: 'date_retur', name: 'date_retur' },
                { data: 'total_price', name: 'total_price' },
            ],
            language: {
                "sProcessing": "Sedang proses...",
                "sLengthMenu": "Tampilan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Awal",
                    "sPrevious": "Balik",
                    "sNext": "Lanjut",
                    "sLast": "Akhir"
                }
            }
        });


    $('#filterPelanggan').on('keyup', function () { // This is for news page
      refundTable.draw();
    });
    $('#filterSoNumber').on('keyup', function () { // This is for news page
      refundTable.draw();
    });
    }
  });
</script>
