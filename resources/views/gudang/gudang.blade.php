<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l6 m6 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Gudang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formGudang">
                <div class="row margin-top">
                  <div class="col l12">
                    <div class="row">
                      <div class="input-field col l12 m12 s12">
                        <input id="id" type="text" class="f-input" name="id" hidden>
                        <input id="nama" type="text" class="f-input" name="nama">
                        <label>Nama Gudang</label>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <select id="head" type="text" class="browser-default selectpicker" data-live-search="true" data-size="5" name="head">
                          <option value="">Pilih Akun</option>
                          @foreach($account as $key => $value)
                          <option value="{{$value->account_id}}">{{$value->full_name}}</option>
                          @endforeach
                        </select>
                        <label>Kepala Gudang</label>
                      </div>
                      <div class="input-field col l6 m12 s12">
                        <select id="provinsi" class="browser-default selectpicker" data-live-search="true" data-size="5" name="provinsi">
                          <option value="">Pilih Provinsi</option>
                          @foreach($provinsi as $key => $value)
                          <option value="{{$value->id}}">{{$value->name}}</option>
                          @endforeach
                        </select>
                        <label>Provinsi</label>
                      </div>
                      <div class="input-field col l6 m12 s12">
                        <select id="kota" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="kota">
                          <option value="">Pilih Kota</option>
                        </select>
                        <label>Kota</label>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <textarea id="alamat" type="text" class="f-txt" name="alamat"></textarea>
                        <label>Alamat</label>
                      </div>
                      <div class="input-field col l4 m4 s12">
                        <input id="telp1" type="text" class="f-input" name="telp1">
                        <label>Telp 1</label>
                      </div>
                      <div class="input-field col l4 m4 s12">
                        <input id="telp2" type="text" class="f-input" name="telp2">
                        <label>Telp 2</label>
                      </div>
                      <div class="input-field col l4 m4 s12">
                        <input id="fax" type="text" class="f-input" name="fax">
                        <label>Fax</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col l12 m12 s12">
                    <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                    <a href="#" class="btn btn-raised btn-sm orange reset">batal</a>
                  </div>
                </div>
                {{csrf_field()}}
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l6 m6 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Filter Gudang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l5">
                  <input id="findKode" type="text" class="f-input" placeholder="Search Gudang">
                </div>
                <div class="col l12 m12 s12">
                  <div>
                    <table id="gudangTable" class="table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                        <tr>
                          <th class="theader">Nama</th>
                          <th class="theader">Provinsi</th>
                          <th class="theader">Tindakan</th>
                        </tr>
                      </thead>
                      <tbody id="gudangData">
                        @foreach($gudang as $key => $value)
                        <tr value="{{$value->warehouse_id}}">
                          <td>{{$value->warehouse_name}}</td>
                          <td>{{$value->p_name}}</td>
                          <td>
                            <a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
                            <a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Modal Delete Gudang -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data ini?</h5>
    <table id="modal-delete-data">

    </table>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius delete">hapus</a>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    // $.ajax({
    //   url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
    //   dataType: "script",
    // });

    $('#provinsi').on('change',function(){
      var provinsi = $('#provinsi option:selected').val();
      getCity(provinsi);
    });

    $('body').on('click','#formGudang .reset', function(event){
      event.stopImmediatePropagation();
      $(this).closest('#formGudang').find("input[type=text], textarea").val("");
      $('#btn-submit').attr('mode', 'save');
    });

    gudangTable = $('#gudangTable').DataTable({ // This is for home page
      searching: true,
      responsive: true,
      'sDom':'tip',
      "bPaginate":true,
      "bFilter": false,
      "sPaginationType": "numbers",
      "iDisplayLength": 3,
      language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
    });

    $('#formGudang').submit(function(event){
      event.preventDefault();
      var mode = $(this).find('#btn-submit').attr('mode');
      console.log(mode);
      var empty = required(['nama','alamat', 'provinsi', 'kota', 'telp1']);
      var validangka = cekmanynumbers(['telp1','telp2','fax']);

      if(empty != '0')
      {
        toastr.warning(empty+' tidak boleh kosong!');
      }if(validangka != 0){
        toastr.warning(validangka+' harus angka!');
      }
      else{
        if (mode == 'save')
        {
          var id = $(this).find('#id').val();
          $.ajax({
            type:"POST",
            url:"createGudang",
            data: $("#formGudang").serialize(),
            success:function(response){
              toastr.success('Gudang Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
            }
          });
        }else if(mode == 'edit'){
          var id = $(this).find('#id').val();
          $.ajax({
            type:"POST",
            url:"updateGudang",
            data: $("#formGudang").serialize()+"&id="+id,
            success:function(response){
              toastr.success('Gudang Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
            }
          });
        }
      }
    });

    $("#findKode").on('keyup', function(event){
      event.stopImmediatePropagation();
      gudangTable.column(0).search(this.value).draw();
    });

    $('body').on('click', '#gudangTable .edit', function(event){
      event.stopImmediatePropagation(); // as long as not bubbling up the DOM is ok?
      var id = $(this).closest('tr').attr('value');
      $.ajax({
        type:"GET",
        url:"getGudangData",
        data: {id:id},
        success:function(response){
          $('#provinsi').val(response.p_id);
          getCity(response.p_id, response.city_id);
          $('#id').val(response.warehouse_id);
          $('#nama').val(response.warehouse_name);
          $('#head').val(response.head_warehouse);
          $('#alamat').val(response.address);
          $('#telp1').val(response.phone);
          $('#telp2').val(response.phone2);
          $('#fax').val(response.fax);
          kota = response.city_id;
          $('.selectpicker').selectpicker('refresh');
        }
      });
      $('#btn-submit').attr('mode', 'edit');
    });

    $("body").on('click', '#gudangTable .delete-modal' ,function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      var id = $(this).closest('tr').attr('value');

      $.ajax({
        type:"GET",
        url:"modalGudangData",
        async:false,
        data:{
          id:id,
        },
        success:function(response){
          $('#modal-delete-data').html(response);
        }
      });
      $('#delete_data').modal('open');
    });

    $(".delete").on('click',function(event){
      var id = $(this).closest('.modal').find('#id-delete').attr('value');
      console.log(id);
      $.ajax({
        type:"Get",
        url:"deleteGudang",
        data:{id:id},
        success:function(response){
          toastr.success('Berhasil Menghapus Gudang!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
        }
      });
    });

    $('.selectpicker').selectpicker('render');

    //function
    function getCity(provinsi,city)
    {
      city = city || 0;
      $.ajax({
        type:"GET",
        url:"cities",
        data:{
          provinsi:provinsi, city:city,
        },
        success:function(response){
          $('#kota').html(response);
        },
        complete:function(){
          $('.selectpicker').selectpicker('refresh');
        }
      });
    }
  });
</script>