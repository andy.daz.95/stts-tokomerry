<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
 <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <tr>
    <th>No PO</th>
    <th>Supplier</th>
    <th>Tanggal</th>
    <th>Total</th>
    <th>Diskon</th>
    <th>Potong Nota</th>
    <th>Potongan Invoice Sebelumnya</th>
    <th>Grand Total</th>
    <th>Payment</th>
  </tr>
  <tbody>
    @php
      $totalreduction = 0;
      $po = $po->where('po_status','<>', 'Paid')
    @endphp
    @foreach($po->where('po_status','<>', 'Paid') as $key => $value)
    @php
    $discount = $value->discount_nominal ? $value->discount_nominal : 0;
    $reduction = $value->reduction ? $value->reduction : 0;
    $prev_reduction = $value->prev_invoice_reduction ? $value->prev_invoice_reduction : 0;

    $totalreduction = $totalreduction + $discount + $reduction + $prev_reduction
    @endphp
    <tr id="{{$value->purchase_order_id}}" class="po-row">
      <td class="nopo">{{$value->purchase_order_number}}</td>
      <td>{{$value->company_name}}</td>
      <td>{{$value->date_purchase_order}}</td>
      <td class="total">{{number_format($value->grand_total_idr)}}</td>
      <td class="diskon">{{$value->discount_nominal ? number_format($value->discount_nominal) : '-'}}</td>
      <td class="reduction">{{$value->reduction ? number_format($value->reduction) : '-'}}</td>
      <td class="prev-invoice-reduction">{{$value->prev_invoice_reduction ? number_format($value->prev_invoice_reduction) : '-'}}</td>
      <td class="grandtotal">{{number_format($value->grand_total_idr - $discount - $reduction - $prev_reduction)}}</td>
      <td class="payment">{{$value->totalpaid ? number_format($value->totalpaid) : '-' }}</td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="8">Total Nilai PO</td>
      <td id="total-po">{{$po->sum('grand_total_idr')}}</td>
    </tr>
    <tr>
      <td colspan="8">Total Nilai PO Setelah Diskon Dan Potong Nota</td>
      <td id="total-potong">{{$po->sum('grand_total_idr') - $totalreduction}}</td>
    </tr>
    <tr>
      <td colspan="8">Total Pembayaran</td>
      <td id="total-payment">{{$po->sum('total_paid')}}</td>
    </tr>
    <tr>
      <td colspan="8"> Total Utang Tersisa</td>
      <td id="total-payable">{{ ($po->sum('grand_total_idr') - $totalreduction) - $po->sum('total_paid')}}</td>
    </tr>
  </tfoot>
</table>
</body>
</html>