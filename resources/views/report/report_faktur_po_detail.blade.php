<table id="reportFakturTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No PO</th>
    <th>Nama Supplier</th>
    <th>Tangal PO</th>
    <th>Total</th>
    <th>Tanggal Tanda Terima</th>
    <th>Potongan Invoice</th>
    <th>Nilai Invoice</th>
    <th>Diskon</th>
    <th>Total Bayar</th>
    <th>Sisa Bayar</th>
    <th>Status</th>
  </thead>
  <tbody>
    @php
      $totalpayment = 0;
    @endphp
    @foreach($po as $key => $value)
    <tr id="{{$value->purchase_order_id}}" class="faktur-row">
      <td>{{$value->purchase_order_number}}</td>
      <td>{{$value->supplier->company_name}}</td>
      <td>{{$value->date_purchase_order}}</td>
      <td class="total">{{number_format($value->grand_total_idr)}}</td>
      <td>
        @if($value->invoice)
        {{$value->invoice->date_invoice_purchase}}
        @else
        {{'-'}}
        @endif
      </td>
      <td>
        @if($value->invoice)
        {{number_format($value->invoice->prev_invoice_reduction + $value->reduction)}}
        @else
        {{number_format($value->reduction)}}
        @endif
      </td>
      <td>
        @if($value->invoice)
        @php $invoice = $value->invoice->total_amount; @endphp
        {{number_format($value->invoice->total_amount)}}</td>
        @else
        @php $invoice = 0; @endphp
        {{0}}
        @endif
      <td>
        @if($value->invoice)
        @php
          $diskon = $value->invoice->discount_nominal;
        @endphp
        {{number_format($value->invoice->discount_nominal)}}
        @else
        @php $diskon = 0; @endphp
        {{0}}
        @endif
      </td>
      <td>
        @if($value->invoice && $value->invoice->payment)
          @php
            $payment = $value->invoice->payment->sum('paid');
            $totalpayment += $payment;
          @endphp
          {{number_format($value->invoice->payment->sum('paid'))}}
        @else
          @php $payment = 0; @endphp
          {{0}}
          @endif
      </td>
      <td>
        {{number_format($invoice - $diskon - $payment)}}
      </td>
      <td>
        @if(!$value->invoice)
        {{'Invoice belum dibuat'}}
        @elseif($invoice == $payment)
        {{'Lunas'}}
        @elseif($invoice > $payment)
        {{'Belum Lunas'}}
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    @php
      $totalinvoice = $po->sum('invoice.total_amount');
      $totalreduction = $po->sum('invoice.prev_invoice_reduction') + $po->sum('invoice.reduction');
      $totaldiskon = $po->sum('invoice.discount_nominal');
    @endphp
    <tr>
      <td colspan="9">Total PO</td>
      <td id="grandtotal">{{$po->sum('grand_total_idr')}}</td>
    </tr>
    <tr>
      <td colspan="9">Total Invoice Yang Belum Dibuat</td>
      <td id="totalinvoicenotcreated">{{number_format($powithoutinvoice->sum('grand_total_idr'))}}</td>
    </tr>
    <tr>
      <td colspan="9">Total Invoice Yang Telah Dibuat</td>
      <td id="totalinvoicecreated">{{number_format($totalinvoice)}}</td>
    </tr>
    <tr>
      <td colspan="9">Total Potongan Invoice</td>
      <td id="totalinvoicereduction">{{number_format($totalreduction)}}</td>
    </tr>
    <tr>
      <td colspan="9">Total Bayar</td>
      <td id="totalpayment">{{number_format($totalpayment)}}</td>
    </tr>
    <tr>
      <td colspan="9">Total Diskon</td>
      <td id="totaldiskon">{{number_format($totaldiskon)}}</td>
    </tr>
    <tr>
      <td colspan="9">Total Yang Belum Dibayar</td>
      <td id="invoicereduction">{{number_format($totalinvoice - $totaldiskon - $totalpayment)}}</td>
    </tr>
  </tfoot>
</table>
<script type="text/javascript">
  $(document).ready(function(event){
      reportFakturTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
  });
</script>