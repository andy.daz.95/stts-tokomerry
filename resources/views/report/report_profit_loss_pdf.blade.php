<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
.page-break {
    page-break-after: always;
}
</style>
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">Laporan Laba Rugi Periode {{$data['periode']}}</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr>
        <th>Keterangan</th>
        <th>$</th>
        <th>$</th>
      </tr>
  </thead>
  <tbody>
    <tr>
      <td colspan="3"><strong>Pendapatan</strong></td>
    </tr>
    <tr>
      <td>Total Penjualan</td>
      <td>{{number_format($data['totalsales'])}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Retur Penjualan</td>
      <td>{{'('.number_format($data['retur']->sum('total_price')).')'}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Cost Of Good Sold</td>
      <td>{{'('.number_format($data['totalcogs']).')'}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Pengurangan COGS dari Retur Penjualan</td>
      <td>{{number_format($data['retur']->sum('totalcogs'))}}</td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2">Pendapatan Bersih</td>
      @php 
        if($data['totalsales'] + $data['retur']->sum('totalcogs') > $data['totalcogs'] + $data['retur']->sum('total_price')){
          $penjualanbersih = ($data['totalsales'] + $data['retur']->sum('totalcogs') ) - ( $data['totalcogs'] + $data['retur']->sum('total_price') ) ;
          $type = 'positif';
        }else{
          $penjualanbersih = ( $data['totalcogs'] + $data['retur']->sum('total_price') ) - ( $data['totalsales'] + $data['retur']->sum('totalcogs'));
          $type = 'negatif';
        }
      @endphp
      <td>{{$type == 'positif' ?  number_format($penjualanbersih) : '('.number_format($penjualanbersih).')'}}</td>
    </tr>
    <tr>
      <td colspan="3"><strong>Beban</strong></td>
    </tr>
    @foreach($data['expense'] as $key => $value)
    <tr id="{{$value->expense_id}}" class="expense-row">
      <td>{{$value->description}}</td>
      <td>{{number_format($value->nominal)}}</td>
      <td></td>
    </tr>
    @endforeach
    <tr>
      <td colspan="2">Total Beban</td>
      <td>{{'('.number_format($data['expense']->sum('nominal')).')'}}</td>
    </tr>
    <tr>
      <td colspan="2">Laba / Rugi Bersih</td>
      @php
          $totalexpense = $data['expense']->sum('nominal');
          if($type == 'positif')
          {
              if($penjualanbersih > $totalexpense){
                $laba = $penjualanbersih - $totalexpense;
              }else{
                $rugi = $totalexpense - $penjualanbersih;  
              }
          }else{
                $rugi = $penjualanbersih + $totalexpense;
          }
      @endphp 
      <td>{{isset($laba) ? number_format($laba) : '('.number_format($rugi).')'}}</td>
    </tr>
  </tbody>
  </tbody>
</table>
</div>
