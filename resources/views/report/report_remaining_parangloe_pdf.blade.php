<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
.page-break {
    page-break-after: always;
}
</style>
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">SALES ORDER</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <tr>
    <th>No PO</th>
    <th>No Barang</th>
    <th>Jumlah Pemesanan</th>
    <th>Jumlah Diterima</th>
    <th>Jumlah Kirim Bulukumba</th>
    <th>Sisa Tidak Terkirim</th>
    </tr>
  </thead>
  <tbody>
    @foreach($remaining as $key => $value)
      <tr id="{{$value->purchase_order_details_id}}" class="po-row">
        @php
          $receive = $value->detailgoodreceive->sum('quantity_received');
          $sm = $value->detailsm->sum('quantity');
        @endphp
        <td class="nopo">{{$value->po->purchase_order_number}}</td>
        <td>{{$value->product->product_name}}</td>
        <td>{{$value->quantity + $value->free_qty}}</td>
        <td>{{$receive}}</td>
        <td>{{$sm}}</td>
        <td>{{$receive - $sm}}</td>
      </tr>
    @endforeach
  </tbody>
</table>