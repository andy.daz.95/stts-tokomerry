<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
.page-break {
    page-break-after: always;
}
</style>
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">Laporan Pendapatan Bersih Periode {{$periode}}</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
  <thead>
    <tr>
    <th>No Payment</th>
    <th>Nama Customer</th>
    <th>Grand Total</th>
    <th>Total COGS</th>
    <th>Penjualan Bersih</th>
  </tr>
  </thead>
  <tbody>
    @foreach($sales as $key => $value)
    <tr id="{{$value->sales_order_id}}" class="so-row">
      <td>{{$value->sales_order_number}}</td>
      <td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>
      <td>{{$value->grand_total_idr}}</td>
      <td>{{$value->totalcogs->totalcogs}}</td>
      <td class="sales">{{$value->grand_total_idr - $value->totalcogs->totalcogs}}</td>
    </tr>
    @endforeach
  </tbody>
    <tr>
      <td colspan="4">Total Penjualan Bersih</td>
      <td id="totalpenjualanbersih">{{$totalpenjualanbersih}}</td>
    </tr>
  @if(isset($retur))
  <thead>
    <tr>
    <th>No Retur</th>
    <th>Nama Customer</th>
    <th>Total Retur</th>
    <th>Total COGS</th>
    <th>Potongan Retur</th>
  </tr>
  </thead>
  <tbody>
    @foreach($retur as $key => $value)
    <tr id="{{$value->sales_retur_id}}" class="retur-row">
      <td>{{$value->retur_number}}</td>
      <td>{{$value->so->customer->first_name.' '.$value->so->customer->last_name}}</td>
      <td>{{$value->total_price}}</td>
      <td>{{$value->totalcogs->totalcogs}}</td>
      <td class="retur">{{$value->total_price - $value->totalcogs->totalcogs}}</td>
    </tr>
    @endforeach
    <tr>
      <td colspan="4">Total Potongan Dari Retur</td>
      <td id="totalreturbersih">{{$totalreturbersih}}</td>
    </tr>
    <tfoot>
      <tr>
        <td colspan="4">Total Pendapatan Bersih</td>
        <td id="totalpendapatanbersih">{{$totalpenjualanbersih - $totalreturbersih}}</td>
      </tr>
    </tfoot>
  </tbody>
  @endif
</table>
</div>