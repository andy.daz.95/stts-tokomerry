<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
  <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <tr>
      <td>No Stock Transfer</td>
      <td>Tanggal</td>
      <td>Barang</td>
      <td>Dari Gudang</td>
      <td>Ke Gudang</td>
      <td>No Faktur</td>
      <td>Qty</td>
      <td>Supir</td>
    </tr>
  </thead>
  <tbody>
    @foreach($stockmovement as $key => $value)
    <tr>
      <td>{{$value->stock_movement->stock_movement_number}}</td>
      <td>{{$value->stock_movement->date_stock_movement}}</td>
      <td>{{$value->product->product_name}}</td>
      <td>{{$value->stock_movement->warehouse_from_data->warehouse_name}}</td>
      <td>{{$value->stock_movement->warehouse_to_data->warehouse_name}}</td>
      <td>{{$value->faktur_number}}</td>
      <td>{{$value->quantity}}</td>
      <td>{{$value->stock_movement->driver_name}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
</body>
</html>