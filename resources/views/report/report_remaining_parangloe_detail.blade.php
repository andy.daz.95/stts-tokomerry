<table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No PO</th>
    <th>No Barang</th>
    <th>Jumlah Pemesanan</th>
    <th>Jumlah Diterima</th>
    <th>Jumlah Kirim Bulukumba</th>
    <th>Sisa Tidak Terkirim</th>
  </thead>
  <tbody>
    @foreach($remaining as $key => $value)
    <tr id="{{$value->purchase_order_details_id}}" class="po-row">
      @php
        $receive = $value->detailgoodreceive->sum('quantity_received');
        $sm = $value->detailsm->sum('quantity');
      @endphp
      <td class="nopo">{{$value->po->purchase_order_number}}</td>
      <td>{{$value->product->product_name}}</td>
      <td>{{$value->quantity + $value->free_qty}}</td>
      <td>{{$receive}}</td>
      <td>{{$sm}}</td>
      <td>{{$receive - $sm}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
<script type="text/javascript">
      reportPoTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'tip',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
</script>