<table id="reportUnsettledPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No PO</th>
    <th>Supplier</th>
    <th>Tanggal</th>
    <th>Total</th>
    <th>Diskon</th>
    <th>Potong <br>Nota</th>
    <th>Potongan Invoice <br>Sebelumnya</th>
    <th>Grand Total</th>
    <th>Payment</th>
  </thead>
  <tbody>
    @foreach($po->where('po_status','<>', 'Paid') as $key => $value)
    @php
    $discount = $value->discount_nominal ? $value->discount_nominal : 0;
    $reduction = $value->reduction ? $value->reduction : 0;
    $prev_reduction = $value->prev_invoice_reduction ? $value->prev_invoice_reduction : 0;
    @endphp
    <tr id="{{$value->purchase_order_id}}" class="po-row">
      <td class="nopo">{{$value->purchase_order_number}}</td>
      <td>{{$value->company_name}}</td>
      <td>{{$value->date_purchase_order}}</td>
      <td class="total">{{number_format($value->grand_total_idr)}}</td>
      <td class="diskon">{{$value->discount_nominal ? number_format($value->discount_nominal) : '-'}}</td>
      <td class="reduction">{{$value->reduction ? number_format($value->reduction) : '-'}}</td>
      <td class="prev-invoice-reduction">{{$value->prev_invoice_reduction ? number_format($value->prev_invoice_reduction) : '-'}}</td>
      <td class="grandtotal">{{number_format($value->grand_total_idr - $discount - $reduction - $prev_reduction)}}</td>
      <td class="payment">{{$value->totalpaid ? number_format($value->totalpaid) : '-' }}</td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    @if(sizeOf($po) > 0)
    <tr>
      <td colspan="8">Total Nilai PO</td>
      <td id="total-po"></td>
    </tr>
    <tr>
      <td colspan="8">Total Nilai PO Setelah Diskon Dan Potong Nota</td>
      <td id="total-potong"></td>
    </tr>
    <tr>
      <td colspan="8">Total Pembayaran</td>
      <td id="total-payment"></td>
    </tr>
    <tr>
      <td colspan="8"> Total Utang Tersisa</td>
      <td id="total-payable"></td>
    </tr>
    @endif
  </tfoot>
</table>
<script type="text/javascript">
      reportPoTable = $('#reportUnsettledPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
</script>