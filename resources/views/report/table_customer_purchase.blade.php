<div class="input-field" class="">
  <input id="grandtotal" type="text" class="f-input" disabled value="{{number_format($detailso->sum('total_price'))}}">
  <label for="">Total</label>
</div>
@foreach($idbarang as $key => $value)
  <div class="row">
    <table class="highlight table table-bordered display nowrap dataTable dtr-inline">
      <thead>
      <tr>
        <td>Nama Barang</td>
        <td>No SO</td>
        <td>Tanggal SO</td>
        <td>quantity</td>
        <td>subtotal</td>
      </tr>
      </thead>
      <tbody>
      @php($detailproduct = $detailso->where('product_id',$value))
      @foreach($detailproduct as $key => $value)
        <tr>
          <td>{{$value->product->product_name}}</td>
          <td>{{$value->so->sales_order_number}}</td>
          <td>{{$value->so->date_sales_order}}</td>
          <td>{{$value->quantity}}</td>
          <td>{{number_format($value->total_price)}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
@endforeach