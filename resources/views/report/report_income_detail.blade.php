<table id="reportIncomeTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No Payment</th>
    <th>Nama Customer</th>
    <th>Metode Pembayaran</th>
    <th>Bank</th>
    <th>Nominal Pembayaran</th>
  </thead>
  <tbody>
    @foreach($payment as $key => $value)
    <tr id="{{$value->payment_sales_id}}" class="income-row">
      <td>{{$value->payment_sales_number}}</td>
      <td>{{$value->first_name.' '.$value->last_name}}</td>
      <td class="paymentmethod">{{$value->payment_methods == 1 ? 'Tunai' : 'Transfer'}}</td>
      <td>{{$value->bank_name ?  $value->bank_name : '-'}}</td>
      <td class="nominal">{{number_format($value->total_paid)}}</td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="4">Total Pembayaran Tunai</td>
      <td id="totaltunai"></td>
    </tr>
    <tr>
      <td colspan="4">Total Pembayaran Transfer</td>
      <td id="totaltransfer"></td>
    </tr>
  </tfoot>
</table>
<script type="text/javascript">
  $(document).ready(function(event){

      reportIncomeTable = $('#reportIncomeTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
  });
</script>