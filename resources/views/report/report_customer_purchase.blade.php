<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Report</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Tanggal Awal</label>
                  <input id="tglmulai" type="text" class="f-input" placeholder="Tanggal Awal">
                </div>
                <div class="input-field col l3">
                  <label>Tanggal Akhir</label>
                  <input id="tglakhir" type="text" class="f-input" placeholder="Tanggal Akhir">
                </div>
                <div class="input-field col l3">
                  <label>Customer</label>
                  <select id="customer" name="customer" class="browser-default selectpicker" data-live-search="true" data-size="5"  >
                    <option value="0">Select Customer</option>
                    @foreach($customer as $key => $value)
                      <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="input-field col l6">
                  <label></label>
                  <input id="namabarang" type="text" class="f-input" placeholder="Barang" disabled>
                  <input id="idbarang" type="text" class="f-input" hidden>
                </div>
                <div class="input-field col l3">
                  <button id="tambah-barang" data-target="modal-tambah-barang" class="btn-stoko btn-stoko-primary">Tambah Barang</button>
                </div>
              </div>
              <div class="row">
                <div class="input-field col l3">
                  <label></label>
                  <a id="preview-customer-purchase" href="#" class="btn btn-raised blue white-text">preview</a>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir PO</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="col l12 m12 s12">
                </div>
                <br>
                <div class="col l12 m12 s12">
                  <div id="report-table" class="table-responsive">

                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-tambah-barang" class="modal">
  <form id="form-list-barang">
    <div class="modal-content">
      <h4>Tambah Barang</h4>
      <div class="row" id="append-barang">
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-barang">OK</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        $('#preview-customer-purchase').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var start = $('#tglmulai').val();
            var end = $('#tglakhir').val();
            var customer = $('#customer').val();
            var idbarang = $('#idbarang').val();
            $.ajax({
                method:"get",
                url:"getreportcustomerpurchase",
                data:{start:start, end:end, customer:customer, idbarang:idbarang},
                success:function(response){
                  $('#report-table').html(response);
                }
            })
        });

        $('#tambah-barang').on('click', function (event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            var src = "po";
            $.ajax({
                type: "GET",
                url: "modalListBarang",
                data: {src: src},
                success: function (response) {
                    $('#append-barang').html(response);
                },
                complete: function () {
                    $('#modal-tambah-barang').modal('open');
                }
            })
        });

        $('.submit-barang').on('click', function(event) {
            event.stopImmediatePropagation();
            modalBarangTable.search('').draw();

            var barang = [];
            var nama = [];
            modalBarangTable.rows().nodes().to$().find('input[type="checkbox"]:checked').each(function () {
                console.log($(this).closest('tr').attr('value'));
                console.log($(this).closest('tr').find('td:eq(1)').text());
                barang.push($(this).closest('tr').attr('value'));
                nama.push($(this).closest('tr').find('td:eq(1)').text());
            });

            console.log(barang);
            console.log(nama);

            $('#namabarang').val(nama.join(' - '));
            $('#idbarang').val(barang);

            $('#modal-tambah-barang').modal('close');
        });

        firstload();

        function firstload(){
            $('#tglmulai, #tglakhir').datepicker({
                dateFormat : 'dd-mm-yyyy',
                language: 'en',
            });

            var date = moment();
            var month = date.month()+1;
            var year = date.year();
            var endofmonth = moment().endOf('month').format('DD');
            var start = '01-'+month+'-'+year;
            var end = endofmonth+'-'+month+'-'+year;
            $('#tglmulai').val(start);
            $('#tglakhir').val(end);

            $('.selectpicker').selectpicker('render');
            $('#export-so').attr('href',"excelso/"+start+"/"+end);
        }
    });

</script>