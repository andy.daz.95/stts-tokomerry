<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
 <table>
 	<tr>
 		<td>No PO</td>
 		<td>Tanggal PO</td>
 		<td>Nama Product</td>
 		<td>Harga Satuan</td>
 		<td>Quantity</td>
 		<td>Subtotal</td>
 	</tr>
 	@foreach($po as $key => $value)
 		<tr>
 			<td>{{$value->purchase_order_number}}</td>
 			<td>{{$value->date_purchase_order}}</td>
 			<td>{{$value->product_name}}</td>
 			<td>{{$value->price}}</td>
 			<td>{{$value->quantity}}</td>
 			<td>{{$value->sub_total}}</td>
 		</tr>
 	@endforeach
 </table>
</body>
</html>