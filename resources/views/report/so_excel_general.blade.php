<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
 <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <tr>
    <th>No SO</th>
    <th>Customer</th>
    <th>Tanggal</th>
    <th>Total</th>
    <th>Invoice</th>
    <th>Status</th>
  </tr>
  <tbody>
   @foreach($general as $key => $value)
    <tr id="{{$value->sales_order_id}}" class="so-row">
      <td class="nopo">{{$value->sales_order_number}}</td>
      <td>{{$value->first_name.' '.$value->last_name}}</td>
      <td>{{$value->date_sales_order}}</td>
      <td class="total">{{$value->grand_total_idr}}</td>
      <td class="invoice">{{$value->totalinvoice ? $value->totalinvoice : 0 }}</td>
      <td>{{$value->so_status}}</td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    @if(sizeOf($general) > 0)
    <tr>
      <td colspan="5">Total Nilai SO</td>
      <td id="total-so">{{$totalso}}</td>
    </tr>
    <tr>
      <td colspan="5">Total Invoice</td>
      <td id="total-invoice">{{$totalinvoice}}</td>
    </tr>
    <tr>
      <td colspan="5"> Total Ivoice Yang Belum Dibuat</td>
      <td id="total-invoice-kurang">{{$totalso - $totalinvoice}}</td>
    </tr>
    @endif
  </tfoot>
</table>
</body>
</html>