<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
  <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <tr>
      <td>No PO</td>
      <td>Supplier</td>
      <td>Tanggal</td>
      <td>Tipe Faktur</td>
      <td>Nama Barang</td>
      <td>Harga Barang</td>
      <td>Qty</td>
      <td>Subtotal</td>
    </tr>
  </thead>
  <tbody>
    @foreach($po as $key => $value)
    <tr id="{{$value->po->purchase_order_id}}" class="po-row">
      <td class="nopo">{{$value->po->purchase_order_number}}</td>
      <td>{{$value->po->supplier->company_name}}</td>
      <td>{{$value->po->date_purchase_order}}</td>
      <td>{{$value->po->is_pkp == 0? 'Non Faktur': 'Faktur'}}</td>
      <td>{{$value->product->product_name}}</td>
      <td>{{$value->price}}</td>
      <td>{{$value->quantity}}</td>
      <td>{{$value->sub_total}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
</body>
</html>