<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
 <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <tr>
    <th>No Payment</th>
    <th>Tangal PO</th>
    <th>No PO</th>
    <th>Nama Supplier</th>
    <th>Tanggal Tanda Terima</th>
    <th>Nilai Invoice</th>
    <th>Tanggal Pembayaran</th>
    <th>Total Pembayaran</th>
  </tr>
  <tbody>
    @php
      $totalpayment = 0;
    @endphp
    @foreach($payment as $key => $value)
      <tr>
        @php
          $totalpayment += $value->details[0]["paid"]
        @endphp
        <td>{{$value->payment_purchase_number}}</td>
        <td>{{$value->invoice->po->date_purchase_order}}</td>
        <td>{{$value->invoice->po->purchase_order_number}}</td>
        <td>{{$value->invoice->po->supplier->company_name}}</td>
        <td>{{$value->invoice->date_invoice_purchase}}</td>
        <td>{{number_format($value->invoice->details[0]["total_amount"])}}</td>
        <td>{{$value->date_payment_purchase}}</td>
        <td>{{number_format($value->details[0]["paid"])}}</td>
      </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="7">Total Pembayaran</td>
      <td id="total-payment">{{number_format($totalpayment)}}</td>
    </tr>
  </tfoot>
</table>
</body>
</html>