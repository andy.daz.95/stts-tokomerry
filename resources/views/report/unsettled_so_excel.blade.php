<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
 <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <tr>
    <th>No SO</th>
    <th>Customer</th>
    <th>Tanggal</th>
    <th>Total</th>
    <th>Invoice</th>
    <th>Pembayaran</th>
  </tr>
  <tbody>
    @php
      $totalso = $totalinvoice = $totalpayment = 0;
    @endphp
    @foreach($so as $key => $value)
    @if(!$value->invoice || !$value->invoice->payments || ($value->invoice && $value->invoice->payments && $value->invoice->payments->sum('total_paid') < $value->grand_total_idr))
      @php
        $totalso += $value->grand_total_idr;
        $totalinvoice += $value->invoice ? $value->invoice->total_amount : 0;
        $totalpayment += $value->invoice ? $value->invoice->payments ? $value->invoice->payments->sum('total_paid') : 0 : 0;
      @endphp
    <tr id="{{$value->sales_order_id}}" class="so-row">
      <td class="nopo">{{$value->sales_order_number}}</td>
      <td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>
      <td>{{$value->date_sales_order}}</td>
      <td class="total">{{number_format($value->grand_total_idr)}}</td>
      <td class="invoice">{{$value->invoice ? number_format($value->invoice->total_amount) : '-' }}</td>
      <td class="payments">{{$value->invoice ? $value->invoice->payments ? number_format($value->invoice->payments->sum('total_paid')) : '-' : '-'}}</td>
    </tr>
    @endif
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="5">Total Nilai SO</td>
      <td id="total-so">{{number_format($totalso)}}</td>
    </tr>
    <tr>
      <td colspan="5">Total Invoice</td>
      <td id="total-invoice">{{number_format($totalinvoice)}}</td>
    </tr>
    <tr>
      <td colspan="5">Total Invoice Belum Terbit</td>
      <td id="total-invoice-kurang">{{number_format($totalso - $totalinvoice)}}</td>
    </tr>
    <tr>
      <td colspan="5">Total Pembayaran</td>
      <td id="total-pembayaran">{{number_format($totalpayment)}}</td>
    </tr>
    <tr>
      <td colspan="5">Total Belum Bayar</td>
      <td id="total-kurang-bayar">{{number_format($totalinvoice - $totalpayment)}}</td>
    </tr>
  </tfoot>
</table>
</body>
</html>