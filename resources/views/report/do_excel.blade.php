<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
 <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <tr><th>Report Pengeluaran Barang {{$product->product_name}} Periode {{$periode}}</th></tr>
  <tr>
    <th>No DO</th>
    <th>Tanggal</th>
    <th>No SO</th>
    <th>Gudang</th>
    <th>Dikirim Oleh</th>
    <th>No Kendaraan</th>
    <th>Qty</th>
  </tr>
  <tbody>
    @php 
    $warehouseArray = [];
    foreach ($warehouse as $key => $value){
      $warehouseArray[$value->warehouse_id] = 0;
    } 
    @endphp
    @foreach($do as $key => $value)
    <tr id="{{$value->delivery_order_id}}" class="po-row">
      <td class="nodo">{{$value->delivery_order_number}}</td>
      <td>{{$value->date_delivery}}</td>
      @if($value->draft->so)
      <td class="noso">{{$value->draft->so->sales_order_number}}</td>
      @else
      <td class="noso">{{$value->draft->retur->so->sales_order_number.' (Retur)'}}</td>
      @endif
      <td>{{$value->draft->warehouse->warehouse_name}}</td>
      <td>{{$value->draft->driver_name? $value->draft->driver_name : '-' }}</td>
      <td>{{$value->draft->vehicle_number? $value->draft->vehicle_number : '-'}}</td>
      <td class="qty">{{$value->details->first()->quantity_sent}}</td>
      @php 
        $warehouseArray[$value->draft->warehouse->warehouse_id] += $value->details->first()->quantity_sent;
      @endphp
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td  colspan="6">Total Qty Terkirim</td>
      <td id='totalqty'>{{$totalqty}}</td>
    </tr>
    @foreach($warehouse as $key => $value)
      <tr>
        <td colspan = "6">Total Qty Terkirim dari {{$value->warehouse_name}}</td>
        <td>{{$warehouseArray[$value->warehouse_id]}}</td>
      </tr>
    @endforeach
  </tfoot>
</table>
</body>
</html>