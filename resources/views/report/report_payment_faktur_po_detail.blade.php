<table id="reportFakturTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No Payment</th>
    <th>Tangal PO</th>
    <th>No PO</th>
    <th>Nama Supplier</th>
    <th>Tanggal Tanda Terima</th>
    <th>Nilai Invoice</th>
    <th>Tanggal Pembayaran</th>
    <th>Total Pembayaran</th>
  </thead>
  <tbody>
    @php
      $totalpayment = 0;
    @endphp
    @foreach($payment as $key => $value)
      @php
        $totalpayment += $value->details[0]["paid"];
      @endphp
      <tr>
        <td>{{$value->payment_purchase_number}}</td>
        <td>{{$value->invoice->po->date_purchase_order}}</td>
        <td>{{$value->invoice->po->purchase_order_number}}</td>
        <td>{{$value->invoice->po->supplier->company_name}}</td>
        <td>{{$value->invoice->date_invoice_purchase}}</td>
        <td>{{number_format($value->invoice->details[0]["total_amount"])}}</td>
        <td>{{$value->date_payment_purchase}}</td>
        <td>{{number_format($value->details[0]["paid"])}}</td>
      </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="7">Total Pembayaran</td>
      <td>{{number_format($totalpayment)}}</td>
    </tr>
  </tfoot>
</table>
<script type="text/javascript">
  $(document).ready(function(event){
      reportFakturTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
  });
</script>