<!DOCTYPE html>
<html>
<head>
	<title>{{$periode}}</title>
</head>
<body>
 <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <tr>
    <th>No Expense</th>
    <th>Tanggal Expense</th>
    <th>Keterangan</th>
    <th>Jumlah</th>
  </tr>
  <tbody>
    @foreach($expense as $key => $value)
    <tr id="{{$value->expense_id}}" class="expense-row">
      <td>{{$value->expense_number}}</td>
      <td>{{$value->date_expense}}</td>
      <td>{{$value->description}}</td>
      <td>{{$value->nominal}}</td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="3">Total Nilai Expense</td>
      <td id="total-po">{{$expense->sum('nominal')}}</td>
    </tr>
  </tfoot>
</table>
</body>
</html>