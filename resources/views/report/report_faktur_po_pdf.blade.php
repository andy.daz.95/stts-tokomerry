<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
.page-break {
    page-break-after: always;
}
</style>
@foreach($po as $key => $value1)
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">PURCHASE ORDER</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 50%">Kepada YTH,</td>
      <td style="width: 25%">PO Number</td>
      <td>: {{$value1->purchase_order_number}}</td>
    </tr>
    <tr>
      <td>{{$value1->supplier->company_name}}</td>
      <td>Tanggal PO</td>
      <td>: {{$value1->date_purchase_order}}</td>
    </tr>
    <tr>
      <td>{{$value1->address}}</td>
      <td>Payment Type</td>
      <td>: {{$value1->paymenttype->payment_description}}</td>
    </tr>
    <tr>
      <td>{{$value1->supplier->postal_code}}</td>
      <td></td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr style="background-color: #8B8C89">
        <th class="color" style="text-align: center;">Nama</th>
        <th class="color" style="width: 75px; text-align: center;">Qty</th>
        <th class="color" style="text-align: center;">Harga</th>
        <th class="color" style="text-align: center;">Sub Total</th>
      </tr>
    </thead>
    <tbody >
      @foreach($value1->detail as $key => $value2)
      <tr >
        <td>
          <span>{{$value2->product->product_name}}</span>
        </td>
        <td>
          <span>{{$value2->quantity}}</span>
        </td>
        <td>
          <span style="text-align:left;">Rp.</span>
          <span style="display: inline-block; float:right">{{number_format($value2->price)}}</span>
        </td>
        <td>
          <span style="text-align:left;">Rp.</span>
          <span style="display: inline-block; float:right; clear:both;">{{number_format($value2->sub_total)}}</span>
        </td>
      </tr>
      @endforeach
      <tr style="background-color: #8B8C89">
        <th colspan="3"><span>Grand Total</span></th>
        <th colspan="1">
          <span style="float:left;">Rp.</span>
          <span style="float:right;">{{number_format($value1->grand_total_idr)}}</span></th>
      </tr>
    </tbody>
  </table>
</div>
@if(!$loop->last)
<div class="page-break"></div>
@endif
@endforeach