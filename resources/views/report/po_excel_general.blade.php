<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
 <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <tr>
    <th>No PO</th>
    <th>Supplier</th>
    <th>Tanggal</th>
    <th>Status</th>
    <th>Tipe Faktur</th>
    <th>Total</th>
    <th>Diskon</th>
    <th>Potong Nota</th>
    <th>Potongan Invoice Sebelumnya</th>
    <th>Payment</th>
  </tr>
  <tbody>
    @foreach($general as $key => $value)
    <tr id="{{$value->purchase_order_id}}" class="po-row">
      <td class="nopo">{{$value->purchase_order_number}}</td>
      <td>{{$value->company_name}}</td>
      <td>{{$value->date_purchase_order}}</td>
      <td>
        @if($value->po_status == 'Paid')
        {{$value->totalpaid < $value->total_amount - $value->discount_nominal - $value->reduction? 'Belum Lunas':'Lunas'}}
        @else
        {{'Belum Bayar'}}
        @endif
      </td>
      <td>{{$value->is_pkp == 0 ? 'P' : '-'}}</td>
      <td class="total">{{number_format($value->grand_total_idr)}}</td>
      <td class="diskon">{{$value->discount_nominal ? number_format($value->discount_nominal) : '-'}}</td>
      <td class="reduction">{{$value->reduction ? number_format($value->reduction) : '-'}}</td>
      <td class="prev-invoice-reduction">{{$value->prev_invoice_reduction ? number_format($value->prev_invoice_reduction) : '-'}}</td>
      <td class="payment">{{$value->totalpaid ? number_format($value->totalpaid) : '-' }}</td>    </tr>
    @endforeach
  </tbody>
  <tfoot>
    @if(sizeOf($general) > 0)
    <tr>
      <td colspan="8">Total Nilai PO</td>
      <td id="total-po">{{$totalpo}}</td>
    </tr>
    <tr>
      <td colspan="8">Total Nilai PO Setelah Diskon dan Potong Nota</td>
      <td id="total-diskon">{{$totalpo - $totaldiskon - $totalreduction - $totalprevreduction}}</td>
    </tr>
    <tr>
      <td colspan="8">Total Pembayaran</td>
      <td id="total-payment">{{$totalpayment}}</td>
    </tr>
    <tr>
      <td colspan="8">Total Utang Tersisa</td>
      <td id="total-payable">{{$totalpo - $totaldiskon - $totalreduction - $totalprevreduction -  $totalpayment}}</td>
    </tr>
    @endif
  </tfoot>
</table>
</body>
</html>