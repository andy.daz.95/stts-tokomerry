<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
 <table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <tr>
    <th>No Payment</th>
    <th>Tangal SO</th>
    <th>No SO</th>
    <th>Nama Customer</th>
    <th>Tanggal Invoice</th>
    <th>Nilai Invoice</th>
    <th>Tanggal Pembayaran</th>
    <th>Total Pembayaran</th>
  </tr>
  <tbody>
    @php
      $totalpayment = 0;
    @endphp
    @foreach($payment as $key => $value)
      <tr>
        <td>{{$value->payment_sales_number}}</td>
        <td>{{$value->invoice->so->date_sales_order}}</td>
        <td>{{$value->invoice->so->sales_order_number}}</td>
        <td>{{$value->invoice->so->customer->first_name}}</td>
        <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->invoice->created_at)->toDateString()}}</td>
        <td>{{number_format($value->invoice->total_amount)}}</td>
        <td>{{$value->date_payment_sales}}</td>
        <td>{{number_format($value->total_paid)}}</td>
      </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="7">Total Payment</td>
      <td>{{number_format($payment->sum('total_paid'))}}</td>
    </tr>
  </tfoot>
</table>
</body>
</html>