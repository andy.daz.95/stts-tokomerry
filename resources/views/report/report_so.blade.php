<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Filter Report Sales Order</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Tanggal Awal</label>
                  <input id="tglmulai" type="text" class="f-input" placeholder="Tanggal Awal">
                </div>
                <div class="input-field col l3">
                  <label>Tanggal Akhir</label>
                  <input id="tglakhir" type="text" class="f-input" placeholder="Tanggal Akhir">
                </div>
                <div class="input-field col l3">
                  <label>Customer</label>
                  <select id="customer" name="customer" class="browser-default selectpicker" data-live-search="true" data-size="5"  >
                    <option value="0">Select Customer</option>
                    @foreach($customer as $key => $value)
                    <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="input-field col l3">
                  <label>Account</label>
                  <select id="account" name="account" class="browser-default selectpicker" data-live-search="true" data-size="5"  >
                    <option value="0">Select Account</option>
                    @foreach($account as $key => $value)
                      <option value="{{$value->account_id}}">{{$value->username}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="input-field col l3">
                  <label></label>
                  <a id="preview-so" href="#" class="btn btn-raised blue white-text">preview</a>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Formulir PO</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="col l12 m12 s12">
                </div>
                <br>
                <div class="col l12 m12 s12">
                  <div class="row">
                    <a id="export-so" href="#" target="_blank" class="btn btn-stoko-primary right orange darken-4">Export</a>
                  </div>
                  <div id="report-table" class="table-responsive">
                    <h4 id="report-title"></h4>
                    @include('report.report_so_detail')
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="modal-detail-so" class="modal">
  <div class="modal-content">
    <h4 id="modal-title"></h4>
    <div class="row">
      <table class="table table-bordered display nowrap dataTable dtr-inline">
        <thead>
          <th>Kode Barang</th>
          <th>Nama Barang</th>
          <th>Quantity</th>
        </thead>
        <tbody id="modal-data">

        </tbody>
      </table>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius">OK</a>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $.ajax({
      url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
      dataType: "script",
    });

    $('#preview-so').on('click', function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      var start = $('#tglmulai').val();
      var end = $('#tglakhir').val();
      var customer = $('#customer').val();
      var account = $('#account').val();
      $.ajax({
        method:"get",
        url:"reportso",
        data:{start:start, end:end, customer:customer, account:account},
        success:function(response){
          $('#report-table').html(response);
          $('#export-so').attr('href','excelso/'+start+'/'+end+'/'+customer).removeAttr('hidden');
          calculatetotal();
          changetitle();
        }
      })
    });

    $('.highlight tbody tr').on('click',function(event){
      var id = $(this).attr('id');
      var noso = $(this).find('.noso').html();
      $.ajax({
        method:"get",
        url:"detailreportso",
        data:{id:id},
        success:function(response){
          $('#modal-title').html('Detail SO '+ noso);
          $('#modal-data').html(response);
          $('#modal-detail-so').modal('open');
        }
      })
    });

    firstload();

    function firstload(){
      $('#tglmulai, #tglakhir').datepicker({
        dateFormat : 'dd-mm-yyyy',
        language: 'en',
      });

      var date = moment();
      var month = date.month()+1;
      var year = date.year();
      var endofmonth = moment().endOf('month').format('DD');
      var start = '01-'+month+'-'+year;
      var end = endofmonth+'-'+month+'-'+year;
      $('#tglmulai').val(start);
      $('#tglakhir').val(end);

      $('.selectpicker').selectpicker('render');
      $('#export-so').attr('href',"excelso/"+start+"/"+end);

      changetitle();
      calculatetotal();

      // alert(moment().endOf('month').format('DD'));
      // alert(moment().year());


      // var enddate = moment().endOf('month');
      // var end = moment(enddate+'-MM-YYYY');

      // $('#tglmulai').val(start);
      // $('#tglakhir').val(end);

    }
    function changetitle(){
      $('#report-title').html("Report SO Periode "+$('#tglmulai').val()+" - "+$('#tglakhir').val());
    }

    function calculatetotal(){
      var total = 0;
      var invoice = 0;
      var payments = 0;

      if($('.so-row').length > 0)
      {
        $('.total').each(function(){
          if($(this).html() =='-') {
            total += 0;
          }else{
            total += parseInt($(this).html().replace(/[.,]/gi,''));
          }
        });

        $('.invoice').each(function(){
          if($(this).html() =='-') {
            invoice += 0;
          }else{
            invoice += parseInt($(this).html().replace(/[.,]/gi,''));
          }
        });

        $('.payments').each(function(){
          if($(this).html() =='-') {
            payments += 0;
          }else{
            payments += parseInt($(this).html().replace(/[.,]/gi,''));
          }
        });

        $('#total-so').html(accounting.formatMoney(total,'',0,','));
        $('#total-invoice').html(accounting.formatMoney(invoice,'',0,','));
        $('#total-invoice-kurang').html(accounting.formatMoney(total-invoice,'',0,','));
        $('#total-pembayaran').html(accounting.formatMoney(payments,'',0,','));
        $('#total-kurang-bayar').html(accounting.formatMoney(invoice - payments,'',0,','));
      }
    }
  });

</script>