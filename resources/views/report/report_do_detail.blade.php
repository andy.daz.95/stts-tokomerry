<table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
  <th>No DO</th>
  <th>Tanggal</th>
  <th>No SO</th>
  <th>Warehouse</th>
  <th>Dikirim Oleh</th>
  <th>No Kendaraan</th>
  <th>Qty</th>
  </thead>
  <tbody>
  @php
    $total = 0;
    $warehouseArray = [];
    foreach ($warehouse as $key => $value){
      $warehouseArray[$value->warehouse_id] = 0;
    }
  @endphp
  @foreach($do as $key => $value)
    <tr id="{{$value->delivery_order_id}}" class="po-row">
      <td class="nodo">{{$value->delivery_order_number}}</td>
      <td>{{$value->date_delivery}}</td>
      @if($value->draft->so)
        <td class="noso">{{$value->draft->so->sales_order_number}}</td>
      @else
        <td class="noso">{{$value->draft->retur->so->sales_order_number.' (Retur)'}}</td>
      @endif
      <td>
        @if($value->draft->warehouse)
          @php $warehouse_id = $value->draft->warehouse->warehouse_id @endphp
          {{$value->draft->warehouse->warehouse_name}}
        @else
          @php $warehouse_id = $value->warehouse->warehouse_id @endphp
          {{$value->warehouse->warehouse_name}}
        @endif
      </td>
      <td>{{$value->draft->driver_name? $value->draft->driver_name : '-' }}</td>
      <td>{{$value->draft->vehicle_number? $value->draft->vehicle_number : '-'}}</td>
      @php
        foreach ($value->details as $detail)
        {
          if($detail->unit_type == "child") {
            $multiplier = \App\unitchild::where('unit_child_id',$detail->unit_id)->first()->multiplier;
            $qty = round( $detail->quantity_sent / $multiplier, 3, PHP_ROUND_HALF_DOWN);
          }else{
            $qty = $detail->quantity_sent;
          }
        }
        $total += $qty;
        $warehouseArray[$warehouse_id] += $qty;
      @endphp
      <td class="qty">{{$qty}}</td>
    </tr>
  @endforeach
  </tbody>
  <tfoot>
  <tr>
    <td  colspan="6">Total Qty Terkirim</td>
    <td id='totalqty'>{{$total}}</td>
  </tr>
  @foreach($warehouse as $key => $value)
    <tr>
      <td colspan = "6">Total Qty Terkirim dari {{$value->warehouse_name}}</td>
      <td>{{$warehouseArray[$value->warehouse_id]}}</td>
    </tr>
  @endforeach
  </tfoot>
</table>
<script type="text/javascript">
    reportPoTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
            "sProcessing":   "Sedang proses...",
            "sLengthMenu":   "Tampilan _MENU_ entri",
            "sZeroRecords":  "Tidak ditemukan data yang sesuai",
            "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix":  "",
            "sSearch":       "Cari:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Awal",
                "sPrevious": "Balik",
                "sNext":     "Lanjut",
                "sLast":     "Akhir"
            }
        },
    });
</script>