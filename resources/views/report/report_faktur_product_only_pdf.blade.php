<style type="text/css">
    table.table {
        border-collapse: collapse;
    }
    table.table, table.table th, table.table td{
        border: 1px solid black;
    }
    .page-break {
        page-break-after: always;
    }
</style>
<h3 style="text-align: center">Gunung Sari Jaya Bulukumba</h3>
<h3 style="text-align: center">Report Penjualan Produk Faktur</h3>
<div>
    <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
        <thead>
        <tr style="background-color: #8B8C89">
            <th class="color" style="text-align: center;">Tanggal</th>
            <th class="color" style="text-align: center;">Nama</th>
            <th class="color" style="width: 60px; text-align: center;">Qty</th>
            <th class="color" style="width: 60px; text-align: center;">Weight</th>
            <th class="color" style="text-align: center;">Harga</th>
            <th class="color" style="text-align: center;">Sub Total</th>
        </tr>
        </thead>
        <tbody>
        @php
                $grandtotal = 0;
        @endphp
        @foreach($so as $key => $value1)
            @if($fakturtype == 1)
                @php
                    $details = $value1->details_faktur;
                @endphp
            @else
                @php
                    $details = $value1->details_non_faktur;
                @endphp
            @endif
            @foreach($details as $key => $value2)
                <tr>
                    <td>{{$value1->date_sales_order}}</td>
                    <td>
                        <span>
                          {{$value2->product->product_name}}
                        </span>
                    </td>
                    <td>
                        <span>{{$value2->quantity}}</span>
                    </td>
                    <td>
                        <span>{{$value2->weight}}</span>
                    </td>
                    <td>
                        <span style="text-align:left;">Rp.</span>
                        <span style="display: inline-block; float:right; clear:both;">{{number_format($value2->price)}}</span>
                    </td>
                    <td>
                        <span style="text-align:left;">Rp.</span>
                        <span style="display: inline-block; float:right; clear:both;">{{number_format($value2->sub_total)}}</span>
                    </td>
                    @php
                        $grandtotal += $value2->sub_total;
                    @endphp
                </tr>
            @endforeach
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">Grand Total</td>
                <td>
                    <span style="text-align:left;">Rp.</span>
                    <span style="display: inline-block; float:right; clear:both;">{{number_format($grandtotal)}}</span>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
