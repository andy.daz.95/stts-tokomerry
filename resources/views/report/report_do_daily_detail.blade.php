<table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
  <th>No DO</th>
  <th>No SO</th>
  <th>Nama Barang</th>
  <th>Tanggal</th>
  <th>Warehouse</th>
  <th>Qty</th>
  <th>Unit</th>
  </thead>
  <tbody>
  @foreach($do as $key => $value)
    <tr id="{{$value->delivery_order_id}}" class="po-row">
      <td class="nodo">{{$value->delivery->delivery_order_number}}</td>
      @if($value->delivery->draft->so)
        <td class="noso">{{$value->delivery->draft->so->sales_order_number}}</td>
      @else
        <td class="noso">{{$value->delivery->draft}}</td>
      @endif
      <td>{{$value->product->product_name}}</td>
      <td>{{$value->delivery->date_delivery}}</td>
      <td>
        @if($value->delivery->draft->warehouse)
          @php $warehouse_id = $value->delivery->draft->warehouse->warehouse_id @endphp
          {{$value->delivery->draft->warehouse->warehouse_name}}
        @else
          @php $warehouse_id = $value->delivery->warehouse->warehouse_id @endphp
          {{$value->delivery->warehouse->warehouse_name}}
        @endif
      </td>
      <td class="qty">{{$value->quantity_sent}}</td>
      <td>
        @if($value->unit_type == 'main')
        @php
        $satuan = App\satuan::where('unit_id', $value->unit_id)->first()->unit_description;
        @endphp
        {{$satuan}}
        @else
        {{App\unitchild::where('unit_child_id', $value->unit_id)->first()->unit_child_name}}
        @endif
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
<script type="text/javascript">
    reportPoTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        'bPaginate': false,
        language: {
            "sProcessing":   "Sedang proses...",
            "sLengthMenu":   "Tampilan _MENU_ entri",
            "sZeroRecords":  "Tidak ditemukan data yang sesuai",
            "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix":  "",
            "sSearch":       "Cari:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Awal",
                "sPrevious": "Balik",
                "sNext":     "Lanjut",
                "sLast":     "Akhir"
            }
        },
    });
</script>