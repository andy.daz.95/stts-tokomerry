<table id="reportFakturTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No Payment</th>
    <th>Tangal SO</th>
    <th>No SO</th>
    <th>Nama Customer</th>
    <th>Tanggal Invoice</th>
    <th>Nilai Invoice</th>
    <th>Tanggal Pembayaran</th>
    <th>Total Pembayaran</th>
    <th>Sisa</th>
  </thead>
  <tbody>
    @php
      $totalpayment = 0;
    @endphp
    @foreach($payment as $key => $value)
      <tr>
        <td>{{$value->payment_sales_number}}</td>
        <td>{{$value->invoice->so->date_sales_order}}</td>
        <td>{{$value->invoice->so->sales_order_number}}</td>
        <td>{{$value->invoice->so->customer->first_name.' '.$value->invoice->so->customer->last_name}}</td>
        <td>{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->invoice->created_at)->toDateString()}}</td>
        <td>{{number_format($value->invoice->total_amount)}}</td>
        <td>{{$value->date_payment_sales}}</td>
        <td>{{number_format($value->total_paid)}}</td>
        <td>{{number_format($value->all_payments_for_this_invoice->sum('total_paid'))}}</td>
      </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="7">Total Payment</td>
      <td>{{number_format($payment->sum('total_paid'))}}</td>
    </tr>
  </tfoot>
</table>
<script type="text/javascript">
  $(document).ready(function(event){
      reportFakturTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
  });
</script>