<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Filter Report Stock Movement</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Tanggal Awal</label>
                  <input id="tglmulai" type="text" class="f-input" placeholder="Tanggal Awal">
                </div>
                <div class="input-field col l3">
                  <label>Tanggal Akhir</label>
                  <input id="tglakhir" type="text" class="f-input" placeholder="Tanggal Akhir">
                </div>
                <div class="input-field col l3">
                  <label></label>
                  <a id="preview" href="#" class="btn btn-raised blue white-text">preview</a>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Sisa Parangloe</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="col l12 m12 s12">
                </div>
                <br>
                <div class="col l12 m12 s12">
                  <div id="report-table" class="table-responsive">
                    @include('report.report_stock_movement_detail')
                  </div>
                  <a id="export-stock-movement" href="#" target="_blank" class="btn btn-stoko-primary right orange darken-4">Export</a>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $('#preview').on('click', function(event){
      event.stopPropagation();
      event.stopImmediatePropagation();
      var start = $('#tglmulai').val();
      var end = $('#tglakhir').val();
      $.ajax({
        method:"get",
        url:"reportstockmovement",
        data:{start:start, end:end},
        success:function(response){
          $('#report-table').html(response);
          $('#export-stock-movement').attr('href','excelstockmovement/'+start+'/'+end).removeAttr('hidden');
        }
      })
    });

    firstload();

    function firstload(){
      $('#tglmulai, #tglakhir').datepicker({
        dateFormat : 'dd-mm-yyyy',
        language: 'en',
      });

      var date = moment();
      var month = date.month()+1;
      var year = date.year();
      var endofmonth = moment().endOf('month').format('DD');
      var start = '01-'+month+'-'+year;
      var end = endofmonth+'-'+month+'-'+year;
      $('#tglmulai').val(start);
      $('#tglakhir').val(end);

      $('.selectpicker').selectpicker('render');
      $('#export-stock-movement').attr('href','excelstockmovement/'+start+'/'+end);
    }

    function changetitle(){
      $('#report-title').html("Report PO Periode "+$('#tglmulai').val()+" - "+$('#tglakhir').val());
    }

    function calculatetotal(){
      var total = 0
      var payment = 0
      var diskon = 0
      var reduction = 0
      var prevreduction = 0

      if($('.po-row').length > 0)
      {
        $('.total').each(function(){
          if($(this).html() =='-') {
            total += 0;
          }else{
            total += parseInt($(this).html().replace(/[.,]/gi,''));
          }
        });

        $('.payment').each(function(){
          if($(this).html() =='-') {
            payment += 0;
          }else{
            payment += parseInt($(this).html().replace(/[.,]/gi,''));
          }
        });

        $('.reduction').each(function(){
          if($(this).html() =='-') {
            reduction += 0;
          }else{
            reduction += parseInt($(this).html().replace(/[.,]/gi,''));
          }
        });

        $('.prev-invoice-reduction').each(function(){
          if($(this).html() =='-') {
            prevreduction += 0;
          }else{
            prevreduction += parseInt($(this).html().replace(/[.,]/gi,''));
          }
        });


        $('.diskon').each(function(){
          if($(this).html() =='-') {
            diskon += 0;
          }else{
            diskon += parseInt($(this).html().replace(/[.,]/gi,''));
          }
        });

        $('#total-po').html(accounting.formatMoney(total,'',0,','));
        $('#total-potong').html(accounting.formatMoney(total - diskon - reduction - prevreduction,'',0,','));
        $('#total-payment').html(accounting.formatMoney(payment,'',0,','));
        $('#total-payable').html(accounting.formatMoney(total-diskon-reduction-prevreduction-payment,'',0,','));
      }
    }
  });

</script>