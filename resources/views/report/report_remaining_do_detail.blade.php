<table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No SO</th>
    <th>Nama Customer</th>
    <th>Nama Barang</th>
    <th>Jumlah Order</th>
    <th>Total Terkirim</th>
    <th>Sisa</th>
    <th>Note</th>
  </thead>
  <tbody>
    @foreach($so as $key => $value)
      @if($value->send_sum["total"] < $value->quantity)
        @php
          $totalsend = $value->send_sum["total"]? $value->send_sum["total"]: 0
        @endphp
        <tr class="po-row">
          <td>{{$value->so->sales_order_number}}</td>
          <td>{{$value->so->customer->first_name.' '.$value->so->customer->last_name}}</td>
          <td>{{$value->product? $value->product->product_name : '-'}}</td>
          <td>{{$value->quantity}}</td>
          <td>{{$totalsend}}</td>
          <td>{{$value->quantity-$totalsend}}</td>
          <td>{{$value->so->note}}</td>
        </tr>
      @endif
    @endforeach
  </tbody>
</table>