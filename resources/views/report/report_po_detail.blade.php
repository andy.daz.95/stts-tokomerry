<table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No PO</th>
    <th>Supplier</th>
    <th>Tanggal</th>
    <th>Status</th>
    <th>Tipe Faktur</th>
    <th>Total</th>
    <th>Diskon</th>
    <th>Potong <br>Nota</th>
    <th>Potongan Invoice <br>Sebelumnya</th>
    <th>Tanggal Tanda Terima</th>
    <th>Payment</th>
    <th>Tanggal Pembayaran</th>
  </thead>
  <tbody>
    @foreach($po as $key => $value)
    <tr id="{{$value->purchase_order_id}}" class="po-row">
      <td class="nopo">{{$value->purchase_order_number}}</td>
      <td>{{$value->company_name}}</td>
      <td>{{$value->date_purchase_order}}</td>
      <td>
        @if($value->po_status == 'Paid')
          {{$value->totalpaid < $value->total_amount - $value->discount_nominal - $value->reduction? 'Belum Lunas':'Lunas'}}
        @else
        {{'Belum Bayar'}}
        @endif
      </td>
      <td>{{$value->is_pkp == 0 ? 'P' : '-'}}</td>
      <td class="total">{{number_format($value->grand_total_idr)}}</td>
      <td class="diskon">{{$value->discount_nominal ? number_format($value->discount_nominal) : '-'}}</td>
      <td class="reduction">{{$value->reduction ? number_format($value->reduction) : '-'}}</td>
      <td class="prev-invoice-reduction">{{$value->prev_invoice_reduction ? number_format($value->prev_invoice_reduction) : '-'}}</td>
      <td>{{$value->date_invoice_purchase ? $value->date_invoice_purchase : '-'}}</td>
      <td class="payment">{{$value->totalpaid ? number_format($value->totalpaid) : '-' }}</td>
      <td>
        @if($value->po_status == 'Paid')
          {{$value->date_payment_purchase}}
        @else
          {{'-'}}
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    @if(sizeOf($po) > 0)
    <tr>
      <td colspan="11">Total Nilai PO</td>
      <td id="total-po"></td>
    </tr>
    <tr>
      <td colspan="11">Total Nilai PO Setelah Diskon Dan Potong Nota</td>
      <td id="total-potong"></td>
    </tr>
    <tr>
      <td colspan="11">Total Pembayaran</td>
      <td id="total-payment"></td>
    </tr>
    <tr>
      <td colspan="11"> Total Utang Tersisa</td>
      <td id="total-payable"></td>
    </tr>
    @endif
  </tfoot>
</table>
<script type="text/javascript">
      reportPoTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
</script>