<!DOCTYPE html>
<html>
<head>
	<title>Export</title>
</head>
<body>
 <table>
 	<tr>
 		<td>No SO</td>
 		<td>Tanggal SO</td>
 		<td>Nama Product</td>
 		<td>Harga Satuan</td>
 		<td>Quantity</td>
 		<td>Subtotal</td>
 	</tr>
 	@foreach($so as $key => $value)
 		<tr>
 			<td>{{$value->sales_order_number}}</td>
 			<td>{{$value->date_sales_order}}</td>
 			<td>{{$value->product_name}}</td>
 			<td>{{$value->price}}</td>
 			<td>{{$value->quantity}}</td>
 			<td>{{$value->sub_total}}</td>
 		</tr>
 	@endforeach
 </table>
</body>
</html>