<table id="reportIncomeTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>Keterangan</th>
    <th>$</th>
    <th>$</th>
  </thead>
  <tbody>
    <tr>
      <td colspan="3"><strong>Pendapatan</strong></td>
    </tr>
    <tr>
      <td>Total Penjualan</td>
      <td>{{number_format($data['totalsales'])}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Retur Penjualan</td>
      <td>{{'('.number_format($data['retur']->sum('total_price')).')'}}</td>
      <td></td>
    </tr>
    <tr>
      <td>HPP Penjualan</td>
      <td>{{'('.number_format($data['totalcogs']).')'}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Pengurangan HPP Retur</td>
      <td>{{number_format($data['retur']->sum('totalcogs'))}}</td>
      <td></td>
    </tr>
    <tr>
      <td colspan="2">Pendapatan Bersih</td>
      @php 
        if($data['totalsales'] + $data['retur']->sum('totalcogs') > $data['totalcogs'] + $data['retur']->sum('total_price')){
          $penjualanbersih = ($data['totalsales'] + $data['retur']->sum('totalcogs') ) - ( $data['totalcogs'] + $data['retur']->sum('total_price') ) ;
          $type = 'positif';
        }else{
          $penjualanbersih = ( $data['totalcogs'] + $data['retur']->sum('total_price') ) - ( $data['totalsales'] + $data['retur']->sum('totalcogs'));
          $type = 'negatif';
        }
      @endphp
      <td>{{$type == 'positif' ?  number_format($penjualanbersih) : '('.number_format($penjualanbersih).')'}}</td>
    </tr>
    <tr>
      <td colspan="3"><strong>Beban</strong></td>
    </tr>
    @foreach($data['expense'] as $key => $value)
    <tr id="{{$value->expense_id}}" class="expense-row">
      <td>{{$value->description}}</td>
      <td>{{number_format($value->nominal)}}</td>
      <td></td>
    </tr>
    @endforeach
    <tr>
      <td colspan="2">Total Beban</td>
      <td>{{'('.number_format($data['expense']->sum('nominal')).')'}}</td>
    </tr>
    <tr>
      <td colspan="2">Laba / Rugi Bersih</td>
      @php
          $totalexpense = $data['expense']->sum('nominal');
          if($type == 'positif')
          {
              if($penjualanbersih > $totalexpense){
                $laba = $penjualanbersih - $totalexpense;
              }else{
                $rugi = $totalexpense - $penjualanbersih;  
              }
          }else{
                $rugi = $penjualanbersih + $totalexpense;
          }
      @endphp 
      <td>{{isset($laba) ? number_format($laba) : '('.number_format($rugi).')'}}</td>
    </tr>
  </tbody>
</table>
<script type="text/javascript">
  $(document).ready(function(event){
      reportIncomeTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
  });
</script>