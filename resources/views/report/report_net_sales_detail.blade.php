<table id="reportNetSalesTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No Payment</th>
    <th>Nama Customer</th>
    <th>Grand Total</th>
    <th>Total HPP</th>
    <th>Penjualan Bersih</th>
  </thead>
  <tbody>
    @foreach($sales as $key => $value)
    <tr id="{{$value->sales_order_id}}" class="so-row">
      <td>{{$value->sales_order_number}}</td>
      <td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>
      <td>{{number_format($value->grand_total_idr)}}</td>
      <td>{{number_format($value->totalcogs->totalcogs)}}</td>
      <td class="sales">{{number_format($value->grand_total_idr - $value->totalcogs->totalcogs)}}</td>
    </tr>
    @endforeach
  </tbody>
    <tr>
      <td colspan="4">Total Penjualan Bersih</td>
      <td id="totalpenjualanbersih"></td>
    </tr>
  @if(isset($retur))
  <thead>
    <th>No Retur</th>
    <th>Nama Customer</th>
    <th>Total Retur</th>
    <th>Total HPP</th>
    <th>Potongan Retur</th>
  </thead>
  <tbody>
    @foreach($retur as $key => $value)
    <tr id="{{$value->sales_retur_id}}" class="retur-row">
      <td>{{$value->retur_number}}</td>
      <td>{{$value->so->customer->first_name.' '.$value->so->customer->last_name}}</td>
      <td>{{number_format($value->total_price)}}</td>
      <td>{{number_format($value->totalcogs->totalcogs)}}</td>
      <td class="retur">{{number_format($value->total_price - $value->totalcogs->totalcogs)}}</td>
    </tr>
    @endforeach
    <tr>
      <td colspan="4">Total Potongan Dari Retur</td>
      <td id="totalreturbersih"></td>
    </tr>
    <tfoot>
      <tr>
        <td colspan="4">Total Pendapatan Bersih</td>
        <td id="totalpendapatanbersih"></td>
      </tr>
    </tfoot>
  </tbody>
  @endif
</table>
<script type="text/javascript">
  $(document).ready(function(event){
      reportNetSalesTable = $('#reportNetSalesTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
  });
</script>