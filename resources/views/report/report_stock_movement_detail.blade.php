<table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No Stock Transfer</th>
    <th>Tanggal</th>
    <th>Barang</th>
    <th>Dari Gudang</th>
    <th>Ke Gudang</th>
    <th>No Faktur</th>
    <th>Qty</th>
    <th>Supir</th>
  </thead>
  <tbody>
    @foreach($stockmovement as $key => $value)
    <tr>
      <td>{{$value->stock_movement->stock_movement_number}}</td>
      <td>{{$value->stock_movement->date_stock_movement}}</td>
      <td>{{$value->product->product_name}}</td>
      <td>{{$value->stock_movement->warehouse_from_data->warehouse_name}}</td>
      <td>{{$value->stock_movement->warehouse_to_data->warehouse_name}}</td>
      <td>{{$value->faktur_number}}</td>
      <td>{{$value->quantity}}</td>
      <td>{{$value->stock_movement->driver_name}}</td>
    </tr>
    @endforeach
  </tbody>
</table>