<table id="reportSoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No SO</th>
    <th>Customer</th>
    <th>Account</th>
    <th>Tanggal</th>
    <th>Total</th>
    <th>Invoice</th>
    <th>Pembayaran</th>
    <th>Status</th>
  </thead>
  <tbody>
    @foreach($so as $key => $value)
    <tr id="{{$value->sales_order_id}}" class="so-row">
      <td class="nopo">{{$value->sales_order_number}}</td>
      <td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>
      <td>{{$value->account->username}}</td>
      <td>{{$value->date_sales_order}}</td>
      <td class="total">{{number_format($value->grand_total_idr)}}</td>
      <td class="invoice">{{$value->invoice ? number_format($value->invoice->total_amount) : '-' }}</td>
      <td class="payments">{{$value->invoice ? $value->invoice->payments ? number_format($value->invoice->payments->sum('total_paid')) : '-' : '-'}}</td>
      <td>
        @if($value->invoice)
          @if($value->invoice->payments)
            {{$value->invoice->payments->sum('total_paid') == $value->invoice->total_amount ? 'Lunas' : 'Belum Lunas'}}
          @endif
        @else
          {{'Belum Bayar'}}
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    @if(sizeOf($so) > 0)
    <tr>
      <td colspan="7">Total Nilai SO</td>
      <td id="total-so"></td>
    </tr>
    <tr>
      <td colspan="7">Total Invoice</td>
      <td id="total-invoice"></td>
    </tr>
    <tr>
      <td colspan="7">Total Invoice Belum Terbit</td>
      <td id="total-invoice-kurang"></td>
    </tr>
    <tr>
      <td colspan="7">Total Pembayaran</td>
      <td id="total-pembayaran"></td>
    </tr>
    <tr>
      <td colspan="7">Total Belum Bayar</td>
      <td id="total-kurang-bayar"></td>
    </tr>
    @endif
  </tfoot>
</table>
<script type="text/javascript">
  reportSoTable = $('#reportSoTable').DataTable({ // This is for home page
    responsive: true,
    "bPaginate": false,
    'sDom':'ti',
    language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
     }
  });
</script>