<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Filter Report Laba Rugi</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l3">
                  <label>Tanggal Awal</label>
                  <input id="tglmulai" type="text" class="f-input" placeholder="Tanggal Awal">
                </div>
                <div class="input-field col l3">
                  <label>Tanggal Akhir</label>
                  <input id="tglakhir" type="text" class="f-input" placeholder="Tanggal Akhir">
                </div>
                <div class="input-field col l3">
                  <label></label>
                  <a id="preview" href="#" class="btn btn-raised blue white-text">preview</a>
                  <a id="print" href="#" target="_blank" class="btn btn-raised orange darken-1 white-text" disabled>print</a>

                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Detail Laba Rugi</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="col l12 m12 s12">
                </div>
                <br>
                <div class="col l12 m12 s12">
                  <h4 id="report-title"></h4>
                  <div id="report-table" class="table-responsive">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $.ajax({
      url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
      dataType: "script",
    });

    firstload();

    $('#preview').on('click', function(event){
      event.stopPropagation();
      event.stopImmediatePropagation();
      var start = $('#tglmulai').val();
      var end = $('#tglakhir').val();
      $.ajax({
        method:"get",
        url:"reportprofitloss",
        data:{start:start, end:end},
        success:function(response){
          $('#report-table').html(response);
           calculatetotal();
           $('#print').removeAttr('disabled').attr('href','printreportprofitloss/'+start+'/'+end);

        }
      })
    });

    function firstload(){
      $('#tglmulai, #tglakhir').datepicker({
        dateFormat : 'dd-mm-yyyy',
        language: 'en',
      });

      var date = moment();
      var month = date.month()+1; 
      var year = date.year();
      var endofmonth = moment().endOf('month').format('DD');
      var start = '01-'+month+'-'+year;
      var end = endofmonth+'-'+month+'-'+year;
      $('#tglmulai').val(start);
      $('#tglakhir').val(end);

      $('.selectpicker').selectpicker('render');
      calculatetotal();
    }

    function calculatetotal(){
      var total = 0;
      var length = $('.so-row').length;
      $('.so-row').each(function(){ 
        total += parseInt($(this).find('.nominal').html());
      });

      $('#totalpenjualanbersih').html(total);
    }
  });
</script>