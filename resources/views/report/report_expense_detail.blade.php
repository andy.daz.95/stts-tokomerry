<table id="reportExpenseTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <th>No Expense</th>
    <th>Tanggal Expenese</th>
    <th>Keterangan</th>
    <th>Jumlah</th>
  </thead>
  <tbody>
    @foreach($expense as $key => $value)
    <tr id="{{$value->payment_sales_id}}" class="expense-row">
      <td>{{$value->expense_number}}</td>
      <td>{{$value->date_expense}}</td>
      <td>{{$value->description}}</td>
      <td class="expense">{{number_format($value->nominal)}}</td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <td colspan="3">Total Expense</td>
      <td id="totalexpense"></td>
    </tr>
  </tfoot>
</table>
<script type="text/javascript">
  $(document).ready(function(event){

      reportExpenseTable = $('#reportExpenseTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
  });
</script>