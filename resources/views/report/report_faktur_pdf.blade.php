<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
.page-break {
    page-break-after: always;
}
</style>
@foreach($so as $key => $value1)
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">SALES ORDER {{$fakturtype == 1? 'FAKTUR' : 'NON FAKTUR'}}</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 50%">Kepada YTH,</td>
      <td style="width: 25%">SO Number</td>
      <td>:{{$value1->sales_order_number}}</td>
    </tr>
    <tr>
      <td>{{$value1->customer->first_name.' '.$value1->customer->last_name}}</td>
      <td>Tanggal SO</td>
      <td>: {{$value1->date_sales_order}}</td>
    </tr>
    <tr>
      <td>{{$value1->customer->address}}</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>{{$value1->customer->postal_code}}</td>
      <td></td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr style="background-color: #8B8C89">
        <th class="color" style="text-align: center; width: 375px">Nama</th>
        <th class="color" style="width: 60px; text-align: center;">Qty</th>
        <th class="color" style="width: 60px; text-align: center;">Unit</th>
        <th class="color" style="text-align: center;">Harga</th>
        <th class="color" style="text-align: center;">Sub Total</th>
      </tr>
    </thead>
    <tbody>
      @if($fakturtype == 1)
        @php
        $details = $value1->details_faktur;
        @endphp
      @else
      @php
        $details = $value1->details_non_faktur;
        @endphp
      @endif 
      @foreach($details as $key => $value2)
      <tr>
        <td>
          <span>
            @if($fakturtype == 1)
            {{$value2->product->product_name}}
            @else
            {{$value2->product->product_name}}
            @endif
          </span>
        </td>
        <td>
          <span>{{$value2->quantity}}</span>
        </td>
        <td>
          @php
            if($value2->unit_type == 'child'){
              $unit =
                \App\unitchild::where('unit_child_id',$value2->unit_id)
                ->first()
                ->unit_child_name;
            }else{
              $unit = $value2->unit->unit_description;
            }
          @endphp
          <span>{{ $unit }}</span>
        </td>
        <td>
          <span style="text-align:left;">Rp.</span>
          <span style="display: inline-block; float:right; clear:both;">{{number_format($value2->price)}}</span>
        </td>
        <td>
          <span style="text-align:left;">Rp.</span>
          <span style="display: inline-block; float:right; clear:both;">{{number_format($value2->sub_total)}}</span>
        </td>
      </tr>
      @endforeach
      <tr style="background-color: #8B8C89">
        <th colspan="4"><span>Grand Total</span></th>
        <th colspan="1">
          <span style="float:left;">Rp.</span>
          <span style="float:right;">
            @if($fakturtype == 1)
            {{number_format($details->sum('sub_total'))}}
            @else
            {{number_format($details->sum('sub_total'))}}
            @endif
          </span>
        </th>
      </tr>
    </tbody>
  </table>
</div>
@if(!$loop->last)
<div class="page-break"></div>
@endif
@endforeach