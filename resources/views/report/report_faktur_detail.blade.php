<table id="reportFakturTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
    <thead>
    <th>No SO</th>
    <th>Nama Customer</th>
    <th>Tangal SO</th>
    <th>Total</th>
    </thead>
    <tbody>
    @foreach($so as $key => $value)
        <tr id="{{$value->sales_order_id}}" class="faktur-row">
            <td>{{$value->sales_order_number}}</td>
            <td>{{$value->customer->first_name.' '.$value->customer->last_name}}</td>
            <td>{{$value->date_sales_order}}</td>
            <td class="total">
                @if($fakturtype == 1)
                    @php
                        $total = 0;
                        foreach($value->details_faktur as $key => $value2){
                            $total += $value2->sub_total;
                        }
                    @endphp
                    {{$total}}
                @else
                    @php
                        $total = 0;
                        foreach($value->details_non_faktur as $key => $value2){
                            $total += $value2->sub_total;
                        }
                    @endphp
                    {{number_format($total)}}
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="3">Total</td>
        <td id="grandtotal"></td>
    </tr>
    </tfoot>
</table>
<script type="text/javascript">
    $(document).ready(function(event){
        reportFakturTable = $('#reportPoTable').DataTable({ // This is for home page
            responsive: true,
            'sDom':'ti',
            language: {
                "sProcessing":   "Sedang proses...",
                "sLengthMenu":   "Tampilan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Awal",
                    "sPrevious": "Balik",
                    "sNext":     "Lanjut",
                    "sLast":     "Akhir"
                }
            },
        });
    });
</script>