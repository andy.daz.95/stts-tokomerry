<table id="reportPoTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
  <thead>
    <tr>
      <th>No PO</th>
      <th>Supplier</th>
      <th>Tanggal</th>
      <th>Tipe Faktur</th>
      <th>Nama Barang</th>
      <th>Harga Barang</th>
      <th>Qty</th>
      <th>Subtotal</th>
    </tr>
  </thead>
  <tbody>
    @foreach($po as $key => $value)
    <tr id="{{$value->po->purchase_order_id}}" class="po-row">
      <td class="nopo">{{$value->po->purchase_order_number}}</td>
      <td>{{$value->po->supplier->company_name}}</td>
      <td>{{$value->po->date_purchase_order}}</td>
      <td>{{$value->po->is_pkp == 0? 'Non Faktur': 'Faktur'}}</td>
      <td>{{$value->product->product_name}}</td>
      <td>{{$value->price}}</td>
      <td>{{$value->quantity}}</td>
      <td>{{$value->sub_total}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
<script type="text/javascript">
      reportPoTable = $('#reportPoTable').DataTable({ // This is for home page
        responsive: true,
        'sDom':'ti',
        language: {
       "sProcessing":   "Sedang proses...",
       "sLengthMenu":   "Tampilan _MENU_ entri",
       "sZeroRecords":  "Tidak ditemukan data yang sesuai",
       "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
       "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
       "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
       "sInfoPostFix":  "",
       "sSearch":       "Cari:",
       "sUrl":          "",
       "oPaginate": {
         "sFirst":    "Awal",
         "sPrevious": "Balik",
         "sNext":     "Lanjut",
         "sLast":     "Akhir"
       }
     },
      });
</script>