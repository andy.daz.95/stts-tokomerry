<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    @if(Session('roles')->name == 'master' || Session('roles')->name == 'Gudang Makassar')
      <div class="col l12 m12 s12">
        <ul class="collapsible" data-collapsible="accordion">
          <li>
            <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Import Barang</div>
            <div class="collapsible-body">
              <div class="container-fluid">
                <div class="row margin-top">
                  <form id="importBarang" enctype="multipart/form-data">
                    <div class="col l10">
                      <div class="input-field">
                        <input id="file" type="file" name="file" class="f-input">
                      </div>
                    </div>
                    <div class="col-l2">
                      <button id="submit" type="button" class="btn btn-raised light-blue darken-2">save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="col l12 m12 s12">
        <ul class="collapsible" data-collapsible="accordion">
          <li>
            <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Formulir Barang</div>
            <div class="collapsible-body">
              <div class="container-fluid">
                <form id="formBarang">
                  <div class="row margin-top">
                    <div class="col l6">
                      <div class="row">
                        <div class="input-field col l12 m12 s12">
                          <input id="id" type="text" class="f-input" name="id" hidden>
                          <input id="kode" type="text" class="f-input" name="kode">
                          <label>Kode</label>
                        </div>
                        <div class="input-field col l12 m12 s12">
                          <select id="productcategory" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="productcategory">
                            @foreach($productcategory as $key => $value)
                              <option value="{{$value->product_category_id}}">{{$value->description}}</option>
                            @endforeach
                          </select>
                          <label>Kategori Barang</label>
                        </div>
                      </div>
                    </div>
                    <div class="col l6">
                      <div class="row">
                        <div class="input-field col l12 m12 s12">
                          <input id="nama" type="text" class="f-input" name="nama">
                          <label>Nama Barang</label>
                        </div>
                        <div class="input-field col l6">
                          <select id="satuan" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="satuan">
                            @foreach($satuan as $key => $value)
                              <option value="{{$value->unit_id}}">{{$value->unit_description}}</option>
                            @endforeach
                          </select>
                          <label>Satuan</label>
                        </div>
                        <div class="input-field col l6">
                          <select id="faktur" class="browser-default selectpicker" data-live-search="true" data-size="5" name="faktur">
                            <option value="0">Non Faktur</option>
                            <option value="1">Faktur</option>
                          </select>
                          <label>Jenis Faktur</label>
                        </div>
                      </div>
                    </div>
                    <div class="col l12 m12 s12">
                      <div class="row">
                        <div class="input-field col l12 m12 s12">
                          <label>Gudang</label>
                        </div>
                      </div>
                      @foreach($gudang as $key => $value)
                        @if($key % 4 == 0)
                          <div class="row">
                            @endif
                            <div class="col l3 m6 s6">
                              <input type="checkbox" class="filled-in gudangcbox" id="gudang-{{$value->warehouse_id}}"/>
                              <label for="gudang-{{$value->warehouse_id}}">{{$value->warehouse_name}}</label>
                              <div class="input-field" hidden>
                                <input name="idgudang[]" type="text" class="f-input" value={{$value->warehouse_id}} hidden>
                                <input id="qty-gudang-{{$value->warehouse_id}}" name="qtygudang[]" type="text" class="f-input">
                                <label>Quantity for {{$value->warehouse_name}}</label>
                              </div>
                            </div>
                            @if($key % 4 == 3 || $key == sizeof($gudang)-1)
                          </div>
                        @endif
                      @endforeach
                    </div>
                    <div class="col l4 m12 s12">
                      <div class="row">
                        <div class="input-field col l12 m12 s12">
                          <div class="group-input-right">
                            <input id="weight" type="text" class="f-input" name="weight">
                            <label>Berat</label>
                            <label class="group-input-addon right">Gram</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col l4 m12 s12">
                      <div class="row">
                        <div class="input-field col l12 m4 s12">
                          <div class="group-input">
                            <input id="hargajual" type="text" class="f-input" placeholder="Harga Jual" name="hargajual">
                            <label>Harga Jual Eceran</label>
                            <label class="group-input-addon">Rp</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col l4 m12 s12">
                      <div class="row">
                        <div class="input-field col l12 m4 s12">
                          <div class="group-input">
                            <input id="hargajualpartai" type="text" class="f-input" placeholder="Harga Jual Partai" name="hargajualpartai">
                            <label>Harga Jual Partai</label>
                            <label class="group-input-addon">Rp</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="productchild" class="col l12" hidden>
                      <div class="row">
                        <div class="input-field col l6 m6 s12">
                          <label id="labelunitchild"></label>
                          <input id="unitchild" name="unitchild" hidden disabled>
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col l6 m6 s12">
                          <div class="group-input">
                            <input id="hargajualpartaichild" type="text" class="f-input" placeholder="Harga Jual Partai" name="hargajualpartaichild">
                            <label>Harga Jual Partai</label>
                            <label class="group-input-addon">Rp</label>
                          </div>
                        </div>
                        <div class="input-field col l6 m6 s12">
                          <div class="group-input">
                            <input id="hargajualchild" type="text" class="f-input" placeholder="Harga Jual" name="hargajualchild">
                            <label>Harga Jual Eceran</label>
                            <label class="group-input-addon">Rp</label>
                          </div>
                        </div>
                      </div>
                      <!-- <a href="#modal-input-quantity" class="btn-stoko btn-stoko-primary modal-trigger">Input Quantity</a>  -->
                    </div>
                  </div>
                  <div class="row">
                    <div class="col l12 m12 s12">
                      <button id="btn-submit" type="submit" class="btn-stoko btn-stoko-primary" mode="save">simpan</button>
                      <button class="btn-stoko orange reset">batal</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </li>
        </ul>
      </div>
    @endif
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Barang</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l3">
                  <input id="filterKode" type="text" class="f-input">
                  <label>Filter Kode</label>
                </div>
                <div class="input-field col l3">
                  <input id="filterNama" type="text" class="f-input">
                  <label>Filter Nama</label>
                </div>
                <div class="col l12 m12 s12">
                  <table id="barangTable" class="table table-bordered display nowrap dataTable dtr-inline">
                    <thead>
                    <tr>
                      <th>Kode</th>
                      <th>Nama</th>
                      <th>Jenis Faktur</th>
                      <th>HPP</th>
                      <th>Harga Jual Eceran</th>
                      <th>Harga Jual Partai</th>
                      <th>Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{--@foreach($product as $key => $value)--}}
                    {{--<tr value="{{$value->product_id}}">--}}
                    {{--<td>{{$value->product_code}}</td>--}}
                    {{--<td>{{$value->product_name}}</td>--}}
                    {{--<td>{{$value->is_faktur == 0 ? 'Faktur' : 'Non Faktur' }}</td>--}}
                    {{--<td>{{$value->hpp ? $value->hpp : 0 }}</td>--}}
                    {{--<td>{{$value->price_sale}}</td>--}}
                    {{--<td>{{$value->price_wholesale}}</td>--}}
                    {{--<td>--}}
                    {{--@if(Session('roles')->name == 'master')--}}
                    {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>--}}
                    {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                    {{-- <a href="#delete_data" class="btn btn-sm btn-raised orange darken-1"><i class="material-icons">print</i></a> --}}
                    {{--@endif--}}
                    {{--</td>--}}
                    {{--</tr>--}}
                    {{--@endforeach--}}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- Modal Delete Barang -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="row">
      <div class="col l12">
        <table id="modal-delete-data">

        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text delete">Hapus</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<!-- Modal Tambah Satuan -->
<div id="tambah_satuan" class="modal modal-satuan">
  <form>
    <div class="modal-content">
      <h5>Tambah Satuan</h5>
      <div class="row">
        <div class="input-field col l12 m12 s12">
          <input type="text" class="validate" placeholder="Cm / M / Inch / Lembar">
          <label>Satuan Barang</label>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action waves-effect btn-flat light-blue darken-2 white-text">tambah</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </div>
  </form>
</div>

<div id="modal-input-quantity" class="modal">
  <form id="form-list-barang">
    <div class="modal-content">
      <h4>Input Quantity</h4>
      <div class="row">
        <div class='col l4'></div>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text no-border-radius submit-barang">OK</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat orange white-text no-border-radius">batal</a>
    </div>
  </form>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        // $.ajax({
        //     url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
        //     dataType: "script",
        // });

        barangTable = $('#barangTable').DataTable({ // This is for home page
            searching: true,
            responsive: true,
            serverSide: true,
            'sDom': 'tip',
            'pagingType': 'numbers',
            ajax: {
                url : 'getBarangTable',
                data : function (d){
                    d.code = $('#filterKode').val();
                    d.name = $('#filterNama').val();
                }
            },
            rowId : 'product_id',
            columns: [
                { data: 'product_code', name: 'product_code'},
                { data: 'product_name', name: 'product_name'},
                { data: 'is_faktur', name: 'is_faktur'},
                { data: 'hpp', name: 'hpp'},
                { data: 'price_sale', name: 'price_sale'},
                { data: 'price_wholesale', name: 'price_wholesale'},
                { data: 'action', name:'action'},
            ],
            language: {
                "sProcessing":   "Sedang proses...",
                "sLengthMenu":   "Tampilan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Awal",
                    "sPrevious": "Balik",
                    "sNext":     "Lanjut",
                    "sLast":     "Akhir"
                }
            },
        });

        $('#filterKode').on('keyup', function () { // This is for news page
            barangTable.draw();
        });
        $('#filterNama').on('keyup', function () { // This is for news page
            barangTable.draw();
        });
        // $('#filterDiameter').on('keyup', function () { // This is for news page
        //   barangTable.column(3).search(this.value).draw();
        // });

        $('#filterNama').on('keyup', function () { // This is for news page
            if($(this).val() =="")
            {
                $('#btn-search').click();
            }
        });

        $('#btn-search').on('click', function () { // This is for news page
            var filter = $(this).closest('.row').find('#filterNama').val()
        });

        $('#formBarang').submit(function(event){
            event.preventDefault();
            var mode = $(this).find('#btn-submit').attr('mode');
            var kode = $(this).find('#kode').val();
            var id = $(this).find('#id').val();
            console.log(mode);

            var empty = required(['kode' ,'nama']);
            if(empty != '0')
            {
                toastr.warning(empty+' tidak boleh kosong!');
            }else{
                if (mode == 'save')
                {
                    $.ajax({
                        type:"POST",
                        url:"createBarang",
                        data: $("#formBarang").serialize(),
                        success:function(response){
                            toastr.success('Barang '+kode+' Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                        }
                    });
                }else if(mode == 'edit'){
                    var id = $(this).find('#id').val();
                    $.ajax({
                        type:"POST",
                        url:"updateBarang",
                        data: $("#formBarang").serialize()+"&id="+id,
                        success:function(response){
                            toastr.success('Barang '+kode+' Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                        }
                    });
                }
            }
        });

        $('body').on('click','#formBarang .reset', function(event){
            event.stopImmediatePropagation();
            $(this).closest('#formBarang').find("input[type=text], textarea").val("");
            $('#btn-submit').attr('mode', 'save');
        });

        $('body').on('change','#formBarang #satuan', function(event){
            event.stopImmediatePropagation();
            var id = $(this).val();

            $.ajax({
                type:"GET",
                url:"checkUnitChild",
                data: {id:id},
                success:function(response){
                    if(response != 0)
                    {
                        $('#productchild').removeAttr('hidden');
                        $('#labelunitchild').html('Harga Jual per '+ response.unit_child_name);
                        $('#unitchild').removeAttr('disabled').val(response.unit_child_id);
                    }else{
                        $('#productchild').attr('hidden',true);
                        $('#unitchild').attr('disabled',true).val('');
                    }
                }
            });
        });

        $('body').on('click','#barangTable .edit', function(event){
            event.stopImmediatePropagation();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                type:"GET",
                url:"getBarangData",
                data: {id:id},
                success:function(response){
                    $('#id').val(response.product.product_id);
                    $('#kode').val(response.product.product_code);
                    $('#nama').val(response.product.product_name);
                    $('#satuan').val(response.product.unit_id);
                    $('#diameter').val(response.product.diameter);
                    $('#faktur').val(response.product.is_faktur);
                    $('#weight').val(response.product.weight);
                    $('#hargajual').val(response.product.price_sale);
                    $('#hargajualpartai').val(response.product.price_wholesale);
                    $('#qty').val(response.product.total_quantity);
                    $.each(response.product_warehouse, function(i,v){
                        $('#gudang-'+v.warehouse_id).trigger('click');
                        $('#gudang-'+v.warehouse_id).closest('div').find('#qty-gudang-'+v.warehouse_id).val(v.quantity).attr('disabled',true);
                    });
                    if(response.product.unit.unit_child)
                    {
                        $('#productchild').removeAttr('hidden');
                        $('#labelunitchild').html('Harga Jual per '+ response.product.unit.unit_child.unit_child_name);
                        $('#unitchild').removeAttr('disabled').val(response.product.unit.unit_child.unit_child_id);
                        $('#hargajualpartaichild').val(response.product.product_child.price_wholesale);
                        $('#hargajualchild').val(response.product.product_child.price_sale);
                    }else{
                        $('#productchild').attr('hidden',true);
                        $('#unitchild').attr('disabled',true).val('');
                    }
                    $('.selectpicker').selectpicker('refresh');
                }
            });
            $('#btn-submit').attr('mode', 'edit');
        });

        $('body').on('click',"#barangTable .delete-modal",function(event){
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');

            $.ajax({
                type:"GET",
                url:"modalBarangData",
                async:false,
                data:{
                    id:id,
                },
                success:function(response){
                    $('#modal-delete-data').html(response);
                }
            });
            $('#delete_data').modal('open');
        });

        $('.gudangcbox').on('click', function(event)
        {
            if($(this).is(':checked'))
            {
                $(this).closest('div').find('.input-field').removeAttr('hidden');
            }else{
                $(this).closest('div').find('.input-field').attr('hidden',true);
            }
        });

        $(".delete").on('click',function(event){
            var id = $(this).closest('.modal').find('#id-delete').attr('value');
            console.log(id);
            $.ajax({
                type:"Get",
                url:"deleteBarang",
                data:{id:id},
                success:function(response){
                    toastr.success('Berhasil Menghapus Barang!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            });
        });

        $('#importBarang #submit').on('click', function(event){
            event.stopImmediatePropagation()
            event.preventDefault();

            var data = new FormData();
            jQuery.each(jQuery('#file')[0].files, function(i, file) {
                data.append('file', file);
            });

            $.ajax({
                url: 'importBarang',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function(data){
                    toastr.success("Success",{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            });
        });

        $('.selectpicker').selectpicker('render');

    })
</script>