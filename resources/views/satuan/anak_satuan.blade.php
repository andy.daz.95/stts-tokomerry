<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l4 m4 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Anak Satuan</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <form id="formAnakSatuan">
                <div class="row margin-top">
                  <div class="col l12">
                    <div class="row">
                      <div class="input-field col l12 m12 s12">
                        <input id="id-child" type="text" class="f-input" name="id_child" hidden>
                        <input id="kode-child" type="text" class="f-input" name="kode_child">
                        <label>Kode Anak Satuan</label>
                      </div>
                      <div class="input-field col l12 m12 s12">
                        <input id="satuan" name="satuan_child" type="text" class="f-input">
                        <label>Nama Anak Satuan</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col l12 m12 s12">
                    <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">simpan</button>
                    <a href="#" class="btn btn-sm btn-raised grey lighten-4 grey-text text-darken-4 reset">cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l8 m8 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Satuan</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l6">
                  <input id="filterSatuan" type="text" class="f-input">
                  <label>Filter Anak Satuan</label>
                </div>
                <div class="col l12 m12 s12">
                  <table id="satuanTable" class="table table-bordered display nowrap dataTable dtr-inline">
                    <thead>
                    <tr>
                      <th>Kode</th>
                      <th>Satuan</th>
                      <th>Satuan Atasan</th>
                      <th>Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($anaksatuan as $key => $value)
                      <tr value="{{$value->unit_child_id}}">
                        <td>{{$value->unit_child_code}}</td>
                        <td>{{$value->unit_child_name}}</td>
                        <td>{{$value->unit->unit_description}}</td>
                        <td>
                          <a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
                          <a href="#delete_data" class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<!-- Modal Delete Satuan -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="row">
      <div class="col l12">
        <table id="modal-delete-data">

        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <form>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat red white-text delete">Hapus</a>
      <a href="#!" class="modal-action modal-close waves-effect btn-flat">cancel</a>
    </form>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });

        satuanTable = $('#satuanTable').DataTable({ // This is for home page
            searching: true,
            responsive: true,
            'sDom': 'tip',
            'pagingType': 'numbers',
            language: {
                "sProcessing":   "Sedang proses...",
                "sLengthMenu":   "Tampilan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Awal",
                    "sPrevious": "Balik",
                    "sNext":     "Lanjut",
                    "sLast":     "Akhir"
                }
            },
        });

        $('#filterSatuan').on('keyup', function () { // This is for news page
            satuanTable.search($(this).val()).draw();
        });

        $('#formSatuan').submit(function(event){
            event.preventDefault();
            var mode = $(this).find('#btn-submit').attr('mode');
            var empty = required(['kode' ,'satuan']);
            if(empty != '0')
            {
                toastr.warning(empty+' tidak boleh kosong!');
            }else{
                if (mode == 'save')
                {
                    var id = $(this).find('#id').val();
                    $.ajax({
                        type:"POST",
                        url:"createSatuan",
                        data: $("#formSatuan").serialize(),
                        success:function(response){
                            toastr.success('Satuan Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                        }
                    });
                }else if(mode == 'edit'){
                    var id = $(this).find('#id').val();
                    $.ajax({
                        type:"POST",
                        url:"updateSatuan",
                        data: $("#formSatuan").serialize()+"&id="+id,
                        success:function(response){
                            toastr.success('Satuan Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                        }
                    });
                }
            }
        });

        $('body').on('click','#formSatuan .reset', function(event){
            event.stopImmediatePropagation();
            $(this).closest('#formSatuan').find("input[type=text], textarea").val("");
            $('#btn-submit').attr('mode', 'save');
        });

        $('body').on('click','#satuanTable .edit', function(event){
            event.stopImmediatePropagation();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                type:"GET",
                url:"getSatuanData",
                data: {id:id},
                success:function(response){
                    $('#id').val(response.unit_id);
                    $('#kode').val(response.unit_code);
                    $('#satuan').val(response.unit_description);
                }
            });
            $('#btn-submit').attr('mode', 'edit');
        });

        $('body').on('click',"#satuanTable .delete-modal",function(event){
            event.preventDefault();
            var id = $(this).closest('tr').attr('value');

            $.ajax({
                type:"GET",
                url:"modalSatuanData",
                async:false,
                data:{
                    id:id,
                },
                success:function(response){
                    $('#modal-delete-data').html(response);
                }
            });
            $('#delete_data').modal('open');
        });

        $(".delete").on('click',function(event){
            var id = $(this).closest('.modal').find('#id-delete').attr('value');
            console.log(id);
            $.ajax({
                type:"Get",
                url:"deleteSatuan",
                data:{id:id},
                success:function(response){
                    toastr.success('Berhasil Menghapus Satuan!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            });
        });
    })
</script>
