<style type="text/css">
table.table {
  border-collapse: collapse;
}
table.table, table.table th, table.table td{
 border: 1px solid black;
}
</style>
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">Nota Penerimaan Pembayaran</p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 25%">NO</td>
      <td style="width: 25%">: {{$paymentheader->payment_sales_number}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Tanggal</td>
      <td>: {{date('d-m-Y', strtotime($paymentheader->date_payment_sales))}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Customer</td>
      <td>: {{$paymentheader->first_name.' '.$paymentheader->last_name}}</td>
      <td></td>
    </tr>
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
      <tr style="background-color: #8B8C89">
        <th class="color" style="text-align: center;">Keterangan</th>
      </tr>
    </thead>
      <tr>
        <td>Pembayaran Dengan Nominal Rp. {{number_format($paymentheader->total_paid)}} </td>
      </tr>
    <tbody>
    </tbody>
  </table>
  
</div>
</div>