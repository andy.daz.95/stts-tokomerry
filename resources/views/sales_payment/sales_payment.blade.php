<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Pembayaran Sales</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row margin-top">
                <div class="input-field col l3">
                  <input id="filterPaymentNumber" type="text" class="f-input">
                  <label>Filter Purchase Payment No.</label>
                </div>
                <div class="input-field col l3">
                  <input type="text" id="filterPelanggan" class="f-input">
                  <label>Filter Pelanggan</label>
                </div>
                <div class="col l12 m12 s12 margin-top">
                  <div class="table-responsive">
                    <table id="salesPaymentTable" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                      <thead>
                      <tr>
                        <th>No Pembayaran</th>
                        <th>Pelanggan</th>
                        <th>Tgl Pembayaran</th>
                        <th>Jenis Pembayaran</th>
                        <th>Total</th>
                        <th>Tindakan</th>
                      </tr>
                      </thead>
                      <tbody>
                      {{--@foreach($data['payment'] as $key => $value)--}}
                      {{--<tr value="{{$value->payment_sales_id}}" mode="view">--}}
                      {{--<td class="nopayment">{{$value->payment_sales_number}}</td>--}}
                      {{--<td>{{$value->first_name.' '.$value->last_name}}</td>--}}
                      {{--<td>{{$value->date_payment_sales}}</td>--}}
                      {{--<td>--}}
                      {{--{{$value->payment_methods == 1 ? 'Tunai' : 'Transfer'}}--}}
                      {{--</td>--}}
                      {{--<td>{{$value->total_paid}}</td>--}}
                      {{--<td>--}}
                      {{--@if(Session('roles')->name == 'master')--}}
                      {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit" mode="edit" value="{{$value->payment_sales_id}}"><i class="material-icons">edit</i></a>--}}
                      {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                      {{--@endif--}}
                      {{--</td>--}}
                      {{--</tr>--}}
                      {{--@endforeach--}}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Formulir Pembayaran Sales</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <form id="formPayment">
                  <br/>
                  <div class="input-field col l3">
                    <label>No Pembayaran</label>
                    <input id="nopayment" type="text" name="nopayment" class="f-input" placeholder="No Pembayaran">
                    <input id="idpayment" type="text" name="idpayment" class="f-input" hidden>
                  </div>
                  <div class="input-field col l3">
                    <label>Sales Order</label>
                    <select id="invoice" class="browser-default selectpicker"  data-live-search="true" data-size="5" name="invoice">
                      <option value="0">Pilih Nota Penjualan</option>
                      @foreach($data['invoice'] as $key => $value)
                        <option value="{{$value->invoice_sales_id}}">{{$value->so->sales_order_number.' / '.$value->so->customer->first_name.' '.$value->so->customer->last_name.' / '.$value->so->date_sales_order}}</option>
                      @endforeach
                      @foreach($data['invoicetoday'] as $key => $value)
                        <option value="{{$value->invoice_sales_id}}">{{$value->so->sales_order_number.' / '.$value->so->customer->first_name.' '.$value->so->customer->last_name.' / '.$value->so->date_sales_order}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Customer</label>
                    <select id="customer" class="browser-default selectpicker"  data-live-search="true" data-size="5"   name="customer">
                      <option value="0">Pilih Customer</option>
                      @foreach($data['customer'] as $key => $value)
                        <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="input-field col l3">
                    <label>Tgl Pembayaran</label>
                    <input id="tglpayment" type="text" name="tglpayment" class="f-input" placeholder="No Pembayaran">
                  </div>
                  <div class="input-field col l3">
                    <label>Metode Pembayaran</label>
                    <select id="paymentmethod" class="browser-default selectpicker"  data-live-search="true" data-size="5" name="paymentmethod">
                      <option value="0" selected>Pilih metode Pembayaran</option>
                      @foreach($data['payment_method'] as $key => $value)
                        <option value="{{$value->payment_method_id}}">{{$value->payment_method_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div id="bank-div" class="input-field col l3" hidden>
                    <label>Bank</label>
                    <select id="bank" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="bank" disabled>
                      <option value="0">Pilih Bank</option>
                      @foreach($data['bank'] as $key => $value)
                        <option value="{{$value->bank_id}}">{{$value->bank_name}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                        <tr>
                          <th class="theader">Total Tagihan</th>
                          <th class="theader">Sisa Tagihan</th>
                          <th class="theader">Nominal Pembayaran</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id="no-item"><td colspan="3"><span> No Item Selected</span></td></tr>
                        <tr id="payment-row" hidden>
                          <td>
                            <div class="input-field">
                              <input id="total" name="total" type="text" class="f-input" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="remaining" name="remaining" type="text" class="f-input" disabled>
                            </div>
                          </td>
                          <td>
                            <div class="input-field">
                              <input id="pembayaran" type="text" class="f-input">
                              <input id="pembayaran-hidden" name="pembayaran" type="text" class="f-input" hidden>
                            </div>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                      <br>
                    </div>
                  </div>
                  <div class="col l12 s12 m12 margin-top">
                    <a id="submit-payment" href="#" class="btn-stoko teal white-text" mode="save">Simpan</a>
                    <a id="edit-payment" href="#" class="btn-stoko teal white-text" mode="edit" hidden>Edit</a>
                    <a id="clear" href="#" class="btn-stoko orange">batal</a>
                    <a id="cetak-payment" href="" target="_blank" class="btn-stoko btn-stoko-primary orange darken-4" hidden>Cetak Payment</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">search</i>Histori Pembayaran</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <table class="stoko-table no-border">
                  <thead>
                  <tr>
                    <th class="theader">No Pembayaran</th>
                    <th class="theader">Jumlah Pembayaran</th>
                  </tr>
                  </thead>
                  <tbody id="append-history">
                  <tr id="history-no-item"><td colspan="2"><span>Tidak Ada Histori</span></td></tr>
                  <tr class="history-row" hidden>
                    <td>
                      <div class="input-field">
                        <input type="text" class="f-input nopaymenthistory" disabled>
                      </div>
                    </td>
                    <td>
                      <div class="input-field">
                        <input type="text" class="f-input totalhistory" disabled>
                      </div>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<!-- Modal Delete Delivery Order -->
<div id="modal-delete-payment" class="modal">
  <div class="modal-content">
    <h5 id="delete-message"></h5>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a id="confirm-delete-payment" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
  </div>
</div>

<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        $.ajax({
            url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
            dataType: "script",
        });

        firstload();

        // $('#customer').on('change',function(event){
        //   event.stopImmediatePropagation();
        //   var id = $(this).val();
        //   $.ajax({
        //     type:"GET",
        //     url:"getcustomerforpayment",
        //     data:{id:id},
        //     success:function(response){
        //       if(response.length == 0){
        //         $('#payment-row').attr('hidden',true);
        //         $('#no-item').removeAttr('hidden');
        //       }else{
        //         $('#total').val(accounting.formatMoney(response.total_balance,'Rp ',2,',','.'));
        //         $('#payment-row').removeAttr('hidden');
        //         $('#no-item').attr('hidden',true);
        //       }
        //     }
        //   });
        // });

        $('#invoice').on('change',function(event){
            event.stopImmediatePropagation();
            var id = $(this).val();
            $.ajax({
                type:"GET",
                url:"getinvoiceforpayment",
                data:{id:id},
                success:function(response){
                    if(response.length == 0){
                        $('#payment-row').attr('hidden',true);
                        $('#no-item').removeAttr('hidden');
                    }else{
                        $('#customer').val(response.so.customer_id).attr('disabled',true);
                        $('#total').val(accounting.formatMoney(response.total_amount,'Rp ',2,',','.'));
                        if(response.totalpayments.length > 0){
                            $totalbayar = response.totalpayments[0].total;
                        }else{
                            $totalbayar = 0;
                        }
                        $('#remaining').val(accounting.formatMoney((parseInt(response.total_amount)- parseInt( $totalbayar )),'Rp ',2,',','.'));
                        $('#payment-row').removeAttr('hidden');
                        $('#no-item').attr('hidden',true);

                        if(response.payments.length == 0){
                            $('.history-row').attr('hidden',true);
                            $('#history-no-item').removeAttr('hidden');
                        }else{
                            var $original = $('.history-row:first');
                            $('.history-row').remove();
                            for(var i = 0; i < response.payments.length; i++){
                                var $cloned = $original.clone();
                                $cloned.find('.nopaymenthistory').val(response.payments[i].payment_sales_number);
                                $cloned.find('.totalhistory').val(accounting.formatMoney(response.payments[i].total_paid,'Rp. ',2,',','.'));
                                $cloned.appendTo('#append-history');
                            }
                            $('.history-row').removeAttr('hidden');
                            $('#history-no-item').attr('hidden',true);
                        }
                        $('.selectpicker').selectpicker('refresh');
                    }
                }
            });
        });

        $(document).on('click','#salesPaymentTable tr, #salesPaymentTable .edit', function(event){
            event.stopImmediatePropagation();
            var id = $(this).attr('value');
            var mode = $(event.currentTarget).attr('mode');
            $.ajax({
                type:"GET",
                url:"getsalespayment",
                data: {id:id},
                success:function(response){
                    $('#nopayment').val(response.payment_sales_number).attr('disabled',true);
                    $('#idpayment').val(response.payment_sales_id);
                    $('#customer').val(response.customer_id).attr('disabled',true);
                    $('#paymentmethod').val(response.payment_methods).attr('disabled',true);
                    if(response.bank_id != null)
                    {
                        $('#bank-div').removeAttr('hidden');
                        $('#bank').val(response.bank_id).attr('disabled',true);
                    }else{
                        $('#bank-div').attr('hidden',true);
                    }
                    $('#total').val(accounting.formatMoney(response.total_balance,'Rp. ',2,'.','.'));
                    $('#pembayaran').val(accounting.formatMoney(response.total_paid,'Rp. ',2,',','.')).attr('disabled',true);
                    $('#pembayaran-hidden').val(response.total_paid).attr('disabled',true);
                    $('#payment-row').removeAttr('hidden');
                    $('#no-item').attr('hidden',true);
                    $('#tglpayment').attr('disabled',true);
                },complete:function(){
                    if(mode=="edit"){
                        $('#submit-payment').attr('hidden',true);
                        $('#edit-payment').removeAttr('hidden');
                        $('#pembayaran, #pembayaran-hidden, #tglpayment').removeAttr('disabled');
                    }else{
                        $('#cetak-payment').removeAttr('hidden').attr('href',"downloadsalespayment/"+id);
                    }
                    $('.selectpicker').selectpicker('refresh');
                }
            });
        });

        $("#submit-payment, #edit-payment").on('click',function(event){
            event.stopImmediatePropagation();
            event.preventDefault();

            var me = $(this);
            // prevent multiple request at once
            if ( me.data('requestRunning') ) {
                return;
            }
            //
            var kode = $('#nopayment').val();
            var temp = $('#pembayaran-hidden').val();
            var sisa = accounting.unformat($('#remaining').val());
            var bayar = $('#pembayaran-hidden').val();
            $('#pembayaran-hidden').val(Math.abs(temp));

            if($(event.currentTarget).attr('mode') == 'save')
            {
                var url = "createpayment";
                var successmessage = 'Sales Payment '+kode+' telah berhasil dibuat!';
            }else{
                var url = "updatepayment";
                var successmessage = 'Sales Payment '+kode+' telah berhasil diubah!';
            }

            if($('#payment-row').attr('hidden') == "hidden")
            {
                toastr.warning('Anda Belum Memilih Customer dengan Benar');
            }else if($('#paymentmethod').val() == 0){
                toastr.warning('Anda Belum Memilih Metode Pembayaran');
            }else if($('#paymentmethod').val() == 0){
                toastr.warning('Anda Belum Memilih Metode Pembayaran');
            }else if(bayar > sisa){
                toastr.warning('Total Bayar Melebihi Sisa Bayar');
            }else{
                me.invisible();
                me.data('requestRunning', true);
                $('#customer, #total, #pembayaran').removeAttr('disabled');
                $.ajax({
                    type:"POST",
                    url:url,
                    data: $('#formPayment').serialize(),
                    success:function(response){
                        toastr.success(successmessage,{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                    },
                    complete: function() {
                        me.data('requestRunning', false);
                    }
                });
            }
        });

        $('#clear').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click()
        });

        $('#pembayaran').on('focusout', function(){
            $('#pembayaran-hidden').val($(this).val());
            $(this).val(accounting.formatMoney($(this).val(),'Rp ',2,',','.'))
        });

        $('#pembayaran').on('focusin', function(){
            $(this).val("");
        });

        $(document).on('click', '#salesPaymentTable .delete-modal', function(event){
            event.stopImmediatePropagation();
            var nopayment = $(this).closest('tr').find('.nopayment').html();
            var idpayment = $(this).closest('tr').attr('value');
            $('#confirm-delete-payment').attr('value',idpayment).attr('nomor',nopayment);
            $("#delete-message").html("Yakin ingin menghapus data "+nopayment+" ?")
            $('#modal-delete-payment').modal('open');
        });

        $('#confirm-delete-payment').on('click', function(event){
            event.stopImmediatePropagation();
            event.preventDefault();
            $('#modal-delete-payment').modal('close');
            var id = $(this).attr('value');
            var nopayment = $(this).attr('nomor');
            $.ajax({
                type:"POST",
                url:"deletepayment",
                data:{id:id},
                success:function(response){
                    toastr.success('Sales Payment '+nopayment+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            })
        });

        $('#paymentmethod').on('change', function(){
            if($(this).val() == 1){
                $('#bank').attr('disabled',true);
                $('#bank-div').attr('hidden',true);
                $('.selectpicker').selectpicker('refresh');
            }else{
                $('#bank-div').removeAttr('hidden');
                $('#bank').removeAttr('disabled');
                $('.selectpicker').selectpicker('refresh');
            }
        })

        //function
        function firstload(){

            $('#tglpayment').datepicker({
                dateFormat : 'dd-mm-yyyy',
                maxDate: new Date(),
                language: 'en',
            });

            $('#tglpayment').val(moment().format('DD-MM-YYYY'));

            $('.selectpicker').selectpicker('render');
            $.ajax({
                type:"GET",
                url:"lastsalespaymentnumber",
                success:function(response){
                    $('#nopayment').val(response);
                }
            });

            var salesPaymentTable = $('#salesPaymentTable').DataTable({ // This is for home page
                searching: true,
                processing: true,
                serverSide: true,
                "aaSorting": [],
                ajax: {
                    url : 'getsalespaymenttable',
                    data : function (d){
                        d.nopayment = $('#filterPaymentNumber').val();
                        d.customer = $('#filterPelanggan').val();
                    }
                },
                rowId : 'payment_sales_id',
                columns: [
                    { data: 'payment_sales_number', name: 'payment_sales_number', class:'nopayment'},
                    { data: 'customer_name', name: 'customer_name'},
                    { data: 'date_payment_sales', name: 'date_payment_sales'},
                    { data: 'payment_method_name', name: 'payment_method_name'},
                    { data: 'total_paid', name: 'total_paid'},
                    { data: 'action', name: 'action'},
                ],
                language: {
                    "sProcessing": "Sedang proses...",
                    "sLengthMenu": "Tampilan _MENU_ entri",
                    "sZeroRecords": "Tidak ditemukan data yang sesuai",
                    "sInfo": "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty": "Tampilan 0 hingga 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix": "",
                    "sSearch": "Cari:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Awal",
                        "sPrevious": "Balik",
                        "sNext": "Lanjut",
                        "sLast": "Akhir"
                    }
                }
            });

            $('#filterPaymentNumber').on('keyup', function () { // This is for news page
                salesPaymentTable.column(0).search(this.value).draw();
            });
            $('#filterPelanggan').on('keyup', function () { // This is for news page
                salesPaymentTable.column(1).search(this.value).draw();
            });
        }
    });
</script>