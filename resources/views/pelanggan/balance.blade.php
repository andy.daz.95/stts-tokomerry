<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Saldo Pelanggan</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <div class="col l12 m12 s12 margin-top">
                  <div class="col l12 m12 s12">
                    <div class="input-field col l3 m12 s12 no-padding">
                      <select id="customer-select" name="customer" class="browser-default selectpicker" data-live-search="true" data-size="5">
                        <option value='0'>Select Customer</option>
                        @foreach($data['customer'] as $key => $value)
                        <option value="{{$value->customer_id}}">{{$value->first_name.' '.$value->last_name}}</option>
                        @endforeach
                      </select>
                      <label style="left:0px">Customer</label>
                    </div>
                    <div class="input-field col l3">
                      <label>Total Balance</label>
                      <input id="balance" type="text" class="f-input" name="balance" value="0" disabled>
                    </div>
                  </div>
                  <div class="col l12 m12 s12">
                    <div class="table-responsive">
                      <table id="balanceTable" style="width: 100%" class="highlight table table-bordered display nowrap dataTable dtr-inline">
                        <thead>
                          <tr>
                            <th class="theader">Tgl</th>
                            <th class="theader">Description</th>
                            <th class="theader">Debit</th>
                            <th class="theader">Kredit</th>
                          </tr>
                        </thead>
                        <tbody id="detail-table">

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Detail Transaksi</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <form id="formSo">
                  <div class="input-field col l3">
                    <label>No Sales Order</label>
                    <input id="nosalesorder" type="text" class="f-input" name="nosalesorder" disabled>
                    <input id="idsalesorder" type="text" class="f-input" name="idso" disabled hidden>
                  </div>
                  <div class="input-field col l3">
                    <label>No Sales Order</label>
                    <input id="nosalesinovice" type="text" class="f-input" name="nosalesinvoice" disabled>
                  </div>
                  <div class="input-field col l3">
                    <label>Pembayaran</label>
                    <input id="pembayaran" class="f-input" name="pembayaran" disabled>
                  </div>
                  <div class="input-field col l3">
                    <label>Terms</label>
                    <input id="terms" type="text" class="f-input" placeholder="Terms" name="terms" disabled>
                  </div>
                  <div class="input-field col l3">
                    <label>Tgl SO</label>
                    <input id="tglso" type="text" class="f-input datepicker" name="tglso" placeholder="Tanggal SO" disabled>
                  </div>
                  <div class="input-field col l3">
                    <label>Notes</label>
                    <input id="notes" type="text" class="f-input datepicker" name="notes" disabled>
                  </div>
                  <div class="col l12 m12 s12 margin-top">
                    <div class="table-responsive">
                      <table class="stoko-table no-border">
                        <thead>
                          <tr>
                            <th class="theader" style="width: 300px">Nama</th>
                            <th class="theader" style="width: 75px">Qty</th>
                            <th class="theader" style="width: 75px">Weight</th>
                            <th class="theader">Harga</th>
                            <th class="theader">Sub Total</th>
                            <th class="theader" style="width: 125px">Tindakan</th>
                          </tr>
                        </thead>
                        <tbody id="barang-data">
                          <tr id="no-item"><td colspan="6"><span> No Item Selected</span></td></tr>
                          <tr class="barang-row" hidden>
                            <td>
                              <div class="input-field">
                                <input id="id-1" type="text" class="f-input" name="idbarang[]" hidden>
                                <input id="nama-1" type="text" class="f-input">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="qty-1" type="text" class="f-input qty" name="qty[]">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="weight-1" type="text" class="f-input weight" name="weight[]">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="harga-1" type="text" class="f-input harga" name="harga[]">
                              </div>
                            </td>
                            <td>
                              <div class="input-field">
                                <input id="subtotal-1" type="text" class="f-input subtotal" name="subtotal[]">
                              </div>
                            </td>
                            <td>
                              <a href="#" class="btn btn-sm white grey-text"><i class="material-icons">delete</i></a>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                        <br>
                        <div class="input-field" class="">
                          <input id="grandtotal" type="text" class="f-input" disabled>
                          <input id="grandtotal-hidden" type="text" class="f-input" name="grandtotal" hidden>
                          <label>Grand Total</label>
                        </div>
                        <div class="input-field" class="">
                          <input id="reduction" type="text" class="f-input" disabled>
                          <label>Potong Invoice</label>
                        </div>
                        <div class="input-field" class="">
                          <input id="invoice" type="text" class="f-input" disabled>
                          <label>Total Invoice</label>
                        </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.3/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });

    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });
     $.ajax({
      url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
      dataType: "script",
    });

    $('.selectpicker').selectpicker('render');

    $('body').on('click', '#detail-table tr' ,function(event){
      event.stopImmediatePropagation();
      var id = $(this).attr('value');
      if(id != "")
      {
        $.ajax({
          type:"GET",
          url:"gettransactionforbalance",
          data:{id:id}, //id invoice
          success:function(response){
            console.log(response);  
            var $original = $('.barang-row:first');
            var $cloned = $original.clone();
            $('.barang-row').remove();
            if(response.length > 0)
            {
              $('#idsalesorder').val(response[0].sales_order_id).attr('disabled',true);
              $('#nosalesinovice').val(response[0].invoice_sales_number).attr('disabled',true);
              $('#nosalesorder').val(response[0].sales_order_number).attr('disabled',true);
              $('#pelanggan').val(response[0].customer_id).attr('disabled',true);
              $('#terms').val(response[0].term_payment).attr('disabled',true);
              $('#pembayaran').val(response[0].payment_description).attr('disabled',true);
              $('#tglso').val(response[0].date_sales_order).attr('disabled',true);
              for(var i=0; i<response.length; i++){
                var newid = "id-"+(i+1);
                var newnama = "nama-"+(i+1);
                var newqty = "qty-"+(i+1);
                var newweight = "weight-"+(i+1);
                var newharga = "harga-"+(i+1);
                var newsubtotal = "subtotal-"+(i+1);
                $temp = $original.clone();
                $temp.removeAttr('hidden');
                $temp.find('#id-1').attr('id',newid);
                $temp.find('#nama-1').attr('id',newnama);
                $temp.find('#qty-1').attr('id',newqty);
                $temp.find('#weight-1').attr('id',newweight);
                $temp.find('#harga-1').attr('id',newharga);
                $temp.find('#subtotal-1').attr('id',newsubtotal);
                $temp.appendTo('#barang-data')
                $('#id-'+(i+1)).val(response[i].product_id);
                $('#subtotal-'+(i+1)).val(response[i].sub_total).attr('disabled',true);
                $('#nama-'+(i+1)).val(response[i].p_name).attr('disabled',true);
                $('#qty-'+(i+1)).val(response[i].quantity).attr('disabled',true);
                $('#weight-'+(i+1)).val(response[i].weight).attr('disabled',true);
                $('#harga-'+(i+1)).val(response[i].price_sale).attr('disabled',true);
                $('#no-item').attr('hidden',true);
              }
              $('#tambah-barang').attr('disabled',true).css('cursor','not-allowed');
              $('.selectpicker').selectpicker('refresh');
              $('#grandtotal').val(response[0].grand_total_idr);
              $('#reduction').val(response[0].reduction);
              $('#invoice').val(response[0].total_amount);
            }else{
              $original.clone();
              $original.attr('hidden',true)
              $original.appendTo('#barang-data');
              $('#formSo').find("input[type=text], textarea").val("");
            }
          },
          complete:function(){
            // var sum = 0;
            // $('.subtotal').each(function(){
            //   if ($(this).val() > 0) 
            //   {
            //     sum += parseFloat($(this).val())
            //   }
            // });
            // $('#grandtotal').val(accounting.formatMoney(sum,'Rp. ',2,',','.'));
            // $('#grandtotal-hidden').val(sum);
            $('#submit-so').attr('hidden',true);
          }
        });
      }
      
    });

    $('#customer-select').on('change', function(event){
      event.stopImmediatePropagation();
      var id = $(this).val();
      if (id == 0) {
        return;
      }
      $.ajax({
        method : "GET",
        url: 'detail-balance',
        data: {id:id},
        success: function(response){
          $('#detail-table').html(response.view);
          /*$('#balance').val(response.totalbalance);*/
          $('#balance').val(accounting.formatMoney(response.totalbalance,'Rp. ',2,',','.'));
        },
        complete: function(){
          var totalcredit = 0;
          var totaldebit = 0;
          $('.credit').each(function(){
            if ($(this).html() =='-') {
              totalcredit += 0;  
            }else{
              totalcredit += parseInt($(this).html());
            }
          })
          $('.debit').each(function(){
            if ($(this).html() =='-') {
              totaldebit += 0;  
            }else{
              totaldebit += parseInt($(this).html());
            }
          })
          // $(this).val(accounting.formatMoney(totaldebit - totalcredit,'Rp. ',2,',','.'))
          $('#column-total-balance').html(totaldebit - totalcredit);
          formatnumber();
        }  
      });           
    });

    function formatnumber(){
      $('.number').each(function(){
        if($(this).html() != '-')
        {
            $(this).html(accounting.formatMoney($(this).html(),'Rp. ',2,',','.'));
        }
      });
    }
  });
</script>