<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<div class="row">
  <div class="col l6 m12 s12">
    <ul class="collapsible" data-collapsible="accordion">
      <li>
        <div class="collapsible-header grey darken-3 white-text active"><i class="material-icons">create</i>Form Pelanggan</div>
        <div class="collapsible-body">
          <div class="container-fluid" style="padding-top: 15px">
            <form id="formPelanggan">
              <div class="row">
                <div class="col l6">
                  <div class="row">
                    <div class="input-field col l12 m12 s12">
                      <input id="kode" type="text" class="f-input" placeholder="Kode" name="kode" disabled>
                      <label>Kode</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="npwp" type="text" class="f-input" placeholder="NPWP" name="npwp">
                      <label>NPWP</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="namadepan" placeholder="Nama Depan" type="text" class="f-input" name="namadepan">
                      <label>Nama Depan</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="namabelakang" type="text" class="f-input" placeholder="Nama Belakang" name="namabelakang">
                      <label>Nama Belakang</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <row>
                        <div class="group-input">
                          <label>Limit Kredit</label>
                          <input id="limit" class="f-input" type="text" name="limit">
                          <label class="group-input-addon">Rp</label>
                        </div>
                      </row>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <select id="kota" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="kota">
                        <option value="">Pilih Kota</option>
                      </select>
                      <label>Kota</label>
                    </div>
                  </div>
                </div>
                <div class="col l6">
                  <div class="row">
                    <div class="input-field col l12 m12 s12">
                      <input id="perusahaan" type="text" class="f-input" placeholder="Perusahaan" name="perusahaan">
                      <label>Perusahaan</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="norek" type="text" class="f-input" placeholder="No Rekening" name="norek">
                      <label>No Rekening</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="atasnama" type="text" class="f-input" placeholder="Atas Nama" name="atasnama">
                      <label>Atas Nama</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <select id="bank" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="bank">
                        <option value="">Pilih Bank</option>
                        @foreach($bank as $key => $value)
                          <option value="{{$value->bank_id}}">{{$value->bank_name}}</option>
                        @endforeach
                      </select>
                      <label>Bank</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <select id="provinsi" class="browser-default selectpicker" data-live-search="true" data-size="5"   name="provinsi">
                        <option value="">Pilih Provinsi</option>
                        @foreach($provinsi as $key => $value)
                          <option value="{{$value->id}}">{{$value->name}}</option>
                        @endforeach
                      </select>
                      <label>Provinsi</label>
                    </div>
                    <div class="input-field col l12 m12 s12">
                      <input id="kodepos" type="text" class="f-input" placeholder="Kode Pos" name="kodepos">
                      <label>Kode Pos</label>
                    </div>
                  </div>
                </div>
                <div class="col l12 m12 s12">
                  <div class="row">
                    <div class="input-field col l12 m12 s12">
                      <textarea id="alamat" class="f-txt" placeholder="Alamat" name="alamat"></textarea>
                      <label>Alamat</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                      <input id="telp1" type="text" class="f-input" placeholder="Telp 1" name="telp1">
                      <label>Telp 1</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                      <input id="telp2" type="text" class="f-input" placeholder="Telp 2" name="telp2">
                      <label>Telp 2</label>
                    </div>
                    <div class="input-field col l4 m4 s12">
                      <input id="fax" type="text" class="f-input" placeholder="Fax" name="fax">
                      <label>Fax</label>
                    </div>
                  </div>
                  <div class="row">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col l12 m12 s12">
                  <button id="btn-submit" type="submit" class="btn btn-raised btn-sm light-blue darken-2" mode="save">Simpan</button>
                  <button id="batal" type="button" class="btn btn-raised btn-sm orange">Batal</button>
                </div>
              </div>
              {{csrf_field()}}
            </form>
          </div>
        </div>
      </li>
    </ul>
  </div>
   {{--<div class="col l6 m12 s12">--}}
    {{--<ul class="collapsible" data-collapsible="accordion">--}}
      {{--<li>--}}
        {{--<div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Import Pelanggan</div>--}}
        {{--<div class="collapsible-body">--}}
          {{--<div class="container-fluid">--}}
            {{--<div class="row margin-top">--}}
              {{--<form id="importPelanggan" enctype="multipart/form-data">--}}
                {{--<div class="col l10">--}}
                  {{--<div class="input-field">--}}
                    {{--<input id="file" type="file" name="file" class="f-input">--}}
                  {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-l2">--}}
                  {{--<button id="submit" type="button" class="btn btn-raised light-blue darken-2">save</button>--}}
                {{--</div>--}}
              {{--</form>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
      {{--</li>--}}
    {{--</ul>--}}
  {{--</div>--}}
  <div class="col l6 m12 s12">
    <ul class="collapsible" data-collapsible="accordion">
      <li>
        <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>Cari Pelanggan</div>
        <div class="collapsible-body">
          <div class="container-fluid">
            <div class="row">
              <div class="input-field col l6">
                <input id="filterNama" type="text" class="f-input" placeholder="Cari">
                <label>Filter Nama</label>
              </div>
              <div class="col l12 m12 s12">
                <table id="pelangganTable" class="table table-bordered display nowrap dataTable dtr-inline">
                  <thead>
                  <tr>
                    <th>Pelanggan</th>
                    <th>Tindakan</th>
                  </tr>
                  </thead>
                  <tbody>
                  {{--@foreach($customer as $row => $value)--}}
                  {{--<tr value="{{$value->customer_id}}">--}}
                  {{--<td>{{$value->first_name.' '.$value->last_name}}</td>--}}
                  {{--<td>--}}
                  {{--<a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>--}}
                  {{--<a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>--}}
                  {{--</td>--}}
                  {{--</tr>--}}
                  {{--@endforeach--}}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
<!-- Modal Delete Pelanggan -->
<div id="delete_data" class="modal">
  <div class="modal-content">
    <h5>Yakin ingin menghapus data berikut?</h5>
    <div class="col l12">
      <div class="col l6">
        <table id="modal-delete-data">

        </table>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
    <a href="#!" class="modal-action modal-close waves-effect btn-stoko red white-text delete">hapus</a>
  </div>
</div>
<script type="text/javascript" src="js/toastr.js"></script>
<script type="text/javascript" src="js/validation.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-submit').attr('mode', 'save');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: 'js/materialize.min.js',
            dataType: "script",
        });
        $.ajax({
            url: 'js/custom.js',
            dataType: "script",
        });

        // $.ajax({
        //     url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
        //     dataType: "script",
        // });

        pelangganTable = $('#pelangganTable').DataTable({ // This is for home page
            searching: true,
            responsive: true,
            serverSide: true,
            'sDom': 'tip',
            'pagingType': 'numbers',
            ajax: {
                url : 'getPelangganTable',
                data : function (d){
                    d.customer = $('filterNama').val();
                }
            },
            rowId : 'sales_order_id',
            columns: [
                { data: 'fullname', name: 'fullname'},
                { data: 'action', name:'action'},
            ],
            language: {
                "sProcessing":   "Sedang proses...",
                "sLengthMenu":   "Tampilan _MENU_ entri",
                "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                "sInfo":         "Tampilan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty":    "Tampilan 0 hingga 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix":  "",
                "sSearch":       "Cari:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst":    "Awal",
                    "sPrevious": "Balik",
                    "sNext":     "Lanjut",
                    "sLast":     "Akhir"
                }
            },
        });

        $('#filterNama').on('keyup', function () { // This is for news page
            pelangganTable.column(0).search(this.value).draw();
        });

        $('#provinsi').on('change',function(){
            var provinsi = $('#provinsi option:selected').val();
            getCity(provinsi);
        });

        $('#formPelanggan').submit(function(event){
            event.preventDefault();
            var mode = $(this).find('#btn-submit').attr('mode');
            var empty = required(['namadepan', 'namabelakang','alamat','telp1','provinsi','kota']);
            var validangka = cekmanynumbers(['npwp','telp1','telp2','norek','limit','kodepos','fax']);
            var validbank = 0;
            if($('#bank').val() != "")
            {
                var validbank = required(['atasnama','bank','norek']);
            }

            if(empty != '0')
            {
                toastr.warning(empty+' tidak boleh kosong!');
            }else if(validangka != '0'){
                toastr.warning(validangka+' harus angka');
            }else if(validbank != '0'){
                toastr.warning(validbank+' tidak boleh kosong');
            }
            else{
                if (mode == 'save')
                {
                    $.ajax({
                        type:"POST",
                        url:"createPelanggan",
                        data: $("#formPelanggan").serialize(),
                        success:function(response){
                            toastr.success('Pelanggan Telah Berhasil Dibuat!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                        }
                    });
                }else if(mode == 'edit'){
                    var id = $(this).find('#kode').val();
                    $.ajax({
                        type:"POST",
                        url:"updatePelanggan",
                        data: $("#formPelanggan").serialize()+"&id="+id,
                        success:function(response){
                            toastr.success('Pelanggan Telah Berhasil Diubah!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                        }
                    });
                }
            }
        });



        $('body').on('click','#formPelanggan .reset', function(event){
            event.stopImmediatePropagation();
            $(this).closest('#formPelanggan').find("input[type=text], textarea").val("");
            $('#btn-submit').attr('mode', 'save');
        });

        $('body').on('click','#pelangganTable .edit', function(event){
            event.stopImmediatePropagation();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                type:"GET",
                url:"getPelangganData",
                data: {id:id},
                success:function(response){
                    $('#provinsi').val(response.customer.provinsi);
                    $('#kode').val(response.customer.customer_id);
                    $('#npwp').val(response.customer.NPWP);
                    $('#namadepan').val(response.customer.first_name);
                    $('#namabelakang').val(response.customer.last_name);
                    $('#telp1').val(response.customer.phone);
                    $('#telp2').val(response.customer.phone2);
                    $('#fax').val(response.customer.fax);
                    $('#limit').val(response.customer.credit_limit);
                    $('#kodepos').val(response.customer.postal_code);
                    $('#perusahaan').val(response.customer.company_name);
                    $('#alamat').val(response.customer.address);
                    if(typeof response.bank != "undefined" && response.bank != null && response.bank > 0)
                    {
                        $('#bank').val(response.bank.bank_id);
                        $('#atasnama').val(response.bank.on_behalf_of);
                        $('#norek').val(response.bank.bank_account);
                    }
                    $('.selectpicker').selectpicker('refresh');

                    getCity(response.customer.provinsi, response.customer.city_id);
                }
            });
            $('#btn-submit').attr('mode', 'edit');
        });

        $('#importPelanggan #submit').on('click', function(event){
            event.stopImmediatePropagation()
            event.preventDefault();

            var data = new FormData();
            jQuery.each(jQuery('#file')[0].files, function(i, file) {
                data.append('file', file);
            });

            $.ajax({
                url: 'importPelanggan',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                success: function(data){
                    console.log(data);
                    toastr.success("Success",{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            });
        });

        $('body').on('click',"#pelangganTable .delete-modal",function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            var id = $(this).closest('tr').attr('value');
            $.ajax({
                type:"GET",
                url:"modalPelangganData",
                async:false,
                data:{
                    id:id,
                },
                success:function(response){
                    $('#modal-delete-data').html(response);
                }
            });
            $('#delete_data').modal('open');
        });

        $(".delete").on('click',function(event){
            var id = $(this).closest('.modal').find('#id-delete').attr('value');
            console.log(id);
            $.ajax({
                type:"Get",
                url:"deletePelanggan",
                data:{id:id},
                success:function(response){
                    toastr.success('Berhasil Menghapus Pelanggan!',{"onShow":setTimeout(function(){$('.side-nav .active a').click();}, 2600)});
                }
            });
        });

        $('#batal').on('click', function(event){
            event.preventDefault();
            event.stopImmediatePropagation();
            $('.side-nav .active a').click();
        });

        $('.selectpicker').selectpicker('render');

        //function
        function getCity(provinsi,city)
        {
            city = city || 0;
            $.ajax({
                type:"GET",
                url:"cities",
                data:{
                    provinsi:provinsi, city:city,
                },
                success:function(response){
                    $('#kota').html(response);
                },
                complete:function(){
                    $('.selectpicker').selectpicker('refresh');
                }
            });
        }
    });
</script>
