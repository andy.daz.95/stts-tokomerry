@foreach($balance as $key => $value)
  <tr  @if($value->source == 'invoice') value="{{$value->transaction_id}}" @else value="" @endif>
    <td>{{date('d-m-Y', strtotime($value->date))}}</td>
    <td>{{$value->description}}</td>
    @if($value->source == 'invoice')
    <td class="debit number">-</td>
    <td class="credit number">{{$value->total_amount}}</td>
    @else
    <td class="debit number">{{$value->total_amount}}</td>
    <td class="credit number">-</td>
    @endif
  </tr>
@endforeach
<tr>
  <td colspan="3">Total Balance</td>
  <td id="column-total-balance" class="number">0</td>
</tr>