<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sistem Toko</title>
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Styles -->
  <link rel="stylesheet" href="css/materialize.css">
  <link rel="stylesheet" href="css/master.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<div class="login">
  <div class="container">
    <div class="row">
      <div class="col l6 offset-l3">
        <div class="login-card">
          <div class="card-title center">
            <h3>Sign In</h3>
          </div>
          <div class="container">
            <div class="row">
              <form class="form-horizontal" method="POST" action="{{ route('auth') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label for="email" class="col-md-4 control-label">E-Mail</label>

                  <div class="col-md-6">
                    <input id="email" type="email" class="form-control f-input" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                      <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label for="password" class="col-md-4 control-label">Kata Sandi</label>

                  <div class="col-md-6">
                    <input id="password" type="password" class="form-control f-input" name="password" required>

                    @if ($errors->has('password'))
                      <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group" hidden>
                  <label for="access" class="col-md-4 control-label">Access</label>
                  <div class="col-md-6">
                    <input id="access" type="text" class="form-control f-input" name="access">
                  </div>
                </div>

                {{--<div class="form-group" hidden>--}}
                  {{--<label for="ip" class="col-md-4 control-label">Private IP</label>--}}

                  {{--<div class="col-md-6">--}}
                    {{--<input id="ip" type="text" class="form-control f-input" name="ip" required>--}}
                  {{--</div>--}}
                {{--</div>--}}

                <div class="form-group">
                  <div class="col l4">
                    <button id="login" type="submit" class="btn btn-primary">Login</button>
                  </div>
                  <div class="col l8">
                    <a class="btn btn-link" style="" href="">
                      Lupa Kata Sandi Anda?
                    </a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal Structure -->
<div id="password" class="modal modal-lupa-password">
  <form>
    <div class="modal-content">
      <h5 class="title-forgot-password">Forgot Password</h5>
      <div class="row">
        <div class="col l12 m12 s12">
          <div class="input-field col l12 m12 s12">
            <input type="password" class="validate" placeholder="New Password">
          </div>
          <div class="input-field col l12 m12 s12">
            <input type="password" class="validate" placeholder="Re-Password">
          </div>
          <div class="col l12 m12 s12">
            <a href="#!" class="waves-effect btn btn-raised grey lighten-4 grey-text text-darken-2">change password</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
<script src="{{ asset('js/app.js') }}"></script>
<script src="js/jquery.min.js"></script>
<script src="js/materialize.min.js"></script>
<script src="js/custom.js"></script>
<script>
    $("#access").val(localStorage.getItem("access_sistemtoko"));
</script>
{{--<script type="text/javascript">--}}
    {{--window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;   //compatibility for firefox and chrome--}}
    {{--var pc = new RTCPeerConnection({iceServers:[]}), noop = function(){};--}}
    {{--pc.createDataChannel("");    //create a bogus data channel--}}
    {{--pc.createOffer(pc.setLocalDescription.bind(pc), noop);    // create offer and set local description--}}
    {{--pc.onicecandidate = function(ice){  //listen for candidate events--}}
        {{--if(!ice || !ice.candidate || !ice.candidate.candidate)  return;--}}
        {{--var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];--}}
        {{--console.log('my IP: ', myIP);--}}
        {{--$('#ip').val(myIP);--}}
        {{--pc.onicecandidate = noop;--}}
    {{--};--}}
    {{--$('#login').removeAttr('disabled');--}}
{{--</script>--}}
</body>
</html>