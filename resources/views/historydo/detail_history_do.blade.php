<table id="appendhistorydo" class="table-responsive">
  <thead>
  <tr>
    <th>Nama Barang</th>
    <th>Qty</th>
    <th>Unit</th>
    @foreach($do as $key => $value)
      <th>
        {!!  $value->delivery_order_number .'</br>'.
        $value->driver_name .'</br>'.
        $value->vehicle_number .'</br>'.
        $value->warehouse->warehouse_name.'</br>'
       !!}
      </th>
    @endforeach
    <th>Quantity Sisa</th>
  </tr>
  </thead>
  <tbody>
    @foreach($suratantar->details as $key => $value)
     <tr>
      <td>{{$value->product->product_name}}</td>
      <td>{{$value->quantity}}</td>
      <td>
        @if($value->unit_type == 'main')
          {{$value->unit->unit_description}}
        @elseif($value->unit_type == 'child')
          {{$value->unit_child->unit_child_name}}
        @endif
      </td>
       @php
        $productid = $value->product_id;
        $qtyorder = $value->quantity;
        $qtykirim = 0;
       @endphp
       @foreach($do as $key => $value)
         @php
           $qty = $value->details->where('product_id',$productid)->first()['quantity_sent'];
            $qty = $qty > 0 ? $qty : 0;
          $qtykirim = $qtykirim + $qty;
         @endphp
         <th>{{$qty}}</th>
       @endforeach
       <th>{{$qtyorder - $qtykirim}}</th>
     </tr>
    @endforeach
  </tbody>
</table>
<div class="row">
  <a href="downloadremainingdraft/{{$suratantar->draft_delivery_order_id}}" target="_blank" class="btn btn-stoko-primary orange">Print</a>
</div>
