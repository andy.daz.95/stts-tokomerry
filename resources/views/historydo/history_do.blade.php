<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="css/materialize.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/toastr.css">
<div class="container-fluid padding-top">
  <div class="row">
    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>History Pengeluaran Surat Antar</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="input-field col l4">
                  <label>Surat Antar</label>
                  <select id="suratantar" class="f-select selectpicker" data-live-search="true" data-size="10">
                     <option value="0">Pilih Surat Antar</option>
                      @foreach($suratantar as $key => $value)
                        <option value="{{$value->draft_delivery_order_id}}">{{$value->draft_delivery_order_number.' - '.$value->sales_order_number.' - '.$value->first_name.' '.$value->last_name}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div class="col l12 m12 s12">
      <ul class="collapsible" data-collapsible="accordion">
        <li>
          <div class="collapsible-header red darken-1 white-text active"><i class="material-icons">search</i>History</div>
          <div class="collapsible-body">
            <div class="container-fluid">
              <div class="row">
                <br>
                <div class="col l12 m12 s12">
                </div>
                <br>
                <div id="append-history" class="col l12 m12 s12">

                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#btn-submit').attr('mode', 'save');
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: 'js/materialize.min.js',
      dataType: "script",
    });
    $.ajax({
      url: 'js/custom.js',
      dataType: "script",
    });

    $.ajax({
      url: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js',
      dataType: "script",
    });

    $('.selectpicker').selectpicker('render');

    $('#suratantar').on('change', function(event){
      event.preventDefault();
      event.stopImmediatePropagation();
      var id = $(this).val();
      $.ajax({
        method:"get",
        url:"gethistorydobydraft",
        data:{id:id},
        success:function(response){
          $('#append-history').html(response);
        }
      })
    });
  });

</script>