<style type="text/css">
  table.table {
    border-collapse: collapse;
  }
  table.table, table.table th, table.table td{
    border: 1px solid black;
  }
  .red-color{
    color : red;
  }
</style>
@if($draft->so)
  @php $so = $draft->so; @endphp
@elseif($draft->retur->so)
  @php $so = $draft->retur->so; @endphp
@endif
<div>
  <p style="text-align: center; margin:0px; font-weight: 600">Gunung Sari Jaya Bulukumba</p>
  <p style="text-align: center; margin:0px; font-weight: 600">
    @if($draft->shipping_term_id == 1)
      DO {{$draft->warehouse->warehouse_name}}
    @else
      Surat Antaran
    @endif
  </p>
  <p style="text-align: center; margin:0px">0853-9978-7515</p>
  <hr>
  <table style ="width:100%">
    <tr>
      <td style="width: 25%">NO</td>
      <td style="width: 25%">: {{$draft->draft_delivery_order_number}}</td>
      <td></td>
    </tr>
    <tr>
      <td style="width: 25%">NO Nota Penjualan</td>
      <td style="width: 25%">: {{$draft->so->sales_order_number}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Tanggal</td>
      <td>: {{$draft->date_draft_delivery}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Pelanggan</td>
      <td>: {{$so->customer->first_name.' '.$so->customer->last_name}}</td>
      <td></td>
    </tr>
    <tr>
      <td>Notes</td>
      <td colspan="2">: {{$draft->notes}}</td>
    </tr>
    @if($draft->shipping_term_id == 1)
      <tr>
        <td>Dari Gudang</td>
        <td colspan="2">: {{$draft->warehouse->warehouse_name}}</td>
      </tr>
    @endif
  </table>
  <table style="width:100%; clear: both; margin-top:10px" class="table table-bordered">
    <thead>
    <tr style="background-color: #8B8C89">
      <th class="color" style="text-align: center;">No</th>
      <th class="color" style="text-align: center;">Kode</th>
      <th class="color" style="text-align: center;">Nama Barang</th>
      <th class="color" style="text-align: center;">Quantity Sisa</th>
    </tr>
    </thead>
    <tbody >
    @foreach($draft->details as $key => $value)
      <tr>
        <td>
          <span>{{$key+1}}</span>
        </td>
        <td>
          <span>{{$value->product->product_code}}</span>
        </td>
        <td>
          <span>{{$value->product->product_name}}</span>
        </td>
        <td>
          @php
            $productid = $value->product_id;
            $qtykirim = 0;
          @endphp
          @foreach($do as $key => $value2)
            @php
              $qty = $value2->details->where('product_id',$productid)->first()['quantity_sent'];
               $qty = $qty > 0 ? $qty : 0;
             $qtykirim = $qtykirim + $qty;
            @endphp
          @endforeach
          <span>{{$value->quantity - $qtykirim}}</span>
          @if($value->unit_type == 'main')
            <span>{{$value->product->satuan->unit_code}}</span>
          @else
            <span>{{$value->product->satuan->unit_child->unit_child_code}}</span>
          @endif
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
  <div>
    @if($do->count() > 0)
      @if($draft->shipping_term_id == 2)
        <span>Daftar Supir</span>
        @foreach($do as $key => $value)
          <br>
          <span>{{($key+1).' '.$value->driver_name.' - '.$value->date_delivery}}</span>
        @endforeach
      @endif
    @endif
  </div>
  @if($draft->shipping_term_id == 2)
    <table style ="width:100%">
      @foreach($invoicebelumlunas as $key => $value)
        <tr>
          <td style="width: 50%">Sisa Hutang Nota Penjualan {{$value->so->sales_order_number}}</td>
          <td style="width: 50%">: Rp {{number_format($value->total_amount - $value->payments->sum('total_paid'))}}</td>
          <td></td>
        </tr>
      @endforeach
    </table>
  @endif
</div>