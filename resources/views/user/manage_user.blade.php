<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{csrf_token()}}">
  <meta charset="utf-8">
  <title>Sistem Toko</title>
  <link rel="stylesheet" href="css/materialize.css">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/datepicker.min.css">
  <link rel="stylesheet" href="css/master.css">
  <link rel="stylesheet" href="css/toastr.css">
  <link rel="stylesheet" href="css/master.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
  <nav class="nav-extended red darken-1">
    <div class="nav-wrapper">
      <div class="container-fluid">
        <a href="#" class="brand-logo center">Sistem Toko</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="left hide-on-med-and-down">
          <li><a href={{url('/')}}><i class="material-icons left">arrow_back</i>Kembali Ke Halaman Utama</a></li>
        </ul>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li id="clock"></li>
        </ul>
      </div>
    </div>
    <div class="nav-content">
      <ul class="tabs tabs-transparent grey darken-3">
        <li class="tab"><a class="active" href="#manage_profile">Atur Pengguna</a></li>
      </ul>
    </div>
  </nav>
  <div id="manage_user" class="user-profile padding-top">
    <div class="container">
      <h1 class="title-profile grey-text text-darken-1">Atur Pengguna</h1>
      <h3 class="tag-title grey-text text-darken-1">List Staff Dalam Sistem</h3>
      <table class="table bordered striped">
        <thead>
          <tr>
            <th>Nama</th>  
            <th>Email</th>  
            <th>Role</th>  
            <th>Tindakan</th>  
          </tr>
        </thead>
        <tbody>
          @foreach($staff as $key => $value)
            <tr value="{{$value->account_id}}">
              <td class="nama">{{$value->full_name}}</td>
              <td>{{$value->email}}</td>
              <td>{{$value->roles[0]->name}}</td>
              <td>
                <a class="btn btn-sm btn-raised light-blue darken-2 edit"><i class="material-icons">edit</i></a>
                <a class="btn btn-sm btn-raised red delete-modal"><i class="material-icons">delete</i></a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="container">
      <h1 class="title-profile grey-text text-darken-1">Form Pengguna</h1>
      <h3 class="tag-title grey-text text-darken-1">Akun yang akan dibuat memiliki password awal "123123"</h3>
      <div class="container-fluid">
      <form id="formAccount" action="{{url('/create_account')}}" base-url="{{url('/')}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}
          <div class="row">
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Username</label>
              <input id="accountid" name="accountid" type="text" class="f-input" placeholder="ID" hidden/>
              <input id="username" name="username" type="text" class="f-input" placeholder="Username"/>
            </div>
            <div class="col l6 offset-l3 m6 offset-m3 s12">
              <img id="profile-pic" src="images/profile.jpg" alt="no images" class="circle responsive-img">
            </div>
{{--             <div class="col l6 offset-l3 m6 offset-m3 s12">
               <div class="file-field input-field">
                <div class="btn btn-raised grey lighten-1">
                  <span>Pilih Gambar</span>
                  <input type="file" id="avatar" name="avatar">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate f-input" type="text" placeholder="Tidak Ada Gambar yang Dipilih">
                </div>
              </div>
            </div>
 --}}            
            <div class="col l6 offset-l3 m6 offset-m3 s12">
              <div class="file-field input-field">
                <div class="btn btn-raised grey lighten-1">
                  <span>Pilih Gambar</span>
                  <input type="file" id="avatar" name="avatar">
                </div>
              </div>
            </div>
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Peran</label>
              <select id="role" name="role" class="browser-default selectpicker" data-live-search="true" data-size="5">
                <option>Pilih Peran</option>
                @foreach($role as $key => $value)
                  <option value="{{$value->role_id}}">{{$value->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Email</label>
              <input id="email" name="email" type="text" class="f-input" placeholder="Email"/>
            </div>
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Nama Lengkap</label>
              <input id="fullname" name="fullname" type="text" class="f-input"  placeholder="Nama Lengkap"/>
            </div>
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Nomor Handphone</label>
              <input id="phone" name="phone" type="text" class="f-input" placeholder="Nomor Handphone"/>
            </div>
            <div class="col l6 offset-l3 m6 offset-m3 s12">
              <div class="row">
                <div class="input-field col l4 m4 s12">
                  <label>Tahun</label>
                  <select id="years" name="years" class="f-select" ></select>
                </div>
                <div class="input-field col l4 m4 s12" >
                  <label>Bulan</label>
                  <select id="months" name="months" class="f-select" ></select>               
                </div>
                <div class="input-field col l4 m4 s12">
                  <label>Tanggal</label>
                  <select id="days" name="days" class="f-select"></select>               
                </div>
              </div>
            </div>
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Jenis Kelamin</label>
              <select id="gender" name="gender" class="f-select">
                <option value="" disabled selected>- Pilih Salah Satu -</option>
                <option value="1">Laki - laki</option>
                <option value="2">Perempuan</option>
              </select>
            </div>
            <div class="padding-top col l6 offset-l3 m6 offset-m3 s12">
              <button id="submit-account" type="submit" mode="save" class="btn btn-raised light-blue darken-2">Simpan</button>
              <button id="clear" class="btn btn-raised grey lighten-4 grey-text text-darken-4">Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <div id="modal-delete-user" class="modal">
    <div class="modal-content">
      <h5 id="delete-message"></h5>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect btn-stoko">batal</a>
      <a id="confirm-delete-user" class="modal-action modal-close waves-effect btn-stoko red white-text">hapus</a>
    </div>
  </div>

  <script src="{{ asset('js/jquery.min.js') }}"></script>
  
  {{-- <script src="js/dcalendar/dcalendar.picker.min.js"></script> --}}

  <script src="js/datepicker.min.js"></script>
  
  <script src="js/datepicker.en.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script type="text/javascript" src="js/jquery.newsTicker.min.js"></script>

  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
  
  <script src="js/materialize.min.js"></script>
  
  <script src="js/moment.js"></script>
  
  <script src="js/accounting.min.js"></script>

  <script type="text/javascript" src="js/toastr.js"></script>

  <script type="text/javascript" src="js/jquery.canvasjs.min.js"></script>

  <script src="js/custom.js"></script>
  <script type="text/javascript">
    $(document).ready(function(event){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $('.selectpicker').selectpicker('render');

      $('.bootstrap-select>.dropdown-toggle').on('click',function(event){
        event.preventDefault();
        event.stopImmediatePropagation();
        $(this).closest('.bootstrap-select').addClass("open");
        $(this).attr('aria-expanded',true);
      })

      function updateclock() {
        $('#clock').html(moment().format('D. MMMM YYYY H:mm:ss'));
      }
      setInterval(updateclock, 1000);

        //populate our years select box
        for (i = new Date().getFullYear(); i > 1900; i--){
          $('#years').append($('<option />').val(i).html(i));
        }
        //populate our months select box
        for (i = 1; i < 13; i++){
          $('#months').append($('<option />').val(i).html(i));
        }
        //populate our Days select box
        updateNumberOfDays(); 

        //"listen" for change events
        $('#years, #months').change(function(){
          updateNumberOfDays(); 
        });

        //function to update the days based on the current values of month and year
        function updateNumberOfDays(){
          $('#days').html('');
          month = $('#months').val();
          year = $('#years').val();
          days = daysInMonth(month, year);

          for(i=1; i < days+1 ; i++){
            $('#days').append($('<option />').val(i).html(i));
          }
        }

        //helper function
        function daysInMonth(month, year) {
          return new Date(year, month, 0).getDate();
        }

      $('#years').val($('#years').attr('value'));
      $('#months').val($('#months').attr('value'));
      $('#days').val($('#days').attr('value'));

      $('.edit').on('click', function(event){
        var id = $(this).closest('tr').attr('value');
        $.ajax({
            type:"GET",
            url:"get_account_manage",
            data:{id:id},
            success:function(response){
              var d = new Date(response.birth_date);
              $('#accountid').val(response.account_id);
              $('#username').val(response.username);
              $('#email').val(response.email);
              $('#fullname').val(response.full_name);
              $('#phone').val(response.phone);
              $('#gender').val(response.gender);
              $('#years').val(d.getFullYear());
              $('#months').val(d.getMonth()+1);
              $('#days').val(d.getDate());
              alert(response.roles[0].role_id);
              $('#role').val(response.roles[0].role_id);
              $('#submit-account').html('Ubah').attr('mode','edit');
              var base_url = $('#formAccount').attr('base-url');  
              alert(base_url);
              $('#formAccount').attr('action', base_url+'/update_account_manage');
              if(response.profile_picture != "" && response.profile_picture != null)
              {
                $('#profile-pic').attr('src', 'uploads/avatars/'+response.profile_picture);
              }else{
                $('#profile-pic').attr('src', 'uploads/avatars/profile.jpg');
              }
              $('.selectpicker').selectpicker('refresh');
            }
          });
      });


      $('body').on('click','.delete-modal', function(event){
        event.stopImmediatePropagation();
        var id = $(this).closest('tr').attr('value');
        var nama = $(this).closest('tr').find('.nama').html();
        $('#confirm-delete-user').attr('value',id).attr('user',nama);
        $("#delete-message").html("Yakin ingin menghapus user "+nama+" ?")
        $('#modal-delete-user').modal('open');
      });

      $('#confirm-delete-user').on('click', function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
        $('#modal-delete-user').modal('close');
        var id = $(this).attr('value');
        var user = $(this).attr('user');
        $.ajax({
          type:"POST",
          url:"delete_account",
          data:{id:id},
          success:function(response){
            toastr.success('User '+user+' telah berhasil Dihapus!',{"onShow":setTimeout(function(){location.reload(true);})
            });
          }
        })
      });

      $('#clear').on('click',function(event){
        event.preventDefault();
        event.stopImmediatePropagation();
        $(this).closest('#formAccount').find("input[type=text], textarea, select").val("");
        $('#submit-account').html('Simpan').attr('mode', 'save');
      });
    });
</script>
</body>
</html>
