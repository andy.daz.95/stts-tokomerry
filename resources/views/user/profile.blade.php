<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="csrf-token" content="{{csrf_token()}}">
  <meta charset="utf-8">
  <title>Sistem Toko</title>
  <link rel="stylesheet" href="css/materialize.css">
  <link rel="stylesheet" href="css/datepicker.min.css">
  <link rel="stylesheet" href="css/master.css">
  <link rel="stylesheet" href="css/toastr.css">
  <link rel="stylesheet" href="css/master.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
  <nav class="nav-extended red darken-1">
    <div class="nav-wrapper">
      <div class="container-fluid">
        <a href="#" class="brand-logo center">Sistem Toko</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="left hide-on-med-and-down">
          <li><a href={{url('/')}}><i class="material-icons left">arrow_back</i>Kembali Ke Halaman Utama</a></li>
        </ul>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li id="clock"></li>
        </ul>
      </div>
    </div>
    <div class="nav-content">
      <ul class="tabs tabs-transparent grey darken-3">
        <li class="tab"><a class="active" href="#manage_profile">Atur Profil</a></li>
      </ul>
    </div>
  </nav>
  <div id="manage_profile" class="user-profile padding-top">
    <div class="container">
      <h1 class="title-profile grey-text text-darken-1">Atur Profil</h1>
      <h3 class="tag-title grey-text text-darken-1">Atur username yang akan digunakan untuk <strong>Login</strong> beserta Profile Picture yang akan ditampilkan di <strong>Menu</strong></h3>
      <div class="container-fluid">
        <form id="formAccount" action="{{url('/update_account')}}" base-url="{{url('/')}}" method="post" enctype="multipart/form-data">
          {{csrf_field()}}
          <div class="row">
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Username</label>
              <input id="username" name="username" type="text" class="f-input" value="{{Session('user')->username}}" placeholder="Username"/>
            </div>
            <div class="col l6 offset-l3 m6 offset-m3 s12">
              <img id="profile-pic" src="images/profile.jpg" alt="no images" class="circle responsive-img">
            </div>
            <div class="col l6 offset-l3 m6 offset-m3 s12">
              <div class="file-field input-field">
                <div class="btn btn-raised grey lighten-1">
                  <span>Pilih Gambar</span>
                  <input type="file" id="avatar" name="avatar">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate f-input" type="text" placeholder="Tidak Ada Gambar Dipilih">
                </div>
              </div>
            </div>
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>E-mail</label>
              <input id="email" name="email" type="text" class="f-input" placeholder="Email"/>
            </div>
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Nama Lengkap</label>
              <input id="fullname" name="fullname" type="text" class="f-input"  placeholder="Nama Lengkap"/>
            </div>
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Nomor Handphone</label>
              <input id="phone" name="phone" type="text" class="f-input" placeholder="Nomor Handphone"/>
            </div>
            <div class="col l6 offset-l3 m6 offset-m3 s12">
              <div class="row">
                <div class="input-field col l4 m4 s12">
                  <label>Tahun</label>
                  <select id="years" name="years" class="f-select" ></select>
                </div>
                <div class="input-field col l4 m4 s12" >
                  <label>Bulan</label>
                  <select id="months" name="months" class="f-select" ></select>               
                </div>
                <div class="input-field col l4 m4 s12">
                  <label>Tanggal</label>
                  <select id="days" name="days" class="f-select"></select>               
                </div>
              </div>
            </div>
            <div class="input-field col l6 offset-l3 m6 offset-m3 s12">
              <label>Jenis Kelamin</label>
              <select id="gender" name="gender" class="f-select">
                <option value="" disabled selected>- Select -</option>
                <option value="1">Laki - laki</option>
                <option value="2">Perempuan</option>
              </select>
            </div>
            <div class="padding-top col l6 offset-l3 m6 offset-m3 s12">
              <button type="submit" class="btn btn-raised light-blue darken-2">SIMPAN</button>
              <a href="#" class="btn btn-raised grey lighten-4 grey-text text-darken-2">BATAL</a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  {{-- <div id="password" class="user-profile padding-top">
    <div class="container">
      <h1 class="title-profile grey-text text-darken-1">Change Password</h1>
      <div class="row">
        <div class="col l12 m12 s12">
          <div class="card blue darken-1">
            <div class="card-content white-text">
              <p>Untuk menjaga keamanan akun anda, kami <strong>sangat</strong> menyarankan untuk menggunakan kata kunci yang kompleks dimana...</p>
              <br>
              <p>1. Terdiri paling sedikit 8 karakter</p>
              <p>2. Menggunakan kombinasi huruf besar dan kecil</p>
              <p>3. Terdiri dari angka dan karakter khusus</p>
              <p>5. Tidak berdasarkan kamus kata</p>
            </div>
          </div>
          @if ($errors->any())
          <div class="card red darken-1">
            <div class="card-content white-text">
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
          @endif
          @if (session()->has('success'))
          <div class="card green darken-1">
            <div class="card-content white-text">
              <div class="alert alert-danger">
                <ul>
                  <li>{{ session()->get('success') }}</li>
                </ul>
              </div>
            </div>
          </div>
          @endif
        </div>
      </div>
      <div class="container-fluid">
        <form id="passForm" class="padding-top" role="form" method="POST" action="{{ url('/update_password') }}">
          {{csrf_field()}}
          <div class="row">
            <div class="col l6 offset-l3 m6 offset-m3 s12">
              <div class="input-field" style="margin-top: 10px">
                <input id="currentpassword" name="currentpassword" type="password" class="f-input validate" style="margin-bottom: 30px">
                <label>Current Password</label>
              </div>
            </div>
            <div class="col l6 offset-l3 m6 offset-m3 s12">
              <div class="input-field" style="margin-top: 10px">
                <input id="newpassword" name="newpassword" type="password" class="f-input validate" style="margin-bottom: 30px">
                <label>New Password</label>
              </div>
            </div>
            <div class="col l6 offset-l3 m6 offset-m3 s12">
              <div class="input-field" style="margin-top: 10px">
                <input id="retypepassword" name="retypepassword" type="password" class="f-input validate" style="margin-bottom: 30px">
                <label>Re-type New Password</label>
              </div>
            </div>
            <div class="padding-top col l6 offset-l3 m6 offset-m3 s12">
              <button type="submit" class="btn btn-raised light-blue darken-2">save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div> --}}
  <script src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/toastr.js"></script>
  <script src="js/datepicker.min.js"></script>
  <script src="js/datepicker.en.js"></script>
  <script src="js/materialize.min.js"></script>
  <script src="js/moment.js"></script>
  <script type="text/javascript">
    $(document).ready(function(event){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      function updateclock() {
        $('#clock').html(moment().format('D. MMMM YYYY H:mm:ss'));
      }
      setInterval(updateclock, 1000);

      $.ajax({
        type:"GET",
        url:"get_account",
        success:function(response){
          var d = new Date(response.birth_date);
          $('#username').val(response.username);
          $('#email').val(response.email);
          $('#fullname').val(response.full_name);
          $('#phone').val(response.phone);
          $('#gender').val(response.gender);
          $('#years').val(d.getFullYear());
          $('#months').val(d.getMonth()+1);
          $('#days').val(d.getDate());
          if(response.profile_picture != "")
          {
            $('#profile-pic').attr('src', 'uploads/avatars/'+response.profile_picture);
          }else{
            $('#profile-pic').attr('src', 'uploads/avatars/'+profile.jpg);
          }
        }
      });

      $(function() {

      //populate our years select box
      for (i = new Date().getFullYear(); i > 1900; i--){
          $('#years').append($('<option />').val(i).html(i));
      }
      //populate our months select box
      for (i = 1; i < 13; i++){
          $('#months').append($('<option />').val(i).html(i));
      }
      //populate our Days select box
      updateNumberOfDays(); 

      //"listen" for change events
      $('#years, #months').change(function(){
          updateNumberOfDays(); 
      });

      //function to update the days based on the current values of month and year
      function updateNumberOfDays(){
          $('#days').html('');
          month = $('#months').val();
          year = $('#years').val();
          days = daysInMonth(month, year);

          for(i=1; i < days+1 ; i++){
                  $('#days').append($('<option />').val(i).html(i));
          }
      }

      //helper function
      function daysInMonth(month, year) {
          return new Date(year, month, 0).getDate();
      }

      $('#years').val($('#years').attr('value'));
      $('#months').val($('#months').attr('value'));
      $('#days').val($('#days').attr('value'));

    });


    // $('#update-account').on('click',function(event){
    //   event.preventDefault();
    //   event.stopImmediatePropagation();
    //     $.ajax({
    //     type:"Post",
    //     url:"update_profile",
    //     data: $('#formAccount').serialize(),
    //     success:function(response){
    //       toastr.success("Account Telah Berhasil Diubah" ,{"onShow":setTimeout( location.reload(), 3600)});
    //     }
    //   });
    // })
  })
</script>
</body>
</html>
